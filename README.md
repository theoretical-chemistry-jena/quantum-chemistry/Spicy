# Spicy
[![built with nix](https://builtwithnix.org/badge.svg)](https://builtwithnix.org)
[![pipeline status](https://gitlab.com/theoretical-chemistry-jena/quantum-chemistry/Spicy/badges/develop/pipeline.svg)](https://gitlab.com/theoretical-chemistry-jena/quantum-chemistry/Spicy/-/commits/develop)
[![License: AGPL v3](https://img.shields.io/badge/License-AGPLv3-blue.svg)](https://www.gnu.org/licenses/agpl-3.0)

<p align="center">
  <img src="./data/logo/spicy.svg" alt="Spicy logo" width=200"/>
</p>

This is the development version of Spicy, a programme for multilayer fragment methods in computational chemistry.
Spicy is currently undergoing an overhaul for more modularity and Multi-Layer Fragment Combination Ranges to serve as theoretical foundation.
For the working, documented and published version see the `master` branch of this repository.

Development and building of the master branch will require [Nix](https://nixos.org/).
Run `nix build` to build Spicy or `nix develop` to obtain a development environment and hack on Spicy.