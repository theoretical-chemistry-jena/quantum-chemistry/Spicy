git clone --depth 1 --branch v2.8.0 https://github.com/cp2k/dbcsr.git
cd dbcsr
sed -i 's/add_library(dbcsr ${DBCSR_SRCS})/add_library(dbcsr SHARED ${DBCSR_SRCS})/g' src/CMakeLists.txt
sed -i 's/add_library(dbcsr_c ${DBCSR_C_SRCS})/add_library(dbcsr_c SHARED ${DBCSR_C_SRCS})/g' src/CMakeLists.txt
cmake -B build \
  -DUSE_OPENMP=ON \
  -DUSE_SMM=blas \
  -DWITH_C_API=ON \
  -DENABLE_SHARED=ON \
  -DENABLE_SHARED_LIBRARIES=ON \
  -DUSE_MPI=ON
make -C build -j$(nproc)
make -C build install