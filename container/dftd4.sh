git clone --depth 1 --branch v3.7.0 https://github.com/dftd4/dftd4.git
cd dftd4
meson setup _build --prefix=/usr/local
meson compile -C _build
meson test -C _build --print-errorlogs
meson install -C _build