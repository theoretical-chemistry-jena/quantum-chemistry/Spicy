git clone --branch 9.0.2 --depth 1 https://github.com/jacobwilliams/json-fortran.git
cd json-fortran
mkdir build && cd build
cmake ../ -DCMAKE_INSTALL_PREFIX=/usr/local
make -j$(nproc)
make install