git clone --depth 1 --branch v6.1.2 https://github.com/sunqm/libcint.git
cd libcint
cmake -B build \
  -DCMAKE_BUILD_TYPE=Release \
  -DCMAKE_INSTALL_PREFIX=/usr/local \
  -DBUILD_SHARED_LIBS=ON
make -C build -j$(nproc)
make -C build install