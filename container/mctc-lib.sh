git clone --branch v0.3.2 https://github.com/grimme-lab/mctc-lib.git
cd mctc-lib
meson setup _build --prefix=/usr/local
meson compile -C _build
meson test -C _build --print-errorlogs
meson install -C _build