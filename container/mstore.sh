git clone --depth 1 --branch v0.3.0 https://github.com/grimme-lab/mstore.git
cd mstore
meson setup _build --prefix=/usr/local
meson compile -C _build
meson test -C _build --print-errorlogs
meson install -C _build