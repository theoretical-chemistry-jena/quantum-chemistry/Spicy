git clone --depth 1 --branch v0.3.0 https://github.com/grimme-lab/multicharge.git
cd multicharge
meson setup build --prefix=/usr/local
meson compile -C build
meson test -C build --print-errorlogs --timeout-multiplier 4
meson install -C build