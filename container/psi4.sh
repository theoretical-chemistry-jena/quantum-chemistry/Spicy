git clone --depth 1 --branch v1.9.1 https://github.com/psi4/psi4.git
cd psi4
cmake -B build \
  -DCMAKE_BUILD_TYPE=Release \
  -DCMAKE_INSTALL_PREFIX=/usr/local \
  -DENABLE_OPENMP=ON \
  -DBUILD_SHARED_LIBS=ON \
  -DMAX_AM_ERI=6
cmake --build build --parallel $(nproc)
cmake --install build