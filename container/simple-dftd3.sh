git clone --depth 1 --branch v1.2.1 https://github.com/dftd3/simple-dftd3.git
cd simple-dftd3
meson setup _build --prefix=/usr/local
meson compile -C _build
meson test -C _build --print-errorlogs
meson install -C _build