git clone --depth 1 --branch v0.4.0 https://github.com/tblite/tblite.git
cd tblite
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH/usr/local/lib/x86_64-linux-gnu
meson setup build
meson configure build --prefix=/usr/local
meson test -C build --print-errorlogs
meson install -C build