git clone --depth 1 --branch v0.5.0 https://github.com/fortran-lang/test-drive.git
cd test-drive
meson setup _build --prefix=/usr/local
meson compile -C _build
meson test -C _build --print-errorlogs
meson install -C _build