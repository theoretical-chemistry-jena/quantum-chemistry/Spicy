git clone --depth 1 --branch v0.4.2 https://github.com/toml-f/toml-f.git
cd toml-f
meson setup _build --prefix=/usr/local
meson compile -C _build
meson test -C _build --print-errorlogs
meson install -C _build