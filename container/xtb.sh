git clone --depth 1 --branch v6.7.1 https://github.com/grimme-lab/xtb.git
cd xtb

cat > build.patch << EOF
diff --git a/meson/meson.build b/meson/meson.build
index 3554d12..6c5d275 100644
--- a/meson/meson.build
+++ b/meson/meson.build
@@ -198,6 +198,12 @@ mctc_dep = dependency(
 )
 lib_deps += mctc_dep
 
+dftd4_dep = dependency('dftd4')
+lib_deps += dftd4_dep
+
+multicharge_dep = dependency('multicharge')
+lib_deps += multicharge_dep
+
 # Get light-weight tight-binding framework dependency
 tblite_dep = dependency(
   'tblite',
EOF

# This works around a bug in GFortran 12.2.0 that is unfortunately the debian compiler
curl https://patch-diff.githubusercontent.com/raw/grimme-lab/xtb/pull/1051.patch -o gfortran.patch

patch -p1 < build.patch
patch -p1 < gfortran.patch

export LD_LIBRARY_PATH=/usr/local/lib/x86_64-linux-gnu:$LD_LIBRARY_PATH

meson setup _build --buildtype=release --optimization 2 --prefix=/usr/local -Dcpcmx=disabled
meson compile -C _build
meson test -C _build --print-errorlogs --timeout-multiplier 4
meson install -C _build