module TestMolden (buildMolden) where

import Data.Default
import qualified Data.IntMap as IntMap
import Data.Massiv.Array as Massiv
import RIO.Partial
import Spicy.Formats.Molden
import Spicy.Molecule.Physical
import Spicy.Prelude

buildMolden :: Int -> Molden
buildMolden l =
  Molden
    { title = "Molden Format"
    , atoms =
        IntMap.singleton 1 $
          Atom
            { element = He
            , label = mempty
            , isLink = NotLink
            , isDummy = False
            , ffType = FFXYZ
            , coordinates = VectorS . Massiv.fromList Par $ [0, 0, 0]
            , multipoles = def
            , formalCharge = 0
            , formalExcEl = 0
            }
    , atomicOrbitals = Massiv.singleton aoShell
    , moldenFlags =
        MoldenFlags
          { fiveD = True
          , sevenF = True
          , nineG = True
          }
    , molecularOrbitals = compute moVecs
    }
 where
  aoShell =
    MoldenAtomicShell
      { index = 1
      , moldenShells = Massiv.singleton . RegularShell (toEnum l) . Massiv.fromList Par $ [(1, 1)]
      }
  moVecs =
    Massiv.map moVec2Molden . Massiv.outerSlices . compute @U $
      Massiv.identityMatrix (Sz $ 2 * l + 1)
  moVec2Molden v =
    MoldenMO
      { energy = 1.0
      , occupation = 2.0
      , spin = Alpha
      , coefficients = compute v
      }
