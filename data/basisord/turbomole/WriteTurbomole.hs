{-# LANGUAGE OverloadedLabels #-}
{-# LANGUAGE OverloadedStrings #-}

module WriteTurbomole (writeTurbomole) where

import Data.Default
import Data.Foldable
import qualified Data.IntMap as IntMap
import Data.Massiv.Array as Massiv
import Data.Text.Lazy (toStrict)
import Data.Text.Lazy.Builder (toLazyText)
import Formatting
import qualified RIO.Map as Map
import Spicy.Attoparsec
import Spicy.Common
import Spicy.Formats.FChk
import Spicy.Formatting
import Spicy.Molecule.Computational
import Spicy.Molecule.Physical
import Spicy.Prelude hiding ((%))
import Spicy.Wavefunction.Basis
import Spicy.Wavefunction.Basis.Ordering
import Spicy.Wavefunction.Wavefunction
import Spicy.Wrapper.Internal.Output.Turbomole
import qualified System.Path as Path

tmFmt :: Double -> Text
tmFmt = toStrict . toLazyText . fE 20 14

writeTurbomole :: Int -> Text
writeTurbomole l =
  let header = "$scfmo    scfconv=7    format(4d20.14)\n#\n#\n"
      moBlocks = Massiv.imap moVec . Massiv.outerSlices $ moMat
      footer = "$end"
   in header <> (sfoldl (<>) mempty moBlocks) <> footer
 where
  nAO = 2 * l + 1
  moMat :: Matrix U Double
  moMat = compute . Massiv.identityMatrix $ Sz nAO
  moVec :: Ix1 -> Vector U Double -> Text
  moVec i v =
    let number = sformat (left 6 ' ' %. int) (i + 1)
        sym = sformat (left 6 ' ' %. text) "a"
        eigval = "  eigenvalue=" <> tmFmt (fromIntegral $ i + 1)
        nsaos = sformat ("  nsaos=" % int) nAO
        header = number <> sym <> eigval <> nsaos <> "\n"
        coeffs =
          Massiv.ifoldMono
            ( \i e ->
                let post = if (i + 1) `mod` 4 == 0 then "\n" else mempty
                 in e <> post
            )
            . Massiv.map tmFmt
            $ v
     in header <> coeffs <> "\n"

tmToFchk :: FilePath -> IO ()
tmToFchk path = do
  tmBasis <- readFileUtf8 (path <> "/basis") >>= parse' tmBasis >>= \shVec -> buildBasis shVec heAtom
  basPermMat <- getPermutationMat spicy (getVectorG $ tmBasis ^. #shells)
  tmCoeffMat <- readFileUtf8 (path <> "/mos") >>= parse' (tmMolecularOrbitals basPermMat)
  opt <- defIO

  fchk <- dataToFChkSimple tmBasis heAtom (Wavefunction (Just . Restricted . MatrixS $ tmCoeffMat) Nothing) (calcIn opt)
  let Sz (nMO :. nAO) = size tmCoeffMat
      fchkEn =
        fchk & #blocks %~ \o ->
          Map.insert
            "Alpha Orbital Energies"
            (Array . ArrayDouble $ Massiv.makeArray Par (Sz nMO) fromIntegral)
            o
  writeFileUtf8 (path <> "/tm.fchk") . writeFChk $ fchkEn
 where
  heAtom =
    IntMap.singleton 1 $
      Atom
        { element = He
        , label = mempty
        , isLink = NotLink
        , isDummy = False
        , ffType = FFXYZ
        , coordinates = VectorS . Massiv.fromList Par $ [0, 0, 0]
        , multipoles = def
        , formalCharge = 0
        , formalExcEl = 0
        }
  calcIn o =
    CalcInput
      { task = WTEnergy
      , restartFile = Nothing
      , software = XTBData GFNZero
      , prefixName = "tm"
      , permaDir = JDirPathAbs . Path.absDir $ "/home/phillip/test"
      , scratchDir = JDirPathAbs . Path.absDir $ "/home/phillip/test"
      , nProcs = 1
      , nThreads = 1
      , memory = 500
      , qMMMSpec = QM $ QMContext 0 1
      , embedding = Mechanical
      , optimisation = o
      , fragmentMethods = Nothing
      , additionalInput = Nothing
      }
