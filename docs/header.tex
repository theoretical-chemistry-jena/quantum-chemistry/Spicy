\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{chemfig}
\usepackage{ifthen}
\usepackage{xcolor}
\usepackage{tikz}
\usepackage{pgfplots}
\usepackage{siunitx}
\usetikzlibrary{trees}
\usetikzlibrary{calc}
\usetikzlibrary{shapes.geometric}
\usetikzlibrary{arrows}
\usetikzlibrary{fit}
\usetikzlibrary{patterns.meta}
\usetikzlibrary{angles}
\usetikzlibrary{decorations.markings}
\usetikzlibrary{external}
\usetikzlibrary{fpu}
\usetikzlibrary{positioning}
\usetikzlibrary{intersections}
\usepgfplotslibrary{colormaps}

\setchemfig{
  angle increment = 30,
  bond join = true,
  atom sep = 1.75em,
  cram width = 2pt,
  chemfig style = {
      thick,
      line cap = round
    }
}

% Label a chemfig bond:
%   1 : A shift of the node [(0, 0)]
%   2 : Atom 1
%   3 : Atom 2
%   4 : Label
\newcommand{\bondlabel}[4][]{%
  \chemmove{%
    \ifthenelse{\equal{#1}{}}{
      \coordinate (s) at (0, 0);
    }{
      \coordinate (s) at #1;
    }
    \path (#2) -- node[midway] (labRef) {} (#3);
    \draw
    let \p1 =  (labRef),
    \p2 = (s)
    in node at ($(\x1 + \x2, \y1 + \y2)$) {#4};
  }
}

% Label a chemfig angle:
%   1 : A shift of the node [(0, 0)]
%   2 : Atom 1
%   3 : Atom 2
%   4 : Atom 3
%   5 : Label   
\newcommand\arclabel[5][]{%
  \chemmove{%
    \ifthenelse{\equal{#1}{}}{
      \coordinate (s) at (0, 0);
    }{
      \coordinate (s) at #1;
    }
    \pic [draw, thick, -, angle radius = 1.35em] {angle = #2--#3--#4};
    \path
    let \p1 = (#2),
    \p2 = (#3),
    \p3 = (#4),
    \p4 = (s)
    in node at ($(0.33*\x1 + 0.33*\x2 + 0.33*\x3 + \x4, 0.33*\y1 + 0.33*\y2 + 0.33*\y3 + \y4)$) {#5};
  }
}

\newcommand{\highlightatom}[2]{%
  \chemmove{%
    \fill[#2] (#1) circle (3pt);
  }
}

\definecolor{cornflower}{HTML}{8ECAE6}
\definecolor{bluegreen}{HTML}{219EBC}
\definecolor{prussianblue}{HTML}{023047}
\definecolor{honeyyellow}{HTML}{FFB703}
\definecolor{orange}{HTML}{FB8500}
\definecolor{amaranthred}{HTML}{D72638}
\definecolor{auburn}{HTML}{A22C29}

% Colour hierarchy
\colorlet{c1}{prussianblue}
\colorlet{c2}{bluegreen}
\colorlet{c3}{cornflower}
\colorlet{c4}{honeyyellow}
\colorlet{c5}{orange}
\colorlet{c6}{amaranthred}
\colorlet{c7}{auburn}

% Stylings Bubble diagrams
\tikzstyle{atom} = [
circle,
fill=c7,
text width = 1.5em,
text = white,
minimum width = 2em,
inner sep=0,
align=center
]
\tikzstyle{bond} = [draw=c7, thick]
\tikzstyle{oniomtree} = [
text width = 3em,
text = white,
align = center,
shape = circle,
inner sep = 0
]

% Stylings for Graphs and Trees
\newlength{\treeVsep}
\newlength{\treeHsep}
\setlength{\treeVsep}{4em}
\setlength{\treeHsep}{2em} % Multiply with the number of children on a slice
\tikzset{
  level distance=\treeVsep,
  sibling distance=\treeHsep,
  edge from parent/.style = {draw, thick}
}
\tikzstyle{oniomtree} = [
text width=3em,
text = white,
align = center,
shape = circle,
inner sep=0
]
\tikzstyle{OL0} = [oniomtree, fill=c1]
\tikzstyle{OL1} = [oniomtree, fill=c2]
\tikzstyle{OL2} = [oniomtree, fill=c3]
\tikzstyle{OL3} = [oniomtree, fill=c4]
\tikzstyle{OLHighLight} = [
oniomtree,
draw = orange,
ultra thick,
fill = none,
text = black,
align = center
]

% Styling for molecular and group graphs
\newlength{\grBond}
\newlength{\grHbond}
\setlength{\grBond}{5em}
\setlength{\grHbond}{4em}
\tikzstyle{grAtom} = [
circle,
text width = 2.5em,
text = white,
align = center,
node font = \normalsize,
fill = black,
minimum size = 3em
]
\tikzstyle{grHatom} = [
grAtom,
text width = 2.5em,
text = black,
fill = white,
draw = black,
minimum size = 3em
]
\tikzstyle{grSbond} = [
draw=black,
line width=0.2em
]
\tikzstyle{grMbond} = [grSbond, double]
\tikzstyle{grCBond} = [grSbond, dashed]


% Algorithm Flowcharts
\tikzstyle{startstop} = [
rectangle,
rounded corners = 0.75ex,
minimum width = 8em,
minimum height = 2.5\baselineskip,
fill = c2,
inner sep = 1ex,
text = white,
text width = 8em,
font = \footnotesize,
align = center
]
\tikzstyle{io} = [
trapezium,
trapezium left angle = -130, trapezium right angle = -50,
trapezium stretches body = true,
fill = c3,
inner sep = 0ex,
minimum width = 8em,
minimum height = 2.5\baselineskip,
text = white,
text width = 8em,
font = \footnotesize,
align = center
]
\tikzstyle{function} = [
rectangle,
fill = c4,
inner sep = 1ex,
minimum width = 8em,
minimum height = 2.5\baselineskip,
text = white,
text width = 8em,
font = \footnotesize,
align = center
]
\tikzstyle{subprocess} = [
rectangle,
path picture = {
    \draw[white, thick]
    ([xshift = 2mm] {path picture bounding box}.north west) -- ([xshift = 2mm] {path picture bounding box}.south west)
    ([xshift = -2mm] {path picture bounding box}.north east) -- ([xshift = -2mm] {path picture bounding box}.south east);
  },
fill = c4,
inner sep = 1ex,
minimum width = 8em,
minimum height = 2.5\baselineskip,
text = white,
text width = 8em,
font = \footnotesize,
align = center
]
\tikzstyle{decision} = [
diamond,
aspect = 2,
fill = c5,
inner sep = 0pt,
minimum width = 8em,
minimum height = 2.5\baselineskip,
text = white,
text width = 8em,
font = \footnotesize,
align = center
]


% Plot Settings
\pgfplotsset{
  compat = 1.12,
  every axis plot/.append style={ultra thick},
  colormap = {phillip}{
      color = (white),
      color = (c1),
      color = (c2),
      color = (c3),
      color = (c4),
      color = (c5),
      color = (c6),
      color = (c7)
    }
}