{
  description = "Spicy - Computational Chemistry with multilayer/multiscale methods in Haskell";

  inputs = {
    nixBundlers.url = "github:NixOS/bundlers/master";

    nixpkgs.url = "github:NixOS/nixpkgs/nixpkgs-unstable";

    qchem = {
      url = "github:Nix-QChem/NixOS-QChem";
      inputs.nixpkgs.follows = "nixpkgs";
    };

    ihaskell = {
      # This should match the version available in nixpkgs for IHaskell
      # Also update the cabal.project files to use this hash
      url = "github:IHaskell/IHaskell/a23afdf381a88f37cfb92940d1667ed340224ce7";
      flake = false;
    };
  };

  nixConfig = {
    bash-prompt = "\\[\\e[0;1;38;5;214m\\]spicy\\[\\e[0;1m\\]:\\[\\e[0;1;38;5;39m\\]\\w\\[\\e[0;1m\\]$ \\[\\e[0m\\]";
    extra-substituters = [ "https://spicy.cachix.org" ];
    extra-trusted-public-keys = [
      "spicy.cachix.org-1:fiyL1RXw8cudQsVIO77lpAZCUAypzJFZAZQ5hOAhNOo="
    ];
    allow-import-from-derivation = "true";
  };

  outputs = { self, nixBundlers, nixpkgs, qchem, ihaskell, ... }:
    let
      # Overlay creation for Haskell projects
      mkHsOverlay = import ./nix/mkHsOverlay.nix;

      # Create development shells for subprojects
      mkDevShell = import ./nix/mkDevShell.nix pkgs;

      # Simple Haskell subprojects without build system and nix magic
      subprojectOvls = rec {
        spicy-prelude = mkHsOverlay "spicy-prelude" ./spicy-prelude { };
        spicy-finitecompare = mkHsOverlay "spicy-finitecompare" ./spicy-finitecompare { };
        spicy-containers = mkHsOverlay "spicy-containers" ./spicy-containers { };
        spicy-linearalgebra = mkHsOverlay "spicy-linearalgebra" ./spicy-linearalgebra { };
        spicy-textual = mkHsOverlay "spicy-textual" ./spicy-textual { };
        spicy-scoville = mkHsOverlay "spicy-scoville" ./spicy-scoville { };
        spicy-chemistry = mkHsOverlay "spicy-chemistry" ./spicy-chemistry { };
        spicy-fileformats = mkHsOverlay "spicy-fileformats" ./spicy-fileformats { };
        spicy-fragmentmethods = mkHsOverlay "spicy-fragmentmethods" ./spicy-fragmentmethods { };
        spicy-responseproperties = mkHsOverlay "spicy-responseproperties" ./spicy-responseproperties { };
        spicy-qm-wavefunction = mkHsOverlay "spicy-qm-wavefunction" ./spicy-qm-wavefunction {
          overrides = final: _: _: _: { cint = final.libcint; };
        };
        spicy-blocksparse = mkHsOverlay "spicy-blocksparse" ./spicy-blocksparse {
          overrides = final: _: _: _: { dbcsr_c = final.dbcsr; };
          addMpiTests = true;
        };
        spicy-qm-driver = import ./spicy-qm-driver/overlay.nix;
        spicy-qm-driver-orca = spicy-qm-driver;
        spicy-qm-driver-turbomole = spicy-qm-driver;
        spicy-qm-driver-all = spicy-qm-driver;
        spicy-graphviz = mkHsOverlay "spicy-graphviz" ./spicy-graphviz { };
        spicy-ipi = mkHsOverlay "spicy-ipi" ./spicy-ipi { };
        spicy = mkHsOverlay "spicy" ./spicy {
          overrides = final: _: _: _: { mpiexec = final.mpi; };
          addMpiTests = true;
        };
      };

      subPackages = builtins.mapAttrs
        (project: _: pkgs.haskellPackages."${project}")
        subprojectOvls;

      joinedPackages = pkgs.symlinkJoin {
        name = "spicy-packages";
        paths = builtins.attrValues subPackages;
      };

      subprojectShells = {
        spicy-prelude = mkDevShell "spicy-prelude" { };
        spicy-finitecompare = mkDevShell "spicy-finitecompare" { };
        spicy-containers = mkDevShell "spicy-containers" { };
        spicy-linearalgebra = mkDevShell "spicy-linearalgebra" { };
        spicy-textual = mkDevShell "spicy-textual" { };
        spicy-scoville = mkDevShell "spicy-scoville" { };
        spicy-chemistry = mkDevShell "spicy-chemistry" { };
        spicy-fileformats = mkDevShell "spicy-fileformats" { };
        spicy-fragmentmethods = mkDevShell "spicy-fragmentmethods" { };
        spicy-responseproperties = mkDevShell "spicy-responseproperties" { };
        spicy-qm-wavefunction = mkDevShell "spicy-qm-wavefunction" {
          addDeps = [ pkgs.libcint ];
          extraReplLibPaths = [ pkgs.libcint ];
        };
        spicy-blocksparse = mkDevShell "spicy-blocksparse" rec {
          extraReplLibPaths = [ pkgs.dbcsr ];
          extraReplLibs = [ "dbcsr_c" ];
          shellHook = ''
            cabal configure --extra-lib-dirs=${pkgs.lib.strings.makeSearchPath  "lib" extraReplLibPaths}
          '';
        };
        spicy-graphviz = mkDevShell "spicy-graphviz" { };
        spicy-ipi = mkDevShell "spicy-ipi" { };
        spicy = mkDevShell "spicy" { };
      } // (
        let
          python = pkgs.haskellPackages.spicy-qm-driver.passthru.python;
          commonCfg = {
            extraReplLibPaths = with pkgs; [ libcint pkgs.tblite python ];
            extraReplLibs = [
              ("python${python.pythonVersion}")
              "tblite"
            ];
            extraIncludeDirs = [
              "${python}/${python.sitePackages}/numpy/core/include/numpy"
            ];
            shellHook = "autoreconf";
          };
        in
        {
          spicy-qm-driver = mkDevShell "spicy-qm-driver" commonCfg;
          spicy-qm-driver-orca = mkDevShell "spicy-qm-driver-orca" (commonCfg // { addDeps = [ pkgs.qchem.orca ]; });
          spicy-qm-driver-turbomole = mkDevShell "spicy-qm-driver-turbomole" (commonCfg // { addDeps = [ pkgs.qchem.turbomole ]; });
          spicy-qm-driver-all = mkDevShell "spicy-qm-driver-all" (commonCfg // { addDeps = with pkgs.qchem; [ turbomole orca ]; });
        }
      );

      # All overlays
      ovls = {
        baseOvl = import ./nix/overlay.nix { inherit ihaskell; };
      } // subprojectOvls;

      pkgs = import nixpkgs {
        system = "x86_64-linux";
        overlays = [
          qchem.overlays.default
        ] ++ builtins.attrValues ovls;
        config = {
          allowUnfree = true;
          qchem-config = {
            optAVX = true;
            allowEnv = false;
          };
        };
      };

      texEnv = pkgs.texlive.combine {
        inherit (pkgs.texlive)
          scheme-small luatex standalone simplekv luatex85
          fontspec latexmk koma-script pgfplots mhchem
          collection-fontsextra collection-fontsrecommended colortbl
          mathtools chemfig wrapfig datetime academicons
          xetex tcolorbox environ upquote ucs adjustbox
          collectbox titling enumitem rsfs siunitx amsmath;
      };

      pyNbEnv = pkgs.python3.withPackages (p: with p; [
        jupyterlab
        numpy
        scipy
        networkx
        matplotlib
        hyphen
      ]);

      hsNbEnv = pkgs.haskellPackages.ghcWithPackages (p: with p; [
        ihaskell
        ihaskell-aeson
        ihaskell-blaze
        ihaskell-charts
        ihaskell-diagrams
        ihaskell-gnuplot
        ihaskell-graphviz
        ihaskell-hatex
        ihaskell-juicypixels
        ihaskell-magic
        ihaskell-widgets
        rio
        text
        attoparsec
        massiv
        massiv-io
        JuicyPixels
        JuicyPixels-extra
        cairo
        diagrams
        Chart
        containers
        ihaskell-aeson
        fgl
        optics
        graphviz
        fgl-visualize
        hmatrix
        hmatrix-gsl
        ttc
        kdt
        Octree
      ] ++ builtins.attrValues subPackages);

    in
    {
      packages.x86_64-linux = subPackages // { default = subPackages.spicy; };

      devShells.x86_64-linux = subprojectShells // {
        tex = pkgs.mkShell {
          buildInputs = [ texEnv pkgs.pdf2svg ];
        };

        fourmolu = pkgs.mkShell {
          buildInputs = [ pkgs.haskellPackages.fourmolu ];
        };

        notebooks = pkgs.mkShell {
          buildInputs = with pkgs; [
            hsNbEnv
            pyNbEnv
            texEnv
            graphviz
            gnuplot
          ];
          shellHook = ''
            export GHC_PACKAGE_PATH="$(echo ${hsNbEnv}/lib/*/lib/package.conf.d | tr ' ' ':'):$GHC_PACKAGE_PATH"
            ihaskell install
          '';
        };
      };

      formatter.x86_64-linux = pkgs.nixpkgs-fmt;

      checks.x86_64-linux =
        let
          # Packages not to be built
          excludes = [
            # IHaskell is closely tied to the ghc-lib-parser, which is closely
            # tied to the default GHC version.
            "ghc94_spicy-graphviz"
            "ghc98_spicy-graphviz"
            "ghc910_spicy-graphviz"
          ];

          # Dev shells and self.packages with other GHC versions. Their names
          # need to be prefixed so that they don't clash with the default 
          # packages.
          devShells = pkgs.lib.attrsets.mapAttrs'
            (project: drv: { name = "devShell_${project}"; value = drv; })
            self.devShells.x86_64-linux;
          ghc94Packages = pkgs.lib.attrsets.mapAttrs'
            (project: _: { name = "ghc94_${project}"; value = pkgs.haskell.packages.ghc94."${project}"; })
            subprojectOvls;
          ghc98Packages = pkgs.lib.attrsets.mapAttrs'
            (project: _: { name = "ghc98_${project}"; value = pkgs.haskell.packages.ghc98."${project}"; })
            subprojectOvls;
          ghc910Packages = pkgs.lib.attrsets.mapAttrs'
            (project: _: { name = "ghc910_${project}"; value = pkgs.haskell.packages.ghc910."${project}"; })
            subprojectOvls;
        in
        pkgs.lib.attrsets.filterAttrs (name: _: ! (builtins.elem name excludes)) (
          devShells // self.packages.x86_64-linux // ghc94Packages // ghc98Packages // ghc910Packages // {
            inherit (pkgs.haskellPackages)
              ihaskell
              ihaskell-aeson
              ihaskell-blaze
              ihaskell-charts
              ihaskell-diagrams
              ihaskell-gnuplot
              ihaskell-graphviz
              ihaskell-hatex
              ihaskell-juicypixels
              ihaskell-magic
              ihaskell-static-canvas
              ihaskell-widgets
              cpython
              hdf5
              ;
            inherit (pkgs.python3.pkgs)
              hyphen
              ;
          }
        );

      hydraJobs.x86_64-linux = self.checks.x86_64-linux;

      bundlers.x86_64-linux = {
        inherit (nixBundlers.bundlers.x86_64-linux) toArx toDEB toRPM toDockerImage;
      };

      ciGenerator = import ./nix/ci-generator.nix pkgs self;

      overlays = {
        default = builtins.foldl' pkgs.lib.composeExtensions (_: _: { }) (builtins.attrValues ovls);
      } // ovls;
    };
}
