pkgs: flake: with pkgs;

let
  fromYAML = yaml: builtins.fromJSON (
    builtins.readFile (
      runCommand "from-yaml"
        {
          inherit yaml;
          allowSubstitutes = false;
          preferLocalBuild = true;
        }
        ''
          ${remarshal}/bin/remarshal  \
            -if yaml \
            -i <(echo "$yaml") \
            -of json \
            -o $out
        ''
    )
  );

  readYAML = path: fromYAML (builtins.readFile path);

  pseudoPackages = [
    "spicy-qm-driver-orca"
    "spicy-qm-driver-turbomole"
    "spicy-qm-driver-all"
  ];

  cabalPackages = lib.attrsets.filterAttrs
    (name: val: builtins.hasAttr "getCabalDeps" val && ! builtins.elem name pseudoPackages)
    flake.packages.x86_64-linux;

  docPackages = lib.attrsets.filterAttrs
    (name: val: builtins.hasAttr "doc" val && name != "default")
    cabalPackages;

  stdRule = [
    { "if" = "'$CI_PIPELINE_SOURCE == \"merge_request_event\"'"; }
    { "if" = "$CI"; }
  ];

  common = readYAML ../.gitlab/ci-templates/common.yml;

  flakeUpdate = {
    stage = "update";
    script = ''
      # Delete possibly existing update branches
      git fetch origin
      git switch ''${CI_DEFAULT_BRANCH}
      git push -d origin ''${UPDATES_BRANCH} || echo "Nothing to delete"
      git branch -d ''${UPDATES_BRANCH} || echo "Nothing to delete"

      # Create the updates branch, perform the update, commit and push the changes
      git checkout -b ''${UPDATES_BRANCH}
      nix flake update
      git add flake.lock
      git commit -m "update flake.lock" || echo "Nothing to commit"
      git push --set-upstream origin ''${UPDATES_BRANCH}

      # Create a MR from the updates branch to default branch
      export MR_REQUEST_BODY="{
        \"id\": ''${CI_PROJECT_ID},
        \"source_branch\": \"''${UPDATES_BRANCH}\",
        \"target_branch\": \"''${CI_DEFAULT_BRANCH}\",
        \"remove_source_branch\": true,
        \"title\": \"flake update\",
        \"description\": \"Regular, automatic \`flake.lock\` update\nPlease manually check:\n  - [ ] if there is a new GHC version in nixpkgs. If so, make sure to add it to the build matrix in \`checks\` in \`flake.nix\`\n  - [ ] if compiler- or version-specific overrides in \`nix/overlay.nix\` are still required with the updated \`flake.lock\`\"
      }"
      curl -X POST "''${CI_API_V4_URL}/projects/''${CI_PROJECT_ID}/merge_requests" \
        --header "PRIVATE-TOKEN:''${AutoUpdates_TOKEN}" \
        --header "Content-Type: application/json" \
        --data "''${MR_REQUEST_BODY}";
    '';
    rules = [{ "if" = "$AUTO_UPDATES == \"1\""; }];
  };

  checkFormatting = {
    stage = "postcheck";
    script = ''
      nix develop .#fourmolu --command fourmolu --mode check $(git ls-files '*.hs')
    '';
    rules = stdRule;
  };

  checkBounds = {
    stage = "postcheck";
    parallel.matrix = [{ PKG = builtins.attrNames docPackages; }];
    script = ''
      cd $PKG
      nix develop ..#$PKG --command bash -c "cabal update && cabal outdated --exit-code" 2>/dev/null
    '';
    rules = stdRule;
    allow_failure = true;
  };

  checkSdist = {
    stage = "postcheck";
    parallel.matrix = [{ PKG = builtins.attrNames docPackages; }];
    script = ''
      cd $PKG
      nix develop ..#$PKG --command bash -c "cabal sdist"
    '';
    rules = stdRule;
  };

  hlsExecution = {
    stage = "build";
    needs = [ "check" ];
    parallel.matrix = [{ PKG = builtins.attrNames docPackages; }];
    script = ''
      cd $PKG
      nix develop ..#$PKG --command bash -c "cabal update && haskell-language-server-wrapper"
    '';
    rules = stdRule;
  };

  populateNotebooksCache = {
    stage = "build";
    needs = [ "check" ];
    script = ''
      nix build --accept-flake-config .#devShells.x86_64-linux.notebooks -L
      if [ -n "$CACHIX_AUTH_TOKEN" ]; then
        nix develop .#notebooks --profile notebooks-profile && cachix push spicy notebooks-profile        
      fi
    '';
    rules = stdRule;
  };

  populateCache = {
    stage = "build";
    needs = [ "check" ];
    parallel.matrix = [{ PKG = builtins.attrNames docPackages; }];
    script = ''
      nix build --accept-flake-config .#$PKG -L
      if [ -n "$CACHIX_AUTH_TOKEN" ]; then
        realpath result | cachix push spicy
        if [[ -n "$CI_COMMIT_TAG" && "$CI_COMMIT_REF_PROTECTED" == "true" ]]; then
          realpath result | cachix pin spicy "$CI_COMMIT_TAG"
        fi
      fi

      nix build .#devShells.x86_64-linux.$PKG -L
      if [ -n "$CACHIX_AUTH_TOKEN" ]; then
        nix develop .#$PKG --profile ''${PKG}-profile && cachix push spicy ''${PKG}-profile        
      fi
    '';
    rules = stdRule;
  };

  # Build a Debian-based container image for building spicy
  buildContainer = {
    stage = "build";
    needs = [ ];
    tags = [ "docker-in-docker" "uni-jena" ];
    script = ''
      cd container
      docker build . --tag $CI_REGISTRY_IMAGE/debian-build:latest
      docker push $CI_REGISTRY_IMAGE/debian-build:latest
    '';
    rules = stdRule;
  };

  # Debian build
  cabalBuild = {
    stage = "build";
    tags = [ "uni-jena" ];
    needs = [ "buildContainer" ];
    image = "registry.gitlab.com/theoretical-chemistry-jena/quantum-chemistry/spicy/debian-build:latest";
    before_script = ''
      export PATH=$PATH:/root/.cabal/bin:/root/.ghcup/bin
      cabal update
      cd spicy-qm-driver && autoreconf && cd ..
    '';
    script = ''
      for subproject in ${builtins.toString (builtins.attrNames docPackages)}; do
        echo "@@@@@@ Now processing $subproject @@@@@@"
        cd $subproject
        cabal configure --extra-include-dirs=/usr/local/include --extra-lib-dirs=/usr/local/lib
        cabal build -j$(nproc)
        cabal test
        cd ..
      done

      cabal build all -j$(nproc) --extra-include-dirs=/usr/local/include --extra-lib-dirs=/usr/local/lib
    '';
    rules = stdRule;
  };

  check = {
    stage = "build";
    script = ''
      nix flake check --accept-flake-config -L
    '';
    rules = stdRule;
  };

  docs = {
    stage = "docs";
    parallel.matrix = [{ PKG = builtins.attrNames docPackages; }];
    script = ''
      nix build --accept-flake-config .#$PKG.doc -L
    '';
    after_script = ''
      cp -r result-doc/share/doc/*/html docs
    '';
    artifacts.paths = [ "docs" ];
    rules = stdRule;
  };

in
writeTextFile {
  name = "nixCI.yaml";
  text = lib.generators.toYAML { } {
    inherit (common) default stages variables;
    inherit
      flakeUpdate
      checkBounds
      checkFormatting
      checkSdist
      check
      buildContainer
      cabalBuild
      hlsExecution
      populateCache
      populateNotebooksCache
      docs;
  };
} 
