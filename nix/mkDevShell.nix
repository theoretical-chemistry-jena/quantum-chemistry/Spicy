pkgs: name: { addDeps ? [ ]
            , extraReplLibPaths ? [ ]
            , extraReplLibs ? [ ]
            , extraIncludeDirs ? [ ]
            , shellHook ? ""
            }:
with pkgs;
let
  replFlags =
    (lib.strings.optionalString
      (extraReplLibPaths != [ ])
      "-L${lib.strings.makeLibraryPath extraReplLibPaths} ") +
    (lib.strings.optionalString
      (extraReplLibs != null)
      (lib.strings.concatMapStringsSep " " (n: "-l${n}") extraReplLibs)
    );
  cabalWrapped = writeScriptBin "cabal" ''
    #! ${runtimeShell}
    case "$1" in
      repl)
        ${cabal-install}/bin/cabal "$@" --project-file=cabal.nix.project --repl-options="${replFlags}"
        ;;
      build)
        ${cabal-install}/bin/cabal "$@" --project-file=cabal.nix.project
        ;;
      configure)
        ${cabal-install}/bin/cabal "$@" --project-file=cabal.nix.project
        ;;
      test)
        ${cabal-install}/bin/cabal "$@" --project-file=cabal.nix.project
        ;;
      install)
        ${cabal-install}/bin/cabal "$@" --project-file=cabal.nix.project
        ;;
      haddock)
        ${cabal-install}/bin/cabal "$@" --project-file=cabal.nix.project
        ;;
      *)
        ${cabal-install}/bin/cabal "$@"
        ;;
    esac
  '';
in
pkgs.haskellPackages.shellFor ({
  withHoogle = true;
  packages = p: [ p."${name}" ];
  buildInputs = (with pkgs; [
    cabalWrapped
    haskell-language-server
    haskellPackages.fourmolu
    haskellPackages.graphmod
    haskellPackages.calligraphy
    haskellPackages.ghc-prof-flamegraph
    haskellPackages.profiteur
    hlint
    hpack
    ghcid
    pkg-config
    graphviz
  ]) ++ addDeps;
} // lib.attrsets.optionalAttrs (extraIncludeDirs != [ ]) {
  C_INCLUDE_PATH = lib.strings.makeSearchPath "" extraIncludeDirs;
} // lib.attrsets.optionalAttrs (shellHook != "") {
  inherit shellHook;
})


