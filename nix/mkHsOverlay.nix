project: dir: { overrides ? (_final: _prev: _hfinal: _hprev: { })
              , cabalFlags ? [ ]
              , addMpiTests ? false
              , cabalOverrides ? (_final: _prev: old: { })
              }@opts: final: prev: {
  haskell = prev.haskell // {
    packageOverrides = hfinal: hprev: prev.haskell.packageOverrides hfinal hprev // {
      "${project}" =
        let
          prelimStage = hfinal.callCabal2nixWithOptions
            "${project}"
            dir
            (prev.lib.strings.concatStringsSep " " cabalFlags)
            (overrides final prev hfinal hprev);
          cabalStage = prev.haskell.lib.overrideCabal prelimStage (cabalOverrides final prev);
          mpiStage = prev.haskell.lib.addTestToolDepends cabalStage
            (with final; [ openssh mpiCheckPhaseHook ]);
        in
        mpiStage;
    };
  };
}
