{ ihaskell }@extraDeps: final: prev:

{
  haskell = prev.haskell // {
    packageOverrides = hfinal: hprev:
      let
        ghcMajor = prev.lib.versions.major hprev.ghc.version;
        ghcMinor = prev.lib.versions.minor hprev.ghc.version;

      in
      prev.haskell.packageOverrides hfinal hprev // ({

        # IHaskell packages missing from hackage, taken from upstream
        ihaskell-aeson = hfinal.callCabal2nix "ihaskell-aeson" "${ihaskell}/ihaskell-display/ihaskell-aeson" { };
        ihaskell-blaze = hfinal.callCabal2nix "ihaskell-blaze" "${ihaskell}/ihaskell-display/ihaskell-blaze" { };
        ihaskell-charts = hfinal.callCabal2nix "ihaskell-charts" "${ihaskell}/ihaskell-display/ihaskell-charts" { };
        ihaskell-diagrams = hfinal.callCabal2nix "ihaskell-diagrams" "${ihaskell}/ihaskell-display/ihaskell-diagrams" { };
        ihaskell-gnuplot = hfinal.callCabal2nix "ihaskell-gnuplot" "${ihaskell}/ihaskell-display/ihaskell-gnuplot" { };
        ihaskell-graphviz = hfinal.callCabal2nix "ihaskell-graphviz" "${ihaskell}/ihaskell-display/ihaskell-graphviz" { };
        ihaskell-hatex = hfinal.callCabal2nix "ihaskell-hatex" "${ihaskell}/ihaskell-display/ihaskell-hatex" { };
        ihaskell-juicypixels = hfinal.callCabal2nix "ihaskell-juicypixels" "${ihaskell}/ihaskell-display/ihaskell-juicypixels" { };
        ihaskell-magic = hfinal.callCabal2nix "ihaskell-magic" "${ihaskell}/ihaskell-display/ihaskell-magic" { };
        ihaskell-static-canvas = hfinal.callCabal2nix "ihaskell-static-canvas" "${ihaskell}/ihaskell-display/ihaskell-static-canvas" { };
        ihaskell-widgets = hfinal.callCabal2nix "ihaskell-widgets" "${ihaskell}/ihaskell-display/ihaskell-widgets" { };

        # Upstream updates
        # unreleased version required for ihaskell-graphviz changes. Can be removed with IHaskell 0.12
        ihaskell = prev.haskell.lib.dontCheck (hfinal.callCabal2nix "ihaskell" "${ihaskell}" { });

        # Fixes
        derive-storable-plugin = prev.haskell.lib.doJailbreak hprev.derive-storable-plugin;
        generic-override = prev.haskell.lib.markUnbroken (prev.haskell.lib.doJailbreak hprev.generic-override);
        hdf5 = prev.haskell.lib.markUnbroken hprev.hdf5;
        kdt = prev.haskell.lib.markUnbroken (prev.haskell.lib.dontCheck hprev.kdt);
        Octree = prev.haskell.lib.markUnbroken (prev.haskell.lib.doJailbreak hprev.Octree);
        pvar = prev.haskell.lib.markUnbroken (prev.haskell.lib.dontCheck hprev.pvar);
        singletons-base = prev.haskell.lib.markUnbroken (prev.haskell.lib.dontCheck hprev.singletons-base);
      }

      // prev.lib.attrsets.optionalAttrs (ghcMajor == "9" && ghcMinor == "8") {
        derive-storable-plugin = prev.haskell.lib.doJailbreak hprev.derive-storable-plugin;
      }
      // prev.lib.attrsets.optionalAttrs (ghcMajor == "9" && ghcMinor == "10") {
        scheduler = prev.haskell.lib.dontCheck hprev.scheduler;
      }
      );
  };

  python3 = prev.python3.override (old: {
    packageOverrides = prev.lib.composeExtensions (old.packageOverrides or (_: _: { })) (pfinal: pprev: {
      hyphen = pfinal.callPackage ./pkgs/hyphen.nix {
        haskellPackages = prev.haskellPackages;
      };
    });
  });


}
