{ buildPythonPackage, lib, fetchFromGitHub, python3, haskellPackages }:

let
  hsEnv = haskellPackages.ghcWithPackages (p: with p; [
    text
    transformers
    mtl
    parsec
    hashable
    unordered-containers
    ghc-paths
  ]);

in
buildPythonPackage rec {
  pname = "hyphen";
  version = "unstable-2023-09-02";

  src = fetchFromGitHub {
    owner = "tbarnetlamb";
    repo = pname;
    rev = "bff0df758c894906eede38f04d5dbd4897ee442b";
    hash = "sha256-PDnrmiyDqlxGPj0Qv5RR1SaeVd0J6FZ0/QQtRXZsF+s=";
  };

  nativeBuildInputs = [ hsEnv ];

  dontConfigure = true;

  format = "other";

  buildPhase = ''
    cd hyphen
    python3 ./build-extn.py
  '';

  installPhase = ''
    mkdir -p $out/${python3.sitePackages}/hyphen
    cp -r * $out/${python3.sitePackages}/hyphen
  '';
}
