#include "MatMul.h"

void block_sparse_matmul(
    struct matrix_interface *a_matrix, struct matrix_interface *b_matrix,
    struct matrix_interface *c_buf)
{
    // MPI safety checks and setup
    int mpi_size, mpi_rank, mpi_is_initialised;
    MPI_Initialized(&mpi_is_initialised);
    if (!mpi_is_initialised)
    {
        abort();
    };
    MPI_Comm_size(MPI_COMM_WORLD, &mpi_size);
    MPI_Comm_rank(MPI_COMM_WORLD, &mpi_rank);

    // Check that rank 0 has valid pointers to matrices and the other ranks
    // got null pointers
    if (mpi_rank == 0 && (!a_matrix || !b_matrix))
    {
        fprintf(stderr, "MPI rank 0 must have valid input matrices");
        abort();
    };
    if (mpi_rank != 0 && a_matrix && b_matrix)
    {
        fprintf(stderr, "MPI rank != 0 must have NULLs to input matrices");
        abort();
    };

    // Use a Cartesian 2D MPI grid for DBCSR
    int dims[2] = {0, 0};
    int periods[2] = {1, 1};
    const int reorder = 0;
    MPI_Comm cart_group_comm;
    MPI_Dims_create(mpi_size, 2, dims);
    MPI_Cart_create(MPI_COMM_WORLD, 2, dims, periods, reorder, &cart_group_comm);

    // Initialise the DBCSR library
    c_dbcsr_init_lib(MPI_COMM_WORLD, NULL);

    // Setup distributions for the input and output matrices.
    // Input matrices will be distributed across the workers.
    // The output matrix will only reside on MPI rank 0.
    int n;
    int a_n_rows, a_n_cols;         // Number of blocks
    int *a_row_distr, *a_col_distr; // Distribution of blocks over MPI grid
    int *a_row_sz, *a_col_sz;       // Size of blocks
    int b_n_rows, b_n_cols;
    int *b_row_distr, *b_col_distr;
    int *b_row_sz, *b_col_sz;
    int *c_row_distr, *c_col_distr;

    // Prepare block distributions for each matrix on MPI rank 0 and then
    // distribute to all workers.
    if (mpi_rank == 0)
    {
        a_n_rows = a_matrix->n_rows;
        a_n_cols = a_matrix->n_cols;
        a_row_sz = a_matrix->row_blk_sz;
        a_col_sz = a_matrix->col_blk_sz;

        a_row_distr = malloc(sizeof(int) * a_matrix->n_rows);
        for (n = 0; n < a_matrix->n_rows; n++)
        {
            a_row_distr[n] = n % dims[0];
        };

        a_col_distr = malloc(sizeof(int) * a_matrix->n_cols);
        for (n = 0; n < a_matrix->n_cols; n++)
        {
            a_col_distr[n] = n % dims[1];
        };

        b_n_rows = b_matrix->n_rows;
        b_n_cols = b_matrix->n_cols;
        b_row_sz = b_matrix->row_blk_sz;
        b_col_sz = b_matrix->col_blk_sz;

        b_row_distr = malloc(sizeof(int) * b_matrix->n_rows);
        for (n = 0; n < b_matrix->n_rows; n++)
        {
            b_row_distr[n] = n % dims[0];
        };

        b_col_distr = malloc(sizeof(int) * b_matrix->n_cols);
        for (n = 0; n < b_matrix->n_cols; n++)
        {
            b_col_distr[n] = n % dims[1];
        };

        // First, send the sizes of all matrices, then send the actual
        // distributions
        MPI_Bcast(&a_n_rows, 1, MPI_INT, 0, MPI_COMM_WORLD);
        MPI_Bcast(a_row_sz, a_n_rows, MPI_INT, 0, MPI_COMM_WORLD);
        MPI_Bcast(a_row_distr, a_n_rows, MPI_INT, 0, MPI_COMM_WORLD);
        MPI_Bcast(&a_n_cols, 1, MPI_INT, 0, MPI_COMM_WORLD);
        MPI_Bcast(a_col_sz, a_n_cols, MPI_INT, 0, MPI_COMM_WORLD);
        MPI_Bcast(a_col_distr, a_n_cols, MPI_INT, 0, MPI_COMM_WORLD);
        MPI_Bcast(&b_n_rows, 1, MPI_INT, 0, MPI_COMM_WORLD);
        MPI_Bcast(b_row_sz, b_n_rows, MPI_INT, 0, MPI_COMM_WORLD);
        MPI_Bcast(b_row_distr, b_n_rows, MPI_INT, 0, MPI_COMM_WORLD);
        MPI_Bcast(&b_n_cols, 1, MPI_INT, 0, MPI_COMM_WORLD);
        MPI_Bcast(b_col_sz, b_n_cols, MPI_INT, 0, MPI_COMM_WORLD);
        MPI_Bcast(b_col_distr, b_n_cols, MPI_INT, 0, MPI_COMM_WORLD);
    }
    else
    {
        // Clients get the block distribution from rank 0
        MPI_Bcast(&a_n_rows, 1, MPI_INT, 0, MPI_COMM_WORLD);
        a_row_sz = malloc(sizeof(int) * a_n_rows);
        a_row_distr = malloc(sizeof(int) * a_n_rows);
        MPI_Bcast(a_row_sz, a_n_rows, MPI_INT, 0, MPI_COMM_WORLD);
        MPI_Bcast(a_row_distr, a_n_rows, MPI_INT, 0, MPI_COMM_WORLD);
        MPI_Bcast(&a_n_cols, 1, MPI_INT, 0, MPI_COMM_WORLD);
        a_col_sz = malloc(sizeof(int) * a_n_cols);
        a_col_distr = malloc(sizeof(int) * a_n_cols);
        MPI_Bcast(a_col_sz, a_n_cols, MPI_INT, 0, MPI_COMM_WORLD);
        MPI_Bcast(a_col_distr, a_n_cols, MPI_INT, 0, MPI_COMM_WORLD);
        MPI_Bcast(&b_n_rows, 1, MPI_INT, 0, MPI_COMM_WORLD);
        b_row_sz = malloc(sizeof(int) * b_n_rows);
        b_row_distr = malloc(sizeof(int) * b_n_rows);
        MPI_Bcast(b_row_sz, b_n_rows, MPI_INT, 0, MPI_COMM_WORLD);
        MPI_Bcast(b_row_distr, b_n_rows, MPI_INT, 0, MPI_COMM_WORLD);
        MPI_Bcast(&b_n_cols, 1, MPI_INT, 0, MPI_COMM_WORLD);
        b_col_sz = malloc(sizeof(int) * b_n_cols);
        b_col_distr = malloc(sizeof(int) * b_n_cols);
        MPI_Bcast(b_col_sz, b_n_cols, MPI_INT, 0, MPI_COMM_WORLD);
        MPI_Bcast(b_col_distr, b_n_cols, MPI_INT, 0, MPI_COMM_WORLD);
    };

    c_row_distr = calloc(a_n_rows, sizeof(int));
    c_col_distr = calloc(b_n_cols, sizeof(int));

    // Now, actually set DBCSR distributions and initialise the DBCSR matrices
    // on all nodes.
    dbcsr_distribution a_distr = NULL;
    dbcsr_distribution b_distr = NULL;
    dbcsr_distribution c_distr = NULL;
    dbcsr_matrix a_dbcsr_matrix, b_dbcsr_matrix, c_dbcsr_matrix;

    c_dbcsr_distribution_new(&a_distr, cart_group_comm, a_row_distr, a_n_rows, a_col_distr, a_n_cols);
    c_dbcsr_distribution_new(&b_distr, cart_group_comm, b_row_distr, b_n_rows, b_col_distr, b_n_cols);
    c_dbcsr_distribution_new(&c_distr, cart_group_comm, c_row_distr, a_n_rows, c_col_distr, b_n_cols);

    c_dbcsr_create_new(&a_dbcsr_matrix, "Matrix A", a_distr, dbcsr_type_no_symmetry, a_row_sz, a_n_rows, a_col_sz, a_n_cols, NULL, NULL, NULL, NULL, NULL, NULL);
    c_dbcsr_create_new(&b_dbcsr_matrix, "Matrix B", b_distr, dbcsr_type_no_symmetry, b_row_sz, b_n_rows, b_col_sz, b_n_cols, NULL, NULL, NULL, NULL, NULL, NULL);
    c_dbcsr_create_new(&c_dbcsr_matrix, "Matrix C", c_distr, dbcsr_type_no_symmetry, a_row_sz, a_n_rows, b_col_sz, b_n_cols, NULL, NULL, NULL, NULL, NULL, NULL);

    // Fill in values from MPI rank 0
    if (mpi_rank == 0)
    {
        int blk, blk_r_ix, blk_c_ix;
        // Matrix A
        for (blk = 0; blk < a_matrix->nz_blks; blk++)
        {
            blk_r_ix = (a_matrix->blck_r_ix)[blk];
            blk_c_ix = (a_matrix->blck_c_ix)[blk];
            c_dbcsr_put_block2d_d(
                a_dbcsr_matrix,
                blk_r_ix, blk_c_ix,
                (a_matrix->blck_data)[blk],
                (a_matrix->row_blk_sz)[blk_r_ix], (a_matrix->col_blk_sz)[blk_c_ix],
                NULL, NULL);
        };

        // Matrix B
        for (blk = 0; blk < b_matrix->nz_blks; blk++)
        {
            blk_r_ix = (b_matrix->blck_r_ix)[blk];
            blk_c_ix = (b_matrix->blck_c_ix)[blk];
            c_dbcsr_put_block2d_d(
                b_dbcsr_matrix,
                blk_r_ix, blk_c_ix,
                (b_matrix->blck_data)[blk],
                (b_matrix->row_blk_sz)[blk_r_ix], (b_matrix->col_blk_sz)[blk_c_ix],
                NULL, NULL);
        };
    };

    // Finalize matrices on all Nodes
    MPI_Barrier(MPI_COMM_WORLD);
    c_dbcsr_finalize(a_dbcsr_matrix);
    c_dbcsr_finalize(b_dbcsr_matrix);
    c_dbcsr_finalize(c_dbcsr_matrix);

    // Perform the actual matrix multiplication
    c_dbcsr_multiply_d(
        'N', 'N', 1.0, a_dbcsr_matrix, b_dbcsr_matrix, 0.0, c_dbcsr_matrix,
        NULL, NULL, NULL, NULL, NULL, NULL,
        NULL, NULL, NULL);

    // Iterate over the blocks of the result matrix and construct output data
    // structure
    if (mpi_rank == 0)
    {
        // DEBUG - print the result
        // c_dbcsr_print(a_dbcsr_matrix);
        // c_dbcsr_print(b_dbcsr_matrix);
        // c_dbcsr_print(c_dbcsr_matrix);
        // END DEBUG

        dbcsr_iterator c_iter = NULL;
        int r, c, r_sz, c_sz, nblk;
        double *dbcsr_blk = NULL;
        double *blk_persist;
        bool tr;

        c_buf->n_rows = a_n_rows;
        c_buf->n_cols = b_n_cols;
        c_buf->row_blk_sz = malloc(a_n_rows * sizeof(int));
        memcpy(c_buf->row_blk_sz, a_matrix->row_blk_sz, sizeof(int) * a_n_rows);
        c_buf->col_blk_sz = malloc(b_n_cols * sizeof(int));
        memcpy(c_buf->col_blk_sz, b_matrix->col_blk_sz, sizeof(int) * b_n_cols);
        c_buf->nz_blks = 0;
        c_buf->blck_r_ix = malloc(a_n_rows * b_n_cols * sizeof(int));
        c_buf->blck_c_ix = malloc(a_n_rows * b_n_cols * sizeof(int));
        c_buf->blck_data = malloc(a_n_rows * b_n_cols * sizeof(double *));
        c_dbcsr_iterator_start(&c_iter, c_dbcsr_matrix, NULL, NULL, NULL, NULL, NULL);
        while (c_dbcsr_iterator_blocks_left(c_iter))
        {
            c_dbcsr_iterator_next_2d_block_d(c_iter, &r, &c, &dbcsr_blk, &tr, &nblk, &r_sz, &c_sz, NULL, NULL);
            c_buf->blck_r_ix[c_buf->nz_blks] = r;
            c_buf->blck_c_ix[c_buf->nz_blks] = c;

            blk_persist = calloc(c_buf->row_blk_sz[r] * c_buf->col_blk_sz[c], sizeof(double));
            memcpy(blk_persist, dbcsr_blk, sizeof(double) * c_buf->row_blk_sz[r] * c_buf->col_blk_sz[c]);
            c_buf->blck_data[c_buf->nz_blks] = blk_persist;

            (c_buf->nz_blks)++;
        };

        c_dbcsr_iterator_stop(&c_iter);
    };

    // Cleanup all resources
    MPI_Barrier(MPI_COMM_WORLD);

    c_dbcsr_release(&a_dbcsr_matrix);
    c_dbcsr_release(&b_dbcsr_matrix);
    c_dbcsr_release(&c_dbcsr_matrix);

    c_dbcsr_distribution_release(&a_distr);
    c_dbcsr_distribution_release(&b_distr);
    c_dbcsr_distribution_release(&c_distr);

    free(a_row_distr);
    free(a_col_distr);
    free(b_row_distr);
    free(b_col_distr);
    free(c_row_distr);
    free(c_col_distr);
    if (mpi_rank != 0)
    {
        free(a_row_sz);
        free(a_col_sz);
        free(b_row_sz);
        free(b_col_sz);
    };

    MPI_Comm_free(&cart_group_comm);

    c_dbcsr_finalize_lib();

    return;
};

void print_bsm(struct matrix_interface *bsm, char *prefix)
{
    printf("%s -> matrix_interface:\n", prefix);
    printf("%s ->   n_rows: %d\n", prefix, bsm->n_rows);
    printf("%s ->   n_cols: %d\n", prefix, bsm->n_cols);
    printf("%s ->   row_stripes: [", prefix);
    for (int rs = 0; rs < (bsm->n_rows); rs++)
    {
        printf(" %d ", bsm->row_blk_sz[rs]);
    };
    printf("]\n%s  col_stripes: [", prefix);
    for (int cs = 0; cs < (bsm->n_cols); cs++)
    {
        printf(" %d ", bsm->col_blk_sz[cs]);
    };
    printf("]\n%s  nz_blks: %d\n", prefix, bsm->nz_blks);
    printf("%s  non-zero blocks:\n", prefix);
    for (int blkI = 0; blkI < bsm->nz_blks; blkI++)
    {
        printf("%s    Block[%d]\n", prefix, blkI);

        int rowIdx = bsm->blck_r_ix[blkI];
        int colIdx = bsm->blck_c_ix[blkI];
        printf("%s    Block-Index = (%d, %d)\n", prefix, rowIdx, colIdx);

        int rSz = bsm->row_blk_sz[rowIdx];
        int cSz = bsm->col_blk_sz[colIdx];

        printf("%s    Block size = (%d * %d)\n", prefix, rSz, cSz);

        double *blkData = bsm->blck_data[blkI];
        printf("%s    Data:\n", prefix);
        for (int rIx = 0; rIx < rSz; rIx++)
        {
            printf("%s      ", prefix);
            for (int cIx = 0; cIx < cSz; cIx++)
            {
                printf(" %f ", blkData[rIx * cSz + cIx]);
            };
            printf("\n");
        };
        printf("\n");
    };
};