#pragma once

#define nullptr ((void*)0)

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <mpi.h>
#include <dbcsr.h>

// Convenience struct containing relevant information to pass a block sparse
// matrix from Haskell to C
struct matrix_interface
{
    // Shape of the matrix and blocks
    int n_rows, n_cols;           // Number of blocks along rows and columns
    int *row_blk_sz, *col_blk_sz; // Vector of block sizes along rows and columns
    // Non-Zero Block data
    int nz_blks;                  // Number of non-zero blocks
    int *blck_r_ix, *blck_c_ix;   // Indices of the non-zero blocks
    double **blck_data;           // Pointer to the data of each block. Elements are in column major (Fortran order!)
};


// On rank 0, the input matrices must be valid pointer to the matrix_interface
// structs. On all other ranks they should be null pointers. MPI will take care
// of the distribution.
// The result matrix C can be passed as nullptr and a valid pointer to a readily
// computed matrix_interface struct will be obtained.
void block_sparse_matmul(
    struct matrix_interface *a_matrix, struct matrix_interface *b_matrix,
    struct matrix_interface *c_buf);

// Print a block sparse matrix
void print_bsm(struct matrix_interface *bsm, char *prefix);