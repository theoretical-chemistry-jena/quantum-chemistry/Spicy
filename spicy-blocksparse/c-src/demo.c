#include "MatMul.h"

void main(int argc, char *argv[])
{
    MPI_Init(&argc, &argv);

    struct matrix_interface a_matrix;
    struct matrix_interface c_matrix;

    // Make A and B tridiagonal matrices with blocks of size 2x2 and 4x4 blocks.
    a_matrix.n_rows = 4;
    a_matrix.n_cols = 4;
    int a_row_blk_sz[4] = {2, 2, 2, 2};
    int a_col_blk_sz[4] = {2, 2, 2, 2};
    a_matrix.row_blk_sz = a_row_blk_sz;
    a_matrix.col_blk_sz = a_col_blk_sz;
    double blck_data[4] = {1, 2, 3, 4};
    double *blcks[10] = {blck_data, blck_data, blck_data, blck_data, blck_data, blck_data, blck_data, blck_data, blck_data, blck_data};
    a_matrix.blck_data = blcks;
    int nze = 10;
    a_matrix.nz_blks = nze;
    int r_ix[10] = {0, 0, 1, 1, 1, 2, 2, 2, 3, 3};
    int c_ix[10] = {0, 1, 0, 1, 2, 1, 2, 3, 2, 3};
    a_matrix.blck_r_ix = r_ix;
    a_matrix.blck_c_ix = c_ix;
    
    int mpi_rank;
    MPI_Comm_rank(MPI_COMM_WORLD, &mpi_rank);
    if (mpi_rank == 0) {
        block_sparse_matmul(&a_matrix, &a_matrix, &c_matrix);
        printf("Block %d x %d\n", c_matrix.n_rows, c_matrix.n_cols);
        printf("Row block sizes:");
        for (int i = 0; i < c_matrix.n_rows; i++) printf("%d  ", c_matrix.row_blk_sz[i]);
        printf("\n");
        printf("Col block sizes:");
        for (int i = 0; i < c_matrix.n_cols; i++) printf("%d  ", c_matrix.col_blk_sz[i]);
        printf("\n");
        printf("Non-Zero Blocks: %d\n", c_matrix.nz_blks);
        for (int blk = 0; blk < c_matrix.nz_blks; blk++) {
            printf("Block %d (%d, %d)\n", blk, c_matrix.blck_r_ix[blk], c_matrix.blck_c_ix[blk]);
            printf("Address: %p\n", c_matrix.blck_data[blk]);
            printf("Block size: %d x %d\n", c_matrix.row_blk_sz[c_matrix.blck_r_ix[blk]], c_matrix.col_blk_sz[c_matrix.blck_c_ix[blk]]);
            printf("Elements:\n");
            for (int el = 0; el < c_matrix.row_blk_sz[c_matrix.blck_r_ix[blk]] * c_matrix.col_blk_sz[c_matrix.blck_c_ix[blk]]; el++) {
                printf("%f   ", c_matrix.blck_data[blk][el]);
            };
            printf("\n\n");

        };
    } else {
        block_sparse_matmul(NULL, NULL, NULL);
    };

    MPI_Finalize();
}