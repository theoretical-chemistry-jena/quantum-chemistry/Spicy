{-# OPTIONS_GHC -Wno-unrecognised-pragmas #-}
{-# OPTIONS_GHC -fplugin-opt=Foreign.Storable.Generic.Plugin:-v1 #-}
{-# OPTIONS_GHC -fplugin=Foreign.Storable.Generic.Plugin #-}

{-# HLINT ignore "Use camelCase" #-}

{- |
Module      : Spicy.Math.BlockSparse
Description : Block Sparse Matrices and their Multiplication
Copyright   : Phillip Seeber, Sebastian Seidenath 2024
License     : AGPL-3
Maintainer  : phillip.seeber@uni-jena.de
Stability   : experimental
Portability : POSIX, Windows

This module implements block sparse matrices in Haskell and their efficient multiplication backed by the [DBCSR library](https://github.com/cp2k/dbcsr).
C-Interface code can be found at @src\/C\/DBCSR\/@.
The DBCSR backend heavily relies on MPI, and consequently MPI must have been initialised.
If more than one MPI rank is being used, ensure all ranks except the root rank run the DBCSR client code using the 'matMulBsWorker' function.
See also 'DbcsrClient'.
-}
module Spicy.Math.BlockSparse (
  BlockSparseException (..),
  BlockSparseMatrix,
  getBlockMat,
  getRowStripes,
  getColStripes,
  mkMatrix,
  (!?),
  (!?!),
  transpose,
  toDense,
  size,
  matMulBs,
  matMulBsWorker,
)
where

import Control.Distributed.MPI qualified as MPI
import Control.Distributed.MPI.Binary qualified as MPIb
import Control.Monad.Trans
import Data.Coerce
import Data.Massiv.Array as Massiv hiding (size, transpose, (!?))
import Data.Massiv.Array qualified as Massiv
import Data.Massiv.Array.Unsafe (unsafeWithPtr)
import Data.Massiv.Array.Unsafe qualified as Massiv
import Foreign (newForeignPtr)
import Foreign.C
import Foreign.Marshal.Alloc
import Foreign.Ptr
import Foreign.Storable
import Foreign.Storable.Generic
import GHC.Stack
import RIO.Partial (fromJust)
import Spicy.Prelude

-- | Exceptions for block sparse matrices and operations on them.
data BlockSparseException
  = BlockSparseMPIException String
  | BlockSparseDbcsrException String

instance Show BlockSparseException where
  show (BlockSparseMPIException msg) =
    "BlockSparseMPIException: "
      <> msg
      <> "\n"
      <> prettyCallStack callStack
  show (BlockSparseDbcsrException msg) =
    "BlockSparseDbcsrException: "
      <> msg
      <> "\n"
      <> prettyCallStack callStack

instance Exception BlockSparseException

--------------------------------------------------------------------------------

{- | A block sparse matrix with row and column stripes of a given size. Block-
sparse matrices can only store doubles. Conceptually a block sparse matrix in
this module and DBCSR is represented via indexed non-zero blocks in in a matrix
of blocks.

<<docs/images/rendered/blocksparse_matrix.svg>>

Here, the vector @rowStripes@ represents the size of blocks along the row
dimension and @colStripes@ equivalently for the column dimension.
-}
data BlockSparseMatrix = BSM
  { blockMat :: Matrix B (Maybe (Matrix S Double))
  -- ^ Representation of a block-sparse matrix. The outer 'Matrix' 'B'
  -- represents the blocks, i.e. each element would be a block. Non-zero
  -- blocks are encoded via 'Just' values and zero blocks are 'Nothing'.
  -- The smart constructor 'mkMatrix' must ensure, that the size of each block
  -- matches 'rowStripes' and 'colStripes' for a block at a given index.
  , rowStripes :: Vector S Int
  -- ^ Size of a block at the given block index along the rows, i.e. for a
  -- block at @(r :. c)@, look at @rowStripes ! r@ to get its number of rows.
  , colStripes :: Vector S Int
  -- ^ Equivalent to 'rowStripes' for columns.
  }
  deriving (Eq, Show, Generic)

getBlockMat :: BlockSparseMatrix -> Matrix B (Maybe (Matrix S Double))
getBlockMat = blockMat

getRowStripes :: BlockSparseMatrix -> Vector S Int
getRowStripes = rowStripes

getColStripes :: BlockSparseMatrix -> Vector S Int
getColStripes = colStripes

-- | Make a striped block-sparse matrix
mkMatrix ::
  forall m.
  (MonadThrow m) =>
  -- | Row stripes
  Vector S Int ->
  -- | Column stripes
  Vector S Int ->
  -- | Non-zero blocks of the matrix
  Vector B (Ix2, Matrix S Double) ->
  m BlockSparseMatrix
mkMatrix rowStripes colStripes blocks = do
  let Sz rowBlocks = Massiv.size rowStripes
      Sz colBlocks = Massiv.size colStripes
  Massiv.forM_ blocks $ \(rI :. cI, e) -> do
    -- Check if block index is valid
    unless (rI < rowBlocks && rI >= 0 && cI < colBlocks && cI >= 0)
      . throwM
      $ IndexOutOfBoundsException (Sz $ rowBlocks :. colBlocks) (rI :. cI)
    -- Check if block size matches
    let Sz (raSz :. caSz) = Massiv.size e
        reSz = rowStripes Massiv.! rI
        ceSz = colStripes Massiv.! cI
    unless (raSz == reSz && caSz == ceSz)
      . throwM
      $ SizeMismatchException (Sz $ raSz :. caSz) (Sz $ reSz :. ceSz)

  let blockMat =
        withMArrayST_
          (Massiv.replicate Par (Sz $ rowBlocks :. colBlocks) Nothing)
          (\marr -> Massiv.forM_ blocks $ \(i, e) -> write_ marr i (Just e))
  return $ BSM{..}

-- | Indexing in the block domain. The supplied index is a block index.
(!?) :: (MonadThrow m) => BlockSparseMatrix -> Ix2 -> m (Matrix S Double)
BSM{..} !? indx@(rIx :. cIx) = do
  trgt <- blockMat Massiv.!? indx
  rSz <- rowStripes Massiv.!? rIx
  cSz <- rowStripes Massiv.!? cIx
  case trgt of
    Nothing -> return $ Massiv.replicate Par (Sz $ rSz :. cSz) 0
    Just r -> return r

-- | Indexing in the elements domain. The supplied index is an element's index.
(!?!) :: forall m. (MonadThrow m) => BlockSparseMatrix -> Ix2 -> m Double
BSM{..} !?! (rIx :. cIx) = do
  let (rowBlckIx, rOffIx) = flip ixPair rIx . compute @S . blckNelAcc $ rowStripes
      (colBlckIx, cOffIx) = flip ixPair cIx . compute @S . blckNelAcc $ colStripes
  blck <- blockMat Massiv.!? (rowBlckIx :. colBlckIx)
  case blck of
    Nothing -> return 0
    Just m -> m Massiv.!? (rOffIx :. cOffIx)

{- | Use the stripe information to build information how many elements have
been stored along this dimension until given block index.
-}
blckNelAcc ::
  -- | Original stripe data
  Vector S Int ->
  -- | How many elements are stored up to block at index
  Vector DL Int
blckNelAcc blckSzV =
  (\(_, vec) -> 0 `Massiv.cons` vec)
    $ sfoldl
      (\(cnt, resVec) blckSz -> (cnt + blckSz, resVec `Massiv.snoc` (cnt + blckSz)))
      (0, empty)
      (delay . init $ blckSzV)

{- | Given the vector of elements up to a given block (see 'blckNelAcc') and
the index of an element in this dimension, obtain the block index and the
index of the element in the block
-}
ixPair ::
  -- | Vector, that contains the number of elements stored up to a given block
  Vector S Int ->
  -- | Index of the element in the given dimension
  Int ->
  -- | Block index, index of element in the block
  (Int, Int)
ixPair blckSzV elIx =
  let leadingBlcks = takeWhile (<= elIx) blckSzV
      lastBlock = fromMaybe 0 . lastM $ leadingBlcks
      Sz blckNums = Massiv.size leadingBlcks
      elInBlckIx = elIx - lastBlock
   in (blckNums - 1, elInBlckIx)

-- | Transpose a 'BlockSparseMatrix'.
transpose :: BlockSparseMatrix -> BlockSparseMatrix
transpose BSM{..} =
  BSM
    { blockMat =
        compute
          . Massiv.map (fmap $ compute . Massiv.transpose)
          . Massiv.transpose
          $ blockMat
    , rowStripes = colStripes
    , colStripes = rowStripes
    }

-- | Make a standard dense array from a 'BlockSparseMatrix'.
toDense :: forall r. (Manifest r Double) => BlockSparseMatrix -> Matrix r Double
toDense BSM{..} =
  let expandedBlocks = flip Massiv.imap blockMat $ \(rIx :. cIx) blk ->
        case blk of
          Just b -> b
          Nothing ->
            let rBlkSz = rowStripes Massiv.! rIx
                cBlkSz = colStripes Massiv.! cIx
             in Massiv.replicate Par (Sz $ rBlkSz :. cBlkSz) 0
      denseMat =
        concat' 2
          . fmap (compute @r . concat' 1)
          . Massiv.toLists
          $ expandedBlocks
   in compute denseMat

-- | Get the size in elements (not blocks) of the matrix.
size :: BlockSparseMatrix -> Sz2
size BSM{..} =
  let rowSz = ssum rowStripes
      colSz = ssum colStripes
   in Sz $ rowSz :. colSz

--------------------------------------------------------------------------------

-- | Haskell representation of the @matrix_interface@ C struct.
data MatrixInterfaceStruct = MatrixInterfaceStruct
  { n_rows :: {-# UNPACK #-} CInt
  -- ^ Number of blocks along the rows
  , n_cols :: {-# UNPACK #-} CInt
  -- ^ Number of blocks along the columns
  , row_blk_sz :: {-# UNPACK #-} Ptr CInt
  -- ^ Vector of block sizes along rows
  , col_blk_sz :: {-# UNPACK #-} Ptr CInt
  -- ^ Vector of block sizes along columns
  , nz_blks :: {-# UNPACK #-} CInt
  -- ^ Number of non-zero blocks in the block-sparse matrix
  , blck_r_ix :: {-# UNPACK #-} Ptr CInt
  -- ^ Vector of row indices of the non-zero blocks
  , blck_c_ix :: {-# UNPACK #-} Ptr CInt
  -- ^ Vector of column indices of the non-zero blocks
  , blck_data :: {-# UNPACK #-} Ptr (Ptr CDouble)
  -- ^ Pointer vector to the column-major (!) data of the individual blocks (a
  -- vector of vectors (that in turn represent matrices) if you want so).
  }
  deriving (Generic)

instance GStorable MatrixInterfaceStruct

{- | MPI-parallel block-sparse matrix multiplication in C, backed by DBCSR.
Calculates

\[
    \boldsymbol{C} = \boldsymbol{A} \boldsymbol{B} \, .
\]

This function is being used in two different ways

  * In the MPI root rank (Spicy main thread) pointer to valid
    'MatrixInterfaceStructs' must be passed.
    The MPI root rank is the only rank on which we assume that it actually knows
    the matrices \(\boldsymbol{A}\) and \(\boldsymbol{B}\).
    For \(\boldsymbol{C}\) pass a valid buffer pointer.
  * On all other ranks, all arguments must be passed as null pointer.
    This instructs the C side to run the DBCSR client code.
-}
foreign import capi "MatMul.h block_sparse_matmul"
  block_sparse_matmul ::
    -- | Pointer to \(\boldsymbol{A}\) on root rank, null pointer for all other
    Ptr MatrixInterfaceStruct ->
    -- | Pointer to \(\boldsymbol{B}\) on root rank, null pointer for all other
    Ptr MatrixInterfaceStruct ->
    -- | Buffer pointer to \(\boldsymbol{C}\) on root rank, null pointer for all
    -- other. The C side will exchange the pointer in the struct for the result
    -- with properly allocated ones.
    Ptr MatrixInterfaceStruct ->
    IO ()

{-
foreign import capi "MatMul.h print_bsm"
  print_bsm ::
    -- | Pointer to a matrix to be printed
    Ptr MatrixInterfaceStruct ->
    CString ->
    IO ()
-}

{- | Obtain a representation of the 'BlockSparseMatrix' suitable for the
@matrix_interface@ C struct. Note, that the underlying data can be modified
through pointers and thus, referential transparency can be violated if not
careful.
-}
unsafeWithBlockSparseMat ::
  BlockSparseMatrix ->
  (MatrixInterfaceStruct -> IO a) ->
  IO a
unsafeWithBlockSparseMat BSM{..} f = do
  let n_rows = fromIntegral . Massiv.elemsCount $ rowStripes
      n_cols = fromIntegral . Massiv.elemsCount $ colStripes
      row_blk_sz' = compute @S . Massiv.map fromIntegral $ rowStripes
      col_blk_sz' = compute @S . Massiv.map fromIntegral $ colStripes
      blocks = compute @B . smap (second fromJust) . sfilter (isJust . snd) . flatten . Massiv.imap (,) $ blockMat
      Sz nz_blks' = Massiv.size blocks
      nz_blks = fromIntegral nz_blks'
      blck_r_ix' = compute @S . smap (\(r :. _, _) -> fromIntegral r) $ blocks
      blck_c_ix' = compute @S . smap (\(_ :. c, _) -> fromIntegral c) $ blocks
      trBlcks = compute @B . smap (compute @S . Massiv.map coerce . flatten . Massiv.transpose . snd) $ blocks

  blkPointers <- liftIO $ newMArray (Sz1 nz_blks') nullPtr
  iforM_ trBlcks $ \i blk -> unsafeWithPtr blk $ \blkPtr ->
    writeM blkPointers i blkPtr
  blck_data' <- liftIO $ Massiv.unsafeFreeze Par blkPointers

  unsafeWithPtr row_blk_sz' $ \row_blk_sz ->
    unsafeWithPtr col_blk_sz' $ \col_blk_sz ->
      unsafeWithPtr blck_r_ix' $ \blck_r_ix ->
        unsafeWithPtr blck_c_ix' $ \blck_c_ix ->
          unsafeWithPtr blck_data' $ \blck_data ->
            f MatrixInterfaceStruct{..}

{- | Block-sparse matrix multiplication \(\boldsymbol{C} = \boldsymbol{A} \boldsymbol{B}\).
The resulting matrix will inherit its column and row stripe sizes from the input
matrices as @C.rowStripes = A.rowStripes@ and @C.colStripes = B.colStripes@.
Furthermore, the column stripes of \(\boldsymbol{A}\) must be identical to
the row stripes of \(\boldsymbol{B}\).

Call only this function, but if running with MPI, ensure that 'Spicy.Worker'
is running the DBCSR client code ('matMulBsWorker').
-}
matMulBs ::
  forall m.
  (MonadThrow m, PrimMonad m, MonadIO m) =>
  -- | Matrix \(\boldsymbol{A}\)
  BlockSparseMatrix ->
  -- | Matrix \(\boldsymbol{B}\)
  BlockSparseMatrix ->
  -- | Matrix \(\boldsymbol{C}\)
  m BlockSparseMatrix
matMulBs matA matB
  | matA.colStripes /= matB.rowStripes =
      throwM
        . BlockSparseDbcsrException
        $ "A rows /= B cols\nA cols = "
        <> show matA.colStripes
        <> "\nB rows = "
        <> show matB.rowStripes
  | otherwise = do
      -- Check for sane environment.
      isInitialised <- liftIO MPI.initialized
      rank <- liftIO $ MPI.commRank MPI.commWorld
      unless isInitialised
        . throwM
        . BlockSparseMPIException
        $ "MPI is not initialised"
      unless (rank == MPI.rootRank)
        . throwM
        . BlockSparseMPIException
        $ "This function must only be called by the main rank"

      -- Tell the worker to switch to the matrix multiplication MPI context
      void . liftIO $ MPIb.bcast (Just DbcsrClient) MPI.rootRank MPI.commWorld

      -- Perform the matrix multiplication on C side
      MatrixInterfaceStruct{..} <- liftIO . unsafeWithBlockSparseMat matA $ \matAstruct ->
        unsafeWithBlockSparseMat matB $ \matBstruct ->
          alloca $ \matAptr ->
            alloca $ \matBptr ->
              alloca $ \matCptr -> do
                -- Call the matrix multiplication code
                poke matAptr matAstruct
                poke matBptr matBstruct
                block_sparse_matmul matAptr matBptr matCptr
                peek matCptr

      f_row_blk_sz <- liftIO $ newForeignPtr finalizerFree row_blk_sz
      f_col_blk_sz <- liftIO $ newForeignPtr finalizerFree col_blk_sz
      f_blck_r_ix <- liftIO $ newForeignPtr finalizerFree blck_r_ix
      f_blck_c_ix <- liftIO $ newForeignPtr finalizerFree blck_c_ix
      f_blck_data <- liftIO $ newForeignPtr finalizerFree blck_data

      -- Extract data from the struct and reconstruct the
      -- Haskell values from the C-struct
      rowStripes <- compute @S . Massiv.map fromIntegral <$> copyToMassiv f_row_blk_sz (fromIntegral n_rows)
      colStripes <- compute @S . Massiv.map fromIntegral <$> copyToMassiv f_col_blk_sz (fromIntegral n_cols)
      blck_r_ix' <- compute @S . Massiv.map fromIntegral <$> copyToMassiv f_blck_r_ix (fromIntegral nz_blks)
      blck_c_ix' <- compute @S . Massiv.map fromIntegral <$> copyToMassiv f_blck_c_ix (fromIntegral nz_blks)
      blck_data' <- copyToMassiv f_blck_data (fromIntegral nz_blks)

      blck_data'' <- iforM @B blck_data' $ \i cBlkPtr -> do
        let rIx = blck_r_ix' Massiv.! i
            cIx = blck_c_ix' Massiv.! i
            rSz = rowStripes Massiv.! rIx
            cSz = colStripes Massiv.! cIx
        f_mat <- liftIO $ newForeignPtr finalizerFree cBlkPtr
        blk' <- copyToMassiv f_mat (Sz $ rSz * cSz)
        let blk = Massiv.unsafeResize (Sz $ cSz :. rSz) blk'
        return (rIx :. cIx, blk)

      let Sz nRows = Massiv.size rowStripes
          Sz nCols = Massiv.size colStripes
      blockMat' <- Massiv.newMArray (Sz $ nRows :. nCols) Nothing
      Massiv.forM_ blck_data'' $ \(i, blk) ->
        writeM blockMat' i . Just . compute . Massiv.map coerce . Massiv.transpose $ blk
      blockMat <- Massiv.unsafeFreeze Par blockMat'

      return BSM{..}
 where
  copyToMassiv fPtr n = freeze Par $ Massiv.unsafeMArrayFromForeignPtr0 fPtr n

{- | MPI client function for block sparse matrix multiplication using the DBCSR
backend.
-}
matMulBsWorker :: (MonadIO m, MonadThrow m) => m ()
matMulBsWorker = do
  -- Check for sane MPI environment
  isInitialised <- liftIO MPI.initialized
  rank <- liftIO $ MPI.commRank MPI.commWorld
  unless isInitialised
    . throwM
    . BlockSparseMPIException
    $ "MPI is not initialised"
  unless (rank /= MPI.rootRank)
    . throwM
    . BlockSparseMPIException
    $ "This function must only be called by the main rank"

  -- Run client code
  liftIO $ block_sparse_matmul nullPtr nullPtr nullPtr
