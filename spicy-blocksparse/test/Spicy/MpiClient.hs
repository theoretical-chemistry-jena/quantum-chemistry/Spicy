module Spicy.MpiClient (workerMain) where

import Control.Distributed.MPI.Binary qualified as MPI
import GHC.Stack
import Spicy.Math.BlockSparse (matMulBsWorker)
import Spicy.Prelude

-- | Exceptions of the MPI workers
newtype WorkerException
  = WorkerMPIException String

instance Show WorkerException where
  show (WorkerMPIException msg) =
    "WorkerMPIException: "
      <> msg
      <> "\n"
      <> prettyCallStack callStack

instance Exception WorkerException

{- | Main programme of workers. Controlled via MPI messages from the main
programme (MPI rank 0), multiple tasks can be performed.

MPI must have been initialised by the caller already.
-}
workerMain :: (HasLogFunc env) => RIO env ()
workerMain = do
  wRank <- liftIO $ MPI.commRank MPI.commWorld
  let logSrc = "worker " <> tshow wRank
  unless (wRank /= MPI.rootRank)
    . throwM
    . WorkerMPIException
    $ "Workers must have an MPI rank /= 0, but this instance has rank "
    <> show wRank

  workerTask <- liftIO $ MPI.bcast Nothing MPI.rootRank MPI.commWorld
  case workerTask of
    DbcsrClient -> do
      logDebugS logSrc "Starting DBCSR client"
      matMulBsWorker -- Run DBCSR client code
      logDebugS logSrc "Terminating DBCSR client"
      workerMain
    Terminate -> do
      logDebugS logSrc "Shutting down worker"
