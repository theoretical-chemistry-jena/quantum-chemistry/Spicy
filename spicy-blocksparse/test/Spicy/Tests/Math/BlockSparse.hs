module Spicy.Tests.Math.BlockSparse (tests) where

import Data.Massiv.Array as Massiv hiding (forM, forM_)
import Foreign
import Hedgehog
import Hedgehog.Gen qualified as Gen
import Hedgehog.Range qualified as Range
import RIO.Partial (fromJust)
import Spicy.FiniteCompare
import Spicy.Math.BlockSparse as BSM
import Spicy.Math.LinearAlgebra ((##))
import Spicy.Prelude
import Test.Tasty
import Test.Tasty.HUnit
import Test.Tasty.Hedgehog

tests :: TestTree
tests =
  testGroup
    "BlockSparse"
    [ testCase "Reject invalid block size" $ do
        let res =
              mkMatrix @Maybe
                (Massiv.fromList Par [2, 2])
                (Massiv.fromList Par [2, 2])
                . Massiv.fromList Par
                $ [ (0 :. 0, Massiv.makeArrayLinear Par (Sz $ 1 :. 2) fromIntegral)
                  ]
        res @?= Nothing
    , testCase "Reject invalid block index" $ do
        let res =
              mkMatrix @Maybe
                (Massiv.fromList Par [2, 2])
                (Massiv.fromList Par [2, 2])
                . Massiv.fromList Par
                $ [ (10 :. 0, Massiv.makeArrayLinear Par (Sz $ 1 :. 2) fromIntegral)
                  ]
        res @?= Nothing
    , testCase "Block indexing non-zero block" $ do
        res <- bsm1 BSM.!? (0 :. 0)
        res @?= Massiv.fromLists' Par [[0.5]]
    , testCase "Block indexing zero block" $ do
        res <- bsm1 BSM.!? (2 :. 3)
        res @?= Massiv.fromLists' Par [[0, 0, 0], [0, 0, 0], [0, 0, 0]]
    , testCase "Element indexing non-zero" $ do
        res <- bsm1 BSM.!?! (5 :. 5)
        res @?= 0.5
    , testCase "Element indexing zero" $ do
        res <- bsm1 BSM.!?! (5 :. 4)
        res @?= 0
    , testCase "Transpose" $ do
        orig <-
          mkMatrix (Massiv.fromList Par [2, 2]) (Massiv.fromList Par [2, 2])
            . Massiv.fromList Par
            $ [ (0 :. 0, Massiv.fromLists' Par [[1, 2], [0, 3]])
              , (0 :. 1, Massiv.fromLists' Par [[4, 5], [6, 7]])
              , (1 :. 1, Massiv.fromLists' Par [[8, 9], [0, 10]])
              ]
        let res = BSM.transpose orig
        expc <-
          mkMatrix (Massiv.fromList Par [2, 2]) (Massiv.fromList Par [2, 2])
            . Massiv.fromList Par
            $ [ (0 :. 0, Massiv.fromLists' Par [[1, 0], [2, 3]])
              , (1 :. 0, Massiv.fromLists' Par [[4, 6], [5, 7]])
              , (1 :. 1, Massiv.fromLists' Par [[8, 0], [9, 10]])
              ]
        res @?= expc
    , testCase "Sparse to dense conversion" $ toDense bsm2 @?= denseMat2
    , testProperty "Matrix Multiplication" . property $ do
        (bsMatA, bsMatB) <- forAll . Gen.small $ genBSM
        let dMatA = BSM.toDense @S bsMatA
            dMatB = BSM.toDense @S bsMatB
        (bsMatC :: Matrix S Double) <- BSM.toDense <$> matMulBs bsMatA bsMatB
        (dMatC :: Matrix S Double) <- dMatA ## dMatB

        let szCbs = Massiv.size bsMatC
            szCd = Massiv.size dMatC
        szCbs === szCd

        let bsMatCApprox = Massiv.map (RealExpFP @10) bsMatC
            dMatCApprox = Massiv.map (RealExpFP @10) dMatC
        bsMatCApprox === dMatCApprox
    ]

{- FOURMOLU_DISABLE -}
denseMat1 :: Matrix S Double
denseMat1 = Massiv.resize' (Sz $ 13 :. 13) . Massiv.fromList Par $
  -- 0a   1b   2c   3c   4c   5d   6d   7d   8e   9e  10e  11e  12e
  [ 0.5, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0 -- 0a
  , 0.0, 0.4, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0 -- 1b
  , 0.0, 0.0, 0.2, 0.2, 0.6, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0 -- 2c
  , 0.0, 0.0, 0.3, 0.1, 0.6, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0 -- 3c
  , 0.0, 0.0, 0.7, 0.1, 0.3, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0 -- 4c
  , 0.0, 0.0, 0.0, 0.0, 0.0, 0.5, 0.0, 0.5, 0.0, 0.0, 0.0, 0.0, 0.0 -- 5d
  , 0.0, 0.0, 0.0, 0.0, 0.0, 0.1, 0.2, 0.7, 0.0, 0.0, 0.0, 0.0, 0.0 -- 6d
  , 0.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0 -- 7d
  , 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.3, 0.3, 0.0, 0.3, 0.1 -- 8e
  , 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.1, 0.0, 0.5, 0.4 -- 9e
  , 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.4, 0.4, 0.0, 0.2, 0.0 -- 10e
  , 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.3, 0.2, 0.1, 0.4 -- 11e
  , 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.1, 0.0, 0.9 -- 12e
  ]

denseMat2 :: Matrix S Double
denseMat2 = Massiv.resize' (Sz $ 10 :. 13) . Massiv.fromList Par $
  -- 0a   1b   2c   3c   4c   5d   6d   7d   8e   9e  10e  11e  12e
  [ 1.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0 -- 0a
  , 0.0, 1.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0 -- 1b
  , 0.0, 0.0, 0.2, 0.4, 0.4, 0.0, 0.0, 0.0, 1.0, 1.0, 1.0, 1.0, 1.0 -- 2c
  , 0.0, 0.0, 0.4, 0.2, 0.4, 0.0, 0.0, 0.0, 1.0, 1.0, 1.0, 1.0, 1.0 -- 3c
  , 0.0, 0.0, 0.0, 0.0, 0.0, 1.0, 1.0, 1.0, 0.0, 0.0, 0.0, 0.0, 0.0 -- 4d
  , 0.0, 0.0, 0.0, 0.0, 0.0, 0.5, 0.0, 0.5, 0.0, 0.0, 0.0, 0.0, 0.0 -- 5d
  , 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 0.0 -- 6e
  , 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.5, 1.0, 0.0, 1.0, 0.0 -- 7e
  , 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.5, 0.4, 1.0, 0.5, 1.0 -- 8e
  , 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0, 1.0, 0.0, 1.0, 1.0 -- 9e
  ]
{- FOURMOLU_ENABLE -}

bsm1 :: BlockSparseMatrix
bsm1 =
  fromJust
    . mkMatrix
      (Massiv.fromList Par [1, 1, 3, 3, 5])
      (Massiv.fromList Par [1, 1, 3, 3, 5])
    . Massiv.fromList Par
    $ [ (0 :. 0, compute $ Massiv.extract' (0 :. 0) (Sz $ 1 :. 1) denseMat1)
      , (1 :. 1, compute $ Massiv.extract' (1 :. 1) (Sz $ 1 :. 1) denseMat1)
      , (2 :. 2, compute $ Massiv.extract' (2 :. 2) (Sz $ 3 :. 3) denseMat1)
      , (3 :. 3, compute $ Massiv.extract' (5 :. 5) (Sz $ 3 :. 3) denseMat1)
      , (4 :. 4, compute $ Massiv.extract' (8 :. 8) (Sz $ 5 :. 5) denseMat1)
      ]

bsm2 :: BlockSparseMatrix
bsm2 =
  fromJust
    . mkMatrix
      (Massiv.fromList Par [1, 1, 2, 2, 4])
      (Massiv.fromList Par [1, 1, 3, 3, 5])
    . Massiv.fromList Par
    $ [ (0 :. 0, compute $ Massiv.extract' (0 :. 0) (Sz $ 1 :. 1) denseMat2)
      , (1 :. 1, compute $ Massiv.extract' (1 :. 1) (Sz $ 1 :. 1) denseMat2)
      , (2 :. 2, compute $ Massiv.extract' (2 :. 2) (Sz $ 2 :. 3) denseMat2)
      , (2 :. 4, compute $ Massiv.extract' (2 :. 8) (Sz $ 2 :. 5) denseMat2)
      , (3 :. 3, compute $ Massiv.extract' (4 :. 5) (Sz $ 2 :. 3) denseMat2)
      , (4 :. 4, compute $ Massiv.extract' (6 :. 8) (Sz $ 4 :. 5) denseMat2)
      ]

{- | Generate a pair of random block sparse matrices (striping is random,
non-zeroness is random, elements are random), that can be multiplied.
-}
genBSM :: Gen (BlockSparseMatrix, BlockSparseMatrix)
genBSM = do
  -- Generate column and row distributions for matrix A
  aRowStripes <- Massiv.fromList @S @Int Par <$> Gen.small (Gen.list (Range.linear 1 50) (Gen.int $ Range.linearFrom 1 1 50))
  aColStripes <- Massiv.fromList @S @Int Par <$> Gen.small (Gen.list (Range.linear 1 50) (Gen.int $ Range.linearFrom 1 1 50))

  -- Generate a row and column distributionf for B, ensuring it can be multiplied with A
  bColStripes <- Massiv.fromList @S @Int Par <$> Gen.small (Gen.list (Range.linear 1 50) (Gen.int $ Range.linearFrom 1 1 50))
  let bRowStripes = aColStripes

  matA <- genRandomBSM aRowStripes aColStripes
  matB <- genRandomBSM bRowStripes bColStripes

  return (matA, matB)
 where
  -- Generate a random block of required size for a block sparse matrix.
  -- The block may be empty.
  genRandomBlck :: Sz2 -> Gen (Maybe (Matrix S Double))
  genRandomBlck sz@(Sz (rSz :. cSz)) = do
    zeroBlock <- Gen.bool
    blck <-
      resize' sz
        . Massiv.fromList @S Par
        <$> Gen.small (Gen.list (Range.singleton $ rSz * cSz) (Gen.double $ Range.exponentialFloat (-10) 10))
    if zeroBlock
      then return Nothing
      else return . Just $ blck

  genRandomBSM :: Vector S Int -> Vector S Int -> Gen BlockSparseMatrix
  genRandomBSM rowStripes colStripes = do
    rndmBlocks' <- forM matIxs $ \iX@(rIx :. cIx) -> do
      let rSz = rowStripes Massiv.! rIx
          cSz = colStripes Massiv.! cIx
      blck <- genRandomBlck (Sz $ rSz :. cSz)
      return (iX, blck)
    let rndmBlocks = fmap (second fromJust) . filter (isJust . snd) $ rndmBlocks'
    return . fromJust . mkMatrix rowStripes colStripes . Massiv.fromList Par $ rndmBlocks
   where
    Sz nRowStripes = Massiv.size rowStripes
    Sz nColStripes = Massiv.size colStripes
    matIxs =
      [ rIx :. cIx
      | rIx <- [0 .. nRowStripes - 1]
      , cIx <- [0 .. nColStripes - 1]
      ]
