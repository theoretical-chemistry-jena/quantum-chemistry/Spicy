module Main where

import Control.Distributed.MPI.Binary qualified as MPI
import Spicy.MpiClient
import Spicy.Prelude
import Spicy.Tests.Math.BlockSparse qualified
import Test.Tasty

main :: IO ()
main = MPI.mainMPI $ do
  mpiRank <- MPI.commRank MPI.commWorld
  if mpiRank == MPI.rootRank
    then do
      defaultMain tests
      void . liftIO $ MPI.bcast (Just Terminate) MPI.rootRank MPI.commWorld
    else runSimpleApp workerMain

tests :: TestTree
tests =
  testGroup
    "Spicy"
    [Spicy.Tests.Math.BlockSparse.tests]
