{-# LANGUAGE UndecidableInstances #-}

{- |
Module      : Spicy.Chemistry.Atom
Description : Description of atoms
Copyright   : Phillip Seeber, Sebastian Seidenath 2024
License     : AGPL-3
Maintainer  : phillip.seeber@uni-jena.de
Stability   : experimental
Portability : POSIX, Windows
-}
module Spicy.Chemistry.Atom (
  Atom (..),
  getElement,
  setElement,
  getLabel,
  setLabel,
  getCoord,
  setCoord,
  getChrg,
  setChrg,
  getUEl,
  setUEl,
  MkAtom (..),
  LewisAtom (..),
)
where

import Data.Aeson
import Data.Massiv.Array as Massiv
import Spicy.Chemistry.Common
import Spicy.Chemistry.Element hiding (S)
import Spicy.Math.LinearAlgebra.R3 (R3)
import Spicy.Prelude hiding (label)

{- | The most basic data type for atoms. It contains only information that would
be drawn in a Lewis structure.
-}
class Atom a where
  -- | Lens to the 'Element' of the atom
  elementL :: Lens' a Element

  -- | Lens to the label of the atom
  labelL :: Lens' a (Maybe Text)

  -- | Coordinate lens of the atom
  coordL :: Lens' a (R3 S Double)

  -- | Lens to the charge of the atom
  chrgL :: Lens' a Int

  -- | Lens to the number of unpaired electrons of the atom
  uElL :: Lens' a Int

-- | Get the 'Element' of an atom
getElement :: (Atom a) => a -> Element
getElement = (^. elementL)

-- | Set the 'Element' of an atom
setElement :: (Atom a) => a -> Element -> a
setElement a e = a & elementL .~ e

-- | Get the label of an atom
getLabel :: (Atom a) => a -> Maybe Text
getLabel = (^. labelL)

-- | Set the label of an atom
setLabel :: (Atom a) => a -> Maybe Text -> a
setLabel a l = a & labelL .~ l

-- | Get the coordinates of an atom
getCoord :: (Atom a) => a -> R3 S Double
getCoord = (^. coordL)

-- | Set the coordinates of an atom
setCoord :: (Atom a) => a -> R3 S Double -> a
setCoord a c = a & coordL .~ c

-- | Get the charge of an atom
getChrg :: (Atom a) => a -> Int
getChrg = (^. chrgL)

setChrg :: (Atom a) => a -> Int -> a
setChrg a c = a & chrgL .~ c

-- | Get the number of unpaired electrons of an atom
getUEl :: (Atom a) => a -> Int
getUEl = (^. uElL)

-- | Set the number of unpaired electrons of an atom
setUEl :: (Atom a) => a -> Int -> a
setUEl a u = a & uElL .~ u

class (Atom a) => MkAtom a where
  -- | Create an 'Atom'
  mkAtom ::
    -- | Chemical 'Element'
    Element ->
    -- | Label
    Maybe Text ->
    -- | Cartesian coordinates in Bohr
    R3 S Double ->
    -- | Formal charge
    Int ->
    -- | Number of unpaired electrons
    Int ->
    a

instance {-# OVERLAPPABLE #-} (Atom atom) => GetMass atom where
  getMass = getMass . getElement

-- | An atom, usually part of a molecule
data LewisAtom = LewisAtom
  { element :: Element
  -- ^ Chemical element of the atom
  , label :: Maybe Text
  -- ^ An informative label, most often from Mol2, PDB, Tinker, ...
  , coord :: R3 S Double
  -- ^ Cartesian orthonormal coordinates in Bohr and \( \mathbb{R}^3 \)
  , chrg :: Int
  -- ^ Formal charge (as in Lewis structure) for the atom
  , uEl :: Int
  -- ^ Formal number of unpaired electrons in the \( \alpha \) (positive) or
  -- \( \beta \) (negative) channel
  }
  deriving (Show, Eq, Generic)

instance Atom LewisAtom where
  elementL = #element
  labelL = #label
  coordL = #coord
  chrgL = #chrg
  uElL = #uEl

instance MkAtom LewisAtom where
  mkAtom = LewisAtom

instance ToJSON LewisAtom where
  toJSON = spicyToJSON

instance FromJSON LewisAtom where
  parseJSON = spicyParseJSON

instance Binary LewisAtom
