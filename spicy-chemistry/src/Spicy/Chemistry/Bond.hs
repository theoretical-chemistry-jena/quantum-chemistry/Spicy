{- |
Module      : Spicy.Chemistry.Bond
Description : Abstract bond description in molecules
Copyright   : Phillip Seeber, Sebastian Seidenath 2024
License     : AGPL-3
Maintainer  : phillip.seeber@uni-jena.de
Stability   : experimental
Portability : POSIX, Windows

High-level description of the nature of a chemical, covalent bond
-}
module Spicy.Chemistry.Bond (
  LewisBond (..),
  LewisBondOrder (..),
)
where

import Data.Aeson
import Spicy.Prelude

-- | Localised lewis bond order for covalent and coordinative bonds
class LewisBond bond where
  -- | The bond order of a bond
  getBondOrder :: bond -> LewisBondOrder

-- | A localised bond between atoms
data LewisBondOrder
  = CovalentBond Natural
  | CoordinativeBond
  deriving (Eq, Ord, Show, Generic)

instance ToJSON LewisBondOrder where
  toJSON = spicyToJSON

instance FromJSON LewisBondOrder where
  parseJSON = spicyParseJSON

instance Binary LewisBondOrder

instance LewisBond LewisBondOrder where
  getBondOrder = id
