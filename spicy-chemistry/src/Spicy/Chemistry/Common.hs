module Spicy.Chemistry.Common (
  ChemicalDataUnknownException (..),
  GetMass (..),
) where

import GHC.Stack
import Spicy.Prelude

data ChemicalDataUnknownException where
  ChemicalDataUnknownException :: (HasCallStack) => String -> ChemicalDataUnknownException

instance Show ChemicalDataUnknownException where
  show (ChemicalDataUnknownException msg) =
    "ChemicalDataUnknownException: "
      <> msg
      <> "\n"
      <> prettyCallStack callStack

instance Exception ChemicalDataUnknownException

-- | Data that provides information about the atomic/molecular weight.
class GetMass a where
  getMass :: a -> Double
