{-# LANGUAGE DeriveAnyClass #-}

{- |
Module      : Spicy.Chemistry.Element
Description : Chemical elements
Copyright   : Phillip Seeber, Sebastian Seidenath 2024
License     : AGPL-3
Maintainer  : phillip.seeber@uni-jena.de
Stability   : experimental
Portability : POSIX, Windows
-}
module Spicy.Chemistry.Element (
  Element (..),
  covalentRadii,
  possibleValences,
  Metallicity (..),
  metallicity,
)
where

import Data.Aeson
import Data.Default
import Data.List.NonEmpty qualified as NonEmpty
import Spicy.Chemistry.Common
import Spicy.Metrology as Metr
import Spicy.Prelude hiding (No)
import Spicy.Textual as TTC hiding (I)

-- | All chemical elements
data Element
  = H
  | He
  | Li
  | Be
  | B
  | C
  | N
  | O
  | F
  | Ne
  | Na
  | Mg
  | Al
  | Si
  | P
  | S
  | Cl
  | Ar
  | K
  | Ca
  | Sc
  | Ti
  | V
  | Cr
  | Mn
  | Fe
  | Co
  | Ni
  | Cu
  | Zn
  | Ga
  | Ge
  | As
  | Se
  | Br
  | Kr
  | Rb
  | Sr
  | Y
  | Zr
  | Nb
  | Mo
  | Tc
  | Ru
  | Rh
  | Pd
  | Ag
  | Cd
  | In
  | Sn
  | Sb
  | Te
  | I
  | Xe
  | Cs
  | Ba
  | La
  | Ce
  | Pr
  | Nd
  | Pm
  | Sm
  | Eu
  | Gd
  | Tb
  | Dy
  | Ho
  | Er
  | Tm
  | Yb
  | Lu
  | Hf
  | Ta
  | W
  | Re
  | Os
  | Ir
  | Pt
  | Au
  | Hg
  | Tl
  | Pb
  | Bi
  | Po
  | At
  | Rn
  | Fr
  | Ra
  | Ac
  | Th
  | Pa
  | U
  | Np
  | Pu
  | Am
  | Cm
  | Bk
  | Cf
  | Es
  | Fm
  | Md
  | No
  | Lr
  | Rf
  | Db
  | Sg
  | Bh
  | Hs
  | Mt
  | Ds
  | Rg
  | Cn
  | Nh
  | Fl
  | Mc
  | Lv
  | Ts
  | Og
  deriving (Show, Eq, Read, Ord, Enum, Bounded, Generic, NFData, Hashable)

instance ToJSON Element

instance FromJSON Element

instance Binary Element

{- | Parses the standard elemen symbols. This parser takes care of parsing
the most specific match first, e.g. @C@ does **not** consume @Ce@ partially and
leaves an @e@ to be consumed by the next parser.
-}
instance ParseAp Element where
  parser =
    choice
      [ "Zr" $> Zr
      , "Zn" $> Zn
      , "Yb" $> Yb
      , "Y" $> Y
      , "Xe" $> Xe
      , "W" $> W
      , "V" $> V
      , "U" $> U
      , "Ts" $> Ts
      , "Tm" $> Tm
      , "Tl" $> Tl
      , "Ti" $> Ti
      , "Th" $> Th
      , "Te" $> Te
      , "Tc" $> Tc
      , "Tb" $> Tb
      , "Ta" $> Ta
      , "Sr" $> Sr
      , "Sn" $> Sn
      , "Sm" $> Sm
      , "Si" $> Si
      , "Sg" $> Sg
      , "Se" $> Se
      , "Sc" $> Sc
      , "Sb" $> Sb
      , "S" $> S
      , "Ru" $> Ru
      , "Rn" $> Rn
      , "Rh" $> Rh
      , "Rg" $> Rg
      , "Rf" $> Rf
      , "Re" $> Re
      , "Rb" $> Rb
      , "Ra" $> Ra
      , "Pu" $> Pu
      , "Pt" $> Pt
      , "Pr" $> Pr
      , "Po" $> Po
      , "Pm" $> Pm
      , "Pd" $> Pd
      , "Pb" $> Pb
      , "Pa" $> Pa
      , "P" $> P
      , "Os" $> Os
      , "Og" $> Og
      , "O" $> O
      , "Np" $> Np
      , "No" $> No
      , "Ni" $> Ni
      , "Nh" $> Nh
      , "Ne" $> Ne
      , "Nd" $> Nd
      , "Nb" $> Nb
      , "Na" $> Na
      , "N" $> N
      , "Mt" $> Mt
      , "Mo" $> Mo
      , "Mn" $> Mn
      , "Mg" $> Mg
      , "Md" $> Md
      , "Mc" $> Mc
      , "Lv" $> Lv
      , "Lu" $> Lu
      , "Lr" $> Lr
      , "Li" $> Li
      , "La" $> La
      , "Kr" $> Kr
      , "K" $> K
      , "Ir" $> Ir
      , "In" $> In
      , "I" $> I
      , "Hs" $> Hs
      , "Ho" $> Ho
      , "Hg" $> Hg
      , "Hf" $> Hf
      , "He" $> He
      , "H" $> H
      , "Ge" $> Ge
      , "Gd" $> Gd
      , "Ga" $> Ga
      , "Fr" $> Fr
      , "Fm" $> Fm
      , "Fl" $> Fl
      , "Fe" $> Fe
      , "F" $> F
      , "Eu" $> Eu
      , "Es" $> Es
      , "Er" $> Er
      , "Dy" $> Dy
      , "Ds" $> Ds
      , "Db" $> Db
      , "Cu" $> Cu
      , "Cs" $> Cs
      , "Cr" $> Cr
      , "Co" $> Co
      , "Cn" $> Cn
      , "Cm" $> Cm
      , "Cl" $> Cl
      , "Cf" $> Cf
      , "Ce" $> Ce
      , "Cd" $> Cd
      , "Ca" $> Ca
      , "C" $> C
      , "Br" $> Br
      , "Bk" $> Bk
      , "Bi" $> Bi
      , "Bh" $> Bh
      , "Be" $> Be
      , "Ba" $> Ba
      , "B" $> B
      , "Au" $> Au
      , "At" $> At
      , "As" $> As
      , "Ar" $> Ar
      , "Am" $> Am
      , "Al" $> Al
      , "Ag" $> Ag
      , "Ac" $> Ac
      ]

instance Render Element where
  render e = TTC.convert . tshow $ e

-- | Obtains the average isotopic mass of an element. Taken from the radium library.
instance GetMass Element where
  getMass e = case e of
    H -> 1.008
    He -> 4.002602
    Li -> 6.941
    Be -> 9.012182
    B -> 10.811
    C -> 12.011
    N -> 14.007
    O -> 15.999
    F -> 18.9984032
    Ne -> 20.1797
    Na -> 22.989768
    Mg -> 24.305
    Al -> 26.981
    Si -> 28.085
    P -> 30.973
    S -> 32.066
    Cl -> 35.4527
    Ar -> 39.948
    K -> 39.0983
    Ca -> 40.078
    Sc -> 44.95591
    Ti -> 47.88
    V -> 50.9415
    Cr -> 51.9961
    Mn -> 54.93805
    Fe -> 55.845
    Co -> 58.933
    Ni -> 58.6934
    Cu -> 63.546
    Zn -> 65.39
    Ga -> 69.723
    Ge -> 72.61
    As -> 74.92159
    Se -> 78.96
    Br -> 79.904
    Kr -> 83.8
    Rb -> 85.4678
    Sr -> 87.62
    Y -> 88.90585
    Zr -> 91.224
    Nb -> 92.90638
    Mo -> 95.94
    Tc -> 98.9063
    Ru -> 101.07
    Rh -> 102.9055
    Pd -> 106.42
    Ag -> 107.8682
    Cd -> 112.411
    In -> 114.82
    Sn -> 118.71
    Sb -> 121.75
    Te -> 127.6
    I -> 126.90447
    Xe -> 131.29
    Cs -> 132.90543
    Ba -> 137.327
    La -> 138.9055
    Ce -> 140.115
    Pr -> 140.90765
    Nd -> 144.24
    Pm -> 146.9151
    Sm -> 150.36
    Eu -> 151.965
    Gd -> 157.25
    Tb -> 158.92534
    Dy -> 162.5
    Ho -> 164.93032
    Er -> 167.26
    Tm -> 168.93421
    Yb -> 173.04
    Lu -> 174.967
    Hf -> 178.49
    Ta -> 180.9479
    W -> 183.85
    Re -> 186.207
    Os -> 190.2
    Ir -> 192.22
    Pt -> 195.08
    Au -> 196.96654
    Hg -> 200.59
    Tl -> 204.3833
    Pb -> 207.2
    Bi -> 208.98037
    Po -> 208.9824
    At -> 209.9871
    Rn -> 222.0176
    Fr -> 223.0197
    Ra -> 226.0254
    Ac -> 227.0278
    Th -> 232.0381
    Pa -> 231.0359
    U -> 238.0289
    Np -> 237.0482
    Pu -> 244.0642
    Am -> 243.0614
    Cm -> 247.0703
    Bk -> 247.0703
    Cf -> 251.0796
    Es -> 252.0829
    Fm -> 257.0951
    Md -> 258.0986
    No -> 259.1009
    Lr -> 260.1053
    Rf -> 261.1087
    Db -> 262.1138
    Sg -> 263.1182
    Bh -> 262.1229
    Hs -> 265
    Mt -> 266
    Ds -> 269
    Rg -> 272
    Cn -> 285
    Nh -> 284
    Fl -> 289
    Mc -> 288
    Lv -> 293
    Ts -> 294
    Og -> 294

{- | Covalent radii of elements taken from
<https://pubs.rsc.org/en/content/articlelanding/2008/DT/b801115j Covalent radii revisited>
-}
covalentRadii :: (MonadThrow m) => Element -> m Double
covalentRadii e =
  Metr.convert Angstrom def <$> case e of
    H -> return 0.31
    He -> return 0.28
    Li -> return 1.28
    Be -> return 0.96
    B -> return 0.84
    C -> return 0.76
    N -> return 0.71
    O -> return 0.66
    F -> return 0.57
    Ne -> return 0.58
    Na -> return 1.66
    Mg -> return 1.41
    Al -> return 1.21
    Si -> return 1.11
    P -> return 1.07
    S -> return 1.05
    Cl -> return 1.02
    Ar -> return 1.06
    K -> return 2.03
    Ca -> return 1.76
    Sc -> return 1.70
    Ti -> return 1.60
    V -> return 1.53
    Cr -> return 1.39
    Mn -> return 1.61
    Fe -> return 1.52
    Co -> return 1.50
    Ni -> return 1.24
    Cu -> return 1.32
    Zn -> return 1.22
    Ga -> return 1.22
    Ge -> return 1.20
    As -> return 1.19
    Se -> return 1.20
    Br -> return 1.20
    Kr -> return 1.16
    Rb -> return 2.20
    Sr -> return 1.95
    Y -> return 1.90
    Zr -> return 1.75
    Nb -> return 1.64
    Mo -> return 1.54
    Tc -> return 1.47
    Ru -> return 1.46
    Rh -> return 1.42
    Pd -> return 1.39
    Ag -> return 1.45
    Cd -> return 1.44
    In -> return 1.42
    Sn -> return 1.39
    Sb -> return 1.39
    Te -> return 1.38
    I -> return 1.39
    Xe -> return 1.40
    Cs -> return 2.44
    Ba -> return 2.15
    Lu -> return 1.87
    Hf -> return 1.75
    Ta -> return 1.70
    W -> return 1.62
    Re -> return 1.51
    Os -> return 1.44
    Ir -> return 1.41
    Pt -> return 1.36
    Au -> return 1.36
    Hg -> return 1.32
    Tl -> return 1.45
    Pb -> return 1.46
    Bi -> return 1.48
    Po -> return 1.40
    At -> return 1.50
    Rn -> return 1.50
    Fr -> return 2.60
    Ra -> return 2.21
    La -> return 2.07
    Ce -> return 2.04
    Pr -> return 2.03
    Nd -> return 2.01
    Pm -> return 1.99
    Sm -> return 1.98
    Eu -> return 1.98
    Gd -> return 1.96
    Tb -> return 1.50
    Dy -> return 1.92
    Ho -> return 1.92
    Er -> return 1.89
    Tm -> return 1.90
    Yb -> return 1.87
    Ac -> return 1.92
    Th -> return 2.06
    Pa -> return 2.00
    U -> return 1.96
    Np -> return 1.90
    Pu -> return 1.87
    Am -> return 1.80
    Cm -> return 1.69
    _ -> throwM . ChemicalDataUnknownException $ "covalent radius of " <> show e <> " is unknown"

{- | Common valences of elements. Taken from an evaluation with the Radium
library (which is unfortunately not compatible with recent containers
library) for elements up to 'Rn'.
-}
possibleValences :: (MonadThrow m) => Element -> m (NonEmpty Natural)
possibleValences e = case e of
  H -> return . NonEmpty.fromList $ [1]
  He -> return . NonEmpty.fromList $ [0]
  Li -> return . NonEmpty.fromList $ [1]
  Be -> return . NonEmpty.fromList $ [0, 2]
  B -> return . NonEmpty.fromList $ [1, 3]
  C -> return . NonEmpty.fromList $ [2, 4]
  N -> return . NonEmpty.fromList $ [3]
  O -> return . NonEmpty.fromList $ [2]
  F -> return . NonEmpty.fromList $ [1]
  Ne -> return . NonEmpty.fromList $ [0]
  Na -> return . NonEmpty.fromList $ [1]
  Mg -> return . NonEmpty.fromList $ [0, 2]
  Al -> return . NonEmpty.fromList $ [1, 3]
  Si -> return . NonEmpty.fromList $ [2, 4]
  P -> return . NonEmpty.fromList $ [3, 5]
  S -> return . NonEmpty.fromList $ [2, 4, 6]
  Cl -> return . NonEmpty.fromList $ [1, 3, 5, 7]
  Ar -> return . NonEmpty.fromList $ [0, 2, 4, 6, 8]
  K -> return . NonEmpty.fromList $ [1]
  Ca -> return . NonEmpty.fromList $ [0, 2]
  Sc -> return . NonEmpty.fromList $ [3]
  Ti -> return . NonEmpty.fromList $ [4]
  V -> return . NonEmpty.fromList $ [2, 3, 4, 5]
  Cr -> return . NonEmpty.fromList $ [2, 3, 6]
  Mn -> return . NonEmpty.fromList $ [2, 3, 4, 6, 7]
  Fe -> return . NonEmpty.fromList $ [2, 3]
  Co -> return . NonEmpty.fromList $ [2, 3]
  Ni -> return . NonEmpty.fromList $ [2]
  Cu -> return . NonEmpty.fromList $ [1, 2]
  Zn -> return . NonEmpty.fromList $ [2]
  Ga -> return . NonEmpty.fromList $ [1, 3]
  Ge -> return . NonEmpty.fromList $ [2, 4]
  As -> return . NonEmpty.fromList $ [3, 5]
  Se -> return . NonEmpty.fromList $ [2, 4, 6]
  Br -> return . NonEmpty.fromList $ [1, 3, 5, 7]
  Kr -> return . NonEmpty.fromList $ [0, 2, 4, 6, 8]
  Rb -> return . NonEmpty.fromList $ [1]
  Sr -> return . NonEmpty.fromList $ [0, 2]
  Y -> return . NonEmpty.fromList $ [3]
  Zr -> return . NonEmpty.fromList $ [4]
  Nb -> return . NonEmpty.fromList $ [5]
  Mo -> return . NonEmpty.fromList $ [4, 6]
  Tc -> return . NonEmpty.fromList $ [4, 7]
  Ru -> return . NonEmpty.fromList $ [3, 4]
  Rh -> return . NonEmpty.fromList $ [3]
  Pd -> return . NonEmpty.fromList $ [2, 4]
  Ag -> return . NonEmpty.fromList $ [1]
  Cd -> return . NonEmpty.fromList $ [2]
  In -> return . NonEmpty.fromList $ [1, 3]
  Sn -> return . NonEmpty.fromList $ [2, 4]
  Sb -> return . NonEmpty.fromList $ [3, 5]
  Te -> return . NonEmpty.fromList $ [2, 4, 6]
  I -> return . NonEmpty.fromList $ [1, 3, 5, 7]
  Xe -> return . NonEmpty.fromList $ [0, 2, 4, 6, 8]
  Cs -> return . NonEmpty.fromList $ [1]
  Ba -> return . NonEmpty.fromList $ [0, 2]
  La -> return . NonEmpty.fromList $ [3]
  Ce -> return . NonEmpty.fromList $ [3, 4]
  Pr -> return . NonEmpty.fromList $ [3]
  Nd -> return . NonEmpty.fromList $ [3]
  Pm -> return . NonEmpty.fromList $ [3]
  Sm -> return . NonEmpty.fromList $ [3]
  Eu -> return . NonEmpty.fromList $ [2, 3]
  Gd -> return . NonEmpty.fromList $ [3]
  Tb -> return . NonEmpty.fromList $ [3]
  Dy -> return . NonEmpty.fromList $ [3]
  Ho -> return . NonEmpty.fromList $ [3]
  Er -> return . NonEmpty.fromList $ [3]
  Tm -> return . NonEmpty.fromList $ [3]
  Yb -> return . NonEmpty.fromList $ [3]
  Lu -> return . NonEmpty.fromList $ [3]
  Hf -> return . NonEmpty.fromList $ [4]
  Ta -> return . NonEmpty.fromList $ [5]
  W -> return . NonEmpty.fromList $ [4, 6]
  Re -> return . NonEmpty.fromList $ [4]
  Os -> return . NonEmpty.fromList $ [4]
  Ir -> return . NonEmpty.fromList $ [3, 4]
  Pt -> return . NonEmpty.fromList $ [2, 4]
  Au -> return . NonEmpty.fromList $ [1, 3]
  Hg -> return . NonEmpty.fromList $ [2]
  Tl -> return . NonEmpty.fromList $ [1, 3]
  Pb -> return . NonEmpty.fromList $ [2, 4]
  Bi -> return . NonEmpty.fromList $ [3, 5]
  Po -> return . NonEmpty.fromList $ [2, 4, 6]
  At -> return . NonEmpty.fromList $ [1, 3, 5, 7]
  Rn -> return . NonEmpty.fromList $ [0, 2, 4, 6, 8]
  _ -> throwM . ChemicalDataUnknownException $ "possible valences of " <> show e <> " are unknown"

{- | Metallicity of an element, relevant in the perspective of coordination
complexes.
-}
data Metallicity = NonMetal | SemiMetal | Metal deriving (Eq, Show)

-- | Metallicity of an 'Element'
metallicity :: (HasCallStack) => Element -> Metallicity
metallicity e
  | e `elem` nonMetals = NonMetal
  | e `elem` metals = Metal
  | e `elem` semiMetals = SemiMetal
  | otherwise = error $ "metallicity of " <> show e <> " is unknown"
 where
  nonMetals = [H, He] <> [C .. Ne] <> [P .. Ar] <> [Br, Kr, I, Xe, Rn]
  metals = [Li, Be, Na, Mg, Al] <> [K .. Ga] <> [Rb .. Sn] <> [Cs .. Bi] <> [Fr .. Og]
  semiMetals = [B, Si, Ge, As, Se, Sb, Te, Po, At]
