{- |
Module      : Spicy.Chemistry.Molecule
Description : Constructing, querying and interacting with Molecules
Copyright   : Phillip Seeber, Sebastian Seidenath 2024
License     : AGPL-3
Maintainer  : phillip.seeber@uni-jena.de
Stability   : experimental
Portability : POSIX, Windows

Molecules are viewed as undirected graph with the set of 'Atom's \(A = \{a_1, \dots, a_n \} \) as its vertices and the set of 'Bond's \(B\) forming its edges.
'Bond's always connect exactly two atoms, i.e. there are no multicentre bonds in Spicy.
For example, see the following graph:

<<docs/images/rendered/molgraph.svg>>
-}
module Spicy.Chemistry.Molecule (
  -- * Exceptions
  MoleculeException (..),

  -- * Type Aliases
  AtomId,

  -- * Molecule
  Molecule (getAtoms, getBonds, getCharge, getMolGraph, getMult, getUnpEl),
  MkMolecule (mkMolecule),
  UpdMolecule (updCoords),
  DynTopoMolecule (modifyAtom, modifyAtoms, modifyBond, modifyBonds),
  MolGr,
  updCoords',
)
where

import Data.Massiv.Array hiding (zipWith)
import Data.Massiv.Array qualified as Massiv
import Spicy.Chemistry.Molecule.Internal
import Spicy.Math.LinearAlgebra.R3
import Spicy.Prelude

-- | Update all atomic coordinates without changing the topology.
updCoords' ::
  (MonadThrow m, Massiv.Index ix, Source r Double, Load r Ix2 Double, UpdMolecule mol atom bond) =>
  mol atom bond ->
  Array r ix Double ->
  m (mol atom bond)
updCoords' mol c = do
  reshCoords <- compute @S <$> resizeM (Sz $ nAtoms :. 3) c
  coordVecs <- traverse mkR3 $ (reshCoords !>) <$> [0 .. nAtoms - 1]
  let lCoords = zipWith (\(i, _) c' -> (i, c')) atoms coordVecs
  return . updCoords mol $ lCoords
 where
  atoms = getAtoms mol
  nAtoms = length atoms
