{-# LANGUAGE UndecidableInstances #-}

{- |
Module      : Spicy.Chemistry.Molecule.Internal
Description : Unsafely constructing, querying and interacting with Molecules
Copyright   : Phillip Seeber, Sebastian Seidenath 2024
License     : AGPL-3
Maintainer  : phillip.seeber@uni-jena.de
Stability   : experimental
Portability : POSIX, Windows

This is the internal equivalent to 'Spicy.Chemistry.Molecule'.
Use this only to declare new instances of 'Molecule' and it's ilk.
-}
module Spicy.Chemistry.Molecule.Internal (
  -- * Exceptions
  MoleculeException (..),

  -- * Type Aliases
  AtomId,

  -- * Molecule
  Molecule (..),
  MkMolecule (..),
  UpdMolecule (..),
  DynTopoMolecule (..),
  MolGr (..),
)
where

import Data.Aeson hiding (Array)
import Data.Massiv.Array (S)
import GHC.Exception (prettyCallStack)
import GHC.Stack (callStack)
import RIO.HashSet qualified as HashSet
import RIO.Partial (fromJust)
import Spicy.Chemistry.Atom
import Spicy.Chemistry.Common
import Spicy.Graph
import Spicy.HashMap qualified as HashMap
import Spicy.Math.LinearAlgebra.R3
import Spicy.Prelude hiding ((&))

{-# WARNING
  mkMolecule'
  , modifyAtom'
  , modifyAtoms'
  [ "This is an internal function."
  , "Use at your own peril."
  ]
  #-}

--------------------------------------------------------------------------------

-- | Exceptions related to the logic of a 'Molecule'.
data MoleculeException where
  MoleculeException :: (HasCallStack) => String -> MoleculeException

instance Exception MoleculeException

instance Show MoleculeException where
  show (MoleculeException msg) =
    "MoleculeException: "
      <> msg
      <> "\n"
      <> prettyCallStack callStack

type AtomId = Int

--------------------------------------------------------------------------------

class (Atom atom, Eq bond) => Molecule mol atom bond where
  -- | Obtain an undirected 'Gr' representation of the molecule.
  getMolGraph :: mol atom bond -> Gr atom bond

  -- | Check a 'Molecule' for sanity.
  -- Ideally, safe functions should not be able to generate an invalid molecule, so this function is internal only.
  -- The conditions checked are:
  --
  --   * The combination of charge and multiplicity is valid
  --   * The number of unpaired electrons does not exceed the total number of electrons
  --   * The total charge is not greater than the total nuclear charge (positrons not allowed, sorry)
  --   * There are no dangling bonds
  --   * There are no ambiguous bonds
  checkMolecule :: (MonadThrow m) => mol atom bond -> m ()
  checkMolecule mol
    | and [bondsOK, elMultOK, elPairOK, elChargeOK, unambgOK] = pure ()
    | otherwise =
        throwM
          . MoleculeException
          . concat
          $ [ "Conditions violated:\n"
            , not bondsOK ?-> "Bonds bind only to existing atoms"
            , not elMultOK
                ?-> "Molecule has possible combination of charge and multiplicity"
            , not elPairOK
                ?-> "Molecule has more unpaired electrons than electrons"
            , not elChargeOK
                ?-> "Molecule has less charge than sum of nuclear charges"
            , not unambgOK
                ?-> "Bonds are unambigous,\
                    \i.e. have the same bond order in both directions"
            ]
   where
    atoms = getAtoms mol
    bonds = getBonds mol

    bondsOK = HashSet.null (bondIds `HashSet.difference` atomIds)
    elMultOK = sysEl >= s2 && (odd sysEl && odd s2 || even sysEl && even s2)
    elPairOK = sysEl >= sumOver (abs . getUEl)
    elChargeOK = elChrg <= nucChrg
    unambgOK = all unambig bonds

    bondSet = HashMap.fromList . fmap (\(o, t, bo) -> ((o, t), bo)) $ bonds
    unambig (o, t, bo) = case HashMap.lookup (t, o) bondSet of
      Nothing -> True
      Just bo' -> bo == bo'

    sumOver f = sum . fmap (f . snd) $ atoms
    s2 = sumOver getUEl
    elChrg = sumOver getChrg
    nucChrg = sumOver ((+ 1) . fromEnum . getElement)
    sysEl = nucChrg + elChrg

    atomIds = HashSet.fromList . fmap fst $ atoms
    bondIds =
      let bo = HashSet.fromList $ bonds ^.. each % _1
          bt = HashSet.fromList $ bonds ^.. each % _2
       in bo <> bt

  -- | Get all atoms of a molecule.
  getAtoms :: mol atom bond -> [LNode atom]
  getAtoms mol = labNodes . getMolGraph $ mol

  -- | Get all bonds of a molecule.
  getBonds :: mol atom bond -> [LEdge bond]
  getBonds mol = labEdges . getMolGraph $ mol

  -- | Get charge of the molecule.
  getCharge :: mol atom bond -> Int
  getCharge mol = sum . fmap (getChrg . snd) . getAtoms $ mol

  -- | Get number of unpaired electrons.
  getUnpEl :: mol atom bond -> Int
  getUnpEl mol = sum . fmap (abs . getUEl . snd) . getAtoms $ mol

  -- | Get multiplicity of the molecule.
  getMult :: mol atom bond -> Natural
  getMult mol =
    let s2 = fromIntegral . sum . fmap (getUEl . snd) . getAtoms $ mol
     in s2 + 1

  {-# MINIMAL getMolGraph #-}

instance (Molecule mol atom bond, GetMass atom) => GetMass (mol atom bond) where
  getMass mol = sum . fmap (getMass . snd) . getAtoms $ mol

--------------------------------------------------------------------------------

-- | 'Molecule's that can be created from 'Atom's and 'Bond's
class (Atom atom, Molecule mol atom bond) => MkMolecule mol atom bond where
  -- | Make a molecule from indexed atoms and predefined bonds. An instance must
  -- throw an exception if one of the following conditions is violated
  --
  --   * The molecule has a possible combination of charge and multiplicity
  --   * The molecule does not have more unpaired electrons than total electrons
  --   * Bonds bind only to existing atoms
  --   * Bonds are unambigous, i.e. if one is specified bidirectorial it has the
  --     same bond order in both directions
  --
  -- and an instance must automatically fix the following issues:
  --
  --   * Bonds are undirected, irrespective of the input directedness
  --   * Bonds are unique, i.e they do not double
  --   * No self-loop bonds
  mkMolecule ::
    (MonadThrow m, Foldable f1, Foldable f2) =>
    f1 (LNode atom) ->
    f2 (LEdge bond) ->
    m (mol atom bond)
  mkMolecule as bs = do
    let mol = mkMolecule' as bs
    checkMolecule mol
    return mol

  -- | Unsafe version of 'MkMolecule' that does not perform any sanity checks.
  -- It should still make sure that the bonds are unlooped, unique and undirected.
  mkMolecule' ::
    (Foldable f1, Foldable f2) =>
    f1 (LNode atom) ->
    f2 (LEdge bond) ->
    mol atom bond
  mkMolecule' as bs = fromJust $ mkMolecule as bs

  {-# MINIMAL mkMolecule | mkMolecule' #-}

--------------------------------------------------------------------------------

-- | 'Molecule's whose atomic coordinates can be updated
class (Atom atom, Molecule mol atom bond) => UpdMolecule mol atom bond where
  -- | Update the coordinates of a subset of atoms without changing the
  -- topology. Coordinates for atoms not in the molecule will be discarded.
  updCoords :: (Foldable f) => mol atom bond -> f (LNode (R3 S Double)) -> mol atom bond

--------------------------------------------------------------------------------

-- | A 'Molecule' whose topology may be modified after creation
class (Atom atom, Molecule mol atom bond) => DynTopoMolecule mol atom bond where
  -- | Insert or delete an atom at a certain index.
  -- A 'Nothing' will delete, a 'Just' will insert.
  -- An implementation must satisfy:
  --
  --   * Deletion: All associated bonds must also be deleted.
  --   * Insertion: Bonds of changed atoms must not be modified.
  --
  -- This function is possibly unsafe.
  -- The updated atom could make the 'Molecule' violate the laws of the class.
  -- For example, the introduced atom may have too high a charge.
  modifyAtom' :: mol atom bond -> LNode (Maybe atom) -> mol atom bond
  modifyAtom' mol = modifyAtoms' mol . (: [])

  -- | Safe version of 'modifyAtom'.
  -- Performs a potentially expensive sanity check afterwards.
  -- You may want to use 'modifyAtoms' to insert multiple 'Atoms' at once.
  modifyAtom :: (MonadThrow m) => mol atom bond -> LNode (Maybe atom) -> m (mol atom bond)
  modifyAtom mol lnode = do
    let umol = modifyAtom' mol lnode
    checkMolecule umol
    return umol

  -- | Unsafely insert or delete multiple 'Atoms'.
  -- Please see the note at 'modifyAtom''.
  modifyAtoms' :: (Foldable f) => mol atom bond -> f (LNode (Maybe atom)) -> mol atom bond
  modifyAtoms' = foldl' modifyAtom'

  -- | Safe version of 'modifyAtoms''.
  -- Will perform a sanity check after all modifications are finished.
  modifyAtoms ::
    (MonadThrow m, Foldable f) =>
    mol atom bond ->
    f (LNode (Maybe atom)) ->
    m (mol atom bond)
  modifyAtoms mol lnodes = do
    let umol = modifyAtoms' mol lnodes
    checkMolecule umol
    return umol

  -- | Insert or delete a bond between two atoms.
  -- A 'Nothing' deletes while a 'Just' inserts.
  -- An implementation must satisfy:
  --
  --   * No dangling bonds may be inserted
  --   * No looping bonds may be inserted
  --   * No redundant bonds may be inserted
  --   * No undirected bonds may be inserted
  --
  -- If these conditions are satisfied, this function is perfectly safe.
  modifyBond :: mol atom bond -> LEdge (Maybe bond) -> mol atom bond
  modifyBond mol = modifyBonds mol . (: [])

  -- | Modify multiple bonds at once.
  -- The notes at 'modifyBond' apply here as well.
  modifyBonds :: (Foldable f) => mol atom bond -> f (LEdge (Maybe bond)) -> mol atom bond
  modifyBonds = foldl' modifyBond

  {-# MINIMAL (modifyAtom' | modifyAtoms'), (modifyBond | modifyBonds) #-}

--------------------------------------------------------------------------------

newtype MolGr atom bond = MolGr {molGr :: Gr atom bond}
  deriving (Generic, Eq, Show)

instance (Atom atom, Eq bond) => Molecule MolGr atom bond where
  getMolGraph (MolGr molGr) = molGr

instance (Atom atom, Ord bond) => MkMolecule MolGr atom bond where
  mkMolecule' atoms bonds =
    let unloop = filter (\(o, t, _) -> o /= t)
     in MolGr . undir $ mkGraph (toList atoms) (nubOrd . unloop $ toList bonds)

instance (Atom atom, Eq bond) => UpdMolecule MolGr atom bond where
  updCoords mol newCoords =
    let atoms = HashMap.fromList . toList . getAtoms $ mol
        nc = HashMap.fromList . toList $ newCoords
        updSubset = HashMap.intersectionWith setCoord atoms nc
        updAtoms = updSubset `HashMap.union` atoms
     in MolGr $ mkGraph (HashMap.toList updAtoms) (getBonds mol)

instance (Atom atom, Eq bond) => DynTopoMolecule MolGr atom bond where
  modifyAtom' mol (i, ma) = case ma of
    Nothing ->
      MolGr . efilter (\(x, y, _) -> x == i || y == i) . delNode i $ mGr
    Just a -> case match i mGr of
      (Nothing, _) -> MolGr $ insNode (i, a) mGr
      (Just (p, _, _, s), g) -> MolGr $ (p, i, a, s) & g
   where
    mGr = getMolGraph mol

  modifyBond mol (o, t, mbo) = case mbo of
    Nothing ->
      MolGr
        $ efilter (\(x, y, _) -> not $ (x, y) == (o, t) || (y, x) == (o, t)) mGr
    Just bo
      | dangling || looping -> mol
      | otherwise ->
          MolGr
            . insEdge (o, t, bo)
            . insEdge (t, o, bo)
            . efilter (\(x, y, _) -> not $ (x, y) == (o, t) || (y, x) == (o, t))
            $ mGr
   where
    mGr = getMolGraph mol
    atms = fst <$> getAtoms mol
    dangling = o `notElem` atms || t `notElem` atms
    looping = o == t

instance (ToJSON atom, ToJSON bond) => ToJSON (MolGr atom bond) where
  toJSON = spicyToJSON

instance (FromJSON atom, FromJSON bond) => FromJSON (MolGr atom bond) where
  parseJSON = spicyParseJSON

instance (Binary atom, Binary bond) => Binary (MolGr atom bond)
