module Spicy.Tests.Chemistry.Generators (
  genMolGr,
  TestMolecule (..),
  empty,
  h2,
  twoH2,
  ethanal,
  nPentane,
) where

import Hedgehog
import Hedgehog.Gen as Gen
import Spicy.Chemistry
import Spicy.Chemistry.Molecule.Internal
import Spicy.Math.LinearAlgebra.R3
import Spicy.Prelude

genMolGr :: Gen (MolGr LewisAtom LewisBondOrder)
genMolGr =
  Gen.choice
    . fmap pure
    $ [getMol h2, getMol twoH2, getMol ethanal, getMol nPentane]

--------------------------------------------------------------------------------
-- TestMolecule
--------------------------------------------------------------------------------

data TestMolecule where
  TestMolecule ::
    { name :: String
    , getMol :: forall mol. (MkMolecule mol LewisAtom LewisBondOrder, DynTopoMolecule mol LewisAtom LewisBondOrder) => mol LewisAtom LewisBondOrder
    } ->
    TestMolecule

undirBonds :: [(a, a, x)] -> [(a, a, x)]
undirBonds es = es <> (f <$> es)
 where
  f (x, y, z) = (y, x, z)

empty :: TestMolecule
empty =
  TestMolecule
    { name = "Empty molecule"
    , getMol = mkMolecule' [] []
    }

h2 :: TestMolecule
h2 =
  TestMolecule
    { name = "H2"
    , getMol = mkMolecule' n e
    }
 where
  n =
    [ (1, LewisAtom H Nothing (mkR3T (0, 0, 0)) 0 0)
    , (2, LewisAtom H Nothing (mkR3T (1, 0, 0)) 0 0)
    ]
  e =
    undirBonds
      [ (1, 2, CovalentBond 1)
      ]

twoH2 :: TestMolecule
twoH2 =
  TestMolecule
    { name = "Two H2"
    , getMol = mkMolecule' n e
    }
 where
  n =
    [ (1, LewisAtom H Nothing (mkR3T (0, 0, 0)) 0 0)
    , (2, LewisAtom H Nothing (mkR3T (1, 0, 0)) 0 0)
    , (3, LewisAtom H Nothing (mkR3T (0, 0, 100)) 0 0)
    , (4, LewisAtom H Nothing (mkR3T (1, 0, 100)) 0 0)
    ]
  e =
    undirBonds
      [ (1, 2, CovalentBond 1)
      , (3, 4, CovalentBond 1)
      ]

ethanal :: TestMolecule
ethanal =
  TestMolecule
    { name = "Ethanal"
    , getMol = mkMolecule' n e
    }
 where
  n =
    [ (1, LewisAtom C (Just "C1") (mkR3T (0, 0, 0)) 0 0)
    , (2, LewisAtom O (Just "O2") (mkR3T (0, 1, 1)) 0 0)
    , (3, LewisAtom H (Just "H3") (mkR3T (0, 1, -1)) 0 0)
    , (4, LewisAtom C (Just "C4") (mkR3T (0, -1, 0)) 0 0)
    , (5, LewisAtom H (Just "H5") (mkR3T (0, -2, 1)) 0 0)
    , (6, LewisAtom H (Just "H6") (mkR3T (-1, -2, 1)) 0 0)
    , (7, LewisAtom H (Just "H7") (mkR3T (1, -2, 1)) 0 0)
    ]
  e =
    undirBonds
      [ (1, 2, CovalentBond 2)
      , (1, 3, CovalentBond 1)
      , (1, 4, CovalentBond 1)
      , (4, 5, CovalentBond 1)
      , (4, 6, CovalentBond 1)
      , (4, 7, CovalentBond 1)
      ]

nPentane :: TestMolecule
nPentane =
  TestMolecule
    { name = "n-Pentane"
    , getMol = mkMolecule' n e
    }
 where
  n =
    [ (1, LewisAtom C (Just "C1") (mkR3T (0, 0, 0)) 0 0)
    , (2, LewisAtom C (Just "C2") (mkR3T (0, 0, 1)) 0 0)
    , (3, LewisAtom C (Just "C3") (mkR3T (0, 0, 2)) 0 0)
    , (4, LewisAtom C (Just "C4") (mkR3T (0, 0, 3)) 0 0)
    , (5, LewisAtom C (Just "C5") (mkR3T (0, 0, 4)) 0 0)
    , (6, LewisAtom H (Just "H6") (mkR3T (0, 0, -1)) 0 0)
    , (7, LewisAtom H (Just "H7") (mkR3T (1, 0, 0)) 0 0)
    , (8, LewisAtom H (Just "H8") (mkR3T (-1, 0, 0)) 0 0)
    , (9, LewisAtom H (Just "H9") (mkR3T (1, 0, 1)) 0 0)
    , (10, LewisAtom H (Just "H10") (mkR3T (-1, 0, 1)) 0 0)
    , (11, LewisAtom H (Just "H11") (mkR3T (1, 0, 2)) 0 0)
    , (12, LewisAtom H (Just "H12") (mkR3T (-1, 0, 2)) 0 0)
    , (13, LewisAtom H (Just "H13") (mkR3T (1, 0, 3)) 0 0)
    , (14, LewisAtom H (Just "H14") (mkR3T (-1, 0, 3)) 0 0)
    , (15, LewisAtom H (Just "H15") (mkR3T (1, 0, 4)) 0 0)
    , (16, LewisAtom H (Just "H16") (mkR3T (-1, 0, 4)) 0 0)
    , (17, LewisAtom H (Just "H17") (mkR3T (0, 0, 5)) 0 0)
    ]
  e =
    undirBonds
      [ (1, 2, CovalentBond 1)
      , (2, 3, CovalentBond 1)
      , (3, 4, CovalentBond 1)
      , (4, 5, CovalentBond 1)
      , (1, 6, CovalentBond 1)
      , (1, 7, CovalentBond 1)
      , (1, 8, CovalentBond 1)
      , (2, 9, CovalentBond 1)
      , (2, 10, CovalentBond 1)
      , (3, 11, CovalentBond 1)
      , (3, 12, CovalentBond 1)
      , (4, 13, CovalentBond 1)
      , (4, 14, CovalentBond 1)
      , (5, 15, CovalentBond 1)
      , (5, 16, CovalentBond 1)
      , (5, 17, CovalentBond 1)
      ]
