module Spicy.Tests.Chemistry (tests) where

import Spicy.Tests.Chemistry.Atom qualified
import Spicy.Tests.Chemistry.Molecule qualified
import Test.Tasty

tests :: TestTree
tests =
  testGroup
    "Chemistry"
    [ Spicy.Tests.Chemistry.Atom.tests
    , Spicy.Tests.Chemistry.Molecule.tests
    ]
