module Spicy.Tests.Chemistry.Atom (tests, genAtom) where

import Data.Aeson as Aeson
import Data.Binary as Binary
import Hedgehog hiding (label)
import Hedgehog.Gen qualified as Gen
import Hedgehog.Range qualified as Range
import Spicy.Chemistry.Atom
import Spicy.Prelude hiding (label)
import Spicy.Tests.Chemistry.Element
import Spicy.Tests.Math.LinearAlgebra.R3.Generators
import Test.Tasty
import Test.Tasty.Hedgehog (testProperty)

tests :: TestTree
tests =
  testGroup
    "LewisAtom"
    [ testProperty "JSON trip" . property $ do
        atm <- forAll genAtom
        tripping atm Aeson.encode Aeson.decode
    , testProperty "Binary trip" . property $ do
        atm <- forAll genAtom
        tripping atm Binary.encode (pure @Maybe . Binary.decode)
    ]

genAtom :: Gen LewisAtom
genAtom = do
  element <- genElement
  label <- Gen.maybe $ Gen.text (Range.linear 0 20) Gen.unicode
  coord <- genR3
  chrg <- Gen.int (Range.linear (-1 * fromEnum element) (fromEnum element))
  uEl <- Gen.int (Range.linear (-10) 10)
  return LewisAtom{..}
