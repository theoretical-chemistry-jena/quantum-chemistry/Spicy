module Spicy.Tests.Chemistry.Bond where

import Hedgehog
import Hedgehog.Gen qualified as Gen
import Hedgehog.Range qualified as Range
import Spicy.Chemistry.Bond
import Spicy.Prelude

genBond :: Gen LewisBondOrder
genBond = do
  bondOrder <- fromIntegral <$> Gen.int (Range.linear 1 4)
  bondType <- Gen.element [CovalentBond, const CoordinativeBond]
  pure $ bondType bondOrder
