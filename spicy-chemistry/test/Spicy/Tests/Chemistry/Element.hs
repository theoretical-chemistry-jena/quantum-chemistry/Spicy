module Spicy.Tests.Chemistry.Element where

import Hedgehog
import Hedgehog.Gen qualified as Gen
import Spicy.Chemistry.Element

genElement :: Gen Element
genElement = Gen.enum H Rn
