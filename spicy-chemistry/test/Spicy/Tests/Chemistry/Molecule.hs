{-# LANGUAGE AllowAmbiguousTypes #-}

module Spicy.Tests.Chemistry.Molecule (tests) where

import Data.Aeson as Aeson
import Data.Binary as Binary
import Hedgehog
import RIO.List (delete)
import Spicy.Chemistry
import Spicy.Chemistry.Molecule.Internal
import Spicy.Graph
import Spicy.Math.LinearAlgebra.R3
import Spicy.Prelude
import Spicy.Tests.Chemistry.Generators
import Test.Tasty
import Test.Tasty.HUnit
import Test.Tasty.Hedgehog

tests :: TestTree
tests =
  testGroup
    "Molecule"
    [ testGroup
        "MolGr"
        [ testGroup
            "mkMolecule"
            [ testCase "Restores undirected bonds (Ethanal)"
                $ let e :: MolGr LewisAtom LewisBondOrder = getMol ethanal
                      atms = getAtoms e
                      bnds = filter (\(o, t, _) -> o < t) $ getBonds e
                      res :: Maybe (MolGr LewisAtom LewisBondOrder) = mkMolecule atms bnds
                   in res @?= pure e
            , testCase "Removes self-loops (Ethanal)"
                $ let e :: MolGr LewisAtom LewisBondOrder = getMol ethanal
                      atms = getAtoms e
                      bnds = (1, 1, CovalentBond 2) : getBonds e
                      res :: Maybe (MolGr LewisAtom LewisBondOrder) = mkMolecule atms bnds
                   in res @?= pure e
            ]
        , testGroup
            "checkMolecule"
            [ toCheckTest @MolGr h2
            , toCheckTest @MolGr twoH2
            , toCheckTest @MolGr ethanal
            , toCheckTest @MolGr nPentane
            , testCase "Reject dangling bond (Ethanal)"
                $ let e :: (MolGr LewisAtom LewisBondOrder) = getMol ethanal
                      res =
                        mkMolecule @MolGr
                          (getAtoms e)
                          ((1, 10, CovalentBond 2) : getBonds e)
                   in res @?= Nothing
            , testCase "Reject ambigous bonds (Ethanal)"
                $ let e :: MolGr LewisAtom LewisBondOrder = getMol ethanal
                      res =
                        mkMolecule @MolGr
                          (getAtoms e)
                          ((1, 2, CovalentBond 1) : delete (1, 2, CovalentBond 2) (getBonds e))
                   in res @?= Nothing
            , testCase "Reject invalid charge (Ethanal)"
                $ let e :: MolGr LewisAtom LewisBondOrder =
                        modifyAtoms'
                          (getMol ethanal)
                          [(2, Just $ LewisAtom O (Just "O2") (mkR3T (0, 1, 1)) 1 0)]
                      res = mkMolecule @MolGr (getAtoms e) (getBonds e)
                   in res @?= Nothing
            , testCase "Reject invalid multiplicity (Ethanal)"
                $ let e :: MolGr LewisAtom LewisBondOrder =
                        modifyAtoms'
                          (getMol ethanal)
                          [(2, Just $ LewisAtom O (Just "O2") (mkR3T (0, 1, 1)) 0 1)]
                      res = mkMolecule @MolGr (getAtoms e) (getBonds e)
                   in res @?= Nothing
            , testCase "Reject too many electrons (H2)"
                $ let h2' :: MolGr LewisAtom LewisBondOrder =
                        modifyAtoms'
                          (getMol h2)
                          [ (1, Just $ LewisAtom H Nothing (mkR3T (0, 0, 0)) 0 2)
                          , (2, Just $ LewisAtom H Nothing (mkR3T (0, 0, 0)) 0 (-2))
                          ]
                   in checkMolecule h2' @?= Nothing
            ]
        , -- , testGroup
          --    "updMolecule"
          --    []
          testGroup
            "modifyAtom"
            [ testCase "Dangling bonds Removed" $ do
                let bnds =
                      getBonds
                        . flip modifyAtom' (2, Nothing)
                        $ getMol h2 @MolGr
                bnds @?= []
            , testCase "Bond inheritance" $ do
                let bnds1 = getBonds (getMol h2 @MolGr)
                    chlorine = LewisAtom Cl Nothing (mkR3T (1, 0, 0)) 0 0
                    bnds2 =
                      getBonds
                        . flip modifyAtom' (2, Just chlorine)
                        $ getMol h2 @MolGr
                bnds2 @?= bnds1
            , testCase "Sanity check catches inconsistency" $ do
                let wrongHydrogen = LewisAtom H Nothing (mkR3T (1, 0, 0)) 100 0
                    mh2 = modifyAtom (getMol h2 @MolGr) (2, Just wrongHydrogen)
                mh2 @?= Nothing
            , testCase "Unaffected atoms remain unchanged" $ do
                let e1 = getMol ethanal @MolGr
                    chlorine = LewisAtom Cl Nothing (mkR3T (1, 0, 0)) 0 0
                    e2 = modifyAtom' e1 (1, Just chlorine)
                    (_, e1') = match 1 (molGr e1)
                    (_, e2') = match 1 (molGr e2)
                e1' @?= e2'
            ]
        , testGroup
            "modifyBond"
            [ testCase "Catch looping bonds" $ do
                let h21 = getMol h2 @MolGr
                    h22 = modifyBond h21 (1, 1, Just (CovalentBond 1))
                h22 @?= h21
            , testCase "Catch dangling bonds" $ do
                let h21 = getMol h2 @MolGr
                    h22 = modifyBond h21 (1, 10, Just (CovalentBond 1))
                h22 @?= h21
            , testCase "Catch redundant bond" $ do
                let h21 = getMol h2 @MolGr
                    h22 = modifyBond h21 (1, 2, Just (CovalentBond 1))
                h22 @?= h21
            , testCase "Added bonds are bidirectorial" $ do
                let h21 = getMol h2 @MolGr
                    h22 = modifyBond h21 (1, 2, Just (CovalentBond 2))
                getBonds h22 @?= [(1, 2, CovalentBond 2), (2, 1, CovalentBond 2)]
            ]
        , testGroup
            "Serialization"
            [ testProperty "JSON trip" . property $ do
                mol <- forAll genMolGr
                tripping mol toJSON fromJSON
            , testProperty "Binary trip" . property $ do
                mol <- forAll genMolGr
                tripping mol Binary.encode (pure @Maybe . Binary.decode)
            ]
        ]
    ]

toCheckTest ::
  forall mol.
  ( MkMolecule mol LewisAtom LewisBondOrder
  , DynTopoMolecule mol LewisAtom LewisBondOrder
  ) =>
  TestMolecule ->
  TestTree
toCheckTest TestMolecule{..} =
  testCase name
    . assertBool "Molecule was built incorrectly!"
    . isJust
    . checkMolecule
    $ (getMol @mol)
