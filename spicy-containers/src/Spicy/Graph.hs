{- |
Module      : Spicy.Graph
Description : Working with graphs
Copyright   : Phillip Seeber, Sebastian Seidenath 2024
License     : AGPL-3
Maintainer  : phillip.seeber@uni-jena.de
Stability   : experimental
Portability : POSIX, Windows

This module reexports 'Data.Graph.Inductive' and adds a set of useful functions on top.
It should always be imported instead of 'Data.Graph.Inductive'.
-}
module Spicy.Graph (
  module Data.Graph.Inductive,
  findEdges,
  neighbours,
  neighbours',
  updateNodes,
  updateEdges,
  egoGraph,
  dftWithin,
  paths,
  cyclesWithin,
)
where

import Data.Aeson
import Data.Graph.Inductive
import Data.Tree
import RIO.List qualified as List
import RIO.Set qualified as Set
import Spicy.Prelude hiding (Path, (&))

instance (Binary a, Binary b) => Binary (Gr a b)

instance (ToJSON a, ToJSON b) => ToJSON (Gr a b) where
  toJSON gr = toJSON $ object ["nodes" .= labNodes gr, "edges" .= labEdges gr]

instance (FromJSON a, FromJSON b) => FromJSON (Gr a b) where
  parseJSON (Object o) = mkGraph <$> o .: "nodes" <*> o .: "edges"
  parseJSON o = fail $ "Can not parse " <> show o <> " as Graph"

{- | Find all edges in a graph that fulfill a predicate. Returns those edges and
 the graph without them.

 > 'Data.Graph.Inductive.Basic.efilter' p gr = snd $ findEdges p gr
-}
findEdges ::
  forall gr a b.
  (DynGraph gr) =>
  (LEdge b -> Bool) ->
  gr a b ->
  ([LEdge b], gr a b)
findEdges f gr0 = ufold g ([], empty) gr0
 where
  g :: Context a b -> ([LEdge b], gr a b) -> ([LEdge b], gr a b)
  g (i, n, x, o) = bimap (removedEdges <>) ((iKept, n, x, oKept) &)
   where
    (iRemove, iKept) = List.partition (\(l, s) -> f (s, n, l)) i
    (oRemove, oKept) = List.partition (\(l, e) -> f (n, e, l)) o
    removedIncoming = (\(l, s) -> (s, n, l)) <$> iRemove
    removedOutgoing = (\(l, e) -> (n, e, l)) <$> oRemove
    removedEdges = removedIncoming <> removedOutgoing

neighbours :: (Graph gr) => gr a b -> Node -> [Node]
neighbours = neighbors

neighbours' :: Context a b -> [Node]
neighbours' = neighbors'

{- | Add, replace or delete a set of 'Node's in a 'Graph'. Order of the
'Foldable' will be respected, e.g. a 'Node' can be first added and then
deleted.

  * A 'Nothing' value will delete the 'Node' if it exists.
  * A 'Just' will replace an existing 'Node' and keep its 'Edge's unchanged.
  * A 'Just' will add a non-existing 'Node'.
-}
updateNodes :: (DynGraph gr, Foldable f) => f (LNode (Maybe a)) -> gr a b -> gr a b
updateNodes n g =
  -- The deconstruction -> construction step is introduced due to buggy
  -- behaviour of `insEdges`
  let gr' = foldl' upd g n
   in mkGraph (labNodes gr') (labEdges gr')
 where
  upd gr (nodeId, Nothing) = delNode nodeId gr
  -- inserting an existing node will delete its outward edges, but not its inward edges
  upd gr (nodeId, Just l) =
    let origEdges = out gr nodeId
     in insEdges origEdges . insNode (nodeId, l) $ gr

{- | Add, replace or delete a set of 'Edge's in a 'Graph'. Order of the
'Foldable' will be respected, e.g. a edge can be first added and then
deleted.

  * A 'Nothing' value will delete the 'Edge' if it exists.
  * A 'Just' will replace an existing 'Edge' and an keep its label unchanged.
  * A 'Just' will add a non-existing 'Edge' if the 'Node's exist.
-}
updateEdges :: (DynGraph gr, Foldable f) => f (LEdge (Maybe b)) -> gr a b -> gr a b
updateEdges e g = foldl' upd g e
 where
  upd gr (o, t, Nothing) = delEdge (o, t) gr
  upd gr (o, t, Just l) =
    if hasEdge gr (o, t)
      then insEdge (o, t, l) . delEdge (o, t) $ gr
      else insEdge (o, t, l) gr

-- | Make an ego-graph around a given node using a lazy breadth first search.
egoGraph :: (DynGraph gr) => Natural -> gr a b -> Node -> gr a b
egoGraph r g n =
  let egoNodes =
        fmap fst . List.takeWhile (\(_, d) -> d <= fromIntegral r) $ level n g
   in subgraph egoNodes g

{- | A special depth first walk of the graph starting from a given node.
It will follow outward 'Edge's but only if they are not leading back to
the previous 'Node'. It will take up to a specified number of steps
and even revisit nodes, e.g. if a cycles is encountered within the given
number of steps.
-}
dftWithin :: (Graph gr) => Natural -> gr a b -> Node -> Tree Node
dftWithin dist gr n = go 0 n n
 where
  go :: Natural -> Node -> Node -> Tree Node
  go currDist prevNode currNode =
    Node
      { rootLabel = currNode
      , subForest =
          let currNodeSuccessors =
                filter (\nd -> nd /= prevNode && nd /= currNode)
                  . suc gr
                  $ currNode
           in if currDist >= dist
                then mempty
                else fmap (go (currDist + 1) currNode) currNodeSuccessors
      }

{- | Construct all possible paths from the root of a tree to its terminal
leaves. All paths in a tree from the root to all of its terminal leaves.
-}
paths :: Tree Node -> [Path]
paths (Node a []) = [[a]]
paths (Node a trs) = (a :) <$> concatMap paths trs

{- | Obtain cyclic paths, that start at a given node and reach it with at
most \(d > 2\) steps. Obtained is a set of paths that contains the cycles
including start == end node twice. E.g. the path @[1,2,3,4,5,6,1]@ is cyclic and
represents a 6-membered ring.
-}
cyclesWithin :: (Graph gr) => Natural -> gr a b -> Node -> Set Path
cyclesWithin r gr n =
  let allPaths = paths $ dftWithin r gr n
      cyclicPaths = filter (elem n) . fmap (List.drop 1) $ allPaths
      cyclePaths = Set.fromList . fmap (n :) $ cyclicPaths
   in cyclePaths
