{- |
Module      : Spicy.HashMap
Description : Working with hashmaps
Copyright   : Phillip Seeber, Sebastian Seidenath 2024
License     : AGPL-3
Maintainer  : phillip.seeber@uni-jena.de
Stability   : experimental
Portability : POSIX, Windows

This module reexports 'RIO.HashMap' and adds a set of useful functions on top.
It should always be imported instead of 'RIO.HashMap' or 'Data.HashMap'.

Unfortunally, the nice @fromSet@ function from Data.Map shall be forever denied from HashMaps :(
-}
module Spicy.HashMap (
  HashMapException (..),
  module RIO.HashMap,
  lookup,
  (!?),
  unionsWith,
  invert,
)
where

import Data.Foldable qualified as Foldable
import Data.Tuple (swap)
import GHC.Stack (callStack, prettyCallStack)
import RIO.HashMap hiding (lookup)
import RIO.HashMap qualified as HashMap
import RIO.HashSet qualified as HashSet
import Spicy.Prelude hiding (lookup)

data HashMapException where
  HashMapKeyNotFound :: forall k. (HasCallStack, Show k) => k -> HashMapException

instance Show HashMapException where
  show (HashMapKeyNotFound k) =
    "HashMapKeyNotFound: "
      <> show k
      <> "\n"
      <> prettyCallStack callStack

instance Exception HashMapException

-- | `HashMap.lookup` generalised to 'MonadThrow'.
lookup :: (MonadThrow m, Show k, Hashable k) => k -> HashMap k a -> m a
lookup k m = maybe2MThrow (HashMapKeyNotFound k) $ HashMap.lookup k m

-- | Operator version of 'lookup'.
(!?) :: (MonadThrow m, Show k, Hashable k) => HashMap k a -> k -> m a
(!?) = flip lookup

unionsWith :: (Foldable f, Hashable k) => (v -> v -> v) -> f (HashMap k v) -> HashMap k v
unionsWith f ts = Foldable.foldl' (HashMap.unionWith f) HashMap.empty ts

-- | Invert the keys and values of a 'HashMap' and collect the values with the same key in a list.
invert :: (Hashable v) => HashMap k v -> HashMap v [k]
invert =
  HashMap.fromListWith (<>)
    . fmap (swap . first (pure @[]))
    . HashMap.toList
