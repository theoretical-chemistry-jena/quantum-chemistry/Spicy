{- |
Module      : Spicy.IntMap
Description : Working with integer maps
Copyright   : Phillip Seeber, Sebastian Seidenath 2024
License     : AGPL-3
Maintainer  : phillip.seeber@uni-jena.de
Stability   : experimental
Portability : POSIX, Windows

This module reexports 'Data.IntMap' and adds a set of useful functions on top.
It should always be imported instead of 'Data.IntMap'.
-}
module Spicy.IntMap (
  module Data.IntMap,
  IntMapException (..),
  lookup,
  (!?),
  mapMaybeKeys,
  mapMaybeKeysWith,
  invertInt,
  invertInts,
)
where

import Data.IntMap hiding (lookup, (!?))
import Data.IntMap qualified as IntMap
import Data.IntSet qualified as IntSet
import GHC.Stack (callStack, prettyCallStack)
import Spicy.Prelude hiding (lookup, mapMaybe)

data IntMapException where
  IntMapKeyNotFound :: forall k. (HasCallStack) => Int -> IntMapException

instance Show IntMapException where
  show (IntMapKeyNotFound k) =
    "IntMapException: "
      <> show k
      <> "\n"
      <> prettyCallStack callStack

instance Exception IntMapException

-- | `IntMap.lookup` generalised to 'MonadThrow'.
lookup :: (MonadThrow m) => Int -> IntMap a -> m a
lookup k m = maybe2MThrow (IntMapKeyNotFound k) $ IntMap.lookup k m

-- | Operator version of 'lookup'.
(!?) :: (MonadThrow m) => IntMap a -> Int -> m a
(!?) = flip lookup

{- | 'IntMap.mapKeys' extended with the functionality of 'IntMap.mapMaybe'.
Maps keys to new ones and collect the 'Just' results.
If two or more keys are mapped to the same new key, the value at the greatest of the original keys is retained.
This is consistent with 'IntMap.mapKeys'.
-}
mapMaybeKeys :: (Key -> Maybe Key) -> IntMap v -> IntMap v
mapMaybeKeys f = IntMap.fromList . catMaybes . IntMap.foldrWithKey (\k x xs -> ((,x) <$> f k) : xs) []

{- | 'IntMap.mapKeysWith' extended with the functionality of 'IntMap.mapMaybe'.
Maps keys to new ones and collect the 'Just' results.
If two or more keys map to the same new key, the values associated with these keys will be combined with the provided function.
-}
mapMaybeKeysWith :: (v -> v -> v) -> (Key -> Maybe Key) -> IntMap v -> IntMap v
mapMaybeKeysWith c f = IntMap.fromListWith c . catMaybes . IntMap.foldrWithKey (\k x xs -> ((,x) <$> f k) : xs) []

{- | Invert an 'IntMap' 'Int', so that the old keys become values and vice versa.
If multiple key point to the same value, the value at the highest original key is retained.

> let imap = IntMap.fromList [(1,2),(2,2),(3,4)]
> invertInt imap == fromList [(2,2),(4,3)]
-}
invertInt :: IntMap Int -> IntMap Int
invertInt = IntMap.fromList . IntMap.foldrWithKey (\k x xs -> (x, k) : xs) []

{- | Invert an 'IntMap' 'IntSet', so that the values found in the original 'IntSet's become the keys in the new map.
If the same values are found at different keys, these keys appear together in the 'IntSet's in the result map.

> let imap = IntMap.fromList [(1, IntSet.fromList [3,4]),(2, IntSet.fromList [4,5])]
> invertInts imap == IntMap.fromList [(3, IntSet.fromList [1]),(4, IntSet.fromList [1,2]),(5, IntSet.fromList [2])]
-}
invertInts :: IntMap IntSet -> IntMap IntSet
invertInts = IntMap.unionsWith IntSet.union . IntMap.mapWithKey mappify
 where
  mappify k is = IntMap.fromSet (IntSet.singleton . const k) is
