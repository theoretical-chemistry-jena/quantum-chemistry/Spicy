{- |
Module      : Spicy.IntSet
Description : Working with integer sets
Copyright   : Phillip Seeber, Sebastian Seidenath 2024
License     : AGPL-3
Maintainer  : phillip.seeber@uni-jena.de
Stability   : experimental
Portability : POSIX, Windows

This module reexports 'Data.IntSet' and adds a set of useful functions on top.
It should always be imported instead of 'Data.IntSet'.
-}
module Spicy.IntSet (
  module Data.IntSet,
  mapMaybe,
  allSubsets,
  grow,
) where

import Data.IntSet
import Data.IntSet qualified as IntSet
import Data.Maybe qualified as M (mapMaybe)
import Data.Set qualified as Set
import Spicy.Prelude hiding (mapMaybe)

{- | 'M.mapMaybe' but for 'IntSet's:
Map each value in an 'IntSet' and collect the 'Just' results.
-}
mapMaybe :: (Int -> Maybe Int) -> IntSet -> IntSet
mapMaybe f = IntSet.fromList . M.mapMaybe f . IntSet.toList

-- | Get all subsets of an IntSet. Cf. 'Set.powerSet'.
allSubsets :: IntSet -> Set IntSet
allSubsets = Set.map tither . Set.powerSet . hither
 where
  -- This looks a little ugly, but ultimately scales O(n).
  hither = Set.fromAscList . IntSet.toAscList
  tither = IntSet.fromAscList . Set.toAscList

{- | For each element in an `IntSet`, perform the growing action.
Then return an `IntSet` of all the results.
The resulting 'IntSet' will at least contain the input set.
-}
grow :: (Int -> IntSet) -> IntSet -> IntSet
grow f x = IntSet.foldl' (\acc i -> f i <> acc) x x
