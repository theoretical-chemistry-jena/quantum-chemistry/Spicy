{- |
Module      : Spicy.List
Description : Working with Lists
Copyright   : Phillip Seeber, Sebastian Seidenath 2024
License     : AGPL-3
Maintainer  : phillip.seeber@uni-jena.de
Stability   : experimental
Portability : POSIX, Windows

This module reexports 'RIO.Lists' and adds a set of useful functions on top.
It should always be imported instead of 'RIO.List'.
-}
module Spicy.List (
  module RIO.List,
  ListException (..),
  (!?),
)
where

import GHC.Stack
import RIO.List
import RIO.List.Partial ((!!))
import Spicy.Prelude

data ListException where
  ListIndexNotFound :: (HasCallStack) => Int -> ListException

instance Show ListException where
  show (ListIndexNotFound i) =
    "ListIndexNotFound: "
      <> show i
      <> " is an invalid index for given list\n"
      <> prettyCallStack callStack

instance Exception ListException

-- | Operator version of 'RIO.List.Partial.!!' that throws an exception on failure.
(!?) :: (MonadThrow m) => [a] -> Int -> m a
l !? i
  | i < 0 || i >= len = throwM $ ListIndexNotFound i
  | otherwise = pure $ l !! i
 where
  len = length l
