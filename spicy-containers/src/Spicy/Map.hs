{- |
Module      : Spicy.Map
Description : Working with Maps
Copyright   : Phillip Seeber, Sebastian Seidenath 2024
License     : AGPL-3
Maintainer  : phillip.seeber@uni-jena.de
Stability   : experimental
Portability : POSIX, Windows

This module reexports 'RIO.Map' and adds a set of useful functions on top.
It should always be imported instead of 'RIO.Map' or 'Data.Map'.
-}
module Spicy.Map (
  MapException (..),
  module RIO.Map,
  lookup,
  (!?),
  invert,
)
where

import Data.Tuple (swap)
import GHC.Stack (callStack, prettyCallStack)
import RIO.Map hiding (lookup, (!?))
import RIO.Map qualified as Map
import Spicy.Prelude hiding (lookup)

data MapException where
  MapKeyNotFound :: forall k. (HasCallStack, Show k) => k -> MapException

instance Show MapException where
  show (MapKeyNotFound k) =
    "MapKeyNotFound: "
      <> show k
      <> "\n"
      <> prettyCallStack callStack

instance Exception MapException

-- | `Map.lookup` generalised to 'MonadThrow'.
lookup :: (MonadThrow m, Show k, Ord k) => k -> Map k a -> m a
lookup k m = maybe2MThrow (MapKeyNotFound k) $ Map.lookup k m

-- | Operator version of 'lookup'.
(!?) :: (MonadThrow m, Show k, Ord k) => Map k a -> k -> m a
(!?) = flip lookup

-- | Invert the keys and values of a 'Map' and collect the values with the same key in a list.
invert :: (Ord v) => Map k v -> Map v [k]
invert =
  Map.fromListWith (<>)
    . fmap (swap . first (pure @[]))
    . Map.toList
