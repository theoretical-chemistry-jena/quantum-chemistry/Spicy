{-# OPTIONS_GHC -Wno-orphans #-}

module Spicy.Massiv (
  -- * Finite Precision Wrappers
  ArrayFP (..),
  ArrayExpFP (..),

  -- * Sparse-Dense Conversion
  s2dMapping,
  d2sMapping,
  s2d,
  s2d',
  d2s,
  d2s',

  -- * Vector Operations
  intercalVec,
)
where

import Data.Aeson hiding (Array)
import Data.Binary
import Data.Binary.Get qualified as BGet
import Data.ByteString.Lazy qualified as BL
import Data.Massiv.Array as Massiv hiding (fromList, toList, zip)
import Data.Massiv.Array qualified as Massiv
import GHC.Exception (prettyCallStack)
import GHC.Stack (callStack)
import GHC.TypeNats
import RIO.HashMap qualified as HashMap
import RIO.Partial (fromJust)
import Spicy.FiniteCompare
import Spicy.Prelude hiding (Index)

instance (Source r e, ToJSON e) => ToJSON (Vector r e) where
  toJSON v = toJSON . Massiv.toList $ v

instance (Manifest r e, FromJSON e) => FromJSON (Vector r e) where
  parseJSON o = Massiv.fromList Par <$> parseJSON o

instance (Source r e, ToJSON e) => ToJSON (Matrix r e) where
  toJSON v = toJSON . Massiv.toLists2 $ v

instance (FromJSON e, Manifest r e) => FromJSON (Matrix r e) where
  parseJSON o = do
    ll <- parseJSON @[[e]] o
    case Massiv.fromListsM Par ll of
      Left e -> fail . show $ e
      Right m -> return m

instance (BinaryLE e, Stream r Ix1 e, Manifest r e) => Binary (Vector r e) where
  put arr = do
    putWord64le . fromIntegral . elemsCount $ arr
    smapM_ putLE arr
  get = do
    l <- fromIntegral <$> getWord64le
    dat <- getLazyByteString $ sz * l
    return . computeP @r . runGet getsLE $ dat
   where
    sz = BL.length . runPut $ putDoublele 1
    getsLE = do
      e <- BGet.isEmpty
      if e
        then return mempty
        else do
          v <- getLE
          vs <- getsLE
          return $ v `Massiv.cons` vs

instance (BinaryLE e, Stream r Ix2 e, Manifest r e) => Binary (Matrix r e) where
  put arr = do
    putWord64le . fromIntegral $ r
    putWord64le . fromIntegral $ c
    smapM_ putLE arr
   where
    Sz (r :. c) = Massiv.size arr
  get = do
    r <- getWord64le
    c <- getWord64le
    dat <- getLazyByteString $ sz * fromIntegral r * fromIntegral c
    return
      . resize' (Sz $ fromIntegral r :. fromIntegral c)
      . computeP @r
      . runGet gets
      $ dat
   where
    sz = BL.length . runPut $ putDoublele 1
    gets = do
      e <- BGet.isEmpty
      if e
        then return mempty
        else do
          v <- getLE
          vs <- gets
          return $ v `Massiv.cons` vs

-- | Arrays with a finite precision
newtype ArrayFP (n :: Nat) arr = ArrayFP arr deriving (Generic)

instance (RealFloat e, Source r e, Index ix, KnownNat n) => Eq (ArrayFP n (Array r ix e)) where
  ArrayFP a == ArrayFP b = a' == b'
   where
    a' = Massiv.map (RealFP @n) a
    b' = Massiv.map (RealFP @n) b

newtype ArrayExpFP (n :: Nat) arr = ArrayExpFP arr deriving (Generic)

instance (RealFloat e, Source r e, Index ix, KnownNat n) => Eq (ArrayExpFP n (Array r ix e)) where
  ArrayExpFP a == ArrayExpFP b = a' == b'
   where
    a' = Massiv.map (RealExpFP @n) a
    b' = Massiv.map (RealExpFP @n) b

--------------------------------------------------------------------------------

data IndexTransformationException where
  IndexTransformationException :: (HasCallStack) => String -> IndexTransformationException

instance Exception IndexTransformationException

instance Show IndexTransformationException where
  show (IndexTransformationException msg) =
    "IndexTransformationException"
      <> msg
      <> "\n"
      <> prettyCallStack callStack

-- | Obtain a conversion table from sparse to dense indices.
s2dMapping ::
  (Foldable f, Hashable i) =>
  -- | All sparse indices
  f i ->
  -- | Mapping from sparse to dense indices
  HashMap i Int
s2dMapping i = HashMap.fromList $ zip (toList i) [0 ..]

{- | Obtain a vector with the sparse indices in order. The vector index is the
the lookup index, the value at the position the sparse index.
-}
d2sMapping ::
  (Foldable f, Manifest r i) =>
  -- | All sparse indices
  f i ->
  Vector r i
d2sMapping i = Massiv.fromList Par . toList $ i

{- | Transformation of a sparse index to a dense index using a lookup table as
obtained by 's2dMapping'.
-}
s2d :: (MonadThrow m) => HashMap Int Int -> Int -> m Int
s2d m sIx = m !?! sIx
 where
  hm !?! k = case HashMap.lookup k hm of
    Just r -> return r
    Nothing ->
      throwM
        . IndexTransformationException
        $ "Could not find index "
        <> show sIx
        <> " in transformation map"

-- | Unsafe version of 's2d'
s2d' :: HashMap Int Int -> Int -> Int
s2d' m sIx = fromJust $ HashMap.lookup sIx m

{- | Transformation of a dense index to a sparse index using a lookup table as
obtained by 'd2sMapping'.
-}
d2s :: (Manifest r Int, MonadThrow m) => Vector r Int -> Int -> m Int
d2s v dIx = v !? dIx

-- | Unsafe version of 'd2s'
d2s' :: (Manifest r Int) => Vector r Int -> Int -> Int
d2s' v dIx = v Massiv.! dIx

{- | Intercalate any number of vectors. The shortest determines the length of
the result.

>>> intercalVec (fromList Par [ fromList Par [1,2,3], fromList Par [4,5,6] ])
Array P Par (Sz1 6)
  [ 1, 4, 2, 5, 3, 6 ]
-}
intercalVec ::
  ( Manifest r1 e
  , Load r2 Ix1 e
  , Manifest r0 (Vector r1 e)
  ) =>
  Vector r0 (Vector r1 e) ->
  Vector r2 e
intercalVec vecs
  | nVecs == 0 = Massiv.empty
  | otherwise = makeArray Par (Sz $ minSize * nVecs) $ \i ->
      let outerIx = i `mod` nVecs
          innerIx = i `div` nVecs
       in vecs ! outerIx ! innerIx
 where
  nVecs = elemsCount vecs
  sizes = Massiv.map elemsCount vecs
  minSize = minimum' sizes
