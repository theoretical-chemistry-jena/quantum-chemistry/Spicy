module Spicy.Tests.IntSet.Generators (
  genIntSet,
  subIntSet,
) where

import Hedgehog
import Hedgehog.Gen qualified as Gen
import Spicy.IntSet qualified as IntSet
import Spicy.Prelude

-- | Generator for `IntSet`s.
genIntSet :: Range Int -> Gen Int -> Gen IntSet
genIntSet r g = IntSet.fromList <$> Gen.list r g

-- | Generator for subsets of `IntSet`s.
subIntSet :: IntSet -> Gen IntSet
subIntSet = fmap IntSet.fromList . Gen.subsequence . IntSet.toList
