module Spicy.Tests.Set.Generators (genSet) where

import Hedgehog
import Hedgehog.Gen qualified as Gen
import RIO.Set qualified as Set
import Spicy.Prelude

-- | Generator for `Set`s.
genSet :: (Ord a) => Range Int -> Gen a -> Gen (Set a)
genSet r g = Set.fromList <$> Gen.list r g
