module Spicy.Tests.Graph (tests) where

import Data.Tree
import RIO.Set qualified as Set
import Spicy.Graph
import Spicy.Prelude
import Test.Tasty
import Test.Tasty.HUnit

tests :: TestTree
tests =
  testGroup
    "Graph"
    [ testCase "updateNodes"
        $ let updN = [(2, Just 0), (11, Just 11), (1, Nothing)]
              expc =
                let n = [(2, 0), (3, 3), (4, 4), (5, 5), (6, 6), (7, 7), (8, 8), (9, 9), (10, 10), (11, 11)]
                    e =
                      [ (2, 3, 1)
                      , (3, 4, 1)
                      , (4, 5, 1)
                      , (5, 6, 1)
                      , (6, 7, 2)
                      , (6, 8, 2)
                      , (5, 9, 1)
                      , (9, 10, 1)
                      , (10, 2, 1)
                      ]
                 in mkGraph n e :: Gr Int Int
           in updateNodes updN testGr @?= expc
    , testCase "updateEdges"
        $ let updE = [(2, 3, Nothing), (1, 2, Just 1), (2, 1, Just 2)]
              expc =
                let n = [(1, 1), (2, 2), (3, 3), (4, 4), (5, 5), (6, 6), (7, 7), (8, 8), (9, 9), (10, 10)]
                    e =
                      [ (1, 2, 1)
                      , (2, 1, 2)
                      , (3, 4, 1)
                      , (4, 5, 1)
                      , (5, 6, 1)
                      , (6, 7, 2)
                      , (6, 8, 2)
                      , (5, 9, 1)
                      , (9, 10, 1)
                      , (10, 2, 1)
                      ]
                 in mkGraph n e
           in updateEdges updE testGr @?= expc
    , testCase "egoGraph"
        $ let expc =
                let n = [(6, 6), (7, 7), (8, 8)]
                    e = [(6, 7, 2), (6, 8, 2)]
                 in mkGraph n e
           in egoGraph 1 testGr 6 @?= expc
    , testCase "dftWithin"
        $ let expc = Node 5 [Node 6 [Node 7 [], Node 8 []], Node 9 [Node 10 []]]
           in dftWithin 2 testGr 5 @?= expc
    , testCase "cyclesWithin"
        $ let expc = Set.singleton [2, 3, 4, 5, 9, 10, 2]
           in cyclesWithin 6 testGr 2 @?= expc
    ]

-- | undirected test graph
testGr :: Gr Int Int
testGr =
  let n = [(1, 1), (2, 2), (3, 3), (4, 4), (5, 5), (6, 6), (7, 7), (8, 8), (9, 9), (10, 10)]
      e =
        [ (1, 2, 2)
        , (2, 3, 1)
        , (3, 4, 1)
        , (4, 5, 1)
        , (5, 6, 1)
        , (6, 7, 2)
        , (6, 8, 2)
        , (5, 9, 1)
        , (9, 10, 1)
        , (10, 2, 1)
        ]
   in mkGraph n e
