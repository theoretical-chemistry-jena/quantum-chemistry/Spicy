module Spicy.Tests.Massiv (tests) where

import Data.Aeson as Aeson
import Data.Binary as Binary
import Data.Massiv.Array as Massiv
import Hedgehog
import Spicy.Massiv ()
import Spicy.Prelude
import Spicy.Tests.Prelude.Generators
import Test.Tasty
import Test.Tasty.Hedgehog

tests :: TestTree
tests =
  testGroup
    "Massiv"
    [ testProperty "Vector JSON trip" . property $ do
        vI <- Massiv.fromList @S @Int Seq <$> forAll genVecLint
        vD <- Massiv.fromList @S @Double Seq <$> forAll genVecL
        tripping vI Aeson.encode Aeson.decode
        tripping vD Aeson.encode Aeson.decode
    , testProperty "Matrix JSON trip" . property $ do
        mI <- Massiv.fromLists' @S @Ix2 @Int Seq <$> forAll genMatLint
        mD <- Massiv.fromLists' @S @Ix2 @Double Seq <$> forAll genMatL
        tripping mI Aeson.encode Aeson.decode
        tripping mD Aeson.encode Aeson.decode
    , testProperty "Vector Binary trip" . property $ do
        vD <- Massiv.fromList @S @Double Seq <$> forAll genVecL
        tripping vD Binary.encode (pure @Maybe . Binary.decode)
    , testProperty "Matrix Binary trip" . property $ do
        vD <- Massiv.fromLists' @S @Ix2 @Double Seq <$> forAll genMatL
        tripping vD Binary.encode (pure @Maybe . Binary.decode)
    ]
