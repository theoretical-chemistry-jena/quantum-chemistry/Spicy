module Main where

import Spicy.Prelude
import Spicy.Tests.Graph qualified
import Spicy.Tests.Massiv qualified
import Test.Tasty

main :: IO ()
main = defaultMain tests

tests :: TestTree
tests =
  testGroup
    "Spicy"
    [ Spicy.Tests.Graph.tests
    , Spicy.Tests.Massiv.tests
    ]
