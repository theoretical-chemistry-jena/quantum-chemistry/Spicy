{-# LANGUAGE AllowAmbiguousTypes #-}

{- |
Module      : Spicy.FileFormats.Common
Description : Common interactions with data from chemical file formats
Copyright   : Phillip Seeber, Sebastian Seidenath 2024
License     : AGPL-3
Maintainer  : phillip.seeber@uni-jena.de
Stability   : experimental
Portability : POSIX, Windows
-}
module Spicy.FileFormats.Common (
  -- * Particles
  Particle (..),
  ParticleClassification (..),
  ParticleDataAtom (..),
  ParticleDataMacro (..),
  ParticleDataPhysical (..),

  -- * Molecules
  AtomDataFormat (..),
  ParticlesFormat (..),
  BondDataFormat (..),
  toCoords,
  toMoleculeWith,
  toMolecule,
  FromMolecule (..),
) where

import Data.Default
import Data.Massiv.Array hiding (zipWith)
import RIO.List qualified as List
import RIO.Partial (fromJust)
import Spicy.Chemistry.Atom hiding (LewisAtom (..))
import Spicy.Chemistry.Element
import Spicy.Chemistry.Molecule
import Spicy.Graph
import Spicy.Math.LinearAlgebra.R3 as R3
import Spicy.Prelude hiding (label)

{- | Files may contain other particles besides 'Atom's and additional
information besides what 'Atom' provides. The 'Particle' type provides these
details and can be extended as needed.

This type is meant to be used for two purposes:

  * Convert a file-format specific particle to a more general type without
    exposing the format's potentially unsafe constructors. Thus, you can query
    particle information from a file format from the API without the lossy
    conversion to 'Atom's.
  * Test cases of file formats in the test suite.

The type is not meant to be used to represent the particles in an actual file
format. Instead, a file format should provide more specific constructors.
-}
data Particle = Particle
  { classification :: ParticleClassification
  -- ^ Type of the particle
  , dataAtom :: ParticleDataAtom
  -- ^ Atomic data
  , dataMacro :: ParticleDataMacro
  -- ^ Structural data from macromolecules, e.g. residues, chains, ...
  , dataPhysical :: ParticleDataPhysical
  }
  deriving (Eq, Show, Generic)

instance Default Particle

-- | Classification of a particle
data ParticleClassification = ParticleClassification
  { isAtom :: Ternary
  -- ^ Whether the particle is an 'Atom'
  , isGhost :: Ternary
  -- ^ Whether the particle is a ghost atom (e.g. no charge but basis functions)
  , isDummy :: Ternary
  -- ^ Whether the particle is a dummy atom (e.g. only used for structural properties)
  , isLonepair :: Ternary
  -- ^ Whether the particle represents an electron lone pair
  }
  deriving (Eq, Show, Generic)

instance Default ParticleClassification where
  def =
    ParticleClassification
      { isAtom = Possible
      , isGhost = Possible
      , isDummy = Possible
      , isLonepair = Possible
      }

-- | Atomic data of a particle, e.g. replicating information from 'Atom'.
data ParticleDataAtom = ParticleDataAtom
  { element :: Maybe Element
  -- ^ See 'Atom.element'
  , label :: Maybe Text
  -- ^ See 'Atom.label'
  , coord :: R3 S Double
  -- ^ Coordinates of the particle in 'Spicy.Metrology.Bohr'
  , chrg :: Maybe Int
  -- ^ Formal charge (from a Lewis-like structure) of the particle
  , uEl :: Maybe Int
  -- ^ Formal number of unpaired electrons (from a Lewis-like structure) of the
  -- particle. Positive and negative numbers distinguish \(\alpha\) and
  -- \(\beta\) spin channels.
  }
  deriving (Eq, Show, Generic)

instance Default ParticleDataAtom where
  def =
    ParticleDataAtom
      { element = Nothing
      , label = Nothing
      , coord = mkR3T (0, 0, 0)
      , chrg = Nothing
      , uEl = Nothing
      }

-- | Macromolecular data, e.g. chains, residue names, ...
data ParticleDataMacro = ParticleDataMacro
  { chain :: Maybe Char
  -- ^ E.g. for PDB, chain of the structure the particle belongs to.
  , residueId :: Maybe Int
  -- ^ E.g. for PDB or MOL2, the number of the residue the particle belongs to.
  , residueName :: Maybe Text
  -- ^ E.g. for PDB or Mol2, the name of the residue, most often three letter
  -- code of the amino acid or a ligand.
  }
  deriving (Eq, Show, Generic)

instance Default ParticleDataMacro where
  def =
    ParticleDataMacro
      { chain = Nothing
      , residueId = Nothing
      , residueName = Nothing
      }

-- | Physical data of a 'Particle'.
data ParticleDataPhysical = ParticleDataPhysical
  { partialCharge :: Maybe Double
  -- ^ A partial charge of a particle
  }
  deriving (Eq, Show, Generic)

instance Default ParticleDataPhysical where
  def =
    ParticleDataPhysical
      { partialCharge = Nothing
      }

-- | Formats that provide basic atomic data.
class (Atom atom) => AtomDataFormat fmt atom where
  -- | Obtain 'Atom's from a file. The 'Atom's may be inconsistent, e.g. charge/
  -- number of unpaired electrons combinations, as the underlying type 'a' does
  -- not provide them. Contrary to 'toParticles', instances must ensure ensure
  -- that unique 'Node' keys are obtained so that no 'Atom's are lost upon
  -- conversion to a 'Molecule'.
  toAtoms :: fmt -> [LNode atom]

-- | Formats that provide information about particles
class ParticlesFormat fmt where
  -- | Obtain all particles from a file format. Besides 'Atom's, this may also
  -- contain pseudo particles, e.g. lone pairs or ghost atoms. For certain file
  -- formats the 'Node' may not be unique, e.g. different Particles with the
  -- same 'Node' key may actually be different particles from, e.g., different
  -- chains (as in PDB).
  toParticles :: fmt -> [LNode Particle]

class BondDataFormat fmt bond where
  -- | Obtain bonds from an underlying file. Depending on the file format the
  -- bonds may be uni- or bidirectional.
  toBonds :: fmt -> [LEdge bond]

{- | Get a full set of coordinates of the 'Atom's. An instance must ensure
that the result is suitable for use with 'updCoords'', i.e. provide the
full set of atomic (and only atomic!) coordinates in ascending order of
'LNode Atom'.
-}
toCoords ::
  forall r fmt atom.
  (Load r Ix2 Double, Manifest r Double, AtomDataFormat fmt atom) =>
  fmt ->
  Matrix r Double
toCoords a =
  compute
    . fromJust
    . stackOuterSlicesM
    . fmap (\(_, atm) -> R3.r3 . getCoord $ atm)
    . List.sortOn fst
    . toAtoms @fmt @atom
    $ a

{- | Convert 'a' to a 'Molecule'. Intermediate versions of the raw 'Atom's and
bonds may be modified, e.g. changing charges, before constructing the
result.
-}
toMoleculeWith ::
  ( MkMolecule mol atom2 bond2
  , MonadThrow m
  , Foldable f
  , AtomDataFormat p atom1
  , BondDataFormat p bond1
  ) =>
  p ->
  -- | Modify the 'Atom's before constructing the 'Molecule'
  ([LNode atom1] -> f (LNode atom2)) ->
  -- | Modify the bonds before constructing the 'Molecule'
  ([LEdge bond1] -> f (LEdge bond2)) ->
  m (mol atom2 bond2)
toMoleculeWith a modAtoms modBonds =
  mkMolecule
    (modAtoms . toAtoms $ a)
    (modBonds . toBonds $ a)

{- | Convert to 'Molecule' without changes to the 'Atom's and bonds. For many
formats this is likely to fail for some molecules, as they do not provide
sufficient electronic information.
-}
toMolecule ::
  ( MkMolecule mol atom bond
  , MonadThrow m
  , AtomDataFormat fmt atom
  , BondDataFormat fmt bond
  ) =>
  fmt ->
  m (mol atom bond)
toMolecule a = toMoleculeWith a id id

{- | File formats that can directly be converted from a 'Molecule'. The
conversion does not necessarily need to be lossless. 'toMolecule' and
'fromMolecule' should not be considered to be round-trippable, e.g.

@
toMolecule . fromMolecule /= pure
@
-}
class (Molecule mol atom bond) => FromMolecule mol atom bond a where
  -- | Convert a 'Molecule'
  fromMolecule :: mol atom bond -> a
