{- |
Module      : Spicy.FileFormats.Cube
Description : Gaussian's Cube file format
Copyright   : Phillip Seeber, Sebastian Seidenath 2024
License     : AGPL-3
Maintainer  : phillip.seeber@uni-jena.de
Stability   : experimental
Portability : POSIX, Windows
-}
module Spicy.FileFormats.Cube (
  Cube,
  getCell,
  hasIndexedData,
  hasScalarData,
  hasVectorData,
  getDataDim,
  getScalarField,
  getVectorField,
  getIndexedScalarField,
  mkScalarCube,
  mkScalarCube',
  mkVectorCube,
  mkVectorCube',
  mkIndexedScalarCube,

  -- * Hedgehog Generators
  genCube,
) where

import Data.Attoparsec.Text qualified as AP
import Data.Massiv.Array as Massiv hiding (and, convert)
import Data.Override (As, Override (..))
import Data.Text.Lazy.Builder as TB
import Formatting as F
import Hedgehog
import Hedgehog.Gen qualified as Gen
import Hedgehog.Range qualified as Range
import RIO.Partial (fromJust, toEnum)
import RIO.Text qualified as T
import RIO.Writer
import Spicy.Chemistry.Atom
import Spicy.Chemistry.Element (Element)
import Spicy.Chemistry.Molecule
import Spicy.FileFormats.Common
import Spicy.FiniteCompare
import Spicy.IntMap qualified as IntMap
import Spicy.Massiv
import Spicy.Math.LinearAlgebra.R3 as R3
import Spicy.Math.Voxel
import Spicy.Prelude hiding ((:>))
import Spicy.Tests.Math.LinearAlgebra.R3.Generators
import Spicy.Textual as TTC

-- | Exceptions on operations on cubes.
data CubeException = CubeTypeException String

instance Show CubeException where
  show (CubeTypeException msg) = "CubeTypeException: " <> msg

instance Exception CubeException

{- | The volumetric data of a cube may either contain n-dimensional data per
voxel or many sets of 1-dimensional data per voxel, e.g. multiple orbitals.
-}
data VoxelType
  = -- | n-dimensional data per voxel
    VectorData Natural
  | -- | multiple, indexed 1D data per voxel
    ManyData (Vector S Int)
  deriving (Show, Generic, Eq)

-- | Limited precision newtype wrapper around the 'Cell' type.
newtype CubeCell = CubeCell Cell
  deriving (Generic)
  deriving (Show) via Cell
  deriving
    (Eq)
    via Override
          Cell
          '[R3 S Double `As` R3FP 4 (R3 S Double)]

{- | Gaussian's Cube files. For some specifications see

  * [The Gaussian manual](https://gaussian.com/cubegen/)
  * [The h5cube documentation](https://h5cube-spec.readthedocs.io/en/latest/cubeformat.html#cubeformat-nval)
  * [VMD's cube plugin](http://www.ks.uiuc.edu/Research/vmd/plugins/molfile/cubeplugin.html)

A Cube contains volumetric data in voxels on a regular grid as well as simple atomic data.
There are three flavours the volumetric data can come in:

  * A scalar field, i.e. a single value per voxel
  * A \(\mathbb{R}^n\) vector field, i.e. a vector of \(n\) values per voxel
  * Multiple indexed scalar fields on the same grid.

All three types are supported by this implementation.
However, two exotic cases are not supported:

  * The data is given in Angstrom instead of Bohr (by specifying a negative number of points along the first cell vector)
  * The data is given on custom grid points instead of a regular grid
-}
data Cube = Cube
  { title1 :: Text
  -- ^ An arbitrary, single line of text, most often stating the programme by
  -- which the cube was created.
  , title2 :: Text
  -- ^ An arbitrary, single line of text, most often stating which property it
  -- contains.
  , cell :: CubeCell
  -- ^ Definition of the cube's cell. Note: The cube format does not give the
  -- cell vectors but the increments along the cell vectors per voxel. Parsers
  -- and writers convert these on the fly.
  , cubeAtoms :: Vector B (Int, Double, R3 S Double)
  -- ^ A cube may contain atoms with coordinates in Bohr, nuclear charge and
  -- coordinates in Bohr. The charge may diverge from the atomic number in case
  -- of ECPs.
  , voxelType :: VoxelType
  -- ^ The cube either has n-dimensional data per voxel or many sets of indexed
  -- data per voxel.
  , dat :: ~(Array S Ix4 Double)
  -- ^ 3D array of volumetric data. The first three indices correspond to
  -- (x, y, z), the latter either to the nD vector data if 'voxelType' is
  -- 'VectorData' or to indexed data if the 'voxelType' is 'ManyData'.
  }
  deriving (Show, Generic)

-- Generic Overrides fail to override the @R3 S Double@ values in the `cubeAtoms`.
instance Eq Cube where
  a == b =
    and
      [ a.title1 == b.title1
      , a.title2 == b.title2
      , a.cell == b.cell
      , fmap (^. _1) a.cubeAtoms == fmap (^. _1) b.cubeAtoms
      , fmap (RealFP @4 . (^. _2)) a.cubeAtoms == fmap (RealFP . (^. _2)) b.cubeAtoms
      , fmap (R3FP @4 . (^. _3)) a.cubeAtoms == fmap (R3FP . (^. _3)) b.cubeAtoms
      , a.voxelType == b.voxelType
      , ArrayExpFP @3 a.dat == ArrayExpFP b.dat
      ]

instance Render Cube where
  render Cube{cell = CubeCell cell, ..} =
    convert
      $ bformat
        cubeFmt
        --
        title1
        --
        title2
        --
        ((if indexedData then negate else id) nAtoms)
        (xR3 cell.origin)
        (yR3 cell.origin)
        (zR3 cell.origin)
        nV
        --
        nA
        (xR3 cellIncrA)
        (yR3 cellIncrA)
        (zR3 cellIncrA)
        --
        nB
        (xR3 cellIncrB)
        (yR3 cellIncrB)
        (zR3 cellIncrB)
        --
        nC
        (xR3 cellIncrC)
        (yR3 cellIncrC)
        (zR3 cellIncrC)
        --
        cubeAtoms
        --
        indexedDataIds
        --
        volDatBuilder
   where
    indexedData = case voxelType of
      VectorData _ -> False
      ManyData _ -> True
    Sz (nAtoms :: Int) = size cubeAtoms
    Sz (nA :> nB :> nC :. nV) = Massiv.size dat
    cellIncrA = cell.cellVecA R3..* (1 / fromIntegral nA)
    cellIncrB = cell.cellVecB R3..* (1 / fromIntegral nB)
    cellIncrC = cell.cellVecC R3..* (1 / fromIntegral nC)

    -- Cube formatter
    cubeFmt =
      cmntFmt
        F.% cmntFmt
        F.% headerFmt
        F.% cellVecFmt
        F.% cellVecFmt
        F.% cellVecFmt
        F.% concatenated atmFmt
        F.% optioned builder
        F.% builder

    -- 80 Characters formatter for comments
    cmntFmt :: Format r (Text -> r)
    cmntFmt = fitRight 80 %. stext F.% "\n"

    -- E13.5 formatter for floats
    eFmt :: Format r (Double -> r)
    eFmt = fE 13 5

    -- F12.6 formatter for floats
    fFmt :: Format r (Double -> r)
    fFmt = fF 12 6

    -- I5 formatter for integers
    iFmt :: Format r (Int -> r)
    iFmt = left 5 ' ' %. int

    -- Line 3 formatter: number of atoms, origin and dimensionality of voxel data if not 1
    headerFmt :: Format r (Int -> Double -> Double -> Double -> Int -> r)
    headerFmt = iFmt F.%+ fFmt F.%+ fFmt F.%+ fFmt F.%+ iFmt F.% "\n"

    -- Line 4-6 formatter: number of points and cell vectors
    cellVecFmt :: Format r (Int -> Double -> Double -> Double -> r)
    cellVecFmt = iFmt F.%+ fFmt F.%+ fFmt F.%+ fFmt F.% "\n"

    -- Formatter for the atoms
    atmFmt :: Format r ((Int, Double, R3 S Double) -> r)
    atmFmt = later gen
     where
      gen :: (Int, Double, R3 S Double) -> TB.Builder
      gen (atmNum, chrg, r3) = bformat origFmt atmNum chrg (xR3 r3) (yR3 r3) (zR3 r3)

      origFmt :: Format r (Int -> Double -> Double -> Double -> Double -> r)
      origFmt = (fitLeft 3 %. left 3 ' ' %. int) F.%+ fFmt F.%+ fFmt F.%+ fFmt F.%+ fFmt F.% "\n"

    -- Builder for the index data
    indexedDataIds :: Maybe TB.Builder
    indexedDataIds = case voxelType of
      VectorData _ -> Nothing
      ManyData ixV ->
        let Sz n = size ixV
         in Just $ bformat (iFmt F.% concatenated iFmt F.% "\n") n (delay ixV)

    -- Builder for the volumetric data
    volDatBuilder :: TB.Builder
    volDatBuilder = execWriter . Massiv.iforM_ dat $ \(_iA :> _iB :> iC :. iV) v -> do
      tell . bformat eFmt $ v

      -- Line break after each slice
      let eos = iC == nC - 1 && iV == nV - 1
          sixthVal = (iC * nV + iV + 1) `mod` 6 == 0
      when (eos || sixthVal) . tell $ "\n"

{- | The parser is not as strict as the Fortran parser and accepts numbers in
other formats than E13.5, F12.6 and I5.
-}
instance ParseAp Cube where
  parser = do
    -- Line 1-2 with titles
    title1 <- titleParser
    title2 <- titleParser

    -- Line 3 with number of atoms, origin and possibly dimensionality of voxel data
    nAtoms' <- lex $ signed decimal
    origin <- r3Parser
    nDim <- fromMaybe 1 <$> optional (lex decimal) <* lex endOfLine

    -- Line 4-6 with cell vectors
    [(nA, cellIncrA), (nB, cellIncrB), (nC, cellIncrC)] <- count 3 cellVecParser
    let cell =
          CubeCell
            Cell
              { origin
              , cellVecA = cellIncrA R3..* fromIntegral nA
              , cellVecB = cellIncrB R3..* fromIntegral nB
              , cellVecC = cellIncrC R3..* fromIntegral nC
              }

    -- Atom lines
    cubeAtoms <- Massiv.fromList Par <$> count (abs nAtoms') cubeAtomParser

    -- Possibly the index data
    voxelType <-
      if nAtoms' < 0
        then ixDatParser
        else return . VectorData . fromIntegral $ nDim

    dat <- datParser nA nB nC nDim
    -- dat <- datParserFast nA nB nC nDim

    skipSpace
    endOfInput

    return Cube{..}
   where
    titleParser = takeTill isEndOfLine <* endOfLine

    r3Parser = do
      x <- lex double
      y <- lex double
      z <- lex double
      return . mkR3T $ (x, y, z)

    cellVecParser = do
      n <- lex decimal
      r3 <- r3Parser <* lex endOfLine
      return (n, r3)

    cubeAtomParser = do
      atomicN <- lex decimal
      charge <- lex double
      r3 <- r3Parser <* endOfLine
      return (atomicN, charge, r3)

    ixDatParser = do
      nIx <- lex decimal <* skipSpace
      ixDat <- count nIx $ decimal <* skipSpace
      return . ManyData . Massiv.fromList Par $ ixDat

    datParser nA nB nC m = do
      abSlices <- compute @S . Massiv.fromList @S Par . concat <$> count nSlices sliceParser
      return . setComp Par . resize' (Sz $ nA :> nB :> nC :. m) $ abSlices
     where
      -- Number of ab slices
      nSlices = nA * nB

      -- Number of voxels per ab slice
      sliceSize = nC * m

      -- Number of full lines per ab slice
      fullLines = sliceSize `div` 6
      -- Number of voxels in the last, possibly incomplete line of an ab slices
      incmpltLineLength = sliceSize `mod` 6
      fullLinePrs = count 6 (lex AP.double) <* lex endOfLine
      incmpltLinePrs =
        if incmpltLineLength == 0
          then return mempty
          else count incmpltLineLength (lex AP.double) <* lex endOfLine
      sliceParser = do
        initSlices <- count fullLines fullLinePrs
        lastSlice <- incmpltLinePrs
        return . concat $ initSlices <> [lastSlice]

{- |
  * Atom numbering starts at 0
  * Atoms with atomic numbers outside the 'Bounded' range of 'Element' are silently discarded in 'toAtoms'
  * The atomic charge used in the cube is being used differently by different programmes.
    This class will round the charge to the nearest integer and subtract it from the atomic number.
    The difference will be the 'Atom.chrg'.
  * No bonds are given by 'Cube'.
  * The partial charge of a 'Particle' will be calculated as the difference between the atomic number
    and the charge.
  * In the 'toParticles' instance, the 'Particle' will be classified as an 'Atom'
    if the atomic number is within the 'Bounded' range of 'Element' and as dummy
    particle otherwise.
-}
instance (MkAtom atom) => AtomDataFormat Cube atom where
  toAtoms Cube{cubeAtoms} = stoList . flip simapMaybe cubeAtoms $ \i (z, chrgF, coord) ->
    let chrg' = z - round chrgF
        element' = toEnum $ z + 1
     in if z >= minZ && z <= maxZ
          then Just (i, mkAtom element' Nothing coord chrg' 0)
          else Nothing
   where
    maxZ = (+ 1) . fromEnum $ maxBound @Element
    minZ = (+ 1) . fromEnum $ minBound @Element

instance ParticlesFormat Cube where
  toParticles Cube{cubeAtoms} = stoList . flip simap cubeAtoms $ \i (z, c, coord) ->
    let validZ = z >= minZ && z <= maxZ
     in ( i
        , Particle
            { classification =
                ParticleClassification
                  { isAtom = if validZ then Yes else No
                  , isGhost = No
                  , isDummy = if not validZ then Yes else No
                  , isLonepair = No
                  }
            , dataAtom =
                ParticleDataAtom
                  { element = if validZ then Just (toEnum $ z + 1) else Nothing
                  , label = Nothing
                  , coord
                  , chrg = if validZ then Just (z - round c) else Nothing
                  , uEl = Nothing
                  }
            , dataMacro =
                ParticleDataMacro
                  { chain = Nothing
                  , residueId = Nothing
                  , residueName = Nothing
                  }
            , dataPhysical =
                ParticleDataPhysical
                  { partialCharge = Just $ fromIntegral z - c
                  }
            }
        )
   where
    maxZ = (+ 1) . fromEnum $ maxBound @Element
    minZ = (+ 1) . fromEnum $ minBound @Element

instance BondDataFormat Cube Void where
  toBonds _ = []

{- | The resulting cube will have no volumetric data but be labelled as
containing a scalar field. Furthermore, all vectors of a the 'Cell' will be zeroed.
-}
instance (Molecule mol atom bond) => FromMolecule mol atom bond Cube where
  fromMolecule mol =
    Cube
      { title1 = "Cube generated by Spicy"
      , title2 = "Empty volumetric data"
      , cell = CubeCell $ Cell nullR3 nullR3 nullR3 nullR3
      , voxelType = VectorData 1
      , dat = Massiv.empty
      , cubeAtoms =
          compute
            . Massiv.map atmToCubeAtom
            . Massiv.fromList @B Par
            . fmap snd
            . getAtoms
            $ mol
      }
   where
    nullR3 = mkR3T (0, 0, 0)
    atmToCubeAtom :: atom -> (Int, Double, R3 S Double)
    atmToCubeAtom atom =
      let atmZ = fromEnum (getElement atom) + 1
          chrg = getChrg atom - atmZ
          coord = getCoord atom
       in (atmZ, fromIntegral $ atmZ - chrg, coord)

genCube :: Gen Cube
genCube = do
  title1 <- Gen.text (Range.linear 0 100) Gen.alphaNum
  title2 <- Gen.text (Range.linear 0 100) Gen.alphaNum
  origin <- genR3
  cellVecA <- genR3
  cellVecB <- genR3
  cellVecC <- genR3
  let cell = CubeCell Cell{..}
  cubeAtoms <- Massiv.fromList Par <$> Gen.list (Range.linear 1 100) atmGen
  voxelType <- voxelTypeGen
  nDim <- case voxelType of
    VectorData n -> return . fromIntegral $ n
    ManyData ixL -> let Sz n = Massiv.size ixL in return n
  nA <- Gen.int (Range.linear 1 40)
  nB <- Gen.int (Range.linear 1 40)
  nC <- Gen.int (Range.linear 1 40)
  dat <-
    resize' (Sz $ nA :> nB :> nC :. nDim)
      . Massiv.fromList @S Par
      <$> Gen.list
        (Range.singleton $ nA * nB * nC * nDim)
        (Gen.double $ Range.exponentialFloat (-10e8) 10e8)

  return Cube{..}
 where
  atmGen :: Gen (Int, Double, R3 S Double)
  atmGen = do
    atmZ <- Gen.int (Range.linear 1 100)
    atmCharge <- Gen.double (Range.linearFrac (-100) 100)
    atmPos <- genR3
    return (atmZ, atmCharge, atmPos)

  voxelTypeGen :: Gen VoxelType
  voxelTypeGen = Gen.choice [vectorDataGen, manyDataGen]
   where
    vectorDataGen :: Gen VoxelType
    vectorDataGen = do
      n <- Gen.integral (Range.linear 1 10)
      return $ VectorData n

    manyDataGen :: Gen VoxelType
    manyDataGen = do
      n <- Gen.integral (Range.linear 1 10)
      return $ ManyData (Massiv.fromList Seq [1 .. n])

-- | Get the 'Cell' definition of a 'Cube'.
getCell :: Cube -> Cell
getCell Cube{cell = CubeCell cell} = cell

-- | If the cube contains indexed scalar data per voxel.
hasIndexedData :: Cube -> Bool
hasIndexedData Cube{voxelType} = case voxelType of
  VectorData _ -> False
  ManyData _ -> True

-- | If the cube contains simple scalar data per voxel.
hasScalarData :: Cube -> Bool
hasScalarData Cube{voxelType} = case voxelType of
  VectorData 1 -> True
  ManyData v -> size v == 1
  _ -> False

-- | If the cube contains vector data per voxel.
hasVectorData :: Cube -> Bool
hasVectorData Cube{voxelType} = case voxelType of
  VectorData n
    | n > 1 -> True
    | otherwise -> False
  _ -> False

-- | Get get dimensionality of the cube's volumetric data if it is a vector field.
getDataDim :: (MonadThrow m) => Cube -> m Natural
getDataDim Cube{voxelType} = case voxelType of
  VectorData n -> return n
  _ -> throwM . CubeTypeException $ "Cube does not contain a vector field"

-- | Get the scalar field of a cube
getScalarField :: (MonadThrow m) => Cube -> m (Array S Ix3 Double)
getScalarField Cube{voxelType, dat} = case voxelType of
  VectorData 1 -> return . resize' (Sz $ nA :> nB :. nC) $ dat
  VectorData _ -> throwM . CubeTypeException $ "Cube does not contain a scalar field"
  ManyData _ -> throwM . CubeTypeException $ "Cube does not contain a scalar field"
 where
  Sz (nA :> nB :> nC :. _nD) = size dat

-- | Get the vector field of a cube
getVectorField :: (MonadThrow m) => Cube -> m (Array S Ix4 Double)
getVectorField Cube{voxelType, dat} = case voxelType of
  VectorData _ -> return dat
  _ -> throwM . CubeTypeException $ "Cube does not contain a vector field"

-- | Get indexed scalar fields of a cube
getIndexedScalarField :: (MonadThrow m) => Cube -> m (IntMap (Array S Ix3 Double))
getIndexedScalarField Cube{voxelType, dat} = case voxelType of
  VectorData _ -> throwM . CubeTypeException $ "Cube does not contain indexed scalar fields"
  ManyData inds
    | nV /= elemsCount inds -> throwM $ SizeMismatchException (Sz nV) (size inds)
    | otherwise ->
        return . IntMap.fromList . stoList . Massiv.zip inds . outerSlices $ dat
 where
  Sz (_nA :> _nB :> _nC :. nV) = size dat

-- | Create a cube with a scalar field.
mkScalarCube ::
  ( Molecule mol atom bond
  , MonadThrow m
  ) =>
  -- | String describing the cube
  Text ->
  -- | Cell definition of the cube
  Cell ->
  -- | Size of the voxel grid in the cube
  Sz3 ->
  -- | A 'Molecule' that provides the 'Atom's and for which the volumetric data
  -- is calculated.
  mol atom bond ->
  -- | Function to evaluate volumetric data on a grid.
  (R3 S Double -> m Double) ->
  m Cube
mkScalarCube title cell sz mol f = do
  dat <- fmap (setComp Par)
    . makeArrayA @S (snocSz sz 1)
    $ \(iA :> iB :> iC :. _) -> voxelToCoord cell sz (iA :> iB :. iC) >>= f
  return
    $ cube
      { title2 = T.filter (/= '\n') title
      , cell = CubeCell cell
      , dat
      , voxelType = VectorData 1
      }
 where
  cube = fromMolecule mol

{- | Parallel version of 'mkScalarCube'. This is only safe if the evaluation
function is total (and the type signature is not a lie...).
-}
mkScalarCube' ::
  (Molecule mol atom bond) =>
  -- | String describing the cube
  Text ->
  -- | Cell definition of the cube
  Cell ->
  -- | Size of the voxel grid in the cube
  Sz3 ->
  -- | A 'Molecule' that provides the 'Atom's and for which the volumetric data
  -- is calculated.
  mol atom bond ->
  -- | Function to evaluate volumetric data on a grid.
  (R3 S Double -> Double) ->
  Cube
mkScalarCube' title cell sz mol f =
  cube
    { title2 = T.filter (/= '\n') title
    , cell = CubeCell cell
    , dat
    , voxelType = VectorData 1
    }
 where
  cube = fromMolecule mol
  dat = makeArray Par (snocSz sz 1)
    $ \(iA :> iB :> iC :. _) -> f . fromJust $ voxelToCoord cell sz (iA :> iB :. iC)

-- | Create a cube with a vector field.
mkVectorCube ::
  ( Molecule mol atom bond
  , MonadThrow m
  ) =>
  -- | String describing the cube
  Text ->
  -- | Cell definition of the cube
  Cell ->
  -- | Size of the voxel grid in the cube with the last dimension being the
  -- dimensionality of the vector field.
  Sz4 ->
  -- | A 'Molecule' that provides the 'Atom's and for which the volumetric data
  -- is calculated.
  mol atom bond ->
  -- | Function to evaluate volumetric data on a grid.
  (R3 S Double -> m (Vector S Double)) ->
  m Cube
mkVectorCube title cell sz4@(Sz (nA :> nB :> nC :. nD)) mol f = do
  -- Create the 3D array of vectors
  vecField <- makeArrayA @B sz3 $ \ix3 -> voxelToCoord cell sz3 ix3 >>= f

  -- Make a 4D array
  dat <- makeArrayA sz4 $ \(iA :> iB :> iC :. iD) -> do
    vec <- vecField !? (iA :> iB :. iC)
    vec !? iD

  return
    $ cube
      { title2 = T.filter (/= '\n') title
      , cell = CubeCell cell
      , dat
      , voxelType = VectorData . fromIntegral $ nD
      }
 where
  cube = fromMolecule mol
  sz3 = Sz $ nA :> nB :. nC

{- | Parallel version of 'mkVectorCube' where the dimensions of the vector field
can be evaluated independently.  This is only safe if the evaluation
function is total (and the type signature is not a lie...).
-}
mkVectorCube' ::
  (Molecule mol atom bond) =>
  -- | String describing the cube
  Text ->
  -- | Cell definition of the cube
  Cell ->
  -- | Size of the voxel grid in the cube with the last dimension being the
  -- dimensionality of the vector field.
  Sz4 ->
  -- | A 'Molecule' that provides the 'Atom's and for which the volumetric data
  -- is calculated.
  mol atom bond ->
  -- | Function to evaluate volumetric data on a grid. The 'Ix1' is the
  -- dimension of the vectorial data in the field, e.g. 0 for the x-component of
  -- the electron density gradient.
  (R3 S Double -> Ix1 -> Double) ->
  Cube
mkVectorCube' title cell sz4@(Sz (nA :> nB :> nC :. nD)) mol f =
  cube
    { title2 = T.filter (/= '\n') title
    , cell = CubeCell cell
    , dat
    , voxelType = VectorData . fromIntegral $ nD
    }
 where
  cube = fromMolecule mol
  sz3 = Sz $ nA :> nB :. nC
  dat = makeArray Par sz4 $ \(iA :> iB :> iC :. iD) ->
    let coord = fromJust $ voxelToCoord cell sz3 (iA :> iB :. iC)
     in f coord iD

-- | Create a cube with indexed scalar fields.
mkIndexedScalarCube ::
  ( Molecule mol atom bond
  , MonadThrow m
  ) =>
  -- | String describing the cube
  Text ->
  -- | Cell definition of the cube
  Cell ->
  -- | Size of the voxel grid in the cube
  Sz3 ->
  -- | A 'Molecule' that provides the 'Atom's and for which the volumetric data
  -- is calculated.
  mol atom bond ->
  -- | The indices for evaluation
  [Int] ->
  -- | Function to evaluate volumetric data on a grid. The 'Int' is the index.
  (Int -> R3 S Double -> m Double) ->
  m Cube
mkIndexedScalarCube title cell sz3@(Sz (nA :> nB :. nC)) mol ixL f = do
  dat <- makeArrayA (Sz $ nA :> nB :> nC :. nD) $ \(iA :> iB :> iC :. iD) -> do
    coord <- voxelToCoord cell sz3 (iA :> iB :. iC)
    f iD coord
  return
    $ cube
      { title2 = T.filter (/= '\n') title
      , cell = CubeCell cell
      , dat
      , voxelType = ManyData . Massiv.fromList Par $ ixL
      }
 where
  cube = fromMolecule mol
  nD = length ixL
