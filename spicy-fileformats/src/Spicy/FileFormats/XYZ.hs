{- |
Module      : Spicy.FileFormats.XYZ
Description : XYZ files with positions only
Copyright   : Phillip Seeber, Sebastian Seidenath 2024
License     : AGPL-3
Maintainer  : phillip.seeber@uni-jena.de
Stability   : experimental
Portability : POSIX, Windows
-}
module Spicy.FileFormats.XYZ (
  XYZ,
  XyzTrajectory (..),
) where

import Data.Attoparsec.ByteString.Char8 (isSpace)
import Data.Default
import Data.Massiv.Array as Massiv hiding (convert, take, takeWhile, zip)
import Data.Override (As, Override (Override))
import Data.Text.Lazy.Builder qualified as TB
import Formatting hiding (char)
import Optics qualified as O
import RIO.Text qualified as T
import Spicy.Chemistry.Atom
import Spicy.Chemistry.Element (Element (H))
import Spicy.Chemistry.Molecule
import Spicy.FileFormats.Common
import Spicy.Math.LinearAlgebra.R3
import Spicy.Metrology qualified as M
import Spicy.Prelude hiding (label, take, (%))
import Spicy.Textual

{- | Finite precision comparison of 'Atom's due to floating point errors in
serialisation.
-}
newtype AtomFP = AtomFP LewisAtom
  deriving (Generic)
  deriving (Show) via LewisAtom
  deriving (Eq) via Override LewisAtom '["coord" `As` R3FP 4 (R3 S Double)]

-- | Represents a single line in a 'XYZ' file.
data XyzLine
  = -- | A line representing an 'Atom'
    XyzAtom AtomFP
  | -- | A line representing a pseudoparticle or point
    XyzOther Text (R3 S Double)
  deriving (Show, Generic)
  deriving (Eq) via Override XyzLine '[R3 S Double `As` R3FP 4 (R3 S Double)]

{- | XYZ files as defined by [OpenBabel](http://openbabel.org/wiki/XYZ_%28format%29).
Only standard XYZ files are supported, e.g. not the UniChem XYZ format with
atomic numbers instead of symbols.
-}
data XYZ = XYZ
  { comment :: Text
  -- ^ A single-line title or comment that may contain arbitrary or no information
  , dat :: Vector B XyzLine
  -- ^ A vector of 'Atom's or other (pseudo)particles/points.
  }
  deriving (Show, Eq)

instance Render XYZ where
  render XYZ{..} =
    convert
      . bformat (unlined builder)
      $ bformat int nAtoms
      : bformat commentFmt comment
      : fmap fmtLine (stoList dat)
   where
    -- Number of lines in the XYZ
    Sz nAtoms = size dat

    -- Convert lines to unified representation
    fmtLine :: XyzLine -> TB.Builder
    fmtLine l = case l of
      XyzAtom (AtomFP atom) ->
        let element = getElement atom
            coord = getCoord atom
         in bformat
              lineFmt
              (show element)
              (fmap (M.convert def M.Angstrom) [xR3 coord, yR3 coord, zR3 coord])
      XyzOther lab coord ->
        bformat
          lineFmt
          (T.unpack lab)
          (fmap (M.convert def M.Angstrom) [xR3 coord, yR3 coord, zR3 coord])

    -- Single XYZ line
    elFmt = fitRight 3 %. left 3 ' '
    coordFmt = spaced (left 10 ' ' %. fixed 5)
    lineFmt = elFmt %+ coordFmt

    -- 80 characters maximum comment
    commentFmt = fitRight 80 %. stext

instance ParseAp XYZ where
  parser = do
    nAtoms <- lex decimal <* lex endOfLine
    unless (nAtoms > 0) . fail $ "Number of atoms must be > 0"
    comment <- takeTill isEndOfLine <* endOfLine
    atmsInit <- Massiv.sfromList <$> count (nAtoms - 1) (lineParser <* endOfLine)
    atmsLast <- Massiv.ssingleton <$> lineParser <* endOfLine
    let dat = compute $ atmsInit <> atmsLast
    void . lex $ option Nothing (pure <$> endOfLine)
    return XYZ{..}
   where
    -- Line parser. Does not parse the endOfLine as the last line does not
    -- neccessarily require a new line.
    lineParser :: Parser XyzLine
    lineParser = do
      rawLabel <- lex $ takeWhile (not . isSpace)
      [x, y, z] <- fmap (M.convert M.Angstrom def) <$> count 3 (lex double)
      let coord = mkR3T (x, y, z)

      case readMaybe @Element . T.unpack $ rawLabel of
        Just element ->
          return
            . XyzAtom
            . AtomFP
            $ defAtm
            & (#coord .~ coord)
            & (#element .~ element)
        Nothing -> return $ XyzOther rawLabel coord
     where
      defAtm = mkAtom H Nothing (mkR3T (0, 0, 0)) 0 0

{- |
  * Atom numbering begins at 0
-}
instance (MkAtom atom) => AtomDataFormat XYZ atom where
  toAtoms XYZ{..} = stoList . smap fromXyzAtom . sfilter isAtom . Massiv.imap (,) $ dat
   where
    isAtom (_, XyzAtom _) = True
    isAtom (_, _) = False

    fromXyzAtom (i, XyzAtom (AtomFP LewisAtom{..})) =
      let atom = mkAtom element label coord chrg uEl
       in (i, atom)
    fromXyzAtom (_, o) = error $ "Can not convert " <> show o <> " to an Atom"

instance ParticlesFormat XYZ where
  toParticles XYZ{..} = stoList . Massiv.imap (\i l -> (i, l2p l)) $ dat
   where
    l2p xyzL = case xyzL of
      XyzOther lab coord ->
        (def @Particle)
          & (#classification O.% #isAtom .~ No)
          & (#dataAtom O.% #label ?~ lab)
          & (#dataAtom O.% #coord .~ coord)
      XyzAtom (AtomFP atom) ->
        let element = getElement atom
            coord = getCoord atom
         in (def @Particle)
              & (#classification O.% #isAtom .~ Yes)
              & (#classification O.% #isGhost .~ No)
              & (#classification O.% #isDummy .~ No)
              & (#classification O.% #isLonepair .~ No)
              & (#dataAtom O.% #element ?~ element)
              & (#dataAtom O.% #coord .~ coord)

instance BondDataFormat XYZ Void where
  toBonds _ = []

instance (Molecule mol atom bond) => FromMolecule mol atom bond XYZ where
  fromMolecule mol =
    XYZ
      { comment = "Generated by Spicy"
      , dat = Massiv.fromList Par . fmap (XyzAtom . AtomFP . toLewisAtom . snd) . getAtoms $ mol
      }
   where
    toLewisAtom atom =
      LewisAtom
        { element = getElement atom
        , label = getLabel atom
        , coord = getCoord atom
        , chrg = getChrg atom
        , uEl = getUEl atom
        }

{- | A trajectory of multiple XYZ frames. The number of atoms must stay the same
for each frame.
-}
newtype XyzTrajectory = XyzTrajectory (Vector B XYZ)

instance Render XyzTrajectory where
  render (XyzTrajectory v) =
    convert
      . bformat (list builder)
      . stoList
      . smap render
      $ v

instance ParseAp XyzTrajectory where
  parser = XyzTrajectory . Massiv.fromList Par <$> many1 parser
