module Spicy.Tests.FileFormats (tests) where

import Spicy.Tests.FileFormats.Cube qualified
import Spicy.Tests.FileFormats.XYZ qualified
import Test.Tasty

tests :: TestTree
tests =
  testGroup
    "FileFormats"
    [ Spicy.Tests.FileFormats.XYZ.tests
    , Spicy.Tests.FileFormats.Cube.tests
    ]
