module Spicy.Tests.FileFormats.Common where

import Paths_spicy_fileformats
import RIO.Map qualified as Map
import Spicy.Prelude
import System.Path ((</>))
import System.Path.Directory qualified as Dir

dataDir :: IO AbsDir
dataDir = absDir <$> getDataDir

babelDir :: IO AbsDir
babelDir = fmap (</> relBabel) dataDir
 where
  relBabel = relDir "data" </> relDir "openbabel"

getByDirAndSuffix :: RelDir -> String -> IO (Map String Text)
getByDirAndSuffix d suffix = do
  actDir <- babelDir <&> (</> d)
  fileNames <- Dir.filesInDir actDir
  let matchingFns =
        fmap (toAbsRel . (actDir </>))
          . filter (\fn -> genericTakeExtension fn == suffix)
          $ fileNames
  matchingFiles <- traverse readFileUtf8 matchingFns
  return . Map.fromList $ zip (fmap (toString . takeFileName) matchingFns) matchingFiles
