module Spicy.Tests.FileFormats.Cube (tests) where

import Hedgehog
import Hedgehog.Gen qualified as Gen
import RIO.Map qualified as Map
import RIO.Map.Partial qualified as Map
import Spicy.FileFormats.Cube
import Spicy.IntMap qualified as IntMap
import Spicy.Prelude hiding (parse, (:>))
import Spicy.Tests.FileFormats.Common
import Spicy.Textual
import System.Path ((</>))
import System.Path.Directory qualified as Dir
import Test.Tasty
import Test.Tasty.HUnit
import Test.Tasty.Hedgehog

tests :: TestTree
tests =
  testGroup
    "Cube"
    [ testCase "Parse Valid" $ do
        allCubes <- allCubes'
        void . flip Map.traverseWithKey allCubes $ \name txt -> do
          let res = parseM txt :: Maybe Cube
          assertBool ("Cube name:" <> name) (isJust res)
    , testProperty "Textual Trip" . property $ do
        cube <- forAll . Gen.small $ genCube
        tripping cube render (parseM :: Text -> Maybe Cube)
    , testCase "hasIndexedData" $ do
        allCubes <- allCubes'
        orbCube <- parseM $ allCubes Map.! "g16_multiorb.cube"
        densityCube <- parseM $ allCubes Map.! "g16_density.cube"
        gradientCube <- parseM $ allCubes Map.! "g16_densitygrad.cube"
        assertBool "multiorb file" (hasIndexedData orbCube)
        assertBool "density" (not $ hasIndexedData densityCube)
        assertBool "gradient" (not $ hasIndexedData gradientCube)
    , testCase "hasScalarData" $ do
        allCubes <- allCubes'
        orbCube <- parseM $ allCubes Map.! "g16_multiorb.cube"
        densityCube <- parseM $ allCubes Map.! "g16_density.cube"
        gradientCube <- parseM $ allCubes Map.! "g16_densitygrad.cube"
        assertBool "multiorb file" (not $ hasScalarData orbCube)
        assertBool "density" (hasScalarData densityCube)
        assertBool "gradient" (not $ hasScalarData gradientCube)
    , testCase "hasVectorData" $ do
        allCubes <- allCubes'
        orbCube <- parseM $ allCubes Map.! "g16_multiorb.cube"
        densityCube <- parseM $ allCubes Map.! "g16_density.cube"
        gradientCube <- parseM $ allCubes Map.! "g16_densitygrad.cube"
        assertBool "multiorb file" (not $ hasVectorData orbCube)
        assertBool "density" (not $ hasVectorData densityCube)
        assertBool "gradient" (hasVectorData gradientCube)
    , testCase "getIndexedScalarField" $ do
        allCubes <- allCubes'
        orbCube <- parseM $ allCubes Map.! "g16_multiorb.cube"
        ixScalarField <- getIndexedScalarField orbCube
        IntMap.keys ixScalarField @?= [2, 3, 4, 5]
    ]

allCubes' :: IO (Map String Text)
allCubes' = do
  cubeDir <- fmap (</> relCube) dataDir
  fileNames <- Dir.filesInDir cubeDir
  let matchingFns =
        fmap (toAbsRel . (cubeDir </>))
          . filter (\fn -> genericTakeExtension fn == ".cube")
          $ fileNames
  matchingFiles <- traverse readFileUtf8 matchingFns
  return . Map.fromList $ zip (fmap (toString . takeFileName) matchingFns) matchingFiles
 where
  relCube = relDir "data" </> relDir "cube"
