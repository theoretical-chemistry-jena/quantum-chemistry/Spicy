module Spicy.Tests.FileFormats.XYZ (tests) where

import RIO.Map qualified as Map
import RIO.Map.Partial qualified as Map
import RIO.Set qualified as Set
import Spicy.Chemistry.Atom
import Spicy.Chemistry.Molecule
import Spicy.FileFormats.Common
import Spicy.FileFormats.XYZ
import Spicy.Prelude hiding (parse)
import Spicy.Tests.FileFormats.Common
import Spicy.Textual
import Test.Tasty
import Test.Tasty.HUnit

tests :: TestTree
tests =
  testGroup
    "XYZ"
    [ testCase "Parse Valid"
        . void
        $ traverse (parseM :: Text -> IO XYZ)
        =<< getByDirAndSuffix (relDir "original") ".xyz"
    , testCase "Reject Invalid" $ do
        invalidXyz <- getByDirAndSuffix (relDir "invalid") ".xyz"
        let parseRes :: Map String (Maybe XYZ) = fmap (parseM :: Text -> Maybe XYZ) invalidXyz
        all isNothing parseRes @?= True
    , testCase "Parse Trajectory" $ do
        let trajs = Set.fromList ["met-enkaphalin_movie.xyz"]
        validTrajs <- flip Map.restrictKeys trajs <$> getByDirAndSuffix (relDir "original") ".xyz"
        traverse_ (parseM :: Text -> IO XyzTrajectory) validTrajs
    , testCase "Textual Trip" $ do
        validXyz <-
          traverse (parseM :: Text -> IO XYZ)
            =<< getByDirAndSuffix (relDir "original") ".xyz"
        for_ validXyz $ \xyz -> do
          trippedXyz <- parseM . (render :: XYZ -> Text) $ xyz
          xyz @=? trippedXyz
    , testCase "toMolecule closed shell" $ do
        let closedShell = Set.fromList ["buckyball.xyz"]
        closedShellXyz <-
          traverse (parseM :: Text -> IO XYZ)
            . flip Map.restrictKeys closedShell
            =<< getByDirAndSuffix (relDir "original") ".xyz"
        for_ closedShellXyz $ \xyz -> do
          mol <- toMolecule @MolGr @LewisAtom @Void xyz
          getBonds mol @?= mempty
          getCharge mol @?= 0
          getUnpEl mol @?= 0
          getMult mol @?= 1
    , testCase "toMoleculeWith open shell" $ do
        let openShell = Set.fromList ["met-enkaphalin_movie.xyz"]
        openShellXyz <-
          traverse (parseM :: Text -> IO XYZ)
            . flip Map.restrictKeys openShell
            =<< getByDirAndSuffix (relDir "original") ".xyz"

        -- Neutral open-shell biradical
        let met_enkephalin = openShellXyz Map.! "met-enkaphalin_movie.xyz"
            setRadikal =
              fmap
                ( \(n, atm :: LewisAtom) -> case n of
                    72 -> (n, atm & uElL .~ 1)
                    0 -> (n, atm & uElL .~ (-1))
                    _ -> (n, atm)
                )
        mol <- toMoleculeWith @MolGr @LewisAtom @Void met_enkephalin setRadikal id
        getBonds mol @?= mempty
        getCharge mol @?= 0
        getUnpEl mol @?= 2
        getMult mol @?= 1
    ]
