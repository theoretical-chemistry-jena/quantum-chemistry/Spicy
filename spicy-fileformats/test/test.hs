module Main where

import Spicy.Prelude
import Spicy.Tests.FileFormats qualified
import Test.Tasty

main :: IO ()
main = defaultMain tests

tests :: TestTree
tests =
  testGroup
    "Spicy"
    [Spicy.Tests.FileFormats.tests]
