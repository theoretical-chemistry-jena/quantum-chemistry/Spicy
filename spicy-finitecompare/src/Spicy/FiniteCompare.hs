{- |
Module      : Spicy.FiniteCompare
Description : Floating point comparison with a finite number of significant digits
Copyright   : Phillip Seeber, Sebastian Seidenath 2024
License     : AGPL-3
Maintainer  : phillip.seeber@uni-jena.de
Stability   : experimental
Portability : POSIX, Windows

This module provides a way to compare floating point numbers with a finite number of significant digits.
This is useful for comparing floating point numbers that are the result of a calculation, e.g. the result of a minimization.
The easiest way is to derive the 'Eq' instance via the [generic-override mechanism](https://hackage.haskell.org/package/generic-override).
-}
module Spicy.FiniteCompare where

import Data.Scientific
import GHC.TypeNats
import Spicy.Prelude

-- | Round a floating point number to a given number of significant digits.
roundFin :: (RealFrac a) => Natural -> a -> a
roundFin sg num = (fromIntegral @Int . round $ num * f) / f
 where
  f = 10 ^ fromIntegral sg

-- | Floating point numbers precise up to \(10^{\-n}\).
newtype RealFP (n :: Nat) f = RealFP f deriving (Show, Generic, Ord)

instance (KnownNat n, RealFloat a) => Eq (RealFP n a) where
  RealFP a == RealFP b =
    (rA == rB)
      || (rA' == rB)
      || (rB' == rA)
   where
    n = natVal $ Proxy @n
    rA = roundFin n a
    rA' = roundFin n (a + prec)
    rB = roundFin n b
    rB' = roundFin n (b + prec)
    prec = 10 ** (-fromIntegral (n + 1)) :: a

{- | Floating point numbers precise up to a finite number of significant digits
exponential form, i.e. \(n\) digits in the base are precise.
-}
newtype RealExpFP (n :: Nat) f = RealExpFP f deriving (Show, Generic, Ord)

instance (KnownNat n, RealFloat a) => Eq (RealExpFP n a) where
  RealExpFP a == RealExpFP b =
    (aExp == bExp) && (take n aBase == take n bBase)
   where
    n = fromIntegral . natVal $ Proxy @n
    (aBase, aExp) = toDecimalDigits . fromFloatDigits $ a
    (bBase, bExp) = toDecimalDigits . fromFloatDigits $ b
