{- |
Module      : Spicy.FragmentMethods
Copyright   : Phillip Seeber, Sebastian Seidenath 2024
License     : AGPL-3
Maintainer  : phillip.seeber@uni-jena.de
Stability   : experimental
Portability : POSIX, Windows
-}
module Spicy.FragmentMethods (
  module Spicy.FragmentMethods.Fragment,
  module Spicy.FragmentMethods.Group,
) where

import Spicy.FragmentMethods.Fragment
import Spicy.FragmentMethods.Group
