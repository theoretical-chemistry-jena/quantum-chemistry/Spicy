{- |
Module      : Spicy.FragmentMethods.Fragment
Description : Molecular fragments
Copyright   : Phillip Seeber, Sebastian Seidenath 2024
License     : AGPL-3
Maintainer  : phillip.seeber@uni-jena.de
Stability   : experimental
Portability : POSIX, Windows

'Fragment's are subsets of a 'Molecule', defined in the basis of its 'Group's.

\[
f \subseteq G
\]

While we view a 'Group' solely as a building block of a larger system and a 'Group' can not exist on its own, a 'Fragment' is meant to represent a physically reasonable substructure of the 'Molecule' and can exist on its own.
These 'Fragment's \(f\) are defined in the basis of 'Group' and each 'Fragment' is a subset of the supersystem's 'Group's

\[
f_i \subseteq G
\]

If a set of 'Fragment's \(F = \{ f_1, \dots, f_n \}\) is meant to describe a supersystem with all its atoms \(A\), we require only that the set of fragments comprises the entire supersystem, but contrary to 'Group's, 'Fragment's may overlap arbitrarily

\[
f_i \cap f_j \subset G
\]

\[
\bigcup\limits_{f \in F} f = G \, .
\]

We call the fragment's set of atoms \(A_i\) for the fragment \(f_i\).
As an isolation of this set of atoms \(A_i \subseteq A\) is likely to leave dangling bonds, link atoms are introduced to saturate these open valencies.
Let's define the set \(V_i\) for a fragment \(f_i\), that contains the tuple of all cut bonds

\[
V_i = \{ (m, n) | b_\mathrm{c} (m, n, i) \land m \in A_i \land n \in A  \}
\]

where


\[
b_\mathrm{c} (a, b, i) = b_\mathrm{b} (a, b) \land a \in A_i \land b \notin A_i
\]

and

\[
b_\mathrm{b} (a, b) = b_\mathrm{b} (b, a) = \begin{cases}
  1 & \text{if $a$ and $b$ share a bond} \\
  0 & \text{otherwise.}
\end{cases}
\]

For each element of the set \(V_i\) a link atom needs to be introduced, which is positioned along the original bond vector of \(m\) and \(n\).
This set of link atoms is

\[
A_i^\mathrm{l} = \{F_\mathrm{l}(v) | v \in V_i \}
\]

with some link atom creation function \(F_\mathrm{l} : V_i \rightarrow A_i^\mathrm{l} \).
The link atoms are added to the chosen set of 'MolecularFragment' 'Atom's

\[
A_i' = A_i \cup A_i^\mathrm{l} \, .
\]

For details of link atom creation see 'mkDefLinks' for an example for \(F_\mathrm{l}\).
-}
module Spicy.FragmentMethods.Fragment (
  module Spicy.FragmentMethods.Fragment.FCR,
  module Spicy.FragmentMethods.Fragment.Generation,
  module Spicy.FragmentMethods.Fragment.MolecularFragment,
  module Spicy.FragmentMethods.Fragment.Types,
)
where

import Spicy.FragmentMethods.Fragment.FCR
import Spicy.FragmentMethods.Fragment.Generation
import Spicy.FragmentMethods.Fragment.MolecularFragment
import Spicy.FragmentMethods.Fragment.Types
