{- |
Module      : Spicy.FragmentMethods.Fragment.FCR
Description : Fragment combination ranges
Copyright   : Phillip Seeber, Sebastian Seidenath 2024
License     : AGPL-3
Maintainer  : sebastian.seidenath@uni-jena.de
Stability   : experimental
Portability : POSIX, Windows

Fragment combination ranges (FCRs) are a formalism to describe molecular fragmentation schemes.
This module provides functions to create and manage such FCRs.

For more information, see the paper by Hellmers and König:
[A unified and flexible formulation of molecular fragmentation schemes](https://doi.org/10.1063/5.0059598).
A note on wording: The above paper uses the terms "fragment" and "fragment combination" for what Spicy calls "group" and "fragment".
Additionally, see the [Dissertation of Dr. rer. nat. Phillip Seeber](https://nbn-resolving.org/urn:nbn:de:gbv:27-dbt-20230720-100950-000), chapter 4.2 for a more detailed discussion of these concepts as they apply to Spicy.
-}
module Spicy.FragmentMethods.Fragment.FCR (
  -- * Fragment combination ranges
  FCR,
  getFCR,
  fcrUnion,
  mkFCR,
  fcrCoefficients,

  -- * Multi–layer fragment combination ranges
  mlCoefficients,
) where

import Data.Monoid
import RIO.List
import RIO.Set qualified as Set
import RIO.State
import Spicy.FragmentMethods.Fragment.Types
import Spicy.HashMap qualified as HashMap
import Spicy.IntSet qualified as IntSet
import Spicy.Prelude hiding (get, put)

--------------------------------------------------------------------------------

{- | A FCR is a set of 'Fragment's with the invariant that it must be closed on
forming subsets, i.e. every subset of every member is also included.
-}
newtype FCR = FCR {getFCR :: Set Fragment}
  deriving (Show, Generic)

-- | Union of two 'FCR's.
fcrUnion :: FCR -> FCR -> FCR
fcrUnion (getFCR -> a) (getFCR -> b) = FCR $ Set.union a b

-- | Form the 'FCR' for a set of fragments.
mkFCR :: Set Fragment -> FCR
mkFCR = FCR . Set.unions . Set.map (Set.map Fragment . IntSet.allSubsets . getFragment)

{- | Determine the coefficients of fragments in a 'FCR'.
The result is a mapping from a fragment to the coefficient of its contribution to the total system.
-}
fcrCoefficients :: FCR -> HashMap Fragment Int
fcrCoefficients (FCR fcr) =
  HashMap.filter (/= 0)
    . HashMap.fromList
    . fmap (toSnd coeff)
    . Set.toList
    $ fcr
 where
  toSnd f a = (a, f a)
  coeff (Fragment fragment) =
    getSum
      . foldMap (\x -> (-1) ^ (IntSet.size x - IntSet.size fragment))
      . Set.filter (fragment `IntSet.isSubsetOf`)
      . Set.map getFragment
      $ fcr

--------------------------------------------------------------------------------

{- | Calculate the coefficients for each level of theory in a multi–layer FCR setup.
This function takes as input a list of 'Set's of 'Fragment's, beginning with those on the highest level of theory.
The output is a list of FCR coefficients for each layer.
-}
mlCoefficients :: [Set Fragment] -> [HashMap Fragment Int]
mlCoefficients layers = evalState (for fcrs makeLLcoefficients) HashMap.empty
 where
  fcrs = case layers of
    [] -> []
    _ -> scanl1 fcrUnion (mkFCR <$> layers)
  makeLLcoefficients fcr = do
    prev <- get
    let curr = fcrCoefficients fcr
        ll = HashMap.filter (/= 0) $ HashMap.unionWith (+) curr (negate <$> prev)
    put curr
    return ll
