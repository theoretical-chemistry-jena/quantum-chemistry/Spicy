{- |
Module      : Spicy.FragmentMethods.Fragment.Generation
Description : Automatic generation of fragments
Copyright   : Phillip Seeber, Sebastian Seidenath 2024
License     : AGPL-3
Maintainer  : sebastian.seidenath@uni-jena.de
Stability   : experimental
Portability : POSIX, Windows

Methods for creating sets of fragments from a set of groups.

It turns out that applying FCR theory to a set of fragments represents many fragmentation schemes rather easily.
For example:

* *GMBE*: FCR + N–mers of previously defined and possibly overlapping fragments.
* *GEBF*: FCR + ego–graphs
* *MFCC*: FCR + arbitrary fragments + ego–graphs on these fragments
* *MIM*: Multi–level FCR + ego–graphs/neighbour spheres

Of course, a lot of subtlety lies in the way the groups are chosen.

The functions in this module are intended to be composable in order to enable maximum flexibility when formulating fragmentation schemes.
For example, a GMBE with dimers over GEBF–like fragments could be realized as

> nMers 2 <> egoN 1

Or get every dimer of adjacent groups by

> extendByOne
-}
module Spicy.FragmentMethods.Fragment.Generation (
  FragmentationScheme,
  runFragmentationScheme,

  -- * Fragment combinations
  nMersWithPredicate,
  nMers,
  nMersWithNeighbours,

  -- * Ego graphs
  egoN,

  -- * Other
  extendWithNeighbours,
  extendByOne,
) where

import Spicy.FragmentMethods.Fragment.Generation.Internal qualified as Internal
import Spicy.FragmentMethods.Fragment.Types
import Spicy.FragmentMethods.Group
import Spicy.Math.Neighbours
import Spicy.Prelude

{- | A 'FragmentationScheme' is a recipe on how to construct a set of fragments.
They can be built using a monoidal interface with the '(<>)' operator.
Used with 'runFragmentationScheme', which will apply the scheme using the groups as starting points for the fragmentation.
-}
newtype FragmentationScheme
  = FragmentationScheme
      ( forall groupMol grp mol atom bond.
        (GroupedMolecule groupMol grp mol atom bond) =>
        Set Fragment ->
        Reader (groupMol grp mol atom bond) (Set Fragment)
      )

instance Monoid FragmentationScheme where
  mempty = FragmentationScheme return
instance Semigroup FragmentationScheme where
  (FragmentationScheme a) <> (FragmentationScheme b) =
    FragmentationScheme (a >=> b)

--------------------------------------------------------------------------------

-- | Apply a 'FragmentationScheme' to a 'GroupedMolecule' and recieve a set of 'Fragments'.
runFragmentationScheme ::
  (GroupedMolecule groupMol grp mol atom bond) =>
  groupMol grp mol atom bond ->
  FragmentationScheme ->
  Set Fragment
runFragmentationScheme gmol (FragmentationScheme m) =
  runReader (m frgs) gmol
 where
  frgs = Internal.groupsAsFragments $ getGroupGraph gmol

{- | From a set of 'Fragments', form a set of combinations of those 'Fragments'.
If N–mers are requested but cannot be formed because there are less than N Fragments, this function will return fragments combinations smaller than N.
-}
nMers :: Natural -> FragmentationScheme
nMers n = FragmentationScheme $ return . Internal.nMers n

{- | From a set of 'Fragments', form a set of combinations of those 'Fragments'.
This function allows the caller to specify a predicate determining which fragment combinations are valid.
If N–mers are requested but cannot be formed because there are less than N eligible Fragments, this function will return fragments combinations smaller than N.
-}
nMersWithPredicate :: (Fragment -> Fragment -> Bool) -> Natural -> FragmentationScheme
nMersWithPredicate p n = FragmentationScheme $ return . Internal.nMersWithPredicate p n

{- | From a set of 'Fragments', form a set of combinations of those 'Fragments'.
This function allows the user to specify a distance cutoff, beyond which fragments will not be considered.
If N–mers are requested but cannot be formed because there are less than N eligible Fragments, this function will return fragments combinations smaller than N.
-}
nMersWithNeighbours :: Double -> Natural -> FragmentationScheme
nMersWithNeighbours d n = FragmentationScheme $ \f -> do
  mppng <- asks getAtomToGroupMapping
  mol <- ask
  let nl = groupNeighbours mppng $ neighbourListFromMolecule d mol
  return $ Internal.nMersWithNeighbours nl n f

-- | Form 'Fragments' by growing a set of starting 'Fragments' by a number of steps along the molecular graph.
egoN :: Natural -> FragmentationScheme
egoN n = FragmentationScheme $ \f -> do
  gr <- asks getGroupGraph
  return $ Internal.egoN n gr f

{- | Form spheres around every group in every fragment and add all groups within those spheres to the respective fragment.
If some groups are missing from the 'GroupNeighbourList', those keys will simply be ignored.
-}
extendWithNeighbours :: Double -> FragmentationScheme
extendWithNeighbours d = FragmentationScheme $ \f -> do
  mppng <- asks getAtomToGroupMapping
  mol <- ask
  let nl = groupNeighbours mppng $ neighbourListFromMolecule d mol
  return $ Internal.extendWithNeighbours nl f

{- | Starting from a set of 'Fragment's, generate the set of all extended derivative fragments.
Each such derivative is an original fragment plus one group that it is adjacent to in the input graph.
-}
extendByOne :: FragmentationScheme
extendByOne = FragmentationScheme $ \f -> do
  gr <- asks getGroupGraph
  return $ Internal.extendByOne gr f
