{- |
Module      : Spicy.FragmentMethods.Fragment.Generation.Internal
Description : Automatic generation of fragments
Copyright   : Phillip Seeber, Sebastian Seidenath 2024
License     : AGPL-3
Maintainer  : sebastian.seidenath@uni-jena.de
Stability   : experimental
Portability : POSIX, Windows

This is an internal molecule, defining primitives for fragment generation.
Users should import 'Spicy.FragmentMethods.Fragment.Generation' instead.
-}
module Spicy.FragmentMethods.Fragment.Generation.Internal
  {-# WARNING "This is an internal interface. Use at your own peril." #-} (
  groupsAsFragments,

  -- * Fragment combinations
  nMersWithPredicate,
  nMers,
  nMersWithNeighbours,

  -- * Ego graphs
  ego,
  egoN,

  -- * Other
  extendWithNeighbours,
  extendByOne,
) where

import RIO.List (iterate)
import RIO.List.Partial ((!!))
import RIO.Set qualified as Set
import Spicy.FragmentMethods.Fragment.Types
import Spicy.Graph
import Spicy.IntMap qualified as IntMap
import Spicy.IntSet qualified as IntSet
import Spicy.Math.Neighbours
import Spicy.Prelude

--------------------------------------------------------------------------------

-- | Trivial fragmentation: every group is its own fragment.
groupsAsFragments :: (Graph gr) => gr a b -> Set Fragment
groupsAsFragments gr = Set.fromList $ Fragment . IntSet.singleton <$> nodes gr

--------------------------------------------------------------------------------

-- | From a set of monoids, form a set of combinations of those monoids.
nMers ::
  (Ord a, Monoid a) =>
  -- | Order N of the N–mers
  Natural ->
  -- | Set of monomers
  Set a ->
  -- | Set of N–mers
  Set a
nMers = nMersWithPredicate (const . const True)

{- | From N–mers with a predicate.
The predicate checks if the combination of previous picks allows the next pick.
-}
nMersWithPredicate ::
  (Ord a, Monoid a) =>
  -- | Selection predicate; selects which monomers are valid picks.
  -- The first argument is the set of prior picks.
  -- The second is the next pick.
  (a -> a -> Bool) ->
  -- | Order N of the N–mers
  Natural ->
  -- | Set of monomers
  Set a ->
  -- | Set of N–mers
  Set a
nMersWithPredicate p (fromIntegral -> n) s0 = case n of
  0 -> Set.empty
  _ -> Set.filter (/= mempty) . Set.map fst $ combinations !! (n - 1)
 where
  combination1 = pick (const . const True) (mempty, s0)
  combinations = iterate (Set.unions . Set.map (pick p)) combination1

{- | Generate N–mers as in 'nMers', but use a neighbour list to exclude combinations.
Only groups that neighbour each other according to the provided 'GroupNeighbourList' will be considered for fragment formation.
-}
nMersWithNeighbours ::
  -- | Neighbour list in the group basis
  GroupNeighbourList ->
  -- | Order N of the N–mers
  Natural ->
  -- | Set of monomers
  Set Fragment ->
  -- | Set of N–mers
  Set Fragment
nMersWithNeighbours (getGroupNeighbourList -> nl) =
  nMersWithPredicate containsNeighbourTo
 where
  containsNeighbourTo (getFragment -> a) =
    not . IntSet.disjoint (fold $ IntMap.restrictKeys nl a) . getFragment

{- | Helper function for making picks from a set according to a predicate.
The predicate considers both the previous and the following picks.
-}
pick ::
  (Ord a, Semigroup a) =>
  -- | Selection predicate
  (a -> a -> Bool) ->
  -- | Previous picks and a set of possible picks
  (a, Set a) ->
  -- | Every possible pick and the remaining elements.
  -- Note that those elements are not filtered with the predicate!
  Set (a, Set a)
pick p (a, bs) =
  let bs' = Set.filter (p a) bs
      g acc b = (a <> b, Set.delete b bs) `Set.insert` acc
   in if null bs'
        then Set.singleton (a, bs')
        else Set.foldl' g Set.empty bs'

--------------------------------------------------------------------------------

-- | Ego-graphs from each group.
ego :: (DynGraph gr) => Natural -> gr a b -> Set Fragment
ego n = egoN n <*> groupsAsFragments

-- | From a set of 'Fragment's, grow ego–graphs by a set number of steps.
egoN :: (DynGraph gr) => Natural -> gr a b -> Set Fragment -> Set Fragment
egoN n gr = Set.map growFragment
 where
  growFragment (Fragment f) =
    Fragment $ IntSet.grow (IntSet.fromList . nodes . egoGraph n gr) f

--------------------------------------------------------------------------------

{- | Form spheres around every group in every fragment and add all groups within those spheres to the respective fragment.
If some groups are missing from the 'GroupNeighbourList', those keys will simply be ignored.
-}
extendWithNeighbours :: GroupNeighbourList -> Set Fragment -> Set Fragment
extendWithNeighbours (getGroupNeighbourList -> nl) (Set.map getFragment -> frgs) =
  Set.map (Fragment . IntSet.grow (fromMaybe mempty . (nl IntMap.!?))) frgs

--------------------------------------------------------------------------------

{- | Starting from a set of 'Fragment's, generate the set of all extended derivative fragments.
Each such derivative is an original fragment plus one group that it is adjacent to in the input graph.
-}
extendByOne :: (Graph gr) => gr a b -> Set Fragment -> Set Fragment
extendByOne gr (Set.map getFragment -> frgs) = Set.map (Fragment . IntSet.unions . (<*>) extensions allConnections) frgs
 where
  allConnections = IntSet.grow (IntSet.fromList . neighbours gr)
  extensions f xs =
    if IntSet.null xs
      then Set.singleton f
      else IntSet.foldr (\gid acc -> Set.insert (IntSet.insert gid f) acc) Set.empty xs
