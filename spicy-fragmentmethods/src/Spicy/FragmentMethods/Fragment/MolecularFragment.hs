{- |
Module      : Spicy.FragmentMethods.Fragment.MolecularFragment
Description : Molecular fragments
Copyright   : Phillip Seeber, Sebastian Seidenath 2024
License     : AGPL-3
Maintainer  : phillip.seeber@uni-jena.de
Stability   : experimental
Portability : POSIX, Windows

'Fragment's are subsets of a 'Molecule', defined in the basis of its 'Group's.
This module defines 'MolecularFragment's, which are actual submolecules augmented with link atoms.
-}
module Spicy.FragmentMethods.Fragment.MolecularFragment (
  -- * Molecular Fragments
  MolecularFragment (..),
  FragGr,
  isSuperAtom,

  -- * Link Atom Creation
  LinkInfo (..),
  mkDefLinks,
  mkDefLinksSmf,
)
where

import Data.Aeson
import Data.Foldable
import Data.Massiv.Array as Massiv hiding (S, forM, modify, toList)
import RIO.HashSet qualified as HashSet
import RIO.State
import Spicy.Chemistry
import Spicy.FragmentMethods.Fragment.Types
import Spicy.FragmentMethods.Group
import Spicy.Graph
import Spicy.IntSet qualified as IntSet
import Spicy.Math.LinearAlgebra.R3 as R3
import Spicy.Prelude hiding (get)

{- | A 'MolecularFragment' is an isolated part of a large system, a 'GroupedMolecule',
and defined in terms of the 'GroupedMolecule''s 'Group's. To maintain the
electronic properties of the subsystem, link atoms are introduced to saturate
open valencies. During isolation a Jacobian matrix is obtained to transform
repsonse properties from a representation over the fragment's 'Atom's into a
representation over the full system's 'Atom's. While the Jacobian matrix
connects the supersystem and the subsystem implicitly, this association needs
to be tracked by the user.
-}
class (Molecule frag atom bond) => MolecularFragment frag atom bond where
  -- | Build a 'MolecularFragment' from a 'GroupedMolecule'.
  mkMolecularFragment ::
    (GroupedMolecule groupMol grp mol atom bond, MonadThrow m) =>
    groupMol grp mol atom bond ->
    -- | Link atom creation function. The atom will be inserted with the same
    -- 'AtomId' as the super host atom it replaces.
    -- The 'LEdge' runs from the subsystem to the supersytem.
    (groupMol grp mol atom bond -> LEdge bond -> m (atom, LinkInfo)) ->
    -- | Definition of the 'MolecularFragment' \(f_i \subseteq G \)
    Fragment ->
    m (frag atom bond)

  -- | Obtain the set of link atoms the 'MolecularFragment' has.
  getLinkInfo :: frag atom bond -> [LNode (atom, LinkInfo)]

  {-# MINIMAL mkMolecularFragment, getLinkInfo #-}

--------------------------------------------------------------------------------

{- | Atoms of a 'FragGr' are either already part of the supersystem or
introduced as 'LinkAtom's to saturate open valencies.
-}
data FragAtom atom
  = SuperAtom atom
  | LinkAtom atom LinkInfo
  deriving (Eq, Generic, Show)

instance (ToJSON atom) => ToJSON (FragAtom atom) where
  toJSON = spicyToJSON

instance (FromJSON atom) => FromJSON (FragAtom atom) where
  parseJSON = spicyParseJSON

instance (Binary atom) => Binary (FragAtom atom)

isSuperAtom :: FragAtom atom -> Bool
isSuperAtom (SuperAtom _) = True
isSuperAtom _ = False

fragAtm2Atm :: FragAtom atom -> atom
fragAtm2Atm (SuperAtom atm) = atm
fragAtm2Atm (LinkAtom atm _) = atm

newtype FragGr atom bond = FragGr {fragGr :: Gr (FragAtom atom) bond}
  deriving (Generic, Eq, Show)

instance (ToJSON atom, ToJSON bond) => ToJSON (FragGr atom bond) where
  toJSON = spicyToJSON

instance (FromJSON atom, FromJSON bond) => FromJSON (FragGr atom bond) where
  parseJSON = spicyParseJSON

instance (Binary atom, Binary bond) => Binary (FragGr atom bond)

instance (Atom atom, Eq bond) => Molecule FragGr atom bond where
  getMolGraph (FragGr{fragGr}) = nmap fragAtm2Atm fragGr

instance (Atom atom, Eq bond) => MolecularFragment FragGr atom bond where
  mkMolecularFragment grpMol lAtmGen grps = do
    fragAtoms <- fmap fst . concat <$> traverse (getGroupAtoms grpMol) frg
    let cutBonds = getCutBonds . HashSet.fromList $ fragAtoms
        unsatFrag = nmap SuperAtom $ subgraph fragAtoms molGr
    fragGr <- flip execStateT unsatFrag . for cutBonds $ \(fragO, supT, bo) -> do
      (lAtm, lInfo) <- lift $ lAtmGen grpMol (fragO, supT, bo)
      modify $ \f ->
        insEdge (supT, fragO, bo)
          . insEdge (fragO, supT, bo)
          . insNode (supT, LinkAtom lAtm lInfo)
          $ f
    return FragGr{..}
   where
    frg = IntSet.toList . getFragment $ grps
    molGr = getMolGraph grpMol
    getCutBonds frgAtms =
      concatMap
        (filter (\(_fragO, supT, _bo) -> not $ supT `HashSet.member` frgAtms) . out molGr)
        frgAtms

  getLinkInfo (FragGr{fragGr}) = ufold f [] fragGr
   where
    f (_, n, a, _) acc = case a of
      SuperAtom _ -> acc
      LinkAtom x y -> (n, (x, y)) : acc

--------------------------------------------------------------------------------

-- | Information about a link atom
newtype LinkInfo = LinkInfo
  { gVal :: Double
  }
  deriving (Eq, Generic, Show)

instance ToJSON LinkInfo where
  toJSON = spicyToJSON

instance FromJSON LinkInfo where
  parseJSON = spicyParseJSON

instance Binary LinkInfo

{- | Default link creation function. Will place a link atom
at

\[
  \mathbf{r}_\mathrm{l}(m, n) = \mathbf{r}_m + g(m,n) (\mathbf{r}_n - \mathbf{r}_m)
\]

where

\[
  g(m, n) = \frac{R_m^\mathrm{cov} + R_\mathrm{l}^\mathrm{cov}}{R_n^\mathrm{cov} + R_m^\mathrm{cov}}
\]

and the element of the link atoms is:

  * Hydrogen ('H') for a cut single bond
  * Oxygen ('O') for a cut double bond

An 'Exception' is thrown for a 'Coordinative' bond.
-}
mkDefLinks ::
  (Molecule mol atom bond, MkAtom atom, LewisBond bond, MonadThrow m) =>
  -- | The 'Molecule' the function is applied to
  mol atom bond ->
  -- | The bond of interest that is cut
  LEdge bond ->
  m (atom, LinkInfo)
mkDefLinks mol (atmIdSubSys, atmIdSupSys, bondOrder) = do
  (_, _, atmSub, _) <- atmCntxtLookup atmIdSubSys
  (_, _, atmSup, _) <- atmCntxtLookup atmIdSupSys
  linkElement <- case getBondOrder bondOrder of
    CovalentBond 1 -> return H
    CovalentBond 2 -> return O
    CovalentBond n -> throwM . MoleculeException $ "A cut of a bond of order " <> show n <> " is attempted but refused"
    CoordinativeBond -> throwM $ MoleculeException "A cut of a coordinative bond is attempted but refused"
  linkGval <- do
    rCovSub <- covalentRadii . getElement $ atmSub
    rCovSup <- covalentRadii . getElement $ atmSup
    rCovLink <- covalentRadii linkElement
    return $ (rCovSub + rCovLink) / (rCovSub + rCovSup)
  let coordSub = getCoord atmSub
      coordSup = getCoord atmSup
      linkCoord = coordSub R3.!+! (linkGval R3.*. (coordSup R3.!-! coordSub))
  return (mkAtom linkElement Nothing linkCoord 0 0, LinkInfo linkGval)
 where
  molGr = getMolGraph mol
  atmCntxtLookup i =
    maybe2MThrow
      (MoleculeException $ "Atom with ID " <> show i <> " not found in supersystem")
      . fst
      $ match i molGr

{- | Same as 'mkDefLinks' but only accepts 'isSeparable' bonds. for which a 'H'
atom will be added.
-}
mkDefLinksSmf ::
  (Molecule mol atom bond, MkAtom atom, SmfBond bond, MonadThrow m) =>
  -- | The 'Molecule' the function is applied to
  mol atom bond ->
  -- | The bond of interest that is cut
  LEdge bond ->
  m (atom, LinkInfo)
mkDefLinksSmf mol (atmIdSubSys, atmIdSupSys, bondOrder) = do
  (_, _, atmSub, _) <- atmCntxtLookup atmIdSubSys
  (_, _, atmSup, _) <- atmCntxtLookup atmIdSupSys
  linkElement <-
    if isSeparable bondOrder
      then return H
      else throwM $ MoleculeException "A cut of a Multi bond is attempted but refused"
  linkGval <- do
    rCovSub <- covalentRadii . getElement $ atmSub
    rCovSup <- covalentRadii . getElement $ atmSup
    rCovLink <- covalentRadii linkElement
    return $ (rCovSub + rCovLink) / (rCovSub + rCovSup)
  let coordSub = getCoord atmSub
      coordSup = getCoord atmSup
      linkCoord = coordSub R3.!+! (linkGval R3.*. (coordSup R3.!-! coordSub))
  return (mkAtom linkElement Nothing linkCoord 0 0, LinkInfo linkGval)
 where
  molGr = getMolGraph mol
  atmCntxtLookup i =
    maybe2MThrow
      (MoleculeException $ "Atom with ID " <> show i <> " not found in supersystem")
      . fst
      $ match i molGr
