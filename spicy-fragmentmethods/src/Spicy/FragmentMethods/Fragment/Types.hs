{- |
Module      : Spicy.FragmentMethods.Fragment.Types
Description : Fragment types
Copyright   : Phillip Seeber, Sebastian Seidenath 2024
License     : AGPL-3
Maintainer  : phillip.seeber@uni-jena.de
Stability   : experimental
Portability : POSIX, Windows

This module defines basic types for working with fragments and molecular fragmentation.
-}
module Spicy.FragmentMethods.Fragment.Types (
  Fragment (..),
  Fragmentation,
  getFragmentation,
)
where

import Spicy.Prelude

{- | A pure 'Fragment' is just a set of 'GroupId's.
The Ids specify which groups belong to the fragment.
As an invariant, it is required that all ids must belong to groups that actually exist.
-}
newtype Fragment = Fragment {getFragment :: IntSet}
  deriving (Show, Generic, Eq, Ord)
  deriving newtype (Semigroup, Monoid)

instance Hashable Fragment

{- | A 'Fragmentation' built from a molecule repesents a set of fragments fulfilling the following criteria:

    * All groups occurring in the molecule must appear somewhere in the 'Fragmentation'
    * No Fragment in the 'Fragmentation' may contain unphysical interactions when expanded back into the atomic representation.

The latter would usually occur because of two link atoms getting too close.
-}
newtype Fragmentation = Fragmentation {getFragmentation :: Set Fragment}
  deriving (Show, Generic, Eq)

instance Hashable Fragmentation
