{- |
Module      : Spicy.FragmentMethods.Group
Description : Constructing, querying and interacting with groups
Copyright   : Phillip Seeber, Sebastian Seidenath 2024
License     : AGPL-3
Maintainer  : phillip.seeber@uni-jena.de
Stability   : experimental
Portability : POSIX, Windows

A 'Molecule' can be thought of to be built of 'Group's \(G = \{g_1, \dots, g_n \} \), where each group is a subset of 'Atom's of the 'Molecule'.

\[
g \subseteq A
\]

We require these groups to describe the entire 'Molecule'

\[
\bigcup\limits_{g \in G} g = A
\]

but demand that they do not overlap

\[
g_i \cap g_j = \varnothing, \forall i \neq j \, .
\]

Given such a set of groups, a representation of a 'Molecule' as an undirected graph can be transformed into an undirected graph of groups, where each node represents one 'Group' and the vertices encode the original connectivity groups had, although without any 'Spicy.Chemistry.Bond.BondOrder'.
For example, consider the partioning of the 'Molecule' shown in 'Spicy.Chemistry.Molecule' into groups:

<<docs/images/rendered/molgraph_partition.svg>>

which is transformed into a group graph:

<<docs/images/rendered/groupgraph.svg>>

However, an important aspect is how groups are chosen, and for fragment methods they should be chosen so, that they approximately extensively contribute to the full system's property, e.g. \(P \approx \sum\limits_{g \in G} P(g) \).
Consider for instance \(\mathrm{CH}_2\) groups in alkanes of type \(\mathrm{CH}_3 (- \mathrm{CH}_2 - \mathrm{CH}_2 - )_n \mathrm{CH}_3 \) and \(\mathrm{CH}\) groups in alkenes of type \(\mathrm{CH}_2 (= \mathrm{CH} - \mathrm{CH} = )_n \mathrm{CH}_2 \) and their relative contributions to the systems energy:

<<docs/images/rendered/aliphatic_chain_energy.svg>>

While for the alkane the energy contribution per \(\mathrm{CH}_2\) group is nearly constant, the \(\mathrm{CH}_2\) groups violate the extensibility consideration raised above.
Thus, this group definition is unsuitable and instead the entire conjugated \(\pi\)-system should be considered a single group.

The 'GroupedMolecule' class group below encodes the rules above, but has of course no means to check for the extensibility criterion.
-}
module Spicy.FragmentMethods.Group (
  -- * SMF-Type Bonds
  SmfBond (..),
  SmfBondOrder (..),

  -- * Groups
  Group (..),
  SimpleGroup (..),
  GroupId,
  GroupException (..),

  -- * Grouped molecules
  GroupedMolecule (..),
  MkGroupedMolecule (
    mkGroupedMolecule,
    mkGroupedMoleculeWithDeduction,
    mkPredefined
  ),

  -- ** Retrieving Information
  getGroups,
  getGroupConnections,
  getGroupAtoms,
  getAtomGroup,
  getGroupCharge,
  getGroupUnpEl,
  getGroupMult,

  -- ** Standard Implementation as a Graph
  GroupGr,
)
where

import Data.Aeson
import RIO.List qualified as List
import RIO.Partial (fromJust)
import Spicy.Chemistry
import Spicy.Graph
import Spicy.IntMap qualified as IntMap
import Spicy.IntSet qualified as IntSet
import Spicy.Prelude

{-# WARNING
  mkGroupedMolecule'
  [ "This is an internal function."
  , "Use at your own peril."
  ]
  #-}

type GroupId = Int

data GroupException
  = -- | Atoms are not assigned to any group
    UnassignedAtom AtomId
  | -- | Atoms are assigned to non-existant group
    InvalidAssignment (AtomId, GroupId)
  | -- | Groups have no atoms
    EmptyGroup GroupId
  | -- | Atoms are assigned to multiple groups
    OverlappingGroup (AtomId, NonEmpty GroupId)
  deriving (Show)

instance Exception GroupException

class (Eq g) => Group g where
  -- | Get the 'IntSet' of 'AtomId's that make up the 'Group'.
  getGroup :: g -> IntSet

-- | The minimal definition of group, just a set of atoms.
newtype SimpleGroup = SimpleGroup {simpleGroup :: IntSet}
  deriving (Eq, Show, Hashable, Generic)

instance ToJSON SimpleGroup where
  toJSON = spicyToJSON

instance FromJSON SimpleGroup where
  parseJSON = spicyParseJSON

instance Binary SimpleGroup

instance Group SimpleGroup where
  getGroup = simpleGroup

--------------------------------------------------------------------------------

-- | Bond types that are sufficient for the SMF method.
class SmfBond bond where
  -- | Is the bond a separable in SMF, i.e. it can be electronically decoupled?
  isSeparable :: bond -> Bool

instance SmfBond LewisBondOrder where
  isSeparable (CovalentBond 1) = True
  isSeparable _ = False

-- | Simple type for SMF bonds
data SmfBondOrder = Single | Multi
  deriving (Eq, Ord, Show, Generic)

instance ToJSON SmfBondOrder where
  toJSON = spicyToJSON

instance FromJSON SmfBondOrder where
  parseJSON = spicyParseJSON

instance Binary SmfBondOrder

instance SmfBond SmfBondOrder where
  isSeparable = (== Single)

--------------------------------------------------------------------------------

{- | A 'GroupedMolecule' is a 'Molecule' for which 'Group's have been defined and
which can, consequently, be represented both as a molecular graph and as a
group graph.
-}
class
  (Group grp, Molecule ((groupMol grp) mol) atom bond, Molecule mol atom bond) =>
  GroupedMolecule groupMol grp mol atom bond
  where
  -- | Check whether a 'GroupedMolecule' has a valid internal structure.
  checkGroups ::
    -- forall grp mol atom bond m.
    (MonadThrow m) =>
    groupMol grp mol atom bond ->
    m ()
  checkGroups grpMol =
    when (overlaps || directed || missingAtoms)
      . throwM
      . MoleculeException
      $ "Invalid Groups. Conditions violated:\n"
      <> (overlaps ?-> "Groups must not overlap")
      <> (missingAtoms ?-> "Each atom must be in a Group")
      <> (directed ?-> "Group connections must be undirected")
   where
    groupGr = getGroupGraph grpMol
    f (b, acc) a = (b || not (a `IntSet.disjoint` acc), a <> acc)
    overlaps =
      fst
        $ foldl'
          f
          (False, IntSet.empty)
          (getGroup . snd <$> getGroups grpMol)
    missingAtoms =
      let atmsMol = IntSet.fromList . fmap fst . getAtoms $ grpMol
          atmsGMol = IntSet.unions . fmap (getGroup . snd) . getGroups $ grpMol
       in atmsMol /= atmsGMol
    directed = undir groupGr /= groupGr

  -- | Obtain a graph-like representation of the groups.
  getGroupGraph :: groupMol grp mol atom bond -> Gr grp ()

  -- | Get a mapping from 'AtomId' to 'GroupId' in the form of a map.
  -- The default implementation deduces the mapping from scratch every time.
  getAtomToGroupMapping :: groupMol grp mol atom bond -> IntMap GroupId
  getAtomToGroupMapping = fromJust . deduceMapping . getGroups

  {-# MINIMAL getGroupGraph #-}

-- | Get all 'Group's of the 'GroupedMolecule'.
getGroups :: (GroupedMolecule groupMol grp mol atom bond) => groupMol grp mol atom bond -> [LNode grp]
getGroups grMol = labNodes . getGroupGraph $ grMol

-- | Get all connections between 'Group's.
getGroupConnections :: (GroupedMolecule groupMol grp mol atom bond) => groupMol grp mol atom bond -> [Edge]
getGroupConnections grMol = edges . getGroupGraph $ grMol

-- | Get 'Atom's in a 'Group'.
getGroupAtoms ::
  ( MonadThrow m
  , GroupedMolecule groupMol grp mol atom bond
  ) =>
  groupMol grp mol atom bond ->
  GroupId ->
  m [LNode atom]
getGroupAtoms grMol grId =
  let res = do
        gr <- getGroup <$> lab grGr grId
        return . labNodes . subgraph (IntSet.toAscList gr) $ molGr
   in case res of
        Just r -> return r
        Nothing ->
          throwM
            . MoleculeException
            $ "Can not find group with ID "
            <> show grId
 where
  molGr = getMolGraph grMol
  grGr = getGroupGraph grMol

-- | Get the Group of an atom.
getAtomGroup ::
  (MonadThrow m, GroupedMolecule groupMol grp mol atom bond) =>
  groupMol grp mol atom bond ->
  AtomId ->
  m (Node, grp)
getAtomGroup gmol i =
  case filter (IntSet.member i . getGroup . snd) $ getGroups gmol of
    [x] -> return x
    [] ->
      throwM
        . MoleculeException
        $ "Atom "
        <> show i
        <> " not present in any groups"
    _ ->
      throwM
        . MoleculeException
        $ "Atom "
        <> show i
        <> " present in multiple groups"

-- | Get charge of a 'Group'.
getGroupCharge ::
  (MonadThrow m, GroupedMolecule groupMol grp mol atom bond) =>
  groupMol grp mol atom bond ->
  GroupId ->
  m Int
getGroupCharge grMol grId = do
  grAtoms <- getGroupAtoms grMol grId
  return . sum . fmap (getChrg . snd) $ grAtoms

-- | Get net number of unpaired electrons in a 'Group'.
getGroupUnpEl ::
  (MonadThrow m, GroupedMolecule groupMol grp mol atom bond) =>
  groupMol grp mol atom bond ->
  GroupId ->
  m Int
getGroupUnpEl grMol grId = do
  grAtoms <- getGroupAtoms grMol grId
  return . sum . fmap (getUEl . snd) $ grAtoms

-- | Get the multiplicity of a 'Group'.
getGroupMult ::
  (MonadThrow m, GroupedMolecule groupMol grp mol atom bond) =>
  groupMol grp mol atom bond ->
  GroupId ->
  m Natural
getGroupMult grMol grId = do
  grUEl <- getGroupUnpEl grMol grId
  return . fromIntegral . (+ 1) . abs $ grUEl

--------------------------------------------------------------------------------

class (GroupedMolecule groupMol grp mol atom bond) => MkGroupedMolecule groupMol grp mol atom bond where
  -- | Make a grouped molecule starting from a 'Molecule' and utilising a group
  -- generator function. Two sanity checks must be implemented:
  --
  --   * 'Group's must not overlap
  --   * 'Group's comprise the entire molecule
  mkGroupedMolecule ::
    (Foldable f1, Foldable f2, MonadThrow m, SmfBond bond) =>
    mol atom bond ->
    -- | Function to generate groups and their connections
    (mol atom bond -> (f1 (LNode grp), f2 (LEdge ()))) ->
    m (groupMol grp mol atom bond)
  mkGroupedMolecule mol f = do
    let grMol = mkGroupedMolecule' mol f
    _ <- deduceMapping (getGroups grMol)
    checkGroups grMol
    return grMol

  -- | Unsafe version of 'mkGroupedMolecule' that does not check for sanity.
  mkGroupedMolecule' ::
    (Foldable f1, Foldable f2, SmfBond bond) =>
    mol atom bond ->
    -- | Function to generate groups and their connections
    (mol atom bond -> (f1 (LNode grp), f2 (LEdge ()))) ->
    groupMol grp mol atom bond
  mkGroupedMolecule' m = fromJust . mkGroupedMolecule m

  -- | Make a 'GroupedMolecule' with only a grouping function.
  -- The connectivity of the 'Group's is then deduced from the original atoms.
  -- This needs at least two passes over the molecule.
  mkGroupedMoleculeWithDeduction ::
    (SmfBond bond, Foldable f, MonadThrow m) =>
    mol atom bond ->
    -- | Function to generate groups
    (mol atom bond -> f (LNode grp)) ->
    m (groupMol grp mol atom bond)
  mkGroupedMoleculeWithDeduction mol f =
    let grps = f mol
     in mkGroupedMolecule mol (\m -> (grps, deduceConnections m grps))

  -- | Use predefined 'Group's without a generator function. The same checks as
  -- in 'mkGroupedMolecule' apply.
  mkPredefined ::
    (SmfBond bond, Foldable f, MonadThrow m) =>
    mol atom bond ->
    f (LNode grp) ->
    m (groupMol grp mol atom bond)
  mkPredefined mol grps = mkGroupedMoleculeWithDeduction mol (const grps)

--------------------------------------------------------------------------------

{- | Manually search for connections between groups using the molecule these
groups were built from. Prefer to generate groups and their
connections in one go if possible.
-}
deduceConnections ::
  forall mol atom bond grp f.
  (Molecule mol atom bond, Group grp, Foldable f) =>
  mol atom bond ->
  f (LNode grp) ->
  [LEdge ()]
deduceConnections mol grps = concatMap getGrpCon grps
 where
  -- All atoms in a group, but only it they're in mol
  grpAtms grp = nodes $ subgraph (IntSet.toAscList grp) (getMolGraph mol)
  -- All neighbours of all nodes of a group
  allNghbrs grp =
    IntSet.fromList
      $ concatMap
        (neighbors . getMolGraph $ mol)
        (grpAtms grp)
  -- Not the neighbourhs internal to the group
  grpExtAtoms grp = allNghbrs grp IntSet.\\ grp
  -- Are any atoms in this group connected to the reference group?
  ff grp (_, extGrp) =
    not . IntSet.null . IntSet.intersection (grpExtAtoms grp) $ extGrp
  extGrps grp = filter (ff grp) . fmap (second getGroup) . toList $ grps

  getGrpCon :: LNode grp -> [LEdge ()]
  getGrpCon (grpId, grp) =
    List.zip3
      (List.repeat grpId)
      (fst <$> extGrps (getGroup grp))
      (List.repeat ())

{- | Deduce a mapping from 'AtomId's to 'GroupId's, based on the groups of a molecule.
Will fail if atoms are assigned to multiple groups.
-}
deduceMapping ::
  (Group grp, Foldable f, MonadThrow m) =>
  f (LNode grp) ->
  m (IntMap Int)
deduceMapping grps =
  let groupMap = IntMap.map getGroup . IntMap.fromList . toList $ grps
      atomMapRaw = IntMap.invertInts groupMap
      f k is acc = case IntSet.minView is of
        Nothing -> throwM (UnassignedAtom k) *> acc
        Just (i, is')
          | not (IntSet.null is') ->
              throwM (OverlappingGroup (k, i :| IntSet.toList is'))
                *> (IntMap.insert k i <$> acc)
        Just (i, _) -> IntMap.insert k i <$> acc
   in IntMap.foldrWithKey f (pure mempty) atomMapRaw

-- | A group graph parametrised over the underlying type of the 'Molecule'
data GroupGr grp mol atom bond = GroupGr
  { molGr :: mol atom bond
  , groupGr :: Gr grp ()
  , atmMapping :: IntMap Int
  -- ^ Mapping from atom indices to group indices
  }
  deriving (Generic, Eq, Show)

instance (Molecule mol atom bond, ToJSON (mol atom bond), ToJSON grp) => ToJSON (GroupGr grp mol atom bond) where
  toJSON = spicyToJSON

instance (Molecule mol atom bond, FromJSON (mol atom bond), FromJSON grp) => FromJSON (GroupGr grp mol atom bond) where
  parseJSON = spicyParseJSON

instance (Molecule mol atom bond, Binary (mol atom bond), Binary grp) => Binary (GroupGr grp mol atom bond)

instance (Molecule mol atom bond) => Molecule (GroupGr grp mol) atom bond where
  getMolGraph GroupGr{molGr} = getMolGraph molGr

instance (UpdMolecule mol atom bond) => UpdMolecule (GroupGr grp mol) atom bond where
  updCoords g c = g{molGr = updCoords g.molGr c}

instance (Group grp, Molecule mol atom bond) => GroupedMolecule GroupGr grp mol atom bond where
  getGroupGraph GroupGr{groupGr} = groupGr
  getAtomToGroupMapping = atmMapping

instance (Group grp, Molecule mol atom bond) => MkGroupedMolecule GroupGr grp mol atom bond where
  mkGroupedMolecule' molGr f =
    let (grps, conns) = bimap toList (fmap (\(a, b, _) -> (a, b, ())) . toList) $ f molGr
        groupGr = mkGraph grps conns
        groupMap = IntMap.map getGroup . IntMap.fromList . toList $ grps
        foldToMapping k is = IntMap.union (IntMap.fromSet (const k) is)
        atmMapping = IntMap.foldrWithKey foldToMapping mempty groupMap
     in GroupGr{..}
