{- |
Module      : Spicy.FragmentMethods.Group.Detection
Description : Constructing, querying and interacting with groups
Copyright   : Phillip Seeber, Sebastian Seidenath 2024
License     : AGPL-3
Maintainer  : sebastian.seidenath@uni-jena.de
Stability   : experimental
Portability : POSIX, Windows

This module provides algorithms for separating a 'Molecule' into disjoint groups.
These groups are intended to be used to construct 'GroupedMolecule's; see 'mkGroupedMolecule'.
For more details on 'GroupedMolecule's, see 'Spicy.FragmentMethods.Group'.
For more details on SMF, see [Collins et al., "The Combined Fragmentation and Systematic Molecular Fragmentation Methods"](https://doi.org/10.1021/ar500088d).
-}
module Spicy.FragmentMethods.Group.Detection (
  smf,
  disjointMolecules,
  trivialGroup,
) where

import RIO.List hiding (group)
import RIO.Partial (fromJust)
import Spicy.Chemistry
import Spicy.FragmentMethods.Group
import Spicy.Graph
import Spicy.IntSet qualified as IntSet
import Spicy.Prelude hiding ((&))

{- | Group generation by the rules of SMF (Systematic Molecular Fragmentation).
 Every bond in the molecule is cut, unless it fulfills one of these criteria:

   * One of the bonding atoms is a 'Hydrogen'.
   * The 'BondOrder' is higher than 'Single'.
   * The bond connects to a formally charged or spin-polarized atom, either directly or through a conjugated system.

 The components of the modified graph become the groups.

 This function also maps the cut bonds to connections between the resulting groups and returns those connections.
-}
smf :: (Molecule mol atom bond, SmfBond bond) => mol atom bond -> ([LNode SimpleGroup], [LEdge ()])
smf (getMolGraph -> gr) = (groups, groupConnections)
 where
  (cuts, disconnected) = findEdges p gr
  groups = zip [0 ..] (SimpleGroup . IntSet.fromList <$> components disconnected)
  groupConnections = (\(a1, a2, _) -> (groupOf a1, groupOf a2, ())) <$> cuts

  -- Predicates determining the cuts
  p le = not $ involvesAHydrogen le || isMultiple le || connectsToCharges le
  involvesAHydrogen (n1, n2, _) = fromMaybe False $ do
    c1 <- fst $ match n1 gr
    c2 <- fst $ match n2 gr
    return $ (getElement . lab' $ c1) == H || (getElement . lab' $ c2) == H
  isMultiple (_, _, o) = not . isSeparable $ o
  connectsToCharges (n1, n2, _) = connectedCharge n1 gr || connectedCharge n2 gr
  connectedCharge n0 = any notable . xdfsWith multipleBonds lab' [n0]
  notable a = getChrg a /= 0 || getUEl a /= 0
  multipleBonds (_, _, _, o) = snd <$> filter (not . isSeparable . fst) o

  groupOf i = fst . fromJust $ find (IntSet.member i . getGroup . snd) groups

{- | Every molecule (in the chemical sense) is turned into a single group. There
 will be no edges since no cuts are made.
-}
disjointMolecules :: (Molecule mol atom bond) => mol atom bond -> ([LNode SimpleGroup], [LEdge ()])
disjointMolecules (getMolGraph -> gr) = (groups, [])
 where
  groups = zip [0 ..] $ SimpleGroup . IntSet.fromList <$> components gr

-- | Trivial grouping: One group which includes all atoms.
trivialGroup :: (Molecule mol atom bond) => mol atom bond -> ([LNode SimpleGroup], [LEdge ()])
trivialGroup (getMolGraph -> gr) = (groups, [])
 where
  groups = [(0, SimpleGroup . IntSet.fromList $ nodes gr)]
