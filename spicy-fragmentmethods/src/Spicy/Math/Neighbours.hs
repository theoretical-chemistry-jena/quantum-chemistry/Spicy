{- |
Module      : Spicy.Math.Neighbours
Description : Detection of neighbours in space
Copyright   : Phillip Seeber, Sebastian Seidenath 2024
License     : AGPL-3
Maintainer  : sebastian.seidenath@uni-jena.de
Stability   : experimental
Portability : POSIX, Windows

Functions for finding an objects neighbours in space.
-}
module Spicy.Math.Neighbours (
  AtomNeighbourList,
  getAtomNeighbourList,
  GroupNeighbourList,
  getGroupNeighbourList,
  neighbourList,
  neighbourListFromMolecule,
  mGroupNeighbours,
  groupNeighbours,
) where

import Data.Massiv.Array as Massiv hiding (fold, toList)
import Foreign.Storable.Generic
import Spicy.Chemistry.Atom
import Spicy.Chemistry.Molecule
import Spicy.FragmentMethods.Group
import Spicy.IntMap qualified as IntMap
import Spicy.IntSet qualified as IntSet
import Spicy.Math.LinearAlgebra.R3
import Spicy.Prelude hiding ((:>))
import System.IO.Unsafe (unsafePerformIO)

-- | A neighbour list in the basis of atom indices.
newtype AtomNeighbourList = AtomNeighbourList {getAtomNeighbourList :: IntMap IntSet}
  deriving (Show, Generic)

-- | A neighbour list in the basis of group indices.
newtype GroupNeighbourList = GroupNeighbourList {getGroupNeighbourList :: IntMap IntSet}
  deriving (Show, Generic)

--------------------------------------------------------------------------------

-- | Spatial extent of a 3D system.
data Extent = Extent
  { xMin :: Double
  , xMax :: Double
  , yMin :: Double
  , yMax :: Double
  , zMin :: Double
  , zMax :: Double
  }
  deriving (Show, Generic)

instance GStorable Extent

instance Semigroup Extent where
  a <> b =
    Extent
      { xMin = min (a ^. #xMin) (b ^. #xMin)
      , xMax = max (a ^. #xMax) (b ^. #xMax)
      , yMin = min (a ^. #yMin) (b ^. #yMin)
      , yMax = max (a ^. #yMax) (b ^. #yMax)
      , zMin = min (a ^. #zMin) (b ^. #zMin)
      , zMax = max (a ^. #zMax) (b ^. #zMax)
      }

{- | Genarate the 'AtomNeighbourList' of a system at a certain search distance:
An association of an atom with its close neighbours.
An atom does not count as its own neighbour for this purpose.
-}
neighbourList ::
  (Manifest r Double, Numeric r Double) =>
  -- | Distance cutoff
  Double ->
  -- | Map of coordinates for which to determine neighbour status
  IntMap (R3 r Double) ->
  -- | Map between an index and indices of its neighbours.
  AtomNeighbourList
neighbourList dist crds = AtomNeighbourList neighbours
 where
  -- unsafePerformIO is safe only if the folding functions is associative!
  semiFoldlP f a0 = unsafePerformIO . foldlP f a0 f a0
  mergeNLs = IntMap.unionWith IntSet.union

  -- Extent: In order to create proper bins (see below),
  -- the extent of the system must be known.
  coordsToExtent c =
    let (x, y, z) = xyzR3 c
     in Extent{xMin = x, xMax = x, yMin = y, yMax = y, zMin = z, zMax = z}
  extents =
    Massiv.fromList @S Par . fmap (coordsToExtent . snd) . IntMap.toList $ crds
  e0 = fromMaybe (Extent 0 0 0 0 0 0) $ sheadM extents
  Extent{..} = semiFoldlP (<>) e0 extents

  -- Bins: The algorithm begins by setting up 3D bins of a certain size.
  -- Atoms are then sorted into these bins.
  binSize = max 3 dist
  binIx (x, y, z) =
    let f a b = floor $ (a - b) / binSize
     in f x xMin :> f y yMin :. f z zMin
  nBins = Sz . liftIndex (+ 1) . binIx $ (xMax, yMax, zMax)
  emptyBins = Massiv.replicate @B Par nBins mempty

  binMatrix = withLoadMArrayST_ @B emptyBins $ \bins ->
    let intoBins k c = modify_ bins (pure . IntSet.insert k) (binIx . xyzR3 $ c)
     in IntMap.traverseWithKey intoBins crds

  -- Neighbours: Compute neighbour status between atoms of adjacent bins.
  neighbourhood3x3 = mapStencil (Fill mempty) neighbourStencil
  neighbourStencil = makeStencil (Sz (3 :> 3 :. 3)) (1 :> 1 :. 1) $ \look ->
    let validIx = [-1 .. 1]
        centre = look $ Ix3 0 0 0
        hoods = [look $ Ix3 x y z | x <- validIx, y <- validIx, z <- validIx]
        allNeighbours = neighboursBetween centre <$> hoods
     in foldl' mergeNLs mempty allNeighbours

  neighboursBetween source targets =
    let neighboursOf i = IntSet.filter (isNeighbour i) targets
        addNeighbours acc i = IntMap.insert i (neighboursOf i) acc
     in IntSet.foldl' addNeighbours mempty source

  isNeighbour i k = isJust $ do
    guard $ i /= k
    a <- crds IntMap.!? i
    b <- crds IntMap.!? k
    guard $ dist >= euclideanDistance a b

  neighboursInBins = compute @B $ neighbourhood3x3 binMatrix
  neighbours = semiFoldlP mergeNLs mempty neighboursInBins

-- | Convenience function: construct a neighbour list directly from a `Molecule`.
neighbourListFromMolecule :: (Molecule mol atom bond) => Double -> mol atom bond -> AtomNeighbourList
neighbourListFromMolecule dist mol =
  neighbourList dist . IntMap.fromList . fmap (second getCoord) . getAtoms $ mol

--------------------------------------------------------------------------------

{- | Convert a 'AtomNeighbourList' to a 'GroupNeighbourList'.
Two groups are neighbours if they contain at least one pair of atoms that are neighbours.
Groups can not be neighbours with themselves.
This function will throw an error if it encounters any unassigned atoms.
-}
mGroupNeighbours ::
  (MonadThrow m) =>
  -- | A map from atom id to group id.
  IntMap GroupId ->
  -- | Neighbour list in the atomic basis.
  AtomNeighbourList ->
  -- | Neighbour list in the group basis.
  m GroupNeighbourList
mGroupNeighbours m (getAtomNeighbourList -> nl) =
  case IntSet.minView $ neighbourKeys `IntSet.difference` atomKeys of
    Nothing ->
      return
        . GroupNeighbourList
        . IntMap.map (IntSet.map f)
        $ IntMap.mapKeysWith IntSet.union f nl
    Just (x, _) ->
      throwM $ UnassignedAtom x
 where
  atomKeys = IntMap.keysSet m
  neighbourKeys = IntMap.keysSet nl <> fold nl
  f = (m IntMap.!)

{- | Convert a 'AtomNeighbourList' to a 'GroupNeighbourList'.
Two groups are neighbours if they contain at least one pair of atoms that are neighbours.
Groups can not be neighbours with themselves.
This function will drop any atoms not present in the atom–to–group mapping.
-}
groupNeighbours ::
  -- | A map from atom id to group id.
  IntMap GroupId ->
  -- | Neighbour list in the atomic basis.
  AtomNeighbourList ->
  -- | Neighbour list in the group basis.
  GroupNeighbourList
groupNeighbours a2g =
  GroupNeighbourList
    . IntMap.mapWithKey (\k -> IntSet.delete k . IntSet.mapMaybe toGroup)
    . IntMap.mapMaybeKeysWith IntSet.union toGroup
    . getAtomNeighbourList
 where
  toGroup = (a2g IntMap.!?)
