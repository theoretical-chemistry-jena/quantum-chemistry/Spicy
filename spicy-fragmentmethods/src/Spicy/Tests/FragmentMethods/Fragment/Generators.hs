module Spicy.Tests.FragmentMethods.Fragment.Generators (
  refFrag,
  genFragments,
) where

import Hedgehog
import RIO.Partial (fromJust)
import Spicy.Chemistry (LewisAtom, LewisBondOrder)
import Spicy.FragmentMethods.Fragment
import Spicy.IntSet qualified as IntSet
import Spicy.Prelude
import Spicy.Tests.FragmentMethods.Group.Generators
import Spicy.Tests.IntSet.Generators
import Spicy.Tests.Set.Generators

-- | Generator for `FCR`s.
genFragments ::
  -- | Number of fragments
  Range Int ->
  -- | Size of fragments
  Range Int ->
  -- | IDs of fragments
  Gen Int ->
  Gen (Set Fragment)
genFragments rn rsf gint = genSet rn (Fragment <$> genIntSet rsf gint)

-- | The aldehyde group of ethanal as a fragment with a hydrogen link atom.
refFrag :: FragGr LewisAtom LewisBondOrder
refFrag = fromJust . mkMolecularFragment @FragGr refGroupGr mkDefLinks . Fragment . IntSet.singleton $ 1
