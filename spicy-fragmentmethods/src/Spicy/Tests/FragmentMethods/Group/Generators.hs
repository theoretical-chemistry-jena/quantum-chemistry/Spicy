module Spicy.Tests.FragmentMethods.Group.Generators (
  genGroupGr,
  refGroupGr,
  ethanalValidGroups,
  TestGraph (..),
  testMoleculeToTestGraph,
  emptyGr,
  h2Gr,
  twoH2Gr,
  ethanalGr,
  nPentaneGr,
  hellmersKönigGr,
  linear4Gr,
) where

import Hedgehog hiding (Group)
import Hedgehog.Gen qualified as Gen
import RIO.Partial (fromJust)
import Spicy.Chemistry (LewisAtom, LewisBondOrder)
import Spicy.Chemistry.Molecule
import Spicy.FragmentMethods.Group
import Spicy.FragmentMethods.Group.Detection
import Spicy.Graph hiding (empty)
import Spicy.IntSet qualified as IntSet
import Spicy.Prelude
import Spicy.Tests.Chemistry.Generators

genGroupGr :: Gen (GroupGr SimpleGroup MolGr LewisAtom LewisBondOrder)
genGroupGr = Gen.choice . fmap pure $ [refGroupGr]

refGroupGr :: GroupGr SimpleGroup MolGr LewisAtom LewisBondOrder
refGroupGr = fromJust $ mkPredefined (getMol ethanal) ethanalValidGroups

ethanalValidGroups :: [LNode SimpleGroup]
ethanalValidGroups = zip [1 ..] $ SimpleGroup . IntSet.fromList <$> [[1, 2, 3], [4, 5, 6, 7]]

data TestGraph = TestGraph
  { name :: String
  , getGraph :: Gr SimpleGroup ()
  }

testMoleculeToTestGraph :: TestMolecule -> TestGraph
testMoleculeToTestGraph TestMolecule{..} =
  TestGraph
    { name
    , getGraph = getGroupGraph @GroupGr . fromJust . mkGroupedMolecule (getMol @MolGr) $ smf
    }

emptyGr :: TestGraph
emptyGr = testMoleculeToTestGraph empty

h2Gr :: TestGraph
h2Gr = testMoleculeToTestGraph h2

twoH2Gr :: TestGraph
twoH2Gr = testMoleculeToTestGraph twoH2

ethanalGr :: TestGraph
ethanalGr = testMoleculeToTestGraph ethanal

nPentaneGr :: TestGraph
nPentaneGr = testMoleculeToTestGraph nPentane

-- | Graph from the FCR publication by Hellmers and König (10.1063/5.0059598).
hellmersKönigGr :: TestGraph
hellmersKönigGr =
  TestGraph
    { name = "Hellmers–König"
    , getGraph =
        undir
          $ mkGraph
            [ (0, SimpleGroup $ IntSet.singleton 0)
            , (1, SimpleGroup $ IntSet.singleton 1)
            , (2, SimpleGroup $ IntSet.singleton 2)
            , (3, SimpleGroup $ IntSet.singleton 3)
            , (4, SimpleGroup $ IntSet.singleton 4)
            ]
            [ (0, 1, ())
            , (0, 2, ())
            , (0, 3, ())
            , (3, 4, ())
            ]
    }

linear4Gr :: TestGraph
linear4Gr =
  TestGraph
    { name = "Linear 4–membered Graph"
    , getGraph =
        undir
          $ mkGraph
            [ (0, SimpleGroup $ IntSet.singleton 0)
            , (1, SimpleGroup $ IntSet.singleton 1)
            , (2, SimpleGroup $ IntSet.singleton 2)
            , (3, SimpleGroup $ IntSet.singleton 3)
            ]
            [ (0, 1, ())
            , (1, 2, ())
            , (2, 3, ())
            ]
    }
