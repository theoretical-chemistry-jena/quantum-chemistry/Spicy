module Spicy.Tests.FragmentMethods (tests) where

import Spicy.Tests.FragmentMethods.Fragment qualified
import Spicy.Tests.FragmentMethods.Group qualified
import Spicy.Tests.FragmentMethods.Group.Detection qualified
import Test.Tasty

tests :: TestTree
tests =
  testGroup
    "FragmentMethods"
    [ Spicy.Tests.FragmentMethods.Group.tests
    , Spicy.Tests.FragmentMethods.Group.Detection.tests
    , Spicy.Tests.FragmentMethods.Fragment.tests
    ]
