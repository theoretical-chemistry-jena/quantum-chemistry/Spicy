module Spicy.Tests.FragmentMethods.Fragment (
  tests,
  refFrag,
) where

import Data.Massiv.Array as Massiv
import RIO.List.Partial qualified as List
import RIO.Partial (fromJust)
import Spicy.Chemistry (LewisBondOrder)
import Spicy.Chemistry.Atom
import Spicy.Chemistry.Element hiding (S)
import Spicy.FiniteCompare
import Spicy.FragmentMethods.Fragment
import Spicy.IntSet qualified as IntSet
import Spicy.Math.LinearAlgebra.R3
import Spicy.Prelude
import Spicy.Tests.FragmentMethods.Fragment.FCR qualified
import Spicy.Tests.FragmentMethods.Fragment.Generation qualified
import Spicy.Tests.FragmentMethods.Group hiding (tests)
import Test.Tasty
import Test.Tasty.HUnit

tests :: TestTree
tests =
  testGroup
    "Fragment"
    [ testCase "Ethanal Group 1" $ do
        let resFrag = mkMolecularFragment @FragGr refGroupGr mkDefLinks . Fragment . IntSet.singleton $ 1
            resLinkAtom = List.head . getLinkInfo <$> resFrag
        (resLinkAtom ^? _Just % _1) @?= Just 4
        (resLinkAtom ^? _Just % _2 % _1 % #element) @?= Just H

        let gVal = RealExpFP @5 <$> resLinkAtom ^? _Just % _2 % _2 % #gVal
            gCheck = RealExpFP @5 0.7039476842
        gVal @?= Just gCheck

        let coord = Massiv.map (RealExpFP @5) . r3 <$> resLinkAtom ^? _Just % _2 % _1 % #coord
            coordCheck = Massiv.map (RealExpFP @5) $ fromList @U Seq [0, -0.7039473, 0]
        coord @?= Just coordCheck
    , Spicy.Tests.FragmentMethods.Fragment.Generation.tests
    , Spicy.Tests.FragmentMethods.Fragment.FCR.tests
    ]

-- | The aldehyde group of ethanal as a fragment with a hydrogen link atom.
refFrag :: FragGr LewisAtom LewisBondOrder
refFrag = fromJust $ mkMolecularFragment @FragGr refGroupGr mkDefLinks one

one :: Fragment
one = Fragment $ IntSet.singleton 1
