module Spicy.Tests.FragmentMethods.Fragment.FCR (
  tests,
) where

import Hedgehog hiding (Group)
import Hedgehog.Gen qualified as Gen
import Hedgehog.Range qualified as Range
import RIO.Set qualified as Set
import Spicy.FragmentMethods.Fragment
import Spicy.FragmentMethods.Fragment.Generation.Internal as Internal
import Spicy.HashMap qualified as HashMap
import Spicy.IntSet qualified as IntSet
import Spicy.Prelude
import Spicy.Tests.FragmentMethods.Fragment.Generators
import Spicy.Tests.FragmentMethods.Group.Generators
import Spicy.Tests.IntSet.Generators
import Spicy.Tests.Set.Generators
import Test.Tasty
import Test.Tasty.HUnit
import Test.Tasty.Hedgehog

tests :: TestTree
tests =
  testGroup
    "Fragment Combination Ranges"
    [ allSubsetsIncluded
    , groupCoefficientSum
    , hellmersKönigFCR
    , hellmersKönigMLFCR
    , extendedHKMLFCR
    , mlFCRhighestLevel
    , mlFCRgroupContributions
    , mlFCRfragmentContributions
    ]

-- | Check if an FCR built from random fragments contains all subsets.
allSubsetsIncluded :: TestTree
allSubsetsIncluded = testProperty "Closed on forming subsets" . property $ do
  intsets <-
    forAll
      $ genSet
        (Range.linear 0 100)
        (genIntSet (Range.linear 0 10) (Gen.int $ Range.linear 0 100))
  subsets <- forAll $ traverse subIntSet (Set.toList intsets)
  let fcr = mkFCR $ Set.map Fragment intsets
  for_ subsets $ \sub -> Hedgehog.assert (sub `Set.member` Set.map getFragment (getFCR fcr))

-- Do the coefficients for all groups sum up to 1?
groupCoefficientSum :: TestTree
groupCoefficientSum = testProperty "Groups contribute once" . property $ do
  fragments <-
    forAll
      $ genFragments
        (Range.linear 0 100)
        (Range.linear 0 10)
        (Gen.int $ Range.linear 0 10)
  let fcr = mkFCR fragments
      fcrC = fcrCoefficients fcr
      contributions = groupContributions [fcrC]
  annotateShow fcr
  annotateShow fcrC
  annotateShow contributions
  traverse_ (=== 1) contributions

groupContributions :: [HashMap Fragment Int] -> HashMap Int Int
groupContributions m =
  HashMap.unionsWith (+)
    $ HashMap.foldrWithKey
      (\(getFragment -> k) v acc -> HashMap.unionWith (+) acc . HashMap.fromList $ (,v) <$> IntSet.toList k)
      HashMap.empty
    <$> m

everyGroupOnce :: [HashMap Fragment Int] -> Bool
everyGroupOnce = all (== 1) . groupContributions

fragmentFromList :: [Int] -> Fragment
fragmentFromList = Fragment . IntSet.fromList

-- Example from H.–K. paper
hellmersKönigFCR :: TestTree
hellmersKönigFCR =
  testGroup
    "Hellmers–König graph"
    [ testGroup
        "Hierarchical"
        [ hcfcr
            "Monomers"
            1
            [ (fragmentFromList [0], 1)
            , (fragmentFromList [1], 1)
            , (fragmentFromList [2], 1)
            , (fragmentFromList [3], 1)
            , (fragmentFromList [4], 1)
            ]
        , hcfcr
            "Dimers"
            2
            [ (fragmentFromList [0], -3)
            , (fragmentFromList [1], -3)
            , (fragmentFromList [2], -3)
            , (fragmentFromList [3], -3)
            , (fragmentFromList [4], -3)
            , (fragmentFromList [0, 1], 1)
            , (fragmentFromList [0, 2], 1)
            , (fragmentFromList [0, 3], 1)
            , (fragmentFromList [0, 4], 1)
            , (fragmentFromList [1, 2], 1)
            , (fragmentFromList [1, 3], 1)
            , (fragmentFromList [1, 4], 1)
            , (fragmentFromList [2, 3], 1)
            , (fragmentFromList [2, 4], 1)
            , (fragmentFromList [3, 4], 1)
            ]
        , hcfcr
            "Trimers"
            3
            [ (fragmentFromList [0], 3)
            , (fragmentFromList [1], 3)
            , (fragmentFromList [2], 3)
            , (fragmentFromList [3], 3)
            , (fragmentFromList [4], 3)
            , (fragmentFromList [0, 1], -2)
            , (fragmentFromList [0, 2], -2)
            , (fragmentFromList [0, 3], -2)
            , (fragmentFromList [0, 4], -2)
            , (fragmentFromList [1, 2], -2)
            , (fragmentFromList [1, 3], -2)
            , (fragmentFromList [1, 4], -2)
            , (fragmentFromList [2, 3], -2)
            , (fragmentFromList [2, 4], -2)
            , (fragmentFromList [3, 4], -2)
            , (fragmentFromList [0, 1, 2], 1)
            , (fragmentFromList [0, 1, 3], 1)
            , (fragmentFromList [0, 1, 4], 1)
            , (fragmentFromList [0, 2, 3], 1)
            , (fragmentFromList [0, 2, 4], 1)
            , (fragmentFromList [0, 3, 4], 1)
            , (fragmentFromList [1, 2, 3], 1)
            , (fragmentFromList [1, 2, 4], 1)
            , (fragmentFromList [1, 3, 4], 1)
            , (fragmentFromList [2, 3, 4], 1)
            ]
        , hcfcr
            "Quadramers"
            4
            [ (fragmentFromList [0], -1)
            , (fragmentFromList [1], -1)
            , (fragmentFromList [2], -1)
            , (fragmentFromList [3], -1)
            , (fragmentFromList [4], -1)
            , (fragmentFromList [0, 1], 1)
            , (fragmentFromList [0, 2], 1)
            , (fragmentFromList [0, 3], 1)
            , (fragmentFromList [0, 4], 1)
            , (fragmentFromList [1, 2], 1)
            , (fragmentFromList [1, 3], 1)
            , (fragmentFromList [1, 4], 1)
            , (fragmentFromList [2, 3], 1)
            , (fragmentFromList [2, 4], 1)
            , (fragmentFromList [3, 4], 1)
            , (fragmentFromList [0, 1, 2], -1)
            , (fragmentFromList [0, 1, 3], -1)
            , (fragmentFromList [0, 1, 4], -1)
            , (fragmentFromList [0, 2, 3], -1)
            , (fragmentFromList [0, 2, 4], -1)
            , (fragmentFromList [0, 3, 4], -1)
            , (fragmentFromList [1, 2, 3], -1)
            , (fragmentFromList [1, 2, 4], -1)
            , (fragmentFromList [1, 3, 4], -1)
            , (fragmentFromList [2, 3, 4], -1)
            , (fragmentFromList [0, 1, 2, 3], 1)
            , (fragmentFromList [0, 1, 2, 4], 1)
            , (fragmentFromList [0, 1, 3, 4], 1)
            , (fragmentFromList [0, 2, 3, 4], 1)
            , (fragmentFromList [1, 2, 3, 4], 1)
            ]
        , hcfcr
            "Quintamers"
            5
            [ (fragmentFromList [0, 1, 2, 3, 4], 1)
            ]
        ]
    ]
 where
  hcfcr s n l = testCase s $ do
    let nmers = Internal.nMers n (Internal.groupsAsFragments $ getGraph hellmersKönigGr)
        fcr = fcrCoefficients . mkFCR $ nmers
    assertEqual
      "Not equal to expected FCR"
      (HashMap.fromList l)
      (HashMap.delete mempty fcr)

mlFCRgroupContributions :: TestTree
mlFCRgroupContributions = testProperty "Every group contributes exactly once" . property $ do
  layers <-
    forAll
      . Gen.list (Range.linear 0 10)
      $ genFragments
        (Range.linear 0 100)
        (Range.linear 0 10)
        (Gen.int $ Range.linear 0 10)
  let coeffs = mlCoefficients $ layers
  Hedgehog.assert $ everyGroupOnce coeffs

mlFCRfragmentContributions :: TestTree
mlFCRfragmentContributions = testProperty "Total fragment contributions are same as single layer" . property $ do
  layers <-
    forAll
      . Gen.list (Range.linear 0 10)
      $ genFragments
        (Range.linear 0 100)
        (Range.linear 0 10)
        (Gen.int $ Range.linear 0 10)
  let coeffs = mlCoefficients $ layers
      singleLayer = fcrCoefficients . mkFCR . Set.unions $ layers
      totalCoeffs = HashMap.filter (/= 0) $ HashMap.unionsWith (+) coeffs
  annotateShow coeffs
  totalCoeffs === singleLayer

mlFCRhighestLevel :: TestTree
mlFCRhighestLevel = testProperty "Coefficients for the highest level are unchanged" . property $ do
  layers <-
    forAll
      . Gen.list (Range.linear 0 10)
      $ genFragments
        (Range.linear 0 100)
        (Range.linear 0 10)
        (Gen.int $ Range.linear 0 10)
  let coeffs = mlCoefficients $ layers
  case (layers, coeffs) of
    ([], []) -> success
    (x : _, y : _) -> do
      let singleLayer = fcrCoefficients . mkFCR $ x
      singleLayer === y
    _ -> failure

hellmersKönigMLFCR :: TestTree
hellmersKönigMLFCR = testCase "Hellmers–König MLFCR" $ do
  let hl = Set.fromList [fragmentFromList [0, 1, 2], fragmentFromList [1, 2, 3]]
  let ll =
        Set.fromList
          [ fragmentFromList [0, 1]
          , fragmentFromList [0, 2]
          , fragmentFromList [0, 3]
          , fragmentFromList [1, 2]
          , fragmentFromList [1, 3]
          , fragmentFromList [2, 3]
          ]
      coeffs = mlCoefficients (hl : [ll])
      expected =
        [ HashMap.fromList
            [ (fragmentFromList [0, 1, 2], 1)
            , (fragmentFromList [1, 2, 3], 1)
            , (fragmentFromList [1, 2], -1)
            ]
        , HashMap.fromList
            [ (fragmentFromList [0], -1)
            , (fragmentFromList [3], -1)
            , (fragmentFromList [0, 3], 1)
            ]
        ]
      dbg =
        unlines
          [ "Coefficients:\n" <> show coeffs
          , "FCR Coefficients hl:\n" <> show hl
          , "FCR Coefficients ll:\n" <> show ll
          , "FCR Coefficients hl ∪ ll:\n" <> show (Set.union hl ll)
          ]
  assertEqual
    dbg
    expected
    (HashMap.delete mempty <$> coeffs)
  assertBool
    dbg
    (everyGroupOnce coeffs)

extendedHKMLFCR :: TestTree
extendedHKMLFCR = testCase "3 Layer MLFCR" $ do
  let l1 =
        Set.fromList
          [ fragmentFromList [0, 1]
          , fragmentFromList [1, 2]
          ]
      l2 =
        Set.fromList
          [ fragmentFromList [1, 2, 3]
          ]
      l3 =
        Set.fromList
          [ fragmentFromList [0, 1]
          , fragmentFromList [0, 2]
          , fragmentFromList [0, 3]
          , fragmentFromList [1, 2]
          , fragmentFromList [1, 3]
          , fragmentFromList [2, 3]
          ]
      coeffs = mlCoefficients [l1, l2, l3]
      expected =
        [ HashMap.fromList
            [ (fragmentFromList [0, 1], 1)
            , (fragmentFromList [1, 2], 1)
            , (fragmentFromList [1], -1)
            ]
        , HashMap.fromList
            [ (fragmentFromList [1, 2, 3], 1)
            , (fragmentFromList [1, 2], -1)
            ]
        , HashMap.fromList
            [ (fragmentFromList [0, 2], 1)
            , (fragmentFromList [0, 3], 1)
            , (fragmentFromList [0], -2)
            , (fragmentFromList [2], -1)
            , (fragmentFromList [3], -1)
            ]
        ]
      dbg =
        unlines
          [ "Coefficients:\n" <> show coeffs
          , "FCR Coefficients l1:\n" <> show l1
          , "FCR Coefficients l2:\n" <> show l2
          , "FCR Coefficients l3:\n" <> show l3
          , "FCR Coefficients l1 ∪ l2:\n" <> show (Set.unions [l1, l2])
          , "FCR Coefficients l1 ∪ l2 ∪ l3:\n" <> show (Set.unions [l1, l2, l3])
          ]
  assertEqual
    dbg
    expected
    (HashMap.delete mempty <$> coeffs)
  assertBool
    dbg
    (everyGroupOnce coeffs)
