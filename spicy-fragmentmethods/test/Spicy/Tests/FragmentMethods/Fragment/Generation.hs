module Spicy.Tests.FragmentMethods.Fragment.Generation (
  tests,
) where

import RIO.Set qualified as Set
import Spicy.Chemistry.Molecule
import Spicy.FragmentMethods.Fragment.Generation.Internal
import Spicy.FragmentMethods.Fragment.Types
import Spicy.FragmentMethods.Group
import Spicy.Graph
import Spicy.IntMap qualified as IntMap
import Spicy.IntSet qualified as IntSet
import Spicy.Math.Neighbours
import Spicy.Prelude
import Spicy.Tests.Chemistry.Generators
import Spicy.Tests.FragmentMethods.Fragment.Generators
import Spicy.Tests.FragmentMethods.Group.Generators
import Test.Tasty
import Test.Tasty.HUnit

tests :: TestTree
tests =
  testGroup
    "Fragment Generation"
    -- Todo: property test – nMers 1 is always nMersWithNeighbours 1
    -- Groups as Fragments == nMers 1 == ego 0
    [ toFragmentationTest
        emptyGr
        [nFragments 0]
        [ FragmentationSchemeTests
            "Groups as Fragments"
            id
            []
        , FragmentationSchemeTests
            "Monomers"
            (nMers 1)
            []
        , FragmentationSchemeTests
            "Ego–graphs, size 1"
            (egoN 1 (getGraph emptyGr))
            []
        ]
    , toFragmentationTest
        h2Gr
        [ leftoverAtoms []
        , fragmentsPresent [[1, 2]]
        , atomsTogether [[1, 2]]
        ]
        [ FragmentationSchemeTests
            "Groups as Fragments"
            id
            [ nFragments 1
            ]
        , FragmentationSchemeTests
            "Monomers"
            (nMers 1)
            [ nFragments 1
            ]
        , FragmentationSchemeTests
            "Dimers"
            (nMers 2)
            [ nFragments 1
            ]
        , FragmentationSchemeTests
            "Ego–graphs, size 2"
            (egoN 2 (getGraph h2Gr))
            [ nFragments 1
            ]
        ]
    , toFragmentationTest
        twoH2Gr
        [ leftoverAtoms []
        , atomsTogether [[1, 2], [3, 4]]
        ]
        [ FragmentationSchemeTests
            "Groups as Fragments"
            id
            [ nFragments 2
            , fragmentsPresent [[1, 2], [3, 4]]
            ]
        , FragmentationSchemeTests
            "Monomers"
            (nMers 1)
            [ nFragments 2
            , fragmentsPresent [[1, 2], [3, 4]]
            , atomsApart [[1, 3], [1, 4], [2, 3], [2, 4]]
            ]
        , FragmentationSchemeTests
            "Dimers"
            (nMers 2)
            [ nFragments 1
            , fragmentsPresent [[1, 2, 3, 4]]
            ]
        , FragmentationSchemeTests
            "Dimers with 10A cutoff"
            (nMersWithNeighbours nlTwoH2_10A 2)
            [ nFragments 2
            , fragmentsPresent [[1, 2], [3, 4]]
            , atomsApart [[1, 3], [1, 4], [2, 3], [2, 4]]
            ]
        , FragmentationSchemeTests
            "Dimers with 1kA cutoff"
            (nMersWithNeighbours nlTwoH2_1kA 2)
            [ nFragments 1
            , fragmentsPresent [[1, 2, 3, 4]]
            ]
        , FragmentationSchemeTests
            "Ego–graphs, Size 1"
            (egoN 1 (getGraph twoH2Gr))
            [ nFragments 2
            , fragmentsPresent [[1, 2], [3, 4]]
            , atomsApart [[1, 3], [1, 4], [2, 3], [2, 4]]
            ]
        , FragmentationSchemeTests
            "Ego–graphs, Size 2"
            (egoN 2 (getGraph twoH2Gr))
            [ nFragments 2
            , fragmentsPresent [[1, 2], [3, 4]]
            , atomsApart [[1, 3], [1, 4], [2, 3], [2, 4]]
            ]
        ]
    , toFragmentationTest
        ethanalGr
        [ leftoverAtoms []
        , atomsTogether [[1, 2, 3], [4, 5, 6, 7]]
        ]
        [ FragmentationSchemeTests
            "Groups as Fragments"
            id
            [ nFragments 2
            , fragmentsPresent [[1, 2, 3], [4, 5, 6, 7]]
            ]
        , FragmentationSchemeTests
            "Monomers"
            (nMers 1)
            [ nFragments 2
            , fragmentsPresent [[1, 2, 3], [4, 5, 6, 7]]
            ]
        , FragmentationSchemeTests
            "Dimers"
            (nMers 2)
            [ nFragments 1
            , fragmentsPresent [[1, 2, 3, 4, 5, 6, 7]]
            ]
        , FragmentationSchemeTests
            "Trimers"
            (nMers 3)
            [ nFragments 1
            , fragmentsPresent [[1, 2, 3, 4, 5, 6, 7]]
            ]
        , FragmentationSchemeTests
            "Ego–Graphs, Size 1"
            (egoN 1 (getGraph ethanalGr))
            [ nFragments 1
            , fragmentsPresent [[1, 2, 3, 4, 5, 6, 7]]
            ]
        , FragmentationSchemeTests
            "Ego–Graphs, Size 2"
            (egoN 2 (getGraph ethanalGr))
            [ nFragments 1
            , fragmentsPresent [[1, 2, 3, 4, 5, 6, 7]]
            ]
        ]
    , toFragmentationTest
        nPentaneGr
        [ atomsTogether
            [ [1, 6, 7, 8]
            , [2, 9, 10]
            , [3, 11, 12]
            , [4, 13, 14]
            , [5, 15, 16, 17]
            ]
        , leftoverAtoms []
        ]
        [ FragmentationSchemeTests
            "Groups as Fragments"
            id
            [ nFragments 5
            , fragmentsPresent
                [ [1, 6, 7, 8]
                , [2, 9, 10]
                , [3, 11, 12]
                , [4, 13, 14]
                , [5, 15, 16, 17]
                ]
            ]
        , FragmentationSchemeTests
            "Monomers"
            (nMers 1)
            [ nFragments 5
            , fragmentsPresent
                [ [1, 6, 7, 8]
                , [2, 9, 10]
                , [3, 11, 12]
                , [4, 13, 14]
                , [5, 15, 16, 17]
                ]
            ]
        , FragmentationSchemeTests
            "Dimers"
            (nMers 2)
            [ nFragments 10
            ]
        , FragmentationSchemeTests
            "Dimers with 2 A cutoff"
            (nMersWithNeighbours nlNPentane_2A 2)
            [ nFragments 7
            , atomsApart [[1, 4], [1, 5]]
            ]
        , FragmentationSchemeTests
            "Dimers with 3 A cutoff"
            (nMersWithNeighbours nlNPentane_3A 2)
            [ nFragments 9
            , atomsApart [[1, 5]]
            ]
        , FragmentationSchemeTests
            "Ego–Graphs, Size 1"
            (egoN 1 (getGraph nPentaneGr))
            [ nFragments 5
            , fragmentsPresent
                [ [1, 2, 6, 7, 8, 9, 10]
                , [1, 2, 3, 6, 7, 8, 9, 10, 11, 12]
                ]
            , atomsApart [[1, 4], [2, 5]]
            ]
        , FragmentationSchemeTests
            "Ego–Graphs, Size 2"
            (egoN 2 (getGraph nPentaneGr))
            [ nFragments 5
            , fragmentsPresent
                [ [1, 2, 3, 6, 7, 8, 9, 10, 11, 12]
                , [1 .. 17]
                ]
            ]
        ]
    , toFragmentationTest
        hellmersKönigGr
        []
        [ FragmentationSchemeTests
            "Groups as Fragments"
            id
            [ nFragments 5
            , fragmentsPresent [[0], [1], [2], [3], [4]]
            ]
        , FragmentationSchemeTests
            "Monomers"
            (nMers 1)
            [ nFragments 5
            , fragmentsPresent [[0], [1], [2], [3], [4]]
            ]
        , FragmentationSchemeTests
            "Dimers"
            (nMers 2)
            [ nFragments 10
            , fragmentsPresent
                [ [0, 1]
                , [0, 2]
                , [0, 3]
                , [0, 4]
                , [1, 2]
                , [1, 3]
                , [1, 4]
                , [2, 3]
                , [2, 4]
                , [3, 4]
                ]
            ]
        , FragmentationSchemeTests
            "Trimers"
            (nMers 3)
            [ nFragments 10
            , fragmentsPresent
                [ [0, 1, 2]
                , [0, 1, 3]
                , [0, 1, 4]
                , [0, 2, 3]
                , [0, 2, 4]
                , [0, 3, 4]
                , [1, 2, 3]
                , [1, 2, 4]
                , [1, 3, 4]
                , [2, 3, 4]
                ]
            ]
        , FragmentationSchemeTests
            "Quadramers"
            (nMers 4)
            [ nFragments 5
            , fragmentsPresent
                [ [0, 1, 2, 3]
                , [0, 1, 2, 4]
                , [0, 1, 3, 4]
                , [0, 2, 3, 4]
                , [1, 2, 3, 4]
                ]
            ]
        , FragmentationSchemeTests
            "Quintamers"
            (nMers 5)
            [ nFragments 1
            , fragmentsPresent [[0, 1, 2, 3, 4]]
            ]
        , FragmentationSchemeTests
            "Ego–Graphs, Size 1"
            (egoN 1 (getGraph hellmersKönigGr))
            [ nFragments 5
            , fragmentsPresent [[0, 1, 2, 3], [0, 1], [0, 2], [0, 3, 4], [3, 4]]
            ]
        , FragmentationSchemeTests
            "Ego–Graphs, Size 2"
            (egoN 2 (getGraph hellmersKönigGr))
            [ nFragments 3
            , fragmentsPresent [[0, 1, 2, 3, 4], [0, 1, 2, 3], [0, 3, 4]]
            ]
        ]
    ]
 where
  nlTwoH2_10A =
    groupNeighbours
      ( fmap IntSet.findMin
          . IntMap.invertInts
          . fmap getGroup
          . IntMap.fromList
          . labNodes
          . getGraph
          $ twoH2Gr
      )
      $ neighbourListFromMolecule 10 (getMol twoH2 @MolGr)
  nlTwoH2_1kA =
    groupNeighbours
      ( fmap IntSet.findMin
          . IntMap.invertInts
          . fmap getGroup
          . IntMap.fromList
          . labNodes
          . getGraph
          $ twoH2Gr
      )
      $ neighbourListFromMolecule 1000 (getMol twoH2 @MolGr)
  nlNPentane_2A =
    groupNeighbours
      ( fmap IntSet.findMin
          . IntMap.invertInts
          . fmap getGroup
          . IntMap.fromList
          . labNodes
          . getGraph
          $ nPentaneGr
      )
      $ neighbourListFromMolecule 2 (getMol nPentane @MolGr)
  nlNPentane_3A =
    groupNeighbours
      ( fmap IntSet.findMin
          . IntMap.invertInts
          . fmap getGroup
          . IntMap.fromList
          . labNodes
          . getGraph
          $ nPentaneGr
      )
      $ neighbourListFromMolecule 3 (getMol nPentane @MolGr)

type FragmentationTest = Gr SimpleGroup () -> Set Fragment -> TestTree

data FragmentationSchemeTests = FragmentationSchemeTests
  { schemeName :: String
  , fragmentationScheme :: Set Fragment -> Set Fragment
  , fragmentationTests :: [FragmentationTest]
  }

toFragmentationTest :: TestGraph -> [FragmentationTest] -> [FragmentationSchemeTests] -> TestTree
toFragmentationTest TestGraph{..} alwys xs =
  testGroup
    name
    (realizeScheme <$> xs)
 where
  realizeScheme FragmentationSchemeTests{..} =
    let frgs = fragmentationScheme $ groupsAsFragments getGraph
     in testGroup schemeName
          $ ((\f -> f getGraph frgs) <$> alwys)
          <> ((\f -> f getGraph frgs) <$> fragmentationTests)

nFragments :: Int -> FragmentationTest
nFragments n _ s =
  testCase "Number of Fragments"
    $ assertEqual
      debugString
      n
      (Set.size s)
 where
  debugString = "Fragments: " <> show (Set.toList s)

atomsTogether :: [[Int]] -> FragmentationTest
atomsTogether clusters' gr frgs =
  testCase "Designated atoms together"
    . assertBool debugString
    $ all (allTogetherIn clusters) frgmntdAtms
 where
  debugString =
    unlines
      [ "Expected clusters: " <> show clusters'
      , "Fragments (Atom basis): " <> show (Set.toList frgmntdAtms)
      ]
  clusters = Set.fromList . fmap IntSet.fromList $ clusters'
  frgmntdAtms = Set.map (fragmentToAtoms gr) frgs
  allTogetherIn :: Set IntSet -> IntSet -> Bool
  allTogetherIn atomSets frg = all (\atms -> atms `IntSet.isSubsetOf` frg || atms `IntSet.disjoint` frg) atomSets

atomsApart :: [[Int]] -> FragmentationTest
atomsApart clusters' gr frgs =
  testCase "Designated atoms separated"
    . assertBool debugString
    $ all (allTogetherIn clusters) frgmntdAtms
 where
  debugString =
    unlines
      [ "Expected clusters: " <> show clusters'
      , "Fragments (Atom basis): " <> show (Set.toList frgmntdAtms)
      ]
  clusters = Set.fromList . fmap IntSet.fromList $ clusters'
  frgmntdAtms = Set.map (fragmentToAtoms gr) frgs
  allTogetherIn :: Set IntSet -> IntSet -> Bool
  allTogetherIn atomSets frg = all (\atms -> not $ atms `IntSet.isSubsetOf` frg) atomSets

-- Group and fragment numbering is arbitrary – Thus, check for atoms
-- (which are predefined, see TestMolecules.hs)
fragmentsPresent :: [[Int]] -> FragmentationTest
fragmentsPresent wanted' gr frgs =
  testCase "Expected fragments exist"
    $ assertBool
      debugString
      (Set.isSubsetOf wanted frgmntdAtms)
 where
  debugString =
    unlines
      [ "Fragments (atom basis): " <> show (Set.toList . Set.map IntSet.toList $ frgmntdAtms)
      , "Expected fragments: " <> show wanted'
      ]
  wanted = Set.fromList $ IntSet.fromList <$> wanted'
  frgmntdAtms = Set.map (fragmentToAtoms gr) frgs

-- Check if *only* the selected atoms are not in any fragments.
leftoverAtoms :: [Int] -> FragmentationTest
leftoverAtoms leftAtms gr frgs =
  testCase "Leftover Atoms"
    $ assertEqual
      debugString
      (IntSet.fromList leftAtms)
      (allAtms `IntSet.difference` frgmntdAtms)
 where
  debugString =
    unlines
      [ "Fragments: " <> show frgs
      , "Atoms in Fragments: " <> show frgmntdAtms
      , "All atoms: " <> show allAtms
      , "Graph: " <> show gr
      ]
  frgmntdAtms = fragmentsToAtoms gr frgs
  allAtms = foldMap (getGroup . snd) (labNodes gr)

-- Convert a set of fragments to a set of atoms via the group -> atom mapping in the group graph.
fragmentsToAtoms :: Gr SimpleGroup () -> Set Fragment -> IntSet
fragmentsToAtoms gr frgs = IntSet.unions $ Set.map (fragmentToAtoms gr) frgs

fragmentToAtoms :: Gr SimpleGroup () -> Fragment -> IntSet
fragmentToAtoms gr (getFragment -> frg) =
  ufold (\(_, _, g, _) mp -> IntSet.union mp (getGroup g)) IntSet.empty
    $ subgraph (IntSet.toList frg) gr
