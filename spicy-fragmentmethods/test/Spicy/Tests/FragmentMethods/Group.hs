module Spicy.Tests.FragmentMethods.Group (tests, refGroupGr, genGroupGr) where

import Data.Aeson as Aeson
import Data.Binary as Binary
import Hedgehog hiding (Group)
import Spicy.Chemistry (LewisAtom (LewisAtom))
import Spicy.Chemistry.Bond
import Spicy.Chemistry.Molecule
import Spicy.FragmentMethods
import Spicy.Graph
import Spicy.IntSet qualified as IntSet
import Spicy.Prelude
import Spicy.Tests.Chemistry.Generators
import Spicy.Tests.FragmentMethods.Group.Generators
import Test.Tasty
import Test.Tasty.HUnit
import Test.Tasty.Hedgehog (testProperty)

tests :: TestTree
tests =
  testGroup
    "Group"
    [ testGroup
        "GroupGr MolGr"
        [ testGroup
            "checkGroups"
            [ testCase "Ethanal"
                $ let res = mkPredefined (getMol ethanal) ethanalValidGroups :: Maybe (GroupGr SimpleGroup MolGr LewisAtom LewisBondOrder)
                      expc = ethanalGroupGr
                   in (getGroupGraph <$> res) @?= pure expc
            , testCase "Reject overlapping groups (Ethanal)"
                $ let res = mkPredefined (getMol ethanal) ethanalOvlpGroups :: Maybe (GroupGr SimpleGroup MolGr LewisAtom LewisBondOrder)
                   in res @?= Nothing
            , testCase "Reject incomplete groups (Ethanal)"
                $ let res = mkPredefined (getMol ethanal) ethanalIncompleteGroups :: Maybe (GroupGr SimpleGroup MolGr LewisAtom LewisBondOrder)
                   in res @?= Nothing
            ]
        , testGroup
            "Serialisation"
            [ testProperty "JSON trip" . property $ do
                groupGr <- forAll genGroupGr
                tripping groupGr Aeson.encode Aeson.decode
            , testProperty "Binary trip" . property $ do
                groupGr <- forAll genGroupGr
                tripping groupGr Binary.encode (pure @Maybe . Binary.decode)
            ]
        ]
    ]

ethanalGroupGr :: Gr SimpleGroup ()
ethanalGroupGr = mkGraph ethanalValidGroups [(1, 2, ()), (2, 1, ())]

ethanalMultiComponentGroups :: [LNode SimpleGroup]
ethanalMultiComponentGroups = zip [1 ..] $ SimpleGroup . IntSet.fromList <$> [[1, 2, 3], [4], [5, 6, 7]]

ethanalOvlpGroups :: [LNode SimpleGroup]
ethanalOvlpGroups = zip [1 ..] $ SimpleGroup . IntSet.fromList <$> [[1, 2, 3, 4], [4, 5, 6, 7]]

ethanalIncompleteGroups :: [LNode SimpleGroup]
ethanalIncompleteGroups = zip [1 ..] $ SimpleGroup . IntSet.fromList <$> [[1, 2, 3], [5, 6, 7]]
