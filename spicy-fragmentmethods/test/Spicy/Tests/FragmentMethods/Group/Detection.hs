module Spicy.Tests.FragmentMethods.Group.Detection (tests) where

import RIO.Partial (fromJust)
import RIO.Set qualified as Set
import Spicy.Chemistry
import Spicy.Chemistry.Molecule.Internal
import Spicy.FragmentMethods.Group
import Spicy.FragmentMethods.Group.Detection
import Spicy.Graph
import Spicy.IntSet qualified as IntSet
import Spicy.Math.LinearAlgebra.R3
import Spicy.Prelude
import Spicy.Tests.Chemistry.Generators
import Test.Tasty
import Test.Tasty.HUnit

tests :: TestTree
tests =
  testGroup
    "Group Detection"
    [ testGroup
        "Disjoint Molecules"
        [ toDisjointMoleculeTest h2 [nGroups 1]
        , toDisjointMoleculeTest
            twoH2
            [ nGroups 2
            , connectionsAbsent [(1, 3)]
            ]
        , toDisjointMoleculeTest ethanal [nGroups 1]
        , toDisjointMoleculeTest nPentane [nGroups 1]
        ]
    , testGroup
        "SMF"
        [ toSMFTest
            h2
            [ nGroups 1
            , nodesPresent [[1, 2]]
            ]
        , toSMFTest
            twoH2
            [ nGroups 2
            , nodesPresent [[1, 2], [3, 4]]
            ]
        , toSMFTest
            ethanal
            [ nGroups 2
            , nodesPresent [[1, 2, 3], [4, 5, 6, 7]]
            , connectionsPresent [(1, 4)]
            ]
        , toSMFTest
            nPentane
            [ nGroups 5
            , nodesPresent
                [ [1, 6, 7, 8]
                , [2, 9, 10]
                , [3, 11, 12]
                , [4, 13, 14]
                , [5, 15, 16, 17]
                ]
            , connectionsPresent [(1, 2), (2, 3), (3, 4), (4, 5)]
            ]
        , toSMFTest
            nPentaneCharged
            [ nGroups 3
            , nodesPresent
                [ [1, 6, 7, 8]
                , [2, 3, 4, 9, 10, 11, 12, 13, 14]
                , [5, 15, 16, 17]
                ]
            , connectionsPresent [(1, 2), (4, 5)]
            ]
        , toSMFTest
            nPentanePolarized
            [ nGroups 3
            , nodesPresent
                [ [1, 6, 7, 8]
                , [2, 3, 4, 9, 10, 11, 12, 13, 14]
                , [5, 15, 16, 17]
                ]
            , connectionsPresent [(1, 2), (4, 5)]
            ]
        , toSMFTest
            nPentadi_1_4_ene
            [ nGroups 3
            , nodesPresent
                [ [1, 2, 6, 7, 8, 9, 10]
                , [3, 11, 12]
                , [4, 5, 13, 14, 15, 16, 17]
                ]
            , connectionsPresent [(2, 3), (3, 4)]
            ]
        ]
    ]
 where
  nPentaneCharged =
    TestMolecule
      { name = "n-Pentane (charged)"
      , getMol =
          modifyAtom'
            (getMol nPentane)
            (3, Just $ LewisAtom C (Just "C3") (mkR3T (0, 0, 2)) 1 0)
      }
  nPentanePolarized =
    TestMolecule
      { name = "n-Pentane (polarized)"
      , getMol =
          modifyAtom'
            (getMol nPentane)
            (3, Just $ LewisAtom C (Just "C3") (mkR3T (0, 0, 2)) 0 1)
      }
  nPentadi_1_4_ene =
    TestMolecule
      { name = "n-Pentadi-1,4-ene"
      , getMol =
          modifyBonds
            (getMol nPentane)
            [(1, 2, Just (CovalentBond 2)), (4, 5, Just (CovalentBond 2))]
      }

toSMFTest :: TestMolecule -> [GroupGr SimpleGroup MolGr LewisAtom LewisBondOrder -> TestTree] -> TestTree
toSMFTest = toGroupDetectionTest smf

toDisjointMoleculeTest :: TestMolecule -> [GroupGr SimpleGroup MolGr LewisAtom LewisBondOrder -> TestTree] -> TestTree
toDisjointMoleculeTest = toGroupDetectionTest disjointMolecules

toGroupDetectionTest ::
  ( MkGroupedMolecule groupMol grp mol LewisAtom LewisBondOrder
  , Foldable f1
  , Foldable f2
  , MkMolecule mol LewisAtom LewisBondOrder
  , DynTopoMolecule mol LewisAtom LewisBondOrder
  ) =>
  (mol LewisAtom LewisBondOrder -> (f1 (LNode grp), f2 (LEdge ()))) ->
  TestMolecule ->
  [groupMol grp mol LewisAtom LewisBondOrder -> TestTree] ->
  TestTree
toGroupDetectionTest f TestMolecule{getMol = testMol, name} tsts =
  let groupedMol = fromJust $ mkGroupedMolecule testMol f
      sanityCheck =
        testCase "Sane Groups"
          $ assertBool "Groups not Sane!" (isJust $ checkGroups groupedMol)
   in testGroup name $ sanityCheck : (($ groupedMol) <$> tsts)

nGroups :: (GroupedMolecule groupMol grp mol atom bond) => Int -> groupMol grp mol atom bond -> TestTree
nGroups i m =
  testCase "Number of Groups"
    $ assertEqual "Number of Groups" i (length . getGroups $ m)

nodesPresent ::
  (GroupedMolecule groupMol grp mol atom bond, Show atom, Show bond) =>
  [[Int]] ->
  groupMol grp mol atom bond ->
  TestTree
nodesPresent xs0 gr =
  testCase "Required nodes present"
    $ let
        xs = Set.fromList . fmap IntSet.fromList $ xs0
        grps = Set.fromList . fmap (getGroup . snd) . getGroups $ gr
        missingNodes = xs `Set.difference` grps
        errMsg =
          "Missing nodes. Should have nodes:\n"
            <> show xs
            <> "\nBut has nodes:\n"
            <> show grps
            <> "\nMissing Nodes:\n"
            <> show missingNodes
            <> "\n"
            <> show (getMolGraph gr)
       in
        assertBool errMsg $ Set.null missingNodes

-- Since Group numbering is arbitrary, we check if the groups containing
-- certain atoms are connected.
connectionsPresent ::
  (GroupedMolecule groupMol grp mol atom bond) =>
  [(AtomId, AtomId)] ->
  groupMol grp mol atom bond ->
  TestTree
connectionsPresent xs0 gr = testCase "Required edges present" $ do
  let xs = Set.fromList xs0
      addGroupConnection acc (i, j) = do
        g1 <- fst <$> getAtomGroup gr i
        g2 <- fst <$> getAtomGroup gr j
        return $ Set.insert (g1, g2) acc
  requiredGroupEdges <- foldM addGroupConnection Set.empty xs
  let edgs = Set.fromList . getGroupConnections $ gr
      missingEdges = requiredGroupEdges `Set.difference` edgs
      errMsg =
        "Missing edges:\n Should have edges between the groups of these pairs of atoms:\n"
          <> show xs0
          <> "\nWhich correspond to links between these groups:\n"
          <> show requiredGroupEdges
          <> "\nBut has edges:\n"
          <> show edgs
          <> "\nMissing edges:\n"
          <> show missingEdges
  assertBool errMsg $ Set.null missingEdges

connectionsAbsent ::
  (GroupedMolecule groupMol grp mol atom bond) =>
  [(AtomId, AtomId)] ->
  groupMol grp mol atom bond ->
  TestTree
connectionsAbsent xs0 gr = testCase "Forbidden edges absent" $ do
  let xs = Set.fromList xs0
      addGroupConnection acc (i, j) = do
        g1 <- fst <$> getAtomGroup gr i
        g2 <- fst <$> getAtomGroup gr j
        return $ Set.insert (g1, g2) acc
  forbiddenGroupEdges <- foldM addGroupConnection Set.empty xs
  let edgs = Set.fromList . getGroupConnections $ gr
      offendingEdges = forbiddenGroupEdges `Set.intersection` edgs
      errMsg =
        "Missing edges:\n Should not have edges between the groups of these pairs of atoms:\n"
          <> show xs0
          <> "\nWhich correspond to links between these groups:\n"
          <> show forbiddenGroupEdges
          <> "\nBut has edges:\n"
          <> show edgs
          <> "\nEdges in violation of THE LAW:\n"
          <> show offendingEdges
  assertBool errMsg $ Set.null offendingEdges
