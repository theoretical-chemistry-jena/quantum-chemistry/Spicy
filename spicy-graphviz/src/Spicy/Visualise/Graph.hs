{- |
Module      : Spicy.Visualise.Graph
Description : Visualising molecules as graphs
Copyright   : Phillip Seeber, Sebastian Seidenath 2024
License     : AGPL-3
Maintainer  : phillip.seeber@uni-jena.de
Stability   : experimental
Portability : POSIX, Windows
-}
module Spicy.Visualise.Graph (
  -- * Molecule
  moleculeToDotGeneric,
  moleculeToDotLabel,
  moleculeToDotDef,

  -- * Grouped Molecules
  groupedMoleculeToDotLabel,
  groupedMoleculeToDotDef,

  -- * Colours
  avogadroColours,
  stdColours,

  -- * Defaults
  LabelBond (..),
  defLayout,
  molParams,
  defFmtEdge,
  defFmtAtom,
  defAtmLab,
  defGrpLab,

  -- * IHaskell
  renderMolecule,
  renderGroupedMolecule,
) where

-- renderMolecule,

import Data.GraphViz hiding (C, Gd, N)
import Data.GraphViz qualified as GV
import Data.GraphViz qualified as GVA
import Data.GraphViz.Attributes.Colors
import Data.GraphViz.Attributes.Complete qualified as GVA
import Data.GraphViz.Attributes.HTML qualified as HTML
import IHaskell.Display.Graphviz
import RIO.List.Partial ((!!))
import RIO.Text.Lazy (fromStrict)
import RIO.Text.Lazy qualified as TL
import Spicy.Chemistry
import Spicy.FragmentMethods.Group
import Spicy.Graph
import Spicy.IntMap qualified as IntMap
import Spicy.Prelude hiding (No, (%))

-- | Standard Blue over yellow to red colour scheme with 6 colours
stdColours :: [Color]
stdColours =
  [ RGB 0x02 0x30 0x47
  , RGB 0x21 0x9E 0xBC
  , RGB 0x8E 0xCA 0xE6
  , RGB 0xFF 0xB7 0x03
  , RGB 0xFB 0x85 0x00
  , RGB 0xD7 0x26 0x38
  ]

-- | Avogadro2's colour scheme for elements
avogadroColours :: Element -> Color
avogadroColours el = case el of
  H -> RGB 0xF0 0xF0 0xF0
  He -> RGB 0xD9 0xFF 0xFF
  Li -> RGB 0xCC 0x80 0xFF
  Be -> RGB 0xC2 0xFF 0x00
  B -> RGB 0xFF 0xB5 0xB5
  C -> RGB 0x7F 0x7F 0x7F
  N -> RGB 0x30 0x50 0xFF
  O -> RGB 0xFF 0x0D 0x0D
  F -> RGB 0xB2 0xFF 0xFF
  Ne -> RGB 0xB2 0xE3 0xF5
  Na -> RGB 0xAB 0x5B 0xF2
  Mg -> RGB 0x8A 0xFF 0x00
  Al -> RGB 0xBF 0xA6 0xA6
  Si -> RGB 0xF0 0xC8 0xA0
  P -> RGB 0xFF 0x80 0x00
  S -> RGB 0xFF 0xFF 0x30
  Cl -> RGB 0x1F 0xF0 0x1F
  Ar -> RGB 0x80 0xD1 0xE3
  K -> RGB 0x8F 0x40 0xD4
  Ca -> RGB 0x3D 0xFF 0x00
  Sc -> RGB 0xE6 0xE6 0xE6
  Ti -> RGB 0xBF 0xC2 0xC7
  V -> RGB 0xA6 0xA6 0xAB
  Cr -> RGB 0x8A 0x99 0xC7
  Mn -> RGB 0x9C 0x7A 0xC7
  Fe -> RGB 0xE0 0x66 0x33
  Co -> RGB 0xF0 0x90 0xA0
  Ni -> RGB 0x50 0xD0 0x50
  Cu -> RGB 0xC8 0x80 0x33
  Zn -> RGB 0x7D 0x80 0xB0
  Ga -> RGB 0xC2 0x8F 0x8F
  Ge -> RGB 0x66 0x8F 0x8F
  As -> RGB 0xBD 0x80 0xE3
  Se -> RGB 0xFF 0xA1 0x00
  Br -> RGB 0xA6 0x29 0x29
  Kr -> RGB 0x5C 0xB8 0xD1
  Rb -> RGB 0x70 0x2E 0xB0
  Sr -> RGB 0x00 0xFF 0x00
  Y -> RGB 0x94 0xFF 0xFF
  Zr -> RGB 0x94 0xE0 0xE0
  Nb -> RGB 0x73 0xC2 0xC9
  Mo -> RGB 0x54 0xB5 0xB5
  Tc -> RGB 0x3B 0x9E 0x9E
  Ru -> RGB 0x24 0x8F 0x8F
  Pd -> RGB 0x00 0x69 0x85
  Rh -> RGB 0x0A 0x7D 0x8C
  Ag -> RGB 0xC0 0xC0 0xC0
  Cd -> RGB 0xFF 0xD9 0x8F
  In -> RGB 0xA6 0x75 0x73
  Sn -> RGB 0x66 0x80 0x80
  Sb -> RGB 0x9E 0x63 0xB5
  Te -> RGB 0xD3 0x7A 0x00
  I -> RGB 0x94 0x00 0x94
  Xe -> RGB 0x42 0x9E 0xB0
  Cs -> RGB 0x57 0x17 0x8F
  Ba -> RGB 0x00 0xC9 0x00
  La -> RGB 0x70 0xD4 0xFF
  Ce -> RGB 0xFF 0xFF 0xC7
  Pr -> RGB 0xD9 0xFF 0xC7
  Nd -> RGB 0xC7 0xFF 0xC7
  Pm -> RGB 0xA3 0xFF 0xC7
  Sm -> RGB 0x8F 0xFF 0xC7
  Eu -> RGB 0x61 0xFF 0xC7
  Gd -> RGB 0x45 0xFF 0xC7
  Tb -> RGB 0x30 0xFF 0xC7
  Dy -> RGB 0x1F 0xFF 0xC7
  Ho -> RGB 0x00 0xFF 0x9C
  Er -> RGB 0x00 0xE6 0x75
  Tm -> RGB 0x00 0xD4 0x52
  Yb -> RGB 0x00 0xBF 0x38
  Lu -> RGB 0x00 0xAB 0x24
  Hf -> RGB 0x4D 0xC2 0xFF
  Ta -> RGB 0x4D 0xA6 0xFF
  W -> RGB 0x21 0x94 0xD6
  Re -> RGB 0x26 0x66 0x96
  Os -> RGB 0x26 0x66 0x96
  Ir -> RGB 0x12 0x42 0x6A
  Pt -> RGB 0xD0 0xD0 0xE0
  Au -> RGB 0xFF 0xD1 0x23
  Hg -> RGB 0xB8 0xC2 0xD0
  Tl -> RGB 0xA6 0x54 0x4D
  Pb -> RGB 0x57 0x59 0x61
  Bi -> RGB 0x9E 0x4F 0xB5
  Po -> RGB 0xAB 0x5C 0x00
  At -> RGB 0x75 0x4F 0x45
  Rn -> RGB 0x42 0x82 0x96
  Fr -> RGB 0x42 0x00 0x66
  Ra -> RGB 0x00 0x7C 0x00
  Ac -> RGB 0x70 0xAA 0xF9
  Th -> RGB 0x00 0xBA 0xFF
  Pa -> RGB 0x00 0xA0 0xFF
  U -> RGB 0x00 0x8E 0xFF
  Np -> RGB 0x00 0x7F 0xFF
  Pu -> RGB 0x00 0x6B 0xFF
  Am -> RGB 0x54 0x5B 0xF2
  Cm -> RGB 0x77 0x5B 0xE2
  Bk -> RGB 0x89 0x4F 0xE2
  Cf -> RGB 0xA0 0x35 0xD3
  Es -> RGB 0xB2 0x1E 0xD3
  Fm -> RGB 0xB2 0x1E 0xBA
  Md -> RGB 0xB2 0x0C 0xA5
  No -> RGB 0xBC 0x0C 0x87
  Lr -> RGB 0xC6 0x00 0x66
  Rf -> RGB 0xCC 0x00 0x59
  Db -> RGB 0xD1 0x00 0x4F
  Sg -> RGB 0xD8 0x00 0x44
  Bh -> RGB 0xE0 0x00 0x38
  Hs -> RGB 0xE0 0x00 0x38
  Mt -> RGB 0xE0 0x00 0x38
  Ds -> RGB 0xE0 0x00 0x38
  Rg -> RGB 0xE0 0x00 0x38
  Cn -> RGB 0xE0 0x00 0x38
  Nh -> RGB 0xE0 0x00 0x38
  Fl -> RGB 0xE0 0x00 0x38
  Mc -> RGB 0xE0 0x00 0x38
  Lv -> RGB 0xE0 0x00 0x38
  Ts -> RGB 0xE0 0x00 0x38
  Og -> RGB 0xE0 0x00 0x38

luminance :: Color -> Double
luminance col = case col of
  (RGB r g b) ->
    let r' = fromIntegral r / 255
        g' = fromIntegral g / 255
        b' = fromIntegral b / 255
     in 0.2126 * r' + 0.7152 * g' + 0.0722 * b'
  _ -> 0

-- | Default layout engine for molecules
defLayout :: GraphvizCommand
defLayout = GV.Neato

-- | Common parameters for molecular graphs.
molParams :: GraphvizParams Node atom bond GroupId atom
molParams =
  defaultParams
    { globalAttributes = [NodeAttrs [GVA.Style [GVA.SItem GVA.Filled []]]]
    , isDirected = False
    }

-- | Bonds that can be labelled.
class LabelBond bond where
  labelBond :: LEdge bond -> GVA.Label

instance LabelBond LewisBondOrder where
  labelBond (_, _, bo) = case bo of
    CovalentBond n -> GVA.StrLabel . fromStrict . tshow $ n
    CoordinativeBond -> GVA.StrLabel "~"

instance {-# OVERLAPPABLE #-} LabelBond a where
  labelBond _ = GVA.StrLabel ""

instance LabelBond SmfBondOrder where
  labelBond (_, _, bo) = case bo of
    Single -> GVA.StrLabel ""
    Multi -> GVA.StrLabel "≈"

-- | Default edge style for 'BondOrder's
defFmtEdge :: (LEdge bond -> GVA.Label) -> LEdge bond -> GVA.Attributes
defFmtEdge fmtBond bond =
  [ GVA.Dir NoDir
  , GVA.Color [WC (X11Color Black) Nothing]
  , GVA.Style [GVA.SItem GVA.Solid []]
  , GVA.XLabel . fmtBond $ bond
  ]

-- | Default formatter for atoms.
defFmtAtom ::
  (Atom atom) =>
  -- | Element to colour mapping
  (Element -> Color) ->
  -- | Create a node 'GVA.Label' from an 'Atom'.
  (LNode atom -> GVA.Label) ->
  LNode atom ->
  GVA.Attributes
defFmtAtom e2cMap fmtAtm (n, atm) =
  let atmCol = e2cMap (getElement atm)
   in [ GVA.Shape Circle
      , GVA.FillColor [WC atmCol (Just 1)]
      , GVA.FontColor . X11Color $ if luminance atmCol >= 0.5 then Black else White
      , GVA.FixedSize GVA.SetShapeSize
      , GVA.LabelJust GVA.JCenter
      , GVA.Width 0.75
      , GVA.Height 0.75
      , GVA.Label . fmtAtm $ (n, atm)
      ]

-- | Default atom labeller
defAtmLab :: (Atom atom) => LNode atom -> GVA.Label
defAtmLab (i, atm) =
  let chrg = getChrg atm
      uEl = getUEl atm
   in GVA.HtmlLabel
        . HTML.Text
        $ [ HTML.Format HTML.Superscript [HTML.Str . fromStrict . tshow $ i]
          , HTML.Newline []
          , HTML.Str . fromStrict . tshow . getElement $ atm
          , HTML.Format
              HTML.Superscript
              [ HTML.Str . TL.pack $ case chrg of
                  n
                    | n < 0 -> "-" <> (show . abs $ chrg)
                    | n > 0 -> "+" <> (show . abs $ chrg)
                    | otherwise -> ""
              , HTML.Str " "
              , HTML.Str . TL.pack $ case uEl of
                  n
                    | n < 0 -> (show . abs $ uEl) <> "↓"
                    | n > 0 -> (show . abs $ uEl) <> "↑"
                    | otherwise -> ""
              ]
          , HTML.Newline []
          , HTML.Format HTML.Subscript [HTML.Str . fromStrict . fromMaybe "∅" . getLabel $ atm]
          ]

-- | Default group labeller
defGrpLab :: LNode grp -> Text
defGrpLab (i, _grp) = "Group " <> tshow i

-- | Generate a 'Dot' graph from a 'Molecule' with full customisation options.
moleculeToDotGeneric ::
  forall mol atom bond.
  (Molecule mol atom bond) =>
  mol atom bond ->
  -- | [Attributes](https://graphviz.org/doc/info/attrs.html) to generate for 'Atom's
  (LNode atom -> Attributes) ->
  -- | [Attributes](https://graphviz.org/doc/info/attrs.html) to generate for 'BondOrder's
  (LEdge bond -> Attributes) ->
  DotGraph Node
moleculeToDotGeneric mol fmtNode fmtEdge = graphToDot locParams molGr
 where
  molGr = getMolGraph mol
  locParams = molParams{fmtNode, fmtEdge}

-- | Generate a 'Dot' graph from a 'Molecule' with some useful defaults.
moleculeToDotLabel ::
  forall mol atom bond.
  (Molecule mol atom bond) =>
  mol atom bond ->
  -- | Create a node 'GVA.Label' from an 'Atom'.
  (LNode atom -> GVA.Label) ->
  -- | Create an edge 'GVA.Label' from a 'BondOrder'.
  (LEdge bond -> GVA.Label) ->
  -- | Element to colour mapping
  (Element -> Color) ->
  DotGraph Node
moleculeToDotLabel mol fmtAtm fmtBond e2cMap = graphToDot locParams molGr
 where
  molGr = getMolGraph mol
  locParams = molParams{fmtNode, fmtEdge}
  fmtNode = defFmtAtom e2cMap fmtAtm
  fmtEdge = defFmtEdge fmtBond

{- | Default representation of a 'Molecule' as a 'Dot' graph with colouring and
sensible node labels
-}
moleculeToDotDef ::
  forall mol atom bond.
  (Molecule mol atom bond, LabelBond bond) =>
  mol atom bond ->
  DotGraph Node
moleculeToDotDef mol = moleculeToDotLabel mol defAtmLab labelBond avogadroColours

-- | Render a 'GroupedMolecule' to a 'DotGraph' with label customisation options.
groupedMoleculeToDotLabel ::
  (GroupedMolecule groupMol grp mol atom bond, LabelBond bond) =>
  -- | Group to colour ID
  (GroupId -> Color) ->
  -- | Label generator for groups
  (LNode grp -> Text) ->
  groupMol grp mol atom bond ->
  DotGraph Node
groupedMoleculeToDotLabel colMap grpLab grpMol = graphToDot locParams molGr
 where
  molGr = getMolGraph grpMol
  groups = IntMap.fromList . getGroups $ grpMol
  atmToGrp = getAtomToGroupMapping grpMol
  fmtNode (i, atm) =
    let grpId = atmToGrp IntMap.! i
        grpCol = colMap grpId
        stdAttrs = defFmtAtom avogadroColours defAtmLab (i, atm)
     in stdAttrs
          <> [ GVA.Color [WC grpCol (Just 1)]
             , GVA.PenWidth 5
             ]
  fmtCluster grpId =
    let grp = groups IntMap.! grpId
        grpCol = colMap grpId
     in [ GraphAttrs
            [ GVA.Color [WC grpCol (Just 1)]
            , GVA.Margin . GVA.DVal $ 20
            , GVA.PenWidth 2.5
            , GVA.LabelLoc GVA.VBottom
            , GVA.LabelJust GVA.JRight
            , GVA.Label . GVA.StrLabel . fromStrict . grpLab $ (grpId, grp)
            ]
        ]
  locParams =
    molParams
      { fmtNode
      , fmtEdge = defFmtEdge labelBond
      , isDotCluster = const True
      , clusterBy = \(i, atm) -> GV.C (atmToGrp IntMap.! i) $ GV.N (i, atm)
      , clusterID = \grpId ->
          let grp = groups IntMap.! grpId
           in GV.Str . fromStrict $ grpLab (grpId, grp)
      , fmtCluster
      }

-- | Show a 'GroupedMolecule' with sensible defaults.
groupedMoleculeToDotDef ::
  (GroupedMolecule groupMol grp mol atom bond, LabelBond bond) =>
  groupMol grp mol atom bond ->
  DotGraph Node
groupedMoleculeToDotDef grpMol = groupedMoleculeToDotLabel getStdCol defGrpLab grpMol
 where
  getStdCol grpId =
    let colIx = grpId `mod` length stdColours
     in stdColours !! colIx

-- | Standard renderer for IHaskell using 'moleculeToDotDef' and 'neato'.
renderMolecule :: (Molecule mol atom bond, LabelBond bond) => mol atom bond -> Graphviz
renderMolecule mol = neato . TL.unpack . printDotGraph $ dotGr
 where
  dotGr = moleculeToDotDef mol

-- | Standard renderer for IHaskell using 'groupedMoleculeToDotDef' and 'neato'.
renderGroupedMolecule ::
  (GroupedMolecule groupMol grp mol atom bond, LabelBond bond) =>
  groupMol grp mol atom bond ->
  Graphviz
renderGroupedMolecule grpMol = neato . TL.unpack . printDotGraph $ dotGr
 where
  dotGr = groupedMoleculeToDotDef grpMol
