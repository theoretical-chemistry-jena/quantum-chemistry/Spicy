{- |
Module      : Spicy.IPI
Description : Implementation of the i–PI network communication protocol.
Copyright   : Phillip Seeber, Sebastian Seidenath 2024
License     : AGPL-3
Maintainer  : sebastian.seidenath@uni-jena.de
Stability   : experimental
Portability : POSIX, Windows

This module provides access to functions for transmitting molecular data through
the i–PI network communication protocol as well as Spicy's own implementation of
an i–PI client server.

i–PI is an interface and network protocol for molecular dynamics simulations.
It is based on a client – server model, where one or more clients provide forces
for the server, which handles the evolution of the atomic positions.

Find an introduction to the [i–PI universal force engine interface here](https://ipi-code.org/).
The github of i–PI, including a reference implementation, [can be found here](https://github.com/i-pi/i-pi).
The canonical citation for i–PI is [Kapil et al., Comp. Phys. Comm. 236, 214–223 (2018)](https://doi.org/10.1016/j.cpc.2018.09.020).
-}
module Spicy.IPI (
  module Spicy.IPI.Client,
  module Spicy.IPI.Common,
) where

-- The server module is currently just for testing.
import Spicy.IPI.Client
import Spicy.IPI.Common
