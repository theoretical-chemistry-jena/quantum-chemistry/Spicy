{- |
Module      : Spicy.IPI.Client
Description : Client–side IPI implementation for Spicy
Copyright   : Phillip Seeber, Sebastian Seidenath 2024
License     : AGPL-3
Maintainer  : sebastian.seidenath@uni-jena.de
Stability   : experimental
Portability : POSIX, Windows
-}
module Spicy.IPI.Client (
  initConnection,
  initConnectionWithRetry,
  commLoop,
  IPIPosData (..),
  IPIForceData (..),
  IPIException (..),
) where

import Network.Socket
import Spicy.IPI.Common
import Spicy.Prelude

{- | Initialize a connection to a remote socket.
Will throw an exception if the socket cannot be found.
See `initConnectionWithRetry` for a version that can retry.
-}
initConnection :: SockAddr -> IO Socket
initConnection addr = do
  s <- case addr of
    SockAddrInet{} -> socket AF_INET Stream defaultProtocol
    SockAddrInet6{} -> socket AF_INET6 Stream defaultProtocol
    SockAddrUnix{} -> socket AF_UNIX Stream defaultProtocol
  connect s addr
  return s

{- | Initialize a connection to a remote socket.
Will retry depending on its first argument.
'Nothing' means the function will attempt to reconnect until it succeeds.
'Just' a number means that the connection will be retried that many times.
The second argument is the delay between attempts in microseconds.
For a version without this retrying behaviour, see `initConnection`.
-}
initConnectionWithRetry ::
  (MonadUnliftIO m, MonadReader env m, HasLogFunc env) =>
  Maybe Natural ->
  Int ->
  SockAddr ->
  m Socket
initConnectionWithRetry r delay addr =
  liftIO (initConnection addr) `catchIO` handler
 where
  handler e = case r of
    Nothing -> do
      logWarnS "i–PI" "Could not connect to socket. Will retry indefinitely."
      logDebugS "i–PI"
        $ "Recieved the following exception:\n"
        <> displayShow e
      threadDelay delay
      initConnectionWithRetry Nothing delay addr
    Just 0 -> do
      logErrorS "i–PI" "Could not connect to socket. Aborting."
      logDebugS "i–PI"
        $ "Recieved the following exception:\n"
        <> displayShow e
      throwIO e
    Just n -> do
      logWarnS "i–PI"
        $ "Could not connect to socket. Will retry "
        <> displayShow n
        <> " more times."
      logDebugS "i–PI"
        $ "Recieved the following exception:\n"
        <> displayShow e
      threadDelay delay
      initConnectionWithRetry (Just $ n - 1) delay addr

{- | Basic communications loop for an i–PI client.
Handles sending and recieving all messages.
Will return (with ()) once it recieves an exit command from the server.
-}
commLoop ::
  ( MonadIO m
  , MonadReader env m
  , MonadThrow m
  , HasLogFunc env
  , HasIPISocket env
  , HasRunner env
  ) =>
  m ()
commLoop =
  readMessage >>= \case
    Status -> do
      logD "received STATUS"
      sendMessage Ready
      logD "Sent READY"
      commLoop
    PosData p -> do
      logD "received POSDATA"
      logD "Starting runner…"
      runner <- envView runnerL
      forces <- runRunner runner p
      logD "Runner finished"
      readMessage >>= \case
        Status -> logDebug "received STATUS"
        msg -> throwM $ InappropriateMessage msg
      sendMessage HaveData
      logD "Sent HAVEDATA"
      readMessage >>= \case
        GetForce -> logD "received GETFORCE"
        msg -> throwM $ InappropriateMessage msg
      sendMessage (ForceReady forces)
      logD "Sent FORCEREADY"
      commLoop
    GetForce -> do
      -- Only allowed immediately after a HaveData
      logD "received GETFORCE"
      throwM $ InappropriateMessage GetForce
    Exit -> logD "Recived 'Exit'. Goodbye, cruel world."
 where
  logD = logDebugS "i–PI client communication loop"
