{- |
Module      : Spicy.IPI.Types
Description : Types and functions for the i–PI communication protocol
Copyright   : Phillip Seeber, Sebastian Seidenath 2024
License     : AGPL-3
Maintainer  : sebastian.seidenath@uni-jena.de
Stability   : experimental
Portability : POSIX, Windows

Basic types and communication functions for an i–PI server.
See [the i–PI documentation](https://ipi-code.org/) for details.

Some notes for the unsuspecting user:

  * This module (and the entire Spicy i–PI implementation) uses host endianness throughout.
  * We avoid Data.Binary as much as possible, since its default encodings are an endless source of pitfalls.
-}
module Spicy.IPI.Common (
  Runner (..),
  IPIEnvironment (..),
  HasIPISocket (..),
  HasRunner (..),
  Cell (..),
  nullCell,
  IPIPosData (..),
  encodeIPIPosData,
  IPIForceData (..),
  encodeIPIForceData,
  IPIClientMessage (..),
  sendMessage,
  IPIServerMessage (..),
  readMessage,
  receiveMessage,
  receiveDouble,
  receiveInt32,
  receiveCell,
  receiveCoordinates,
  receiveForces,
  IPIException (..),
) where

import Control.Monad
import Data.Massiv.Array as Massiv
import Data.Text.Lazy.Encoding qualified as TL
import Foreign.Storable
import Network.Socket
import Network.Socket.ByteString.Lazy qualified as N
import RIO.ByteString.Lazy qualified as BL
import RIO.Text.Lazy qualified as TL
import Spicy.Math.LinearAlgebra.R3 qualified as R3
import Spicy.Prelude

--------------------------------------------------------------------------------

newtype Runner = Runner {runRunner :: forall m. (MonadIO m) => IPIPosData -> m IPIForceData}

-- | The runtime environment for the i–PI client.
data IPIEnvironment = IPIEnvironment
  { logFunc :: LogFunc
  -- ^ Logging function for the client.
  , ipiSocket :: Socket
  -- ^ Communication socket. Must be connected.
  , runner :: Runner
  -- ^ A function that performs calculations and returns results.
  -- This function may do factually anything, so that the client code does not need to know about the store, scheduling or any other implementation detail.
  }
  deriving (Generic)

instance HasLogFunc IPIEnvironment where
  logFuncL = toLensVL #logFunc

class HasIPISocket env where
  -- | The socket should be connected, but there's no good way to enforce this
  -- (since the other side may terminate the connection at any point).
  ipiSocketL :: Lens' env Socket

instance HasIPISocket IPIEnvironment where
  ipiSocketL = #ipiSocket

class HasRunner env where
  runnerL :: Lens' env Runner

instance HasRunner IPIEnvironment where
  runnerL = #runner

--------------------------------------------------------------------------------

floatBytes, intBytes :: Int64

-- | The amount of bytes in a 'Double'.
floatBytes = fromIntegral $ sizeOf (undefined :: Double)

{- | The amount of bytes in a 'Int32'.
| The i–PI reference implementation uses 'Int32', thus, so do we.
-}
intBytes = fromIntegral $ sizeOf (undefined :: Int32)

-- | Helper function for recieving data through the i–PI 'Socket'.
receive ::
  (MonadIO m, MonadReader env m, HasIPISocket env) =>
  -- | Number of bytes to receive.
  Int64 ->
  m BL.ByteString
receive n = do
  s <- envView ipiSocketL
  liftIO $ N.recv s n

-- | Send bytes over the i–PI 'Socket'.
send ::
  (MonadIO m, MonadReader env m, HasIPISocket env) =>
  BL.ByteString ->
  m ()
send bs = do
  s <- envView ipiSocketL
  liftIO $ N.sendAll s bs

{- | receive a message as specified by the i–PI network protocol.
Many such messages will be followed by further data.
The message will be exactly 12 bytes long.
For convenience, any spaces at the end are stripped by this function.
-}
receiveMessage ::
  (MonadIO m, MonadReader env m, HasIPISocket env) =>
  m BL.ByteString
receiveMessage = BL.filter (/= 32) <$> receive 12 -- 32 is space

-- | receive a single 'Double'. Uses host endianness.
receiveDouble ::
  (MonadIO m, MonadReader env m, HasIPISocket env) =>
  m Double
receiveDouble = do
  runGet getDoublehost <$> receive floatBytes

-- | receive a single 'Int32'. Uses host endianness.
receiveInt32 ::
  (MonadIO m, MonadReader env m, HasIPISocket env) =>
  m Int
receiveInt32 = do
  fromIntegral . runGet getInt32host <$> receive intBytes

-- | receive 9 'Double's and return them as a 'Cell'.
receiveCell ::
  (MonadIO m, MonadReader env m, HasIPISocket env) =>
  m Cell
receiveCell = do
  x1 <- receiveDouble
  y1 <- receiveDouble
  z1 <- receiveDouble
  x2 <- receiveDouble
  y2 <- receiveDouble
  z2 <- receiveDouble
  x3 <- receiveDouble
  y3 <- receiveDouble
  z3 <- receiveDouble
  let a = R3.mkR3T (x1, y1, z1)
  let b = R3.mkR3T (x2, y2, z2)
  let c = R3.mkR3T (x3, y3, z3)
  return Cell{..}

-- | receive the atomic (cartesian) coordinates.
receiveCoordinates ::
  (MonadIO m, MonadReader env m, HasIPISocket env) =>
  Int ->
  m (Matrix S Double)
receiveCoordinates n = do
  atomicPositions' <- replicateM (3 * n) receiveDouble
  return
    . Massiv.resize' (Sz2 n 3)
    . Massiv.fromList Seq
    $ atomicPositions'

-- | receive the atomic gradient as a vector.
receiveForces ::
  (MonadIO m, MonadReader env m, HasIPISocket env) =>
  Int ->
  m (Vector S Double)
receiveForces n = Massiv.fromList Seq <$> replicateM (3 * n) receiveDouble

--------------------------------------------------------------------------------

-- | Messages which are sent from the client to the server.
data IPIClientMessage
  = -- | Asking for initialization data
    NeedInit
  | -- | Signalling ready to receive atomic positions
    Ready
  | -- | Signaling finshed calculation
    HaveData
  | -- | Sending forces
    ForceReady IPIForceData

-- | Send an 'IPIClientMessage' to the server.
sendMessage ::
  (MonadIO m, MonadReader env m, HasIPISocket env) =>
  IPIClientMessage ->
  m ()
sendMessage msg = do
  let send_ =
        send
          . TL.encodeUtf8
          . TL.justifyLeft 12 ' '
          . utf8BuilderToLazyText
  case msg of
    NeedInit -> send_ "NEEDINIT"
    Ready -> send_ "READY"
    HaveData -> send_ "HAVEDATA"
    ForceReady fs -> send_ "FORCEREADY" >> send (encodeIPIForceData fs)

-- | Messages which are sent from the server to the client.
data IPIServerMessage
  = -- | Asking for the client status
    Status
  | -- | Providing initialization data
    Init IPIInitData
  | -- | Sending atomic positions
    PosData IPIPosData
  | -- | Asking for forces
    GetForce
  | -- | We're done, stop the program
    Exit
  deriving (Show)

-- | Decode a message from an i–PI server.
readMessage ::
  (MonadIO m, MonadReader env m, MonadThrow m, HasIPISocket env) =>
  m IPIServerMessage
readMessage = do
  msg <- receiveMessage
  case msg of
    "STATUS" -> return Status
    "INIT" -> throwM UnsupportedInit
    "POSDATA" -> do
      cell <- receiveCell
      inverseCell <- receiveCell
      nAtoms <- receiveInt32
      atomicPositions <- receiveCoordinates nAtoms
      return $ PosData IPIPosData{..}
    "GETFORCE" -> return GetForce
    "EXIT" -> return Exit
    _ ->
      throwM
        . UnrecognizedMessage
        . utf8BuilderToText
        . displayBytesUtf8
        . toStrictBytes
        $ msg

--------------------------------------------------------------------------------

-- | A (unit) cell, specified by three 3D basis vectors.
data Cell = Cell
  { a :: R3.R3 S Double
  , b :: R3.R3 S Double
  , c :: R3.R3 S Double
  }
  deriving (Show)

{- | A 'Cell' with all vectors being the zero vector.
Since we rarely care about cell data, we can just send this.
-}
nullCell :: Cell
nullCell = Cell nullVec nullVec nullVec
 where
  nullVec = R3.mkR3T (0, 0, 0)

putCell :: Cell -> Put
putCell Cell{..} = do
  let (ax, ay, az) = R3.xyzR3 a
      (bx, by, bz) = R3.xyzR3 b
      (cx, cy, cz) = R3.xyzR3 c
  traverse_ putDoublehost [ax, ay, az, bx, by, bz, cx, cy, cz]

--------------------------------------------------------------------------------

-- | The data received from the i–PI server upon requesting a calculation.
data IPIPosData = IPIPosData
  { cell :: Cell
  , inverseCell :: Cell
  , atomicPositions :: Matrix S Double
  }
  deriving (Show)

-- No binary instance, since recieving and conversion are interleaved

{- | Encode an 'IPIForceData' as a lazy bytestring in the format recognized by
an i–PI client.
-}
encodeIPIPosData :: IPIPosData -> BL.ByteString
encodeIPIPosData IPIPosData{..} = runPut $ do
  putCell cell
  putCell inverseCell
  putInt32host . fromIntegral . (`div` 3) . Massiv.elemsCount $ atomicPositions
  Massiv.traverseA_ putDoublehost atomicPositions

--------------------------------------------------------------------------------

-- | The forces expected by the i–PI server as the result of a calculation.
data IPIForceData = IPIForceData
  { potential :: Double
  -- ^ Energy of the system
  , nAtoms :: Int
  -- ^ Number of atoms in the system
  , forces :: Vector S Double
  -- ^ Forces acting on the atoms of the system
  , virial :: Cell
  -- ^ Virial of the system
  , extra :: BL.ByteString
  -- ^ Extra information
  }
  deriving (Show)

-- No binary instance, since recieving and conversion are interleaved

{- | Encode an 'IPIForceData' as a lazy bytestring in the format recognized by
an i–PI server.
-}
encodeIPIForceData :: IPIForceData -> BL.ByteString
encodeIPIForceData IPIForceData{..} = runPut $ do
  putDoublehost potential
  putInt32host $ fromIntegral nAtoms
  Massiv.traverseA_ putDoublehost forces
  putCell virial
  putInt32host (fromIntegral $ BL.length extra)
  putLazyByteString extra

--------------------------------------------------------------------------------

{- | The initialization data sent from server to client.
We currently do not ever expect to receive this, hence it is uninhabited.
-}
data IPIInitData
  deriving (Show)

--------------------------------------------------------------------------------

-- | Exceptions that can occur during the operation of an i–PI client.
data IPIException
  = -- | received a message that is not recognized by Spicy.
    UnrecognizedMessage Text
  | -- | received a (recognized) message at a time where it was not expected.
    InappropriateMessage IPIServerMessage
  | -- | received a "INIT" message, which Spicy does not ask for.
    UnsupportedInit
  deriving (Show)

instance Exception IPIException
