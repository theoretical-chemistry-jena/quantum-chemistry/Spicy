{- |
Module      : Spicy.IPI.Server
Description : Minimal reference implementation of an i-PI server
Copyright   : Phillip Seeber, Sebastian Seidenath 2024
License     : AGPL-3
Maintainer  : sebastian.seidenath@uni-jena.de
Stability   : experimental
Portability : POSIX, Windows

Very basic implementation of an i–PI server. Useful for testing purposes only.
-}
module Spicy.IPI.Server (
  runServer,
  initSocket,
  dummyServer,
) where

import Control.Monad
import Data.Massiv.Array
import Network.Socket
import Network.Socket.ByteString.Lazy
import Spicy.IPI.Common
import Spicy.Prelude

-- | Simple environment for an i–PI server.
newtype ServerEnv = ServerEnv {getServerEnv :: (LogFunc, Socket)}
  deriving (Generic)

instance HasLogFunc ServerEnv where
  logFuncL = toLensVL (#getServerEnv % _1)

instance HasIPISocket ServerEnv where
  ipiSocketL = #getServerEnv % _2

--------------------------------------------------------------------------------

-- | Open a socket and run a dummy server.
runServer :: String -> IO Void
runServer addr = do
  logOptions <- logOptionsHandle stderr False
  withLogFunc logOptions $ \lf -> do
    let sa = SockAddrUnix addr
    bracket (runRIO lf $ initSocket sa) close $ \s ->
      runRIO (ServerEnv (lf, s)) (dummyServer 10 diatomic)

{- | Initialize a 'Socket' and wait for a connection.
Returns the socket once connected.
-}
initSocket ::
  (MonadIO m, MonadReader env m, HasLogFunc env) =>
  SockAddr ->
  m Socket
initSocket a = do
  s <- liftIO $ socket AF_UNIX Stream defaultProtocol
  logI "Created socket"
  liftIO $ bind s a
  logI $ "Bound socket to address" <> displayShow a
  liftIO $ listen s 1024
  logI "Socket set to listen"
  s2 <- fst <$> liftIO (accept s)
  logI "Connection established. Starting exchange…"
  return s2
 where
  logI = logInfoS "i–PI server"

{- | Simple i–PI server that makes up some mock data.
Meant for testing.
-}
dummyServer ::
  (MonadIO m, MonadReader env m, HasLogFunc env, HasIPISocket env) =>
  -- | Number of communication cycles the server will perform
  Int ->
  -- | Function to produce coordinates based on the cycle number
  (Int -> IPIPosData) ->
  m Void
dummyServer maxIter f = go 0
 where
  logI = logInfoS "i–PI server"
  localSendAll bs = do
    s <- envView ipiSocketL
    liftIO $ sendAll s bs
    logI $ "Sent " <> (displayBytesUtf8 . toStrictBytes $ bs)
  localreceive = do
    msg <- receiveMessage
    logI $ "Received " <> (displayBytesUtf8 . toStrictBytes $ msg)
    return msg
  go n = do
    s <- envView ipiSocketL

    localSendAll "STATUS      "
    void localreceive -- Should be "Ready"
    localSendAll "POSDATA     "
    liftIO . sendAll s $ encodeIPIPosData (f n)

    localSendAll "STATUS      " -- Hey, are you done yet? It's been 3 nanoseconds already!
    void localreceive -- Should be "HaveData"
    localSendAll "GETFORCE    "
    void localreceive -- Should be "ForceReady"

    -- receive the IPIForceData
    potential <- receiveDouble
    nAtoms <- receiveInt32
    forces <- receiveForces nAtoms
    virial <- receiveCell
    extraLength <- receiveInt32
    extra <- case extraLength of -- recv s 0 is invalid.
      0 -> return mempty
      _ -> liftIO $ recv s (fromIntegral extraLength)
    let ipiForceData = IPIForceData{..}
    logI "Received force data"
    logI $ displayShow ipiForceData

    -- Exit when done.
    when (n == (maxIter - 1)) $ do
      localSendAll "EXIT        "
      logI "I'm done."
      exitSuccess

    go (n + 1)

-- | Makes up coordinates for a diatomic molecule.
diatomic ::
  -- | Iteration number.
  Int ->
  IPIPosData
diatomic n = IPIPosData{..}
 where
  cell = nullCell
  inverseCell = nullCell
  atomicPositions = resize' (Sz2 2 3) $ fromList Seq [0, 0, 0, 0, 0, 1 + 0.1 * fromIntegral n]
