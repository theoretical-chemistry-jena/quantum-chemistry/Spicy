module Spicy.Math.IndexTransformation (
  -- * Sparse-Dense Conversion
  s2dMapping,
  d2sMapping,
  s2d,
  s2d',
  d2s,
  d2s',
)
where

import Data.Massiv.Array as Massiv hiding (fromList, toList, zip)
import Data.Massiv.Array qualified as Massiv
import GHC.Exception (prettyCallStack)
import GHC.Stack (callStack)
import RIO.Partial (fromJust)
import Spicy.HashMap qualified as HashMap
import Spicy.Prelude

data IndexTransformationException where
  IndexTransformationException :: (HasCallStack) => String -> IndexTransformationException

instance Exception IndexTransformationException

instance Show IndexTransformationException where
  show (IndexTransformationException msg) =
    "IndexTransformationException"
      <> msg
      <> "\n"
      <> prettyCallStack callStack

-- | Obtain a conversion table from sparse to dense indices.
s2dMapping ::
  (Foldable f) =>
  -- | All sparse indices
  f Int ->
  -- | Mapping from sparse to dense indices
  HashMap Int Int
s2dMapping i = HashMap.fromList $ zip (toList i) [0 ..]

{- | Obtain a vector with the sparse indices in order. The vector index is the
the lookup index, the value at the position the sparse index.
-}
d2sMapping ::
  (Foldable f, Manifest r Int) =>
  -- | All sparse indices
  f Int ->
  Vector r Int
d2sMapping i = Massiv.fromList Par . toList $ i

{- | Transformation of a sparse index to a dense index using a lookup table as
obtained by 's2dMapping'.
-}
s2d :: (MonadThrow m) => HashMap Int Int -> Int -> m Int
s2d m sIx = m !?! sIx
 where
  hm !?! k = case HashMap.lookup k hm of
    Just r -> return r
    Nothing ->
      throwM
        . IndexTransformationException
        $ "Could not find index "
        <> show sIx
        <> " in transformation map"

-- | Unsafe version of 's2d'
s2d' :: HashMap Int Int -> Int -> Int
s2d' m sIx = fromJust $ HashMap.lookup sIx m

{- | Transformation of a dense index to a sparse index using a lookup table as
obtained by 'd2sMapping'.
-}
d2s :: (Manifest r Int, MonadThrow m) => Vector r Int -> Int -> m Int
d2s v dIx = v !? dIx

-- | Unsafe version of 'd2s'
d2s' :: (Manifest r Int) => Vector r Int -> Int -> Int
d2s' v dIx = v Massiv.! dIx
