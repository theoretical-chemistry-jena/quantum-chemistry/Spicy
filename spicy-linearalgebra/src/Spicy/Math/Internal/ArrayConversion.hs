module Spicy.Math.Internal.ArrayConversion where

import Data.Massiv.Array as Massiv hiding (forM_)
import Data.Massiv.Array.Manifest.Vector qualified as Massiv
import Numeric.LinearAlgebra as HMat hiding ((<>))
import RIO.Vector.Storable qualified as VS
import Spicy.Prelude

-- | Convert a Massiv 'Massiv.Vector' to HMatrix 'HMat.Vector'.
v2hv :: (Manifest r e, Load r Ix1 e, Element e) => Massiv.Vector r e -> HMat.Vector e
v2hv v = Massiv.toVector v

-- | Convert a Massiv 'Massiv.Matrix' to a HMatrix 'HMat.Matrix'.
m2hm :: (Element e, Manifest r e, Load r Ix1 e) => Massiv.Matrix r e -> HMat.Matrix e
m2hm mMat = HMat.reshape nCols . v2hv . Massiv.flatten $ mMat
 where
  Sz (_nRows :. nCols) = Massiv.size mMat

-- | Convert a HMatrix 'HMat.Vector' to a Massiv 'Massiv.Vector'.
hv2v :: (Element e, Manifest r e, Load r Ix1 e) => VS.Vector e -> Massiv.Vector r e
hv2v hVec = Massiv.fromVector' Seq (Sz $ VS.length hVec) hVec

-- | Convert a HMatrix 'HMat.Matrix' to a Massiv 'Massiv.Matrix'.
hm2m :: (Manifest r e, Load r Ix1 e, Element e) => HMat.Matrix e -> Massiv.Matrix r e
hm2m hMat = Massiv.resize' (Sz $ nRows :. nCols) . hv2v . HMat.flatten $ hMat
 where
  nRows = HMat.rows hMat
  nCols = HMat.cols hMat
