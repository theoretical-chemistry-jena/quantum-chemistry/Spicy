{-# LANGUAGE DerivingVia #-}

{- |
Module      : Spicy.Math.LinearAlgebra
Description : Efficient and safe linear algebra functions
Copyright   : Phillip Seeber, Sebastian Seidenath 2024
License     : AGPL-3
Maintainer  : phillip.seeber@uni-jena.de
Stability   : experimental
Portability : POSIX, Windows

This module provides common functions to work with vectors and matrices in \(\mathbb{R}^n\).
While all operations are abstracted over Massiv's 'Array' data type, most operations are actually provided by HMatrix and therefore BLAS/LAPACK.
Massiv's 'Array' can be cast from and to the BLAS/LAPACK representation if the 'S' representation is used.
-}
module Spicy.Math.LinearAlgebra (
  -- * Exceptions
  AlgebraException (..),

  -- * \( \mathbb{R}^n \) Vector Operations
  DistFn,
  lpNorm,
  manhattanDistance,
  euclideanDistance,
  magnitude,
  angle,
  distMat,
  distMat',
  rms,

  -- * BLAS/LAPACK Functions
  (<.>),
  (#>),
  (<#),
  (##),
  eig,
  geig,
  eigSH,
  geigSH,
  inv,
  pinv,

  -- * Linear Systems

  -- ** General
  (<\>),
  linearSolveLS,
  linearSolveSVD,

  -- ** Determined
  linearSolve,

  -- * Singular Value Decomposition
  svd,
)
where

import Data.Complex (Complex)
import Data.Massiv.Array as Massiv hiding (all)
import Numeric.LinearAlgebra qualified as HMat
import RIO.Partial (fromJust)
import Spicy.Math.Internal.ArrayConversion
import Spicy.Prelude
import System.IO.Unsafe (unsafePerformIO)

{- | A function to calculate the distance between two points in \(\mathbb{R}^n\).
Functions of this type should throw an exception if the sizes of the size of the
input vectors do not match.
-}
type DistFn m r e = (MonadThrow m, Numeric r e) => Vector r e -> Vector r e -> m e

{- | The \(L_p\) norm between two vectors. Generalisation of Manhattan and Euclidean distances.

\[
  d(\mathbf{a}, \mathbf{b}) = \left( \sum \limits_{i=1}^n \lvert \mathbf{a}_i - \mathbf{b}_i \rvert ^p \right) ^ \frac{1}{p}
\]
-}
lpNorm ::
  ( Numeric r e
  , Source r e
  , Floating e
  , MonadThrow m
  ) =>
  Natural ->
  DistFn m r e
lpNorm p a b
  | szA == szB =
      pure
        . (** (1 / fromIntegral p))
        . Massiv.sum
        . Massiv.map (abs . (^ fromIntegral p))
        $ (a !-! b)
  | otherwise = throwM $ SizeMismatchException szA szB
 where
  szA = Massiv.size a
  szB = Massiv.size b

-- | Manhattan distance of two vectors.
manhattanDistance ::
  ( Source r e
  , Floating e
  ) =>
  DistFn m r e
manhattanDistance a b = lpNorm 1 a b

-- | Euclidean distance of two vectors.
euclideanDistance ::
  ( Source r e
  , Floating e
  ) =>
  DistFn m r e
euclideanDistance a b = lpNorm 2 a b

-- | (Euclidean) Magnitude of a vector
magnitude :: (Floating e, Source r e) => Vector r e -> e
magnitude a = sqrt . Massiv.sum . Massiv.map (^ 2) $ a

{- | Numerically stable angle between two vectors, see <https://www.jwwalker.com/pages/angle-between-vectors.html>

\[
  \alpha = 2 \operatorname{atan2} (\lVert \lVert \boldsymbol{v} \rVert \boldsymbol{u} - \lVert \boldsymbol{u} \rVert v \rVert,
  \lVert \lVert \boldsymbol{v} \rVert \boldsymbol{u} + \lVert \boldsymbol{u} \rVert v \rVert )
\]
-}
angle ::
  (MonadThrow m, Numeric r e, RealFloat e, Source r e) =>
  Vector r e ->
  Vector r e ->
  m e
angle u v = do
  let magU = magnitude u
      magV = magnitude v
      magVxU = magV *. u
      magUxV = magU *. v
  l <- magVxU .-. magUxV
  r <- magVxU .+. magUxV
  return . (2 *) $ atan2 (magnitude l) (magnitude r)

{- | Distance matrix between two sets of points \( \boldsymbol{A} \) and \( \boldsymbol{B} \).
Points are row vectors of each matrix, e.g.

\[
  \boldsymbol{A} = \begin{pmatrix}
    \boldsymbol{a}_1 \\
    \vdots \\
    \boldsymbol{a}_n
  \end{pmatrix} \, .
\]

Consequently, the number of columns must match.
The resulting distance matrix will have a dimension of \( n \times m \),
where \(n\) is the number of rows in \( \boldsymbol{A} \) and \(m\) the number
of rows in \( \boldsymbol{B} \).
-}
distMat ::
  forall e m r.
  (MonadThrow m, Manifest r e, Numeric r e) =>
  DistFn IO r e ->
  Matrix r e ->
  Matrix r e ->
  m (Matrix r e)
distMat distFn a b
  | aR == bR = return
      . unsafePerformIO
      . generateArray Par (Sz $ aC :. bC)
      $ \(iA :. iB) -> distFn (slA ! iA) (slB ! iB)
  | otherwise = throwM $ SizeMismatchException szA szB
 where
  szA@(Sz (aR :. aC)) = Massiv.size a
  szB@(Sz (bR :. bC)) = Massiv.size b
  slA = compute @B . outerSlices $ a
  slB = compute @B . outerSlices $ b

{- | An Euclidean distance matrix of all points to all other points. A point is
given as a row vector of the input matrix. Equivalent to @'distMat' distFn a a@.
-}
distMat' ::
  forall e r.
  (Manifest r e, Numeric r e) =>
  DistFn IO r e ->
  Matrix r e ->
  Matrix r e
distMat' distFn a = fromJust $ distMat distFn a a

-- | Root mean square value of an array
rms :: (Floating e, Stream r ix e) => Array r ix e -> e
rms arr = sqrt . (/ fromIntegral n) . Massiv.ssum . Massiv.smap (^ 2) $ arr
 where
  Sz1 n = Massiv.linearSize arr

-- | Dot product of two 'Vector's
(<.>) ::
  ( MonadThrow m
  , HMat.Numeric a
  , Manifest r1 a
  , Manifest r2 a
  , Load r1 Ix1 a
  , Load r2 Ix1 a
  ) =>
  Vector r1 a ->
  Vector r2 a ->
  m a
a <.> b
  | szA == szB = return $ v2hv a HMat.<.> v2hv b
  | otherwise = throwM $ SizeMismatchException szA szB
 where
  szA = Massiv.size a
  szB = Massiv.size b

-- | Matrix-vector product
(#>) ::
  ( MonadThrow m
  , Manifest r1 e
  , Manifest r2 e
  , Manifest r3 e
  , Load r1 Ix1 e
  , Load r2 Ix1 e
  , Load r3 Ix1 e
  , HMat.Numeric e
  ) =>
  Matrix r1 e ->
  Vector r2 e ->
  m (Vector r3 e)
m #> v
  | c == r = return . hv2v $ hm HMat.#> hv
  | otherwise = throwM $ SizeMismatchException (Sz c) (Sz r)
 where
  hm = m2hm m
  hv = v2hv v
  Sz (_ :. c) = Massiv.size m
  Sz r = Massiv.size v

-- | Vector-matrix product
(<#) ::
  ( MonadThrow m
  , Manifest r1 e
  , Manifest r2 e
  , Manifest r3 e
  , Load r1 Ix1 e
  , Load r2 Ix1 e
  , Load r3 Ix1 e
  , HMat.Numeric e
  ) =>
  Vector r2 e ->
  Matrix r1 e ->
  m (Vector r3 e)
v <# m
  | c == r = return . hv2v $ hv HMat.<# hm
  | otherwise = throwM $ SizeMismatchException (Sz c) (Sz r)
 where
  hm = m2hm m
  hv = v2hv v
  Sz (_ :. c) = Massiv.size m
  Sz r = Massiv.size v

-- | Matrix-matrix multiplication
(##) ::
  ( Manifest r1 e
  , Manifest r2 e
  , Manifest r3 e
  , Load r1 Ix1 e
  , Load r2 Ix1 e
  , Load r3 Ix1 e
  , HMat.Numeric e
  , MonadThrow m
  ) =>
  Matrix r2 e ->
  Matrix r1 e ->
  m (Matrix r3 e)
a ## b
  | c == r = return . hm2m $ ha HMat.<> hb
  | otherwise = throwM $ SizeMismatchException szA szB
 where
  ha = m2hm a
  hb = m2hm b
  szA@(Sz (_ :. c)) = Massiv.size a
  szB@(Sz (r :. _)) = Massiv.size b

-- | Exceptions of linear algebra functions.
data AlgebraException
  = -- | Matrix was numerically singular and is not suitable for requested
    -- operation
    SingularMatrixException
  | -- | Cannot solve an equation system
    EquationSystemException
  deriving (Show, Typeable)

instance Exception AlgebraException

{- | Eigenvalue decomposition; eigenvalues and -vectors are unordered

\[
\boldsymbol{A} \boldsymbol{v} = \lambda \boldsymbol{v}
\]
-}
eig ::
  forall r1 r2 r3 m e.
  ( MonadThrow m
  , Manifest r1 e
  , Manifest r2 (Complex Double)
  , Manifest r3 (Complex Double)
  , Load r1 Ix1 e
  , Load r2 Ix1 (Complex Double)
  , Load r3 Ix1 (Complex Double)
  , HMat.Field e
  ) =>
  Matrix r1 e ->
  m (Vector r2 (Complex Double), Matrix r3 (Complex Double))
eig m
  | r == c = return . bimap hv2v hm2m . HMat.eig $ hm
  | otherwise = throwM $ SizeMismatchException (Sz r) (Sz c)
 where
  Sz (r :. c) = Massiv.size m
  hm = m2hm m

{- | Generalised eigenvalue problem; eigenvalues and -vectors are unordered

\[
\boldsymbol{A} \boldsymbol{v} = \lambda \boldsymbol{B} \boldsymbol{v}
\]
-}
geig ::
  ( MonadThrow m
  , Manifest r1 e
  , Manifest r2 e
  , Manifest r3 (Complex Double)
  , Manifest r4 e
  , Manifest r5 (Complex Double)
  , Load r1 Ix1 e
  , Load r2 Ix1 e
  , Load r3 Ix1 (Complex Double)
  , Load r4 Ix1 e
  , Load r5 Ix1 (Complex Double)
  , HMat.Field e
  ) =>
  Matrix r1 e ->
  Matrix r2 e ->
  m (Vector r3 (Complex Double), Vector r4 e, Matrix r5 (Complex Double))
geig a b
  | all (== rA) [cA, rB, cB] =
      return
        . (\(alphas, betas, v) -> (hv2v alphas, hv2v betas, hm2m v))
        $ HMat.geig (m2hm a) (m2hm b)
  | otherwise = throwM $ SizeMismatchException szA szB
 where
  szA@(Sz (rA :. cA)) = Massiv.size a
  szB@(Sz (rB :. cB)) = Massiv.size b

{- | Eigenvalue decomposition of a hermitian matrix, which should be symmetric.
Symmetry is enforced via \( (\boldsymbol{M} + \boldsymbol{M}^\mathrm{T}) / 2 \) !
Eigenvalues and -vectors are in ascending order.

\[
\boldsymbol{A} \boldsymbol{v} = \lambda \boldsymbol{v}
\]
-}
eigSH ::
  forall r1 r2 r3 m.
  ( MonadThrow m
  , Manifest r1 Double
  , Manifest r2 Double
  , Manifest r3 Double
  , Load r1 Ix1 Double
  , Load r2 Ix1 Double
  , Load r3 Ix1 Double
  ) =>
  Matrix r1 Double ->
  m (Vector r2 Double, Matrix r3 Double)
eigSH m
  | r == c =
      return
        . (orderEigen :: (Vector r2 Double, Matrix r3 Double) -> (Vector r2 Double, Matrix r3 Double))
        . bimap hv2v hm2m
        . HMat.eigSH
        . HMat.sym
        $ hm
  | otherwise = throwM $ SizeMismatchException (Sz r) (Sz c)
 where
  Sz (r :. c) = Massiv.size m
  hm = m2hm m

{- | Generalised eigenvalue problem for symmetric hermitian matrices.
Symmetry is enforced via \( (\boldsymbol{M} + \boldsymbol{M}^\mathrm{T}) / 2 \) !
Eigenvalues and -vectors are in ascending order.

\[
\boldsymbol{A} \boldsymbol{v} = \lambda \boldsymbol{B} \boldsymbol{v}
\]
-}
geigSH ::
  forall r1 r2 r3 r4 m.
  ( MonadThrow m
  , Manifest r1 Double
  , Manifest r2 Double
  , Manifest r3 Double
  , Manifest r4 Double
  , Load r1 Ix1 Double
  , Load r2 Ix1 Double
  , Load r3 Ix1 Double
  , Load r4 Ix1 Double
  ) =>
  Matrix r1 Double ->
  Matrix r2 Double ->
  m (Vector r3 Double, Matrix r4 Double)
geigSH a b
  | all (== rA) [cA, rB, cB] =
      return
        . (orderEigen :: (Vector r3 Double, Matrix r4 Double) -> (Vector r3 Double, Matrix r4 Double))
        . bimap hv2v hm2m
        $ HMat.geigSH (HMat.sym hA) (HMat.sym hB)
  | otherwise = throwM $ SizeMismatchException szA szB
 where
  szA@(Sz (rA :. cA)) = Massiv.size a
  szB@(Sz (rB :. cB)) = Massiv.size b
  hA = m2hm a
  hB = m2hm b

-- | Order eigenvalues and eigenvectors in ascending size.
orderEigen ::
  ( Ord e1
  , Unbox e1
  , Manifest r1 e1
  , Manifest r2 e2
  , Source r3 e2
  ) =>
  (Vector r1 e1, Matrix r3 e2) ->
  (Vector r1 e1, Matrix r2 e2)
orderEigen (eVals, eVecs) = (eValsOrd, compute eVecsOrd)
 where
  eValsOrdAnno = quicksort . compute @U . Massiv.imap (\i e -> (e, i)) $ eVals
  eValsOrd = compute . Massiv.map fst $ eValsOrdAnno
  permVec = compute @U . Massiv.map snd $ eValsOrdAnno
  szVecs = size eVecs
  eVecsOrd = backpermute' szVecs (\(r :. newC) -> r :. (permVec ! newC)) eVecs

-- | Inverse of a matrix.
inv ::
  forall r1 r2 m e.
  ( MonadThrow m
  , Manifest r1 e
  , Manifest r2 e
  , Load r1 Ix1 e
  , Load r2 Ix1 e
  , HMat.Field e
  ) =>
  Matrix r1 e ->
  m (Matrix r2 e)
inv m
  | r == c = case pureTry . hm2m . HMat.inv $ hm of
      Left _ -> throwM SingularMatrixException
      Right res -> return res
  | otherwise = throwM $ SizeMismatchException (Sz r) (Sz c)
 where
  Sz (r :. c) = Massiv.size m
  hm = m2hm m

-- | Pseudo-inverse of a matrix.
pinv ::
  ( MonadThrow m
  , Manifest r1 e
  , Manifest r2 e
  , Load r1 Ix1 e
  , Load r2 Ix1 e
  , HMat.Field e
  ) =>
  Matrix r1 e ->
  m (Matrix r2 e)
pinv m
  | r == c = case pureTry . hm2m . HMat.pinv $ hm of
      Left _ -> throwM SingularMatrixException
      Right res -> return res
  | otherwise = throwM $ SizeMismatchException (Sz r) (Sz c)
 where
  Sz (r :. c) = Massiv.size m
  hm = m2hm m

{- | See 'HMat.<\>'.
Solve a linear system of equations \( \boldsymbol{A} \boldsymbol{x} = \boldsymbol{b} \).
Multiple vectors can be given as columns of \( \boldsymbol{x} \) and multiple
solutions as columns of \( \boldsymbol{b} \) are returned.
-}
(<\>) ::
  ( MonadThrow m
  , Manifest r1 e
  , Manifest r2 e
  , Manifest r3 e
  , Load r1 Ix1 e
  , Load r2 Ix1 e
  , Load r3 Ix1 e
  , HMat.Field e
  ) =>
  Matrix r1 e ->
  Matrix r2 e ->
  m (Matrix r3 e)
a <\> b = do
  case pureTry $ hmA HMat.<\> hmB of
    Left _ -> throwM EquationSystemException
    Right res -> return . hm2m $ res
 where
  hmA = m2hm a
  hmB = m2hm b

-- | See 'HMat.linearSolveLS'.
linearSolveLS ::
  ( MonadThrow m
  , Manifest r1 e
  , Manifest r2 e
  , Manifest r3 e
  , Load r1 Ix1 e
  , Load r2 Ix1 e
  , Load r3 Ix1 e
  , HMat.Field e
  ) =>
  Matrix r1 e ->
  Matrix r2 e ->
  m (Matrix r3 e)
linearSolveLS a b = do
  case pureTry $ HMat.linearSolveLS hmA hmB of
    Left _ -> throwM EquationSystemException
    Right res -> return . hm2m $ res
 where
  hmA = m2hm a
  hmB = m2hm b

-- | See 'HMat.linearSolveSVD'.
linearSolveSVD ::
  ( MonadThrow m
  , Manifest r1 e
  , Manifest r2 e
  , Manifest r3 e
  , Load r1 Ix1 e
  , Load r2 Ix1 e
  , Load r3 Ix1 e
  , HMat.Field e
  ) =>
  Matrix r1 e ->
  Matrix r2 e ->
  m (Matrix r3 e)
linearSolveSVD a b = do
  case pureTry $ HMat.linearSolveSVD hmA hmB of
    Left _ -> throwM EquationSystemException
    Right res -> return . hm2m $ res
 where
  hmA = m2hm a
  hmB = m2hm b

-- | See 'HMat.linearSolve'.
linearSolve ::
  ( MonadThrow m
  , Manifest r1 e
  , Manifest r2 e
  , Manifest r3 e
  , Load r1 Ix1 e
  , Load r2 Ix1 e
  , Load r3 Ix1 e
  , HMat.Field e
  ) =>
  Matrix r1 e ->
  Matrix r2 e ->
  m (Matrix r3 e)
linearSolve a b = do
  case HMat.linearSolve hmA hmB of
    Nothing -> throwM SingularMatrixException
    Just res -> return . hm2m $ res
 where
  hmA = m2hm a
  hmB = m2hm b

-- | See 'HMat.svd'.
svd ::
  ( Manifest r1 e
  , Manifest r2 e
  , Manifest r3 e
  , Manifest r3 Double
  , Load r1 Ix1 e
  , Load r2 Ix1 e
  , Load r3 Ix1 e
  , Load r3 Ix1 Double
  , HMat.Field e
  ) =>
  Matrix r1 e ->
  (Matrix r2 e, Vector r3 Double, Matrix r3 e)
svd m =
  let (u, s, v) = HMat.svd hm
   in (hm2m u, hv2v s, hm2m v)
 where
  hm = m2hm m
