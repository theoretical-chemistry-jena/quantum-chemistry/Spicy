{-# LANGUAGE UndecidableInstances #-}

{- |
Module      : Spicy.Math.LinearAlgebra.R3
Description : Linear algebra specialised to \(\mathbb{R}^3\)
Copyright   : Phillip Seeber, Sebastian Seidenath 2024
License     : AGPL-3
Maintainer  : phillip.seeber@uni-jena.de
Stability   : experimental
Portability : POSIX, Windows
-}
module Spicy.Math.LinearAlgebra.R3 (
  -- * \( \mathbb{R}^3 \) Vectors
  R3,
  R3FP (..),
  R3ExpFP (..),
  r3,
  xR3,
  yR3,
  zR3,
  xyzR3,
  mkR3T,
  mkR3,

  -- * Distance between Points
  DistFn,
  lpNorm,
  manhattanDistance,
  euclideanDistance,

  -- * Sets of Points
  geometricCentre,
  centreOfMass,
  SetDistFn,
  setDistMin,
  setDistMax,
  setDistGeometricCentre,
  setDistCentreOfMass,

  -- * Linear Algebra Operations
  r3sToMat,
  magnitude,
  angle,
  distMat,
  distMat',
  crossProd,
  (<.>),

  -- * Elementwise Operations
  (.+),
  (+.),
  (!+!),
  (.-),
  (-.),
  (!-!),
  (.*),
  (*.),
  (!*!),
  (.^),
)
where

import Data.Aeson hiding (Array)
import Data.Massiv.Array as Massiv hiding (
  (!*!),
  (!+!),
  (!-!),
  (*.),
  (+.),
  (-.),
  (.*),
  (.+),
  (.-),
  (.^),
 )
import Data.Massiv.Array qualified as Massiv
import Data.Monoid
import GHC.TypeNats
import RIO.Partial (fromJust)
import Spicy.FiniteCompare
import Spicy.Massiv ()
import Spicy.Prelude hiding (Index)

{- | A vector in \( \mathbb{R}^3 \) Hilbert space. Operations over those can be
safe.
-}
newtype R3 r e = R3 {r3 :: Vector r e}
  deriving (Generic)

instance (Manifest r e, Show e) => Show (R3 r e) where
  show (R3 v) =
    let x = v Massiv.! 0
        y = v Massiv.! 1
        z = v Massiv.! 2
     in "mkR3T " <> "(" <> show x <> ", " <> show y <> ", " <> show z <> ")"

deriving instance (Eq (Vector r e)) => Eq (R3 r e)

deriving instance (Ord (Vector r e)) => Ord (R3 r e)

deriving instance (Stream r Ix1 Double, Manifest r Double) => Binary (R3 r Double)

instance (NFData (Vector r e)) => NFData (R3 r e)

instance (ToJSON e, Manifest r e) => ToJSON (R3 r e) where
  toJSON (R3 v) = spicyToJSON (v Massiv.! 0, v Massiv.! 1, v Massiv.! 2)

instance (Manifest r e, FromJSON e) => FromJSON (R3 r e) where
  parseJSON v = mkR3T <$> spicyParseJSON @(e, e, e) v

-- | 'R3' vectors compared via 'RealFP'.
newtype R3FP (n :: Nat) v = R3FP v deriving (Generic)

instance (RealFloat e, Source r e, KnownNat n) => Eq (R3FP n (R3 r e)) where
  R3FP (R3 a) == R3FP (R3 b) = a' == b'
   where
    a' = Massiv.map (RealFP @n) a
    b' = Massiv.map (RealFP @n) b

-- | 'R3' vectors compared via 'RealExpFP'.
newtype R3ExpFP (n :: Nat) v = R3ExpFP v deriving (Generic)

instance (RealFloat e, Source r e, KnownNat n) => Eq (R3ExpFP n (R3 r e)) where
  R3ExpFP (R3 a) == R3ExpFP (R3 b) = a' == b'
   where
    a' = Massiv.map (RealExpFP @n) a
    b' = Massiv.map (RealExpFP @n) b

xR3, yR3, zR3 :: (Manifest r e) => R3 r e -> e
xR3 (R3 v) = v ! 0
yR3 (R3 v) = v ! 1
zR3 (R3 v) = v ! 2

xyzR3 :: (Manifest r e) => R3 r e -> (e, e, e)
xyzR3 v = (xR3 v, yR3 v, zR3 v)

-- | Safe constructor for \( \mathbb{R}^3 \) vectors from triples
mkR3T :: (Manifest r e) => (e, e, e) -> R3 r e
mkR3T (x, y, z) = R3 . Massiv.fromList Seq $ [x, y, z]

-- | Safe constructor for \( \mathbb{R}^3 \) vectors from plain 'Array's
mkR3 :: (MonadThrow f, Massiv.Index ix, Size r) => Array r ix e -> f (R3 r e)
mkR3 v = R3 <$> resizeM (Sz 3) v

-- | A function to calculate the distance between two points in \( \mathbb{R}^3 \).
type DistFn r e = R3 r e -> R3 r e -> e

{- | The \(L_p\) norm between two vectors. Generalisation of Manhattan and Euclidean distances.

\[
  d(\mathbf{a}, \mathbf{b}) = \left( \sum \limits_{i=1}^n \lvert \mathbf{a}_i - \mathbf{b}_i \rvert ^p \right) ^ \frac{1}{p}
\]
-}
lpNorm ::
  ( Numeric r e
  , Source r e
  , Floating e
  ) =>
  Natural ->
  DistFn r e
lpNorm p (R3 a) (R3 b) =
  (** (1 / fromIntegral p))
    . Massiv.sum
    . Massiv.map (abs . (^ fromIntegral p))
    $ (a Massiv.!-! b)

-- | Manhattan distance of two vectors.
manhattanDistance ::
  ( Source r e
  , Floating e
  , Numeric r e
  ) =>
  DistFn r e
manhattanDistance a b = lpNorm 1 a b

-- | Euclidean distance of two vectors.
euclideanDistance ::
  ( Source r e
  , Floating e
  , Numeric r e
  ) =>
  DistFn r e
euclideanDistance a b = lpNorm 2 a b

{- | Calculate the geometric centre of a some points

\[
p(X) = \frac{\sum\limits_{x \in X} x}{|X|}
\]
-}
geometricCentre ::
  (Foldable f, Manifest r a, Numeric r a, Fractional a, MonadThrow m) =>
  f (R3 r a) ->
  m (R3 r a)
geometricCentre points
  | null points = throwM $ SizeEmptyException (Sz1 0)
  | otherwise = return $ nominator .* (1 / denominator)
 where
  n = length points
  zeroVec = mkR3T (0, 0, 0)
  nominator = foldl' (!+!) zeroVec points
  denominator = fromIntegral n

{- | Calculate the centre of mass of some points with weights

\[
p(X) = \frac{\sum\limits_{x \in X} x \, m(x)}{\sum\limits_{x \in X} m(x)}
\]
-}
centreOfMass ::
  ( Foldable f
  , Functor f
  , Manifest r a
  , Numeric r a
  , Fractional a
  , MonadThrow m
  ) =>
  f (R3 r a, a) ->
  m (R3 r a)
centreOfMass points
  | null points = throwM $ SizeEmptyException (Sz1 0)
  | otherwise = return $ nominator .* (1 / denominator)
 where
  zeroVec = mkR3T (0, 0, 0)
  nominator = foldl' (\acc (p, m) -> acc !+! (p .* m)) zeroVec points
  denominator = foldl' (+) 0 . fmap snd $ points

{- | Distance function between two sets of weighted points. The distance
involving an empty set throws an exception.
-}
type SetDistFn m f r a = f (R3 r a, a) -> f (R3 r a, a) -> m a

{- | The distance between two sets of points is their minimal distance between
any pair of points.

\[
\min\limits_{a \in A \\ b \in B} d(a, b)
\]
-}
setDistMin ::
  ( Foldable f
  , Manifest r a
  , Load r Ix2 a
  , Functor f
  , MonadThrow m
  , Ord a
  ) =>
  DistFn r a ->
  SetDistFn m f r a
setDistMin dFn setA setB = minimumM dm
 where
  dm = distMat dFn (fmap fst setA) (fmap fst setB)

{- | The distance between two sets of points is their maximal distance between
any pair of points.

\[
\max\limits_{a \in A \\ b \in B} d(a, b)
\]
-}
setDistMax ::
  ( Foldable f
  , Manifest r a
  , MonadThrow m
  , Ord a
  , Load r Ix2 a
  , Functor f
  ) =>
  DistFn r a ->
  SetDistFn m f r a
setDistMax dFn setA setB = maximumM dm
 where
  dm = distMat dFn (fmap fst setA) (fmap fst setB)

-- | The distance between two sets of points is the distance between their geometric centres.
setDistGeometricCentre ::
  ( Foldable f
  , Manifest r a
  , Numeric r a
  , Fractional a
  , Functor f
  , MonadThrow m
  ) =>
  DistFn r a ->
  SetDistFn m f r a
setDistGeometricCentre dFn setA setB = do
  cA <- geometricCentre . fmap fst $ setA
  cB <- geometricCentre . fmap fst $ setB
  return $ dFn cA cB

-- | The distance between two sets of points is the distance between their centres of mass.
setDistCentreOfMass ::
  (Foldable f, Manifest r a, Numeric r a, Fractional a, MonadThrow m, Functor f) =>
  DistFn r a ->
  SetDistFn m f r a
setDistCentreOfMass dFn setA setB = do
  cA <- centreOfMass setA
  cB <- centreOfMass setB
  return $ dFn cA cB

-- | Make a matrix of \( \mathbb{R}^3 \) points as row vectors.
r3sToMat :: (Foldable f, Functor f, Source r e) => f (R3 r e) -> Matrix DL e
r3sToMat vecs = fromJust . stackOuterSlicesM . fmap r3 $ vecs

-- | Magnitude of a vector
magnitude :: (Source r e, Floating e) => R3 r e -> e
magnitude (R3 v) = sqrt . Massiv.sum . Massiv.map (^ 2) $ v

{- | Numerically stable angle between two vectors,
see <https://www.jwwalker.com/pages/angle-between-vectors.html>

\[
  \alpha = 2 \operatorname{atan2} (\lVert \lVert \mathbf{v} \rVert \mathbf{u} - \lVert \mathbf{u} \rVert v \rVert,
  \lVert \lVert \mathbf{v} \rVert \mathbf{u} + \lVert \mathbf{u} \rVert v \rVert )
\]
-}
angle :: (Numeric r e, RealFloat e, Source r e) => R3 r e -> R3 r e -> e
angle u v = do
  let magU = magnitude u
      magV = magnitude v
      magVxU = magV *. u
      magUxV = magU *. v
      l = magVxU !-! magUxV
      r = magVxU !+! magUxV
   in (2 *) $ atan2 (magnitude l) (magnitude r)

{- | Distance matrix between two sets of points \( \boldsymbol{A} \) and \( \boldsymbol{B} \).
Points are row vectors of each matrix, e.g.

\[
  \boldsymbol{A} = \begin{pmatrix}
    \boldsymbol{a}_1 \\
    \vdots \\
    \boldsymbol{a}_n
  \end{pmatrix} \, .
\]

The resulting distance matrix will have a dimension of \( n \times m \),
where \(n\) is the number of rows in \( \boldsymbol{A} \) and \(m\) the number
of rows in \( \boldsymbol{B} \).
-}
distMat ::
  ( Manifest r e
  , Load r Ix2 e
  , Foldable f1
  , Functor f1
  , Foldable f2
  , Functor f2
  ) =>
  DistFn r e ->
  f1 (R3 r e) ->
  f2 (R3 r e) ->
  Matrix r e
distMat distFn a b = makeArray Par (Sz $ rA :. rB) $ \(iA :. iB) ->
  distFn (R3 $ matA Massiv.!> iA) (R3 $ matB Massiv.!> iB)
 where
  matA = Massiv.compute . r3sToMat $ a
  Sz (rA :. _) = size matA
  matB = Massiv.compute . r3sToMat $ b
  Sz (rB :. _) = size matB

-- | A distance matrix of all points to all other points
distMat' ::
  (Manifest r e, Load r Ix2 e, Foldable f2, Functor f2) =>
  DistFn r e ->
  f2 (R3 r e) ->
  Matrix r e
distMat' distFn a = distMat distFn a a

-- | Cross product of two \( \mathbb{R}^3 \) vectors
crossProd ::
  (Num e, Manifest r1 e, Manifest r2 e, Manifest r3 e) =>
  R3 r1 e ->
  R3 r2 e ->
  R3 r3 e
crossProd (R3 a) (R3 b) =
  R3
    $ Massiv.fromList
      Seq
      [ (a ! 1) * (b ! 2) - (a ! 2) * (b ! 1)
      , (a ! 2) * (b ! 0) - (a ! 0) * (b ! 2)
      , (a ! 0) * (b ! 1) - (a ! 1) * (b ! 0)
      ]

-- | Dot product of two vectors
(<.>) :: (Numeric r e, Source r e) => R3 r e -> R3 r e -> e
(R3 a) <.> (R3 b) = a !.! b

-- | Add scalar from right to all elements of a vector.
(.+) :: (Numeric r e) => R3 r e -> e -> R3 r e
(R3 a) .+ b = R3 $ a Massiv..+ b

-- | Add scalar from left to all elements of a vector.
(+.) :: (Numeric r e) => e -> R3 r e -> R3 r e
a +. (R3 b) = R3 $ a Massiv.+. b

-- | Add two vectors.
(!+!) :: (Numeric r e) => R3 r e -> R3 r e -> R3 r e
(R3 a) !+! (R3 b) = R3 $ a Massiv.!+! b

-- | Subtract scalar from right from elements of a vector
(.-) :: (Numeric r e) => R3 r e -> e -> R3 r e
(R3 a) .- b = R3 $ a Massiv..- b

-- | Subtract scalar from left from elements of a vector
(-.) :: (Numeric r e) => e -> R3 r e -> R3 r e
a -. (R3 b) = R3 $ a Massiv.-. b

-- | Subtract two vectors.
(!-!) :: (Numeric r e) => R3 r e -> R3 r e -> R3 r e
(R3 a) !-! (R3 b) = R3 $ a Massiv.!-! b

-- | Multiply vector with a scalar from the right.
(.*) :: (Numeric r e) => R3 r e -> e -> R3 r e
(R3 a) .* b = R3 $ a Massiv..* b

-- | Multiply vector with a scalar from the left.
(*.) :: (Numeric r e) => e -> R3 r e -> R3 r e
a *. (R3 b) = R3 $ a Massiv.*. b

-- | Hadamard product of two vectors.
(!*!) :: (Numeric r e) => R3 r e -> R3 r e -> R3 r e
(R3 a) !*! (R3 b) = R3 $ a Massiv.!*! b

-- | Raise vector to a power.
(.^) :: (Numeric r e) => R3 r e -> Int -> R3 r e
(R3 a) .^ b = R3 $ a Massiv..^ b
