{- |
Module      : Spicy.Math.LinearAlgebra.R3.NeighbourList
Description : Neighbourlists for \(\mathbb{R}^3\) points in space
Copyright   : Phillip Seeber, Sebastian Seidenath 2023
License     : AGPL-3
Maintainer  : phillip.seeber@uni-jena.de
Stability   : experimental
Portability : POSIX, Windows

Neighbour lists allow efficient lookup of neighbours of a point in space up to a given, possibly infinite, distance while preferrably avoiding construction of a distance matrix.
Different types of neighbour lists are available, each with different performance characteristics and limitations.
-}
module Spicy.Math.LinearAlgebra.R3.NeighbourList (
  -- * Exceptions
  NeighbourListException (..),

  -- * Neighbour Lists
  NeighbourList (..),
  DistMatNeighbours,
  getDistMat,
  KdNeighbours,
  getKDT,
  CellList,
  getCellList,

  -- * Neighbour Lists between Sets of Points
  SetNeighbourList (..),
  WeightedGroup,
  IndexedGroups,
  SetNeighbours,
) where

import Spicy.Math.LinearAlgebra.R3.NeighbourList.CellList
import Spicy.Math.LinearAlgebra.R3.NeighbourList.Common
import Spicy.Math.LinearAlgebra.R3.NeighbourList.DistanceMatrix
import Spicy.Math.LinearAlgebra.R3.NeighbourList.KD
import Spicy.Math.LinearAlgebra.R3.NeighbourList.SetNeighbours
