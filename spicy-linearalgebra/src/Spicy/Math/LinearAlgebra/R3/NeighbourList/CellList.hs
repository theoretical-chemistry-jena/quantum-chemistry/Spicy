{- |
Module      : Spicy.Math.LinearAlgebra.R3.NeighbourList.CellList
Description : Cell list based Neighbourlist
Copyright   : Phillip Seeber, Sebastian Seidenath 2023
License     : AGPL-3
Maintainer  : phillip.seeber@uni-jena.de
Stability   : experimental
Portability : POSIX, Windows

A neighbourlist based on a [cell list](https://en.wikipedia.org/wiki/Cell_lists).
See the docs on 'CellList' for more information.
-}
module Spicy.Math.LinearAlgebra.R3.NeighbourList.CellList where

import Data.Massiv.Array as Massiv hiding (toList)
import Data.Massiv.Array.Unsafe (makeUnsafeStencil)
import RIO.HashMap qualified as HashMap
import RIO.Partial (fromJust)
import Spicy.IntMap qualified as IntMap
import Spicy.Massiv
import Spicy.Math.LinearAlgebra.R3 as R3
import Spicy.Math.LinearAlgebra.R3.NeighbourList.Common
import Spicy.Prelude hiding (Index, (:>))

{- | A neighbourlist based on a [cell list](https://en.wikipedia.org/wiki/Cell_lists).
All results are only valid up to the
specified maximum distance.

  * 'mkNeighbourList' works heavily parallel and should scale approximately with
    \( \mathcal{O}(N) \). The neighbourlist is only valid up to the specified
    maximum distance and the computation becomes increasingly more expensive
    with larger distances.
  * 'getAll' provides all neighbours /within the specified maximum distance/
    in order of the input. The time complexity is \( \mathcal{O}(\min(N, 64) \),
    thus approximately constant time for large neighbourlists.
  * 'getNearest' and 'getWithin' just filter the results obtained from 'getAll'
    and thus are also roughly constant time operations.
-}
data CellList i r a = CellList
  { maxValidDist :: a
  , cellList :: IntMap (IntMap a)
  -- ^ Mapping from point index to its neighbours and their distances.
  , distFunction :: DistFn r a
  -- ^ Distance function used to create the neighbourlist
  , origPoints :: HashMap i (R3 r a)
  -- ^ Original set of indexed points
  }
  deriving (Generic)

-- | Obtain the underlying cell list from 'CellList'.
getCellList :: CellList Int r a -> IntMap (IntMap a)
getCellList = cellList

-- | See 'CellList'
instance
  ( Manifest r a
  , Manifest r Int
  , Numeric r a
  , RealFrac a
  , Show a
  ) =>
  NeighbourList CellList Int r a
  where
  mkNeighbourList distFunction ps maxD
    -- The bin grid would have infinite extents for an empty point set. Thus we
    -- catch this case and return an empty neighbourlist.
    | null ps =
        CellList
          { maxValidDist = maxD
          , cellList = mempty
          , distFunction
          , origPoints = mempty
          }
    | otherwise =
        CellList
          { maxValidDist = maxD
          , cellList = Massiv.fold . compute @B $ neighbourGrid
          , distFunction
          , origPoints = HashMap.fromList . toList $ ps
          }
   where
    coordMat = Massiv.compute @r . r3sToMat . fmap snd $ ps
    d2sMap = d2sMapping . fmap fst $ ps :: Vector r Int

    -- Get the extent of all points that need to be spanned by the bin grid.
    minExt = compute @r . foldlInner min (1 / 0) . transpose $ coordMat
    maxExt = compute @r . foldlInner max (-1 / 0) . transpose $ coordMat

    --  Obtain the bin index of a coordinate
    crdToBinIx :: Vector r a -> Ix3
    crdToBinIx crd =
      let ixes = compute @r . Massiv.map (\e -> floor $ e / maxD) $ crd Massiv.!-! minExt
       in (ixes ! 0) :> (ixes ! 1) :. (ixes ! 2)

    -- Get the size of the bin grid and prepare an empty bin grid.
    szBinGrid = Sz . liftIndex (+ 1) . crdToBinIx $ maxExt
    emptyBinGrid = Massiv.replicate @D Par szBinGrid mempty

    -- Sort all points into the bin grid in parallel.
    binGrid :: Array B Ix3 [(Int, Vector r a)]
    binGrid = withLoadMArrayST_ @B emptyBinGrid $ \binGridM -> do
      iforM_ (outerSlices coordMat) $ \dIx crd -> do
        let spIx = d2s' d2sMap dIx
            binIx = crdToBinIx crd
        modify_ binGridM (\e -> return $ (spIx, crd) : e) binIx

    -- Create a 3x3x3 neighbourhood stencil that computes the central cell's
    -- neigbhours.
    nghbrStencil :: Stencil Ix3 [(Int, Vector r a)] (IntMap (IntMap a))
    nghbrStencil = makeUnsafeStencil (Sz $ 3 :> 3 :. 3) (1 :> 1 :. 1) $ \_ look ->
      let validIx = [-1 .. 1]
          centreCell = look (0 :> 0 :. 0)
          superCell = concat [look (x :> y :. z) | x <- validIx, y <- validIx, z <- validIx]
       in corrNeighbours centreCell superCell

    -- Get the neighbours of the points of one cell with all other points in the
    -- supercell.
    corrNeighbours ::
      -- Points in the central cell
      [(Int, Vector r a)] ->
      -- Points in the super cell
      [(Int, Vector r a)] ->
      IntMap (IntMap a)
    corrNeighbours centreCell superCell =
      IntMap.fromListWith
        (<>)
        [ (cIx, IntMap.singleton sIx dist)
        | (cIx, cCrd) <- centreCell
        , (sIx, sCrd) <- superCell
        , let dist = distFunction (fromJust $ mkR3 cCrd) (fromJust $ mkR3 sCrd)
        , cIx /= sIx
        , dist <= maxD
        ]

    -- Apply the neighbour stencil to the bin grid and create chunks of the
    -- final neighbourlist within the bins.
    neighbourGrid = mapStencil (Fill mempty) nghbrStencil binGrid

  maxDist = maxValidDist

  distFn CellList{..} = distFunction

  points CellList{..} = origPoints

  getAll CellList{..} i =
    maybe2MThrow (NeighbourListIndexNotFound $ show i) $ IntMap.toList <$> cellList IntMap.!? i

  getNearest cl i = do
    nghbrs <- getAll cl i
    case length nghbrs of
      0 -> return Nothing
      _ -> return . Just $ foldl' f (undefined, 1 / 0) nghbrs
   where
    f (minK, minD) (k, d) = if d < minD then (k, d) else (minK, minD)

  getWithin cl@CellList{..} d i
    | d > maxValidDist = throwM $ DistanceTooLarge maxValidDist d
    | otherwise = do
        nghbrs <- getAll cl i
        return . filter (\(_, dist) -> dist <= d) $ nghbrs
