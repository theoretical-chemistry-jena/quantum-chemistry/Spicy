{-# OPTIONS_GHC -Wno-redundant-constraints #-}

{- |
Module      : Spicy.Math.LinearAlgebra.R3.NeighbourList.Common
Description : Class and API for Neighbour lists in \(\mathbb{R}^3\)
Copyright   : Phillip Seeber, Sebastian Seidenath 2023
License     : AGPL-3
Maintainer  : phillip.seeber@uni-jena.de
Stability   : experimental
Portability : POSIX, Windows
-}
module Spicy.Math.LinearAlgebra.R3.NeighbourList.Common (
  NeighbourListException (..),
  NeighbourList (..),
) where

import Data.Massiv.Array as Massiv hiding (sum, toList)
import GHC.Stack (callStack, prettyCallStack)
import Spicy.Math.LinearAlgebra.R3 as R3
import Spicy.Prelude

-- | Exceptions that can occur when using neighbourlists.
data NeighbourListException where
  NeighbourListIndexNotFound :: (HasCallStack) => String -> NeighbourListException
  DistanceTooLarge :: (HasCallStack, Show a) => a -> a -> NeighbourListException

instance Exception NeighbourListException

instance Show NeighbourListException where
  show (NeighbourListIndexNotFound msg) =
    "NeighbourListIndexNotFound"
      <> msg
      <> "\n"
      <> prettyCallStack callStack
  show (DistanceTooLarge allowedDist dist) =
    "DistanceTooLarge: "
      <> show dist
      <> " > "
      <> show allowedDist
      <> "\n"
      <> prettyCallStack callStack

{- | Neighbourlists allow to quickly find the neighbours of a point. Note, that we do not consider
a point neighbour to itself. The parameters have the following meaning:

  * @l@ is the type of the neighbourlist
  * @i@ is the type of the index/key of the points
  * @r@ is the Massiv representation of both the points from the input and
    within the neighbourlist data structure instances
  * @a@ is the element type of the points
-}
class (Show i, Hashable i) => NeighbourList l i r a where
  -- | Create a neighbourlist from a list of points and a maximum distance.
  -- We assume that the indexed points are unique in their indices. If this
  -- condition is not satisfied the behaviour depends on the concrete
  -- instance.
  mkNeighbourList :: (Traversable f, Source r a, HasCallStack, Show (Array r Ix1 a)) => R3.DistFn r a -> f (i, R3 r a) -> a -> l i r a

  -- | Maximum distance up to which the neighbourlist is valid.
  maxDist :: l i r a -> a

  -- | Obtain the distance function this neighbour list was created with.
  distFn :: l i r a -> R3.DistFn r a

  -- | Obtain all points in the neighbourlist.
  points :: l i r a -> HashMap i (R3 r a)

  -- | Get the neighbours of a point in arbitrary order.
  getAll :: (MonadThrow m, HasCallStack) => l i r a -> i -> m [(i, a)]

  -- | Get the nearest neighbour of a point.
  getNearest :: (MonadThrow m, HasCallStack) => l i r a -> i -> m (Maybe (i, a))

  -- | Get all neighbours within a given distance in arbitrary order.
  getWithin :: (MonadThrow m, HasCallStack) => l i r a -> a -> i -> m [(i, a)]
