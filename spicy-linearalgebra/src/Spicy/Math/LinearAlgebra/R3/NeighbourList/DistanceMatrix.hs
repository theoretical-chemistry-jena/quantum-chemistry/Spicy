{- |
Module      : Spicy.Math.LinearAlgebra.R3.NeighbourList.DistanceMatrix
Description : Distance matrix based Neighbourlist
Copyright   : Phillip Seeber, Sebastian Seidenath 2023
License     : AGPL-3
Maintainer  : phillip.seeber@uni-jena.de
Stability   : experimental
Portability : POSIX, Windows
-}
module Spicy.Math.LinearAlgebra.R3.NeighbourList.DistanceMatrix (
  DistMatNeighbours,
  getDistMat,
)
where

import Data.Massiv.Array as Massiv hiding (swap, toList, zip)
import Data.Tuple (swap)
import RIO.HashMap qualified as HashMap
import Spicy.Massiv
import Spicy.Math.LinearAlgebra.R3 as R3
import Spicy.Math.LinearAlgebra.R3.NeighbourList.Common
import Spicy.Prelude

{- | A neighbourlist based on the full [distance matrix](https://en.wikipedia.org/wiki/Distance_matrix).

  * Memory requirement scales with \( \mathcal{O}(N^2) \)
  * 'mkNeighbourList' time scales with \( \mathcal{O}(N^2) \) but is at least parallelised.
  * 'getAll' obtains a full slice of the distance matrix and is a \( \mathcal{O}(1) \).
  * 'getNearest' and 'getWithin' merely filter results from 'getAll' and thus are \( \mathcal{O}(N) \) operations.
  * A distance matrix is valid for all distances, thus @'maxDist' _@ = \( \infty \)
-}
data DistMatNeighbours i r a = DistMatNeighbours
  { dists :: Matrix r a
  , origPoints :: HashMap i (R3 r a)
  , origToVec :: HashMap i Ix1
  , vecToOrig :: Vector r i
  , distFunction :: DistFn r a
  }
  deriving (Generic)

-- | Obtain the underlying distance matrix from 'DistMatNeighbours'.
getDistMat :: DistMatNeighbours i r a -> Matrix r a
getDistMat = dists

-- | See 'DistMatNeighbours'.
instance
  ( Manifest r a
  , Load r Ix2 a
  , Fractional a
  , Manifest r Int
  , Unbox a
  , Ord a
  ) =>
  NeighbourList DistMatNeighbours Int r a
  where
  mkNeighbourList distFunction ps _ =
    DistMatNeighbours
      { dists = R3.distMat' distFunction . fmap snd $ ps
      , origToVec = s2dMapping spIxes
      , vecToOrig = d2sMapping spIxes
      , origPoints = HashMap.fromList . toList $ ps
      , distFunction
      }
   where
    spIxes = fst <$> ps

  maxDist _ = 1 / 0

  points = origPoints

  distFn = distFunction

  getAll dm i = Massiv.stoList <$> getNeighbourSlice dm i

  getNearest dm i = do
    nghbrs <- compute @U . Massiv.smap swap <$> getNeighbourSlice dm i
    case minimumM nghbrs of
      Nothing -> return Nothing
      Just (d, k) -> return . Just $ (k, d)

  getWithin dmn d i = do
    nghbrs <- getNeighbourSlice dmn i
    return . stoList . sfilter ((<= d) . snd) $ nghbrs

-- | Get a slice from the neighbourlist, indexed with the original, sparse indices.
getNeighbourSlice ::
  ( MonadThrow m
  , Source r a
  , Manifest r Int
  ) =>
  DistMatNeighbours Int r a ->
  Ix1 ->
  m (Vector DS (Int, a))
getNeighbourSlice DistMatNeighbours{..} i = do
  dIx <- s2d origToVec i
  nghbrs <- dists !?> dIx
  let ixedNghbrs = flip Massiv.imap nghbrs $ \dk d -> (d2s' vecToOrig dk, d)
      selfFreeNghbrs = sfilter ((/= i) . fst) ixedNghbrs
  return selfFreeNghbrs
