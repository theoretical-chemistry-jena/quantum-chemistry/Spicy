{-# OPTIONS_GHC -Wno-redundant-constraints #-}

{- |
Module      : Spicy.Math.LinearAlgebra.R3.NeighbourList.KD
Description : KD-tree based Neighbourlist
Copyright   : Phillip Seeber, Sebastian Seidenath 2023
License     : AGPL-3
Maintainer  : phillip.seeber@uni-jena.de
Stability   : experimental
Portability : POSIX, Windows
-}
module Spicy.Math.LinearAlgebra.R3.NeighbourList.KD (
  KdNeighbours,
  getKDT,
) where

import Data.KdMap.Static as KD
import Data.Massiv.Array as Massiv hiding (swap, toList)
import Data.Massiv.Array qualified as Massiv
import Data.Tuple (swap)
import RIO.HashMap qualified as HashMap

import Spicy.Math.LinearAlgebra.R3 as R3
import Spicy.Math.LinearAlgebra.R3.NeighbourList.Common
import Spicy.Prelude

{- | A neighbourlist based on a (static) [kd-tree](https://en.wikipedia.org/wiki/K-d_tree).

  * 'mkNeighbourList' time scales on average with \( \mathcal{O}(N \log N) \)
  * 'getAll' is a bad idea for this data type and requires the calculation of a
    full row of the distance matrix with \( \mathcal{O}(N) \) time complexity.
  * 'getNearest' time scales with \( \mathcal{O}(\log N) \)
  * 'getWithin' retrieves all points with the given range via 'inRadius' and
    removes the self-interaction.
-}
data KdNeighbours i r a = KdNeighbours
  { kdmap :: KdMap a (R3 r a) i
  , origPoints :: HashMap i (R3 r a)
  , distFunction :: DistFn r a
  }
  deriving (Generic)

-- | Obtain the underlying kd-tree from 'KdNeighbours'.
getKDT :: KdNeighbours i r a -> KdMap a (R3 r a) i
getKDT = kdmap

-- | See 'KdNeighbours'.
instance (Show a, Show i, Hashable a, Hashable i, Real a, Fractional a, Source r a) => NeighbourList KdNeighbours i r a where
  mkNeighbourList distFunction ps _ =
    KdNeighbours
      { kdmap = buildWithDist (Massiv.toList . r3) sqDistFn . fmap swap $ pointList
      , origPoints = HashMap.fromList pointList
      , distFunction
      }
   where
    sqDistFn a b = (^ 2) $ distFunction a b
    pointList = toList ps

  maxDist _ = 1 / 0

  distFn = distFunction

  points = origPoints

  getAll KdNeighbours{..} i = do
    lookupPoint <-
      maybe2MThrow (NeighbourListIndexNotFound $ show i)
        $ HashMap.lookup i origPoints
    let allPoints = fromList @B Par . assocs $ kdmap
        distPoints =
          compute @B
            . flip Massiv.smapMaybe allPoints
            $ \(p, k) ->
              if k == i
                then Nothing
                else Just (distFunction lookupPoint p, p, k)
        ascPoints = Massiv.map (\(d, _p, k) -> (k, d)) distPoints
    return . Massiv.toList $ ascPoints

  getNearest KdNeighbours{..} i = do
    lookupPoint <-
      maybe2MThrow (NeighbourListIndexNotFound $ show i)
        $ HashMap.lookup i origPoints
    case KD.kNearest kdmap 2 lookupPoint of
      [_x] -> return Nothing
      [(p1, i1), (p2, i2)]
        | i1 == i -> do
            let d = distFunction lookupPoint p2
            return $ Just (i2, d)
        | i2 == i -> do
            let d = distFunction lookupPoint p1
            return $ Just (i1, d)
      _ -> error "Impossible: kNearest returned more than one result for k=1"

  getWithin KdNeighbours{..} d i = do
    lookupPoint <-
      maybe2MThrow (NeighbourListIndexNotFound $ show i)
        $ HashMap.lookup i origPoints
    let pointsWithin = Massiv.sfromList $ inRadius kdmap d lookupPoint
        distPoints = flip Massiv.smapMaybe pointsWithin $ \(p, k) ->
          if k == i
            then Nothing
            else Just (k, distFunction lookupPoint p)
    return . Massiv.stoList $ distPoints
