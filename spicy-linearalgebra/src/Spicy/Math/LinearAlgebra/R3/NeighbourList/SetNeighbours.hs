module Spicy.Math.LinearAlgebra.R3.NeighbourList.SetNeighbours (
  SetNeighbourList (..),
  WeightedGroup,
  IndexedGroups,
  SetNeighbours,
)
where

import Data.Bifunctor
import Data.Massiv.Array as Massiv hiding (toList)
import GHC.IO (unsafePerformIO)
import RIO.HashSet qualified as HashSet
import Spicy.HashMap qualified as HashMap
import Spicy.Math.LinearAlgebra.R3 as R3
import Spicy.Math.LinearAlgebra.R3.NeighbourList.Common
import Spicy.Prelude

{- | Neighbour lists between different sets of points. These distances between
groups can be obtained by different (group) distance functions.

  * @l@ is the type of the neighbourlist
  * @i@ is the type of the index/key of the groups
  * @i'@ is the type of the index/key of the points
  * @r@ is the representation of the points
  * @a@ is the element type of the points
-}
class (Ord a) => SetNeighbourList l i i' r a where
  -- | Make a neighbourlist between sets of points. The sets do not necessarily
  -- need to comprise the entirety of the set of points from the original
  -- 'Neighbourlist'.
  mkSetNeighbourList ::
    ( NeighbourList l' i' r a
    , MonadThrow m
    , Traversable f1
    , Traversable f2
    ) =>
    -- | Distance function between two sets
    SetDistFn m f2 r a ->
    -- | 'Neighbourlist' between all points of all sets.
    l' i' r a ->
    -- | Maximum distance of the group neighbourlist
    a ->
    -- | Indexed (@i@) sets and the weight of each indexed (@i'@) point within this
    -- set. Notice, that a point /may/ have different weights in different sets.
    -- Group indices @i@ should be unique, if not the behaviour depends on the
    -- concrete instance.
    f1 (i, f2 (i', a)) ->
    m (l i i' r a)

  -- | Get the original definition of the indexed groups
  groups :: l i i' r a -> IndexedGroups i i' a

  -- | Maximum distance between groups
  maxGrpD :: l i i' r a -> a

  -- | Get all neighbouring groups of a given one.
  getGroupNeighbours :: (MonadThrow m) => l i i' r a -> i -> m (HashMap i a)

  -- | Get all neighbouring groups within a given distance that is smaller than
  -- 'maxGrpD'.
  getGroupsWithin :: (MonadThrow m) => l i i' r a -> a -> i -> m (HashMap i a)

-- | A weighted group is a set of point indices @i@ with a weight @w@ for each point.
type WeightedGroup i w = HashMap i w

-- | A set of indexed (@i@), weighted groups.
type IndexedGroups i i' w = HashMap i (WeightedGroup i' w)

-- | Neighbourlists between sets of points.
data SetNeighbours i i' r a = SetNeighbours
  { groupNl :: IndexedGroups i i a
  , origGroups :: IndexedGroups i i' a
  , maxValidDist :: a
  }
  deriving (Show, Generic)

instance (Hashable i, Show i, Ord a, Show a) => SetNeighbourList SetNeighbours i i' r a where
  mkSetNeighbourList origGrpDistFn nl maxD inpGroups = do
    -- Query all neighbours of all groups in parallel in preparation of
    -- generating an overlap matrix of groups.
    grpNeighbours <- Massiv.sequenceA @B . unsafePerformIO . forIO @B inpGrpsArr $ \(grpIx, grpWghtPIx) -> do
      let !ixedGrpNghbrs = do
            grpPointNghbrs <- fmap (HashSet.fromList . concat) . for grpWghtPIx $ \(pIx, _) -> do
              fmap fst <$> getWithin nl maxD pIx
            let grpSelfPoints = HashSet.fromList . toList . fmap fst $ grpWghtPIx
                selfNghbrs = grpPointNghbrs `HashSet.union` grpSelfPoints
            return (grpIx, selfNghbrs)
      return ixedGrpNghbrs

    -- Check overlaps of all groups. Only for those pairs that have at least
    -- overlap of any two atoms are further checked for the actual distance
    -- criterion.
    let Sz nGrps = size grpNeighbours
    ovlpMat <- Massiv.sequenceA @B . unsafePerformIO . generateArray @B Par (Sz $ nGrps :. nGrps) $ \(i :. j) -> do
      let (iIx, iSelfNghbrs) = grpNeighbours ! i
          (jIx, jSelfNghbrs) = grpNeighbours ! j
          ovlp = iSelfNghbrs `HashSet.intersection` jSelfNghbrs
      if HashSet.null ovlp || i == j
        then return . return $ Nothing
        else do
          let !res = do
                iGrpPts <- do
                  let ixedWeights = snd $ inpGrpsArr ! i
                  for ixedWeights $ \(pIx, w) -> do
                    pnt <- HashMap.lookup pIx (points nl)
                    return (pnt, w)
                jGrpPts <- do
                  let ixedWeights = snd $ inpGrpsArr ! j
                  for ixedWeights $ \(pIx, w) -> do
                    pnt <- HashMap.lookup pIx (points nl)
                    return (pnt, w)
                grpDist <- origGrpDistFn iGrpPts jGrpPts
                if grpDist <= maxD
                  then return . Just $ HashMap.singleton iIx (HashMap.singleton jIx grpDist)
                  else return Nothing
          return res

    -- Fold all Just values of the overlap matrix into the final result
    let groupNl = unsafePerformIO . foldlP resFoldFA mempty resFoldFB mempty $ ovlpMat

    -- Return final group group neighbourlist
    return SetNeighbours{groupNl, origGroups, maxValidDist = maxD}
   where
    resFoldFA acc chunk = case chunk of
      Nothing -> acc
      Just chunk' -> HashMap.unionWith HashMap.union acc chunk'
    resFoldFB acc chunk = HashMap.unionWith HashMap.union acc chunk
    inpGrpsArr = compute @B . sfilter (not . null) . Massiv.sfromList . toList $ inpGroups
    origGroups =
      HashMap.fromListWith (<>)
        . toList
        . fmap (second (HashMap.fromList . toList))
        $ inpGroups

  groups = origGroups

  maxGrpD = maxValidDist

  getGroupNeighbours SetNeighbours{..} i = HashMap.lookup i groupNl

  getGroupsWithin setNl@SetNeighbours{..} maxD i
    | maxD > maxGrpD setNl = throwM $ DistanceTooLarge (maxGrpD setNl) maxD
    | otherwise = do
        grpNl <- getGroupNeighbours SetNeighbours{..} i
        return . HashMap.filter (<= maxD) $ grpNl
