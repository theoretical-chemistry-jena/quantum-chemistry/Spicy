{- |
Module      : Spicy.Math.Voxel
Description : Volumetric data on 3D Voxel grids
Copyright   : Phillip Seeber, Sebastian Seidenath 2024
License     : AGPL-3
Maintainer  : phillip.seeber@uni-jena.de
Stability   : experimental
Portability : POSIX, Windows
-}
module Spicy.Math.Voxel (
  -- * Exceptions
  VoxelException (..),

  -- * Cell
  Cell (..),

  -- * Voxel Coordinates
  voxelToCoord,
  coordToVoxelInterp,
) where

import Data.Massiv.Array as Massiv hiding (sum)
import RIO.Partial (fromJust)
import Spicy.Math.LinearAlgebra as LA
import Spicy.Math.LinearAlgebra.R3 as R3
import Spicy.Prelude hiding ((:>))

-- | Exceptions that can occur when working with volumetric data
data VoxelException
  = CoordinateOutOfGridException Cell (R3 S Double)
  deriving (Eq)

instance Show VoxelException where
  show (CoordinateOutOfGridException cell i) =
    "Coordinate " <> show i <> " is out of grid bounds for cell " <> show cell <> "."

instance Exception VoxelException

-- | A volumetric cell in \( \mathbb{R}^3 \).
data Cell = Cell
  { origin :: R3 S Double
  -- ^ Origin of the volumetric data, i.e. the cube's corner with lowest xyz
  -- value. Given in Bohr.
  , cellVecA :: R3 S Double
  -- ^ Cell vector \(\mathbf{a}\) spanning the volume.
  , cellVecB :: R3 S Double
  -- ^ Cell vector \(\mathbf{b}\) spanning the volume.
  , cellVecC :: R3 S Double
  -- ^ Cell vector \(\mathbf{c}\) spanning the volume.
  }
  deriving (Show, Generic, Eq)

{- | Calculate the actual physical coordinate of a voxel from its data
voxel-array's size, the index of the voxel and the cube's cell definition.
-}
voxelToCoord ::
  (MonadThrow m) =>
  -- | Cell definition
  Cell ->
  -- | Size of the voxel grid
  Sz3 ->
  -- | Index of the voxel
  Ix3 ->
  m (R3 S Double)
voxelToCoord Cell{..} sz@(Sz (nA :> nB :. nC)) i@(iA :> iB :. iC)
  | isSafeIndex sz i = pure coord
  | otherwise = throwM $ IndexOutOfBoundsException sz i
 where
  dA = (fromIntegral iA) / (fromIntegral nA - 1)
  dB = (fromIntegral iB) / (fromIntegral nB - 1)
  dC = (fromIntegral iC) / (fromIntegral nC - 1)
  coord =
    origin
      R3.!+! (dA R3.*. cellVecA)
      R3.!+! (dB R3.*. cellVecB)
      R3.!+! (dC R3.*. cellVecC)

{- | For a given coordinate, get the indices of relevant voxels. I.e. if the
coordinate is not exactly on the voxel a linear interpolation is performed. The
result indices are guaranteed to be safe for the given 'Sz3'.
-}
coordToVoxelInterp ::
  (MonadThrow m) =>
  -- | Cell definition of the volumetric data
  Cell ->
  -- | The voxel grid
  Sz3 ->
  -- | Coordinate within the valid range of the voxel grid
  R3 S Double ->
  -- | Weighted indices of the voxels and their interpolation coefficients
  m [(Ix3, Double)]
coordToVoxelInterp cell@Cell{..} sz3@(Sz (nA :> nB :. nC)) p
  | chk = return coeffs
  | otherwise = throwM $ CoordinateOutOfGridException cell p
 where
  -- Coordinate of the point within the cell in Cartesian coordinates
  p0 = r3 $ p R3.!-! origin

  -- Transformation matrix into the cell's coordinate system
  tM = fromJust . inv . compute @S . transpose . compute @S . r3sToMat $ [cellVecA, cellVecB, cellVecC] :: Matrix S Double

  -- Coordinate of the point in the cell's coordinate system
  p' = fromJust $ tM #> p0 :: Vector S Double
  pA = p' ! 0
  pB = p' ! 1
  pC = p' ! 2

  -- Check if the coordinate is within the cell, i.e. all components are in [0,1)
  chk = Massiv.all (\t -> t >= 0 && t <= 1) p'

  -- Fractional indices of the voxel
  iA = (fromIntegral nA - 1) * pA
  iB = (fromIntegral nB - 1) * pB
  iC = (fromIntegral nC - 1) * pC

  -- Index of the base voxel (lowest values along all axes)
  iA0 = floor iA
  iB0 = floor iB
  iC0 = floor iC
  ix000 = iA0 :> iB0 :. iC0

  -- The fractional distances to the Lower and Upper voxels along each axis
  cAL = 1 - (iA - fromIntegral iA0)
  cBL = 1 - (iB - fromIntegral iB0)
  cCL = 1 - (iC - fromIntegral iC0)
  cAU = iA - fromIntegral iA0
  cBU = iB - fromIntegral iB0
  cCU = iC - fromIntegral iC0

  -- Index-non-normalised coefficient pairs for the linear interpolation
  p000' = (ix000, cAL * cBL * cCL)
  p001' = (liftIndex2 (+) ix000 (0 :> 0 :. 1), cAL * cBL * cCU)
  p010' = (liftIndex2 (+) ix000 (0 :> 1 :. 0), cAL * cBU * cCL)
  p011' = (liftIndex2 (+) ix000 (0 :> 1 :. 1), cAL * cBU * cCU)
  p100' = (liftIndex2 (+) ix000 (1 :> 0 :. 0), cAU * cBL * cCL)
  p101' = (liftIndex2 (+) ix000 (1 :> 0 :. 1), cAU * cBL * cCU)
  p110' = (liftIndex2 (+) ix000 (1 :> 1 :. 0), cAU * cBU * cCL)
  p111' = (liftIndex2 (+) ix000 (1 :> 1 :. 1), cAU * cBU * cCU)

  -- Safe accessor function into the array
  coeffs' =
    filter
      (\(i, c) -> isSafeIndex sz3 i && c >= 1e-10)
      [p000', p001', p010', p011', p100', p101', p110', p111']
  normC = 1 / sum (fmap snd coeffs')
  coeffs = fmap (\(i, c) -> (i, c * normC)) coeffs'
