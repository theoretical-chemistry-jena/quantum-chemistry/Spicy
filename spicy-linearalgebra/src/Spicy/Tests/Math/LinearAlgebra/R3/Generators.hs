module Spicy.Tests.Math.LinearAlgebra.R3.Generators (genR3) where

import Data.Massiv.Array
import Hedgehog
import Hedgehog.Gen qualified as Gen
import Hedgehog.Range qualified as Range
import Spicy.Math.LinearAlgebra.R3
import Spicy.Prelude

genR3 :: Gen (R3 S Double)
genR3 = do
  x <- Gen.double (Range.exponentialFloat (-200) 200)
  y <- Gen.double (Range.exponentialFloat (-200) 200)
  z <- Gen.double (Range.exponentialFloat (-200) 200)
  return . mkR3T $ (x, y, z)
