module Spicy.Tests.Math.Internal (tests) where

import Spicy.Tests.Math.Internal.ArrayConversion qualified
import Test.Tasty

tests :: TestTree
tests =
  testGroup
    "Internal"
    [ Spicy.Tests.Math.Internal.ArrayConversion.tests
    ]
