module Spicy.Tests.Math.Internal.ArrayConversion (tests) where

import Data.Massiv.Array as Massiv
import Hedgehog
import Numeric.LinearAlgebra qualified as HMat
import Spicy.Math.Internal.ArrayConversion
import Spicy.Prelude (Applicative (pure), Double, Maybe, ($), (.))
import Spicy.Tests.Prelude.Generators
import Test.Tasty
import Test.Tasty.Hedgehog

tests :: TestTree
tests = testGroup "ArrayConversion" [vector, matrix]

vector :: TestTree
vector =
  testGroup
    "Vector"
    [ testProperty "Massiv-HMatrix equivalence" vecMassivHMatEquiv
    , testProperty "Massiv-HMatrix trip" vecMassivHmatTrip
    ]

vecMassivHMatEquiv :: Property
vecMassivHMatEquiv = property $ do
  vs <- forAll genVecL
  let v = Massiv.fromList @S @Double Par vs
      hv = HMat.fromList vs
  v === hv2v hv
  v2hv v === hv

vecMassivHmatTrip :: Property
vecMassivHmatTrip = property $ do
  vs <- forAll genVecL
  let v = Massiv.fromList @S @Double Par vs
  tripping v v2hv (pure @Maybe . hv2v)

matrix :: TestTree
matrix =
  testGroup
    "Matrix"
    [ testProperty "Massiv-HMatrix equivalence" matMassivHMatEquiv
    , testProperty "Massiv-HMatrix trip" matMassivHMatTrip
    ]

matMassivHMatEquiv :: Property
matMassivHMatEquiv = property $ do
  ml <- forAll genMatL
  let m = Massiv.fromLists' @S @Ix2 @Double Par ml
      hm = HMat.fromLists ml
  m === hm2m hm
  hm === m2hm m

matMassivHMatTrip :: Property
matMassivHMatTrip = property $ do
  ml <- forAll genMatL
  let m = Massiv.fromLists' @S @Ix2 @Double Par ml
  tripping m m2hm (pure @Maybe . hm2m)
