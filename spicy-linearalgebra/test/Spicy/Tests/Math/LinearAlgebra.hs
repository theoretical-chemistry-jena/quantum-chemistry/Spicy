module Spicy.Tests.Math.LinearAlgebra (tests) where

import Data.Complex (Complex)
import Data.Complex qualified as Cpmlx
import Data.Massiv.Array as Massiv
import Spicy.FiniteCompare
import Spicy.Math.LinearAlgebra
import Spicy.Prelude
import Spicy.Tests.Math.LinearAlgebra.R3 qualified
import Test.Tasty
import Test.Tasty.HUnit

tests :: TestTree
tests =
  testGroup
    "LinearAlgebra"
    [ test_euclideanDistance
    , test_angle
    , test_distMat
    , test_dotProduct
    , test_matrixVectorProduct
    , test_vectorMatrixProduct
    , test_matrixMatrixProduct
    , test_eig
    , test_eigSH
    , test_inv
    , test_pinv
    , Spicy.Tests.Math.LinearAlgebra.R3.tests
    ]

test_euclideanDistance :: TestTree
test_euclideanDistance =
  testGroup
    "euclideanDistance"
    [ testCase "d([0, 0, 0], [0, 0, 1])" $ do
        res <-
          euclideanDistance
            (Massiv.fromList @U @Double Seq [0, 0, 0])
            (Massiv.fromList Seq [0, 0, 1])
        let expc = 1
        RealExpFP @12 res @?= RealExpFP @12 expc
    , testCase "d([0, 0, 0], [0, 0, 0])" $ do
        res <-
          euclideanDistance
            (Massiv.fromList @U @Double Seq [0, 0, 0])
            (Massiv.fromList Seq [0, 0, 0])
        let expc = 0
        RealExpFP @12 res @?= RealExpFP @12 expc
    , testCase "d([0, 0, 0], [1, 0, 1])" $ do
        res <-
          euclideanDistance
            (Massiv.fromList @U @Double Seq [0, 0, 0])
            (Massiv.fromList Seq [1, 0, 1])
        let expc = sqrt 2
        RealExpFP @12 res @?= RealExpFP @12 expc
    , testCase "size mismatch" $ do
        let res =
              euclideanDistance
                (Massiv.fromList @U @Double Seq [0, 0, 0])
                (Massiv.fromList Seq [1, 0, 1, 1])
        res @?= Nothing
    ]

test_angle :: TestTree
test_angle =
  testGroup
    "angle"
    [ testCase "angle([1,0], [0,1])" $ do
        res <-
          angle
            (Massiv.fromList @U @Double Seq [1, 0])
            (Massiv.fromList @U @Double Seq [0, 1])
        let expc = pi / 2
        RealExpFP @12 res @?= RealExpFP @12 expc
    , testCase "angle([1,0], [1,1])" $ do
        res <-
          angle
            (Massiv.fromList @U @Double Seq [1, 0])
            (Massiv.fromList @U @Double Seq [1, 1])
        let expc = pi / 4
        RealExpFP @12 res @?= RealExpFP @12 expc
    , testCase "angle([1,0], [-1,0])" $ do
        res <-
          angle
            (Massiv.fromList @U @Double Seq [1, 0])
            (Massiv.fromList @U @Double Seq [-1, 0])
        let expc = pi
        RealExpFP @12 res @?= RealExpFP @12 expc
    ]

test_distMat :: TestTree
test_distMat =
  testGroup
    "distMat"
    [ testCase "2D" $ do
        let matA =
              Massiv.fromLists' @U @Ix2 @Double
                Seq
                [ [0, 0]
                , [1, 0]
                ]
            matB =
              Massiv.fromLists' @U @Ix2 @Double
                Seq
                [ [1, 1]
                , [2, 0]
                ]
            expc =
              Massiv.fromLists' @U @Ix2 @Double
                Seq
                [ [sqrt 2, 2]
                , [1, 1]
                ]
        res <- distMat euclideanDistance matA matB

        Massiv.map (RealExpFP @12) res @?= Massiv.map (RealExpFP @12) expc
    , testCase "size mismatch"
        $ let
            matA = Massiv.makeArrayLinear @U @Ix2 @Double Seq (Sz $ 3 :. 2) fromIntegral
            matB = Massiv.makeArrayLinear @U @Ix2 @Double Seq (Sz $ 2 :. 2) fromIntegral
            res = distMat euclideanDistance matA matB
           in
            res @?= Nothing
    ]

test_dotProduct :: TestTree
test_dotProduct =
  testGroup
    "<.>"
    [ testCase "[0,0,1] <.> [0,1,0]" $ do
        let vA = Massiv.fromList @U @Double Seq [0, 0, 1]
            vB = Massiv.fromList @U @Double Seq [0, 1, 0]
            expc = 0
        res <- vA <.> vB
        RealExpFP @12 res @?= RealExpFP @12 expc
    , testCase "[0,0,1] <.> [0,0,1]" $ do
        let vA = Massiv.fromList @U @Double Seq [0, 0, 1]
            vB = Massiv.fromList @U @Double Seq [0, 0, 1]
            expc = 1
        res <- vA <.> vB
        RealExpFP @12 res @?= RealExpFP @12 expc
    , testCase "size mismatch" $ do
        let vA = Massiv.fromList @U @Double Seq [0, 0, 1]
            vB = Massiv.fromList @U @Double Seq [0, 0, 0, 0]
            res = vA <.> vB
        res @?= Nothing
    ]

test_matrixVectorProduct :: TestTree
test_matrixVectorProduct =
  testGroup
    "#>"
    [ testCase "E #> [4,5]"
        $ let mat = Massiv.fromLists' @U @Ix2 @Double Seq [[1, 0], [0, 1]]
              vec = Massiv.fromList @U Seq [4, 5]
              res = mat #> vec :: Maybe (Vector U Double)
              expc = vec
           in (Massiv.map (RealExpFP @12) <$> res) @?= pure (Massiv.map (RealExpFP @12) expc)
    , testCase "size mismatch"
        $ let mat = Massiv.fromLists' @U @Ix2 @Double Seq [[1, 0], [0, 1]]
              vec = Massiv.fromList @U Seq [4, 5, 6]
              res = mat #> vec :: Maybe (Vector U Double)
           in res @?= Nothing
    ]

test_vectorMatrixProduct :: TestTree
test_vectorMatrixProduct =
  testGroup
    "<#"
    [ testCase "[4,5] <# E"
        $ let mat = Massiv.fromLists' @U @Ix2 @Double Seq [[1, 0], [0, 1]]
              vec = Massiv.fromList @U Seq [4, 5]
              res = vec <# mat :: Maybe (Vector U Double)
              expc = vec
           in (Massiv.map (RealExpFP @12) <$> res) @?= pure (Massiv.map (RealExpFP @12) expc)
    , testCase "size mismatch"
        $ let mat = Massiv.fromLists' @U @Ix2 @Double Seq [[1, 0], [0, 1]]
              vec = Massiv.fromList @U Seq [4, 5, 6]
              res = vec <# mat :: Maybe (Vector U Double)
           in res @?= Nothing
    ]

test_matrixMatrixProduct :: TestTree
test_matrixMatrixProduct =
  testGroup
    "##"
    [ testCase "simple"
        $ let matA = Massiv.makeArrayLinear @U @Ix2 @Double Seq (Sz $ 3 :. 5) (fromIntegral . (+ 1))
              matB = Massiv.resize' (Sz $ 5 :. 2) $ Massiv.fromList @U @Double Seq [1, 3, 0, 2, -1, 5, 7, 7, 6, 0]
              res = matA ## matB :: Maybe (Matrix U Double)
              expc = Massiv.resize' (Sz $ 3 :. 2) $ Massiv.fromList @U @Double Seq [56, 50, 121, 135, 186, 220]
           in Massiv.map (RealExpFP @12) <$> res @?= Just (Massiv.map (RealExpFP @12) expc)
    , testCase "size mismatch"
        $ let matA = Massiv.makeArrayLinear @U @Ix2 @Double Seq (Sz $ 3 :. 5) (fromIntegral . (+ 1))
              matB = Massiv.makeArrayLinear @U @Ix2 @Double Seq (Sz $ 4 :. 3) (fromIntegral . (+ 1))
              res = (matA ## matB :: Maybe (Matrix U Double))
           in res @?= Nothing
    ]

test_eig :: TestTree
test_eig =
  testGroup
    "eig"
    [ testCase "simple"
        $ let mat = Massiv.resize' (Sz $ 3 :. 3) $ Massiv.fromList @U @Double Seq [1, 2, 3, 2, 4, 5, 3, 5, 6]
              res =
                Massiv.map Cpmlx.realPart
                  . fst
                  <$> (eig mat :: Maybe (Vector U (Complex Double), Matrix U (Complex Double)))
              expc = Massiv.fromList @U Seq [11.344814282762075, -0.5157294715892575, 0.17091518882717918]
           in (Massiv.map (RealExpFP @12) <$> res) @?= Just (Massiv.map (RealExpFP @12) expc)
    ]

test_eigSH :: TestTree
test_eigSH =
  testGroup
    "eigSH"
    [ testCase "simple"
        $ let mat = Massiv.resize' (Sz $ 3 :. 3) $ Massiv.fromList @U @Double Seq [1, 2, 3, 2, 4, 5, 3, 5, 6]
              res =
                fst
                  <$> (eigSH mat :: Maybe (Vector U Double, Matrix U Double))
              expc = Massiv.fromList @U Seq [-0.5157294715892575, 0.17091518882717918, 11.344814282762075]
           in (Massiv.map (RealExpFP @12) <$> res) @?= Just (Massiv.map (RealExpFP @12) expc)
    ]

test_inv :: TestTree
test_inv =
  testGroup
    "inv"
    [ testCase "2x2" $ do
        let mat = Massiv.fromLists' @S @Ix2 @Double Par [[4, 7], [2, 6]]
            expc = Massiv.fromLists' @S Par [[0.6, -0.7], [-0.2, 0.4]]
        res :: Matrix S Double <- inv mat
        Massiv.map (RealFP @10) res @?= Massiv.map RealFP expc
    , testCase "reject non-square" $ do
        let mat = Massiv.fromLists' @S @Ix2 @Double Par [[4, 7, 1], [2, 6, 1]]
            res = inv mat :: Maybe (Matrix S Double)
        res @?= Nothing
    , testCase "reject singular" $ do
        let mat = Massiv.fromLists' @S @Ix2 @Double Par [[4, 7], [8, 14]]
            res = inv mat :: Maybe (Matrix S Double)
        res @?= Nothing
    ]

test_pinv :: TestTree
test_pinv =
  testGroup
    "pinv"
    [ testCase "2x2" $ do
        let mat = Massiv.fromLists' @S @Ix2 @Double Par [[4, 7], [2, 6]]
            expc = Massiv.fromLists' @S Par [[0.6, -0.7], [-0.2, 0.4]]
        res :: Matrix S Double <- pinv mat
        Massiv.map (RealFP @10) res @?= Massiv.map RealFP expc
    , testCase "reject non-square" $ do
        let mat = Massiv.fromLists' @S @Ix2 @Double Par [[4, 7, 1], [2, 6, 1]]
            res = pinv mat :: Maybe (Matrix S Double)
        res @?= Nothing
    ]
