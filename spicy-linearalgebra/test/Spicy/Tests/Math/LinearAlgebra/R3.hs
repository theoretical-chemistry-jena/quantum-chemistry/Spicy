module Spicy.Tests.Math.LinearAlgebra.R3 (tests) where

import Data.Massiv.Array as Massiv
import Spicy.Math.LinearAlgebra.R3 as R3
import Spicy.Prelude
import Spicy.Tests.Math.LinearAlgebra.R3.NeighbourList qualified
import Test.Tasty
import Test.Tasty.HUnit

stdR3 :: R3 S Double
stdR3 = mkR3T (1, 2, 3)

tests :: TestTree
tests =
  testGroup
    "R3"
    [ testCase "mkR3 valid" $ do
        res <- mkR3 . Massiv.fromList @S @Double Par $ [1, 2, 3]
        res @?= mkR3T (1, 2, 3)
    , testCase "mkR3 invalid 2" $ do
        let res = mkR3 . Massiv.fromList @S @Double Par $ [1, 2]
        res @?= Nothing
    , testCase "mkR3 invalid 4" $ do
        let res = mkR3 . Massiv.fromList @S @Double Par $ [1, 2, 3, 4]
        res @?= Nothing
    , testCase "xR3" $ do
        let res = xR3 stdR3
        res @?= 1
    , testCase "yR3" $ do
        let res = yR3 stdR3
        res @?= 2
    , testCase "zR3" $ do
        let res = zR3 stdR3
        res @?= 3
    , testCase "Euclidean Distance" $ do
        let res1 = euclideanDistance @S @Double (mkR3T (0, 0, 0)) (mkR3T (0, 0, 1))
            res2 = euclideanDistance @S @Double (mkR3T (0, 0, 0)) (mkR3T (0, -1, 1))
        res1 @?= 1
        res2 @?= sqrt 2
    , testCase "Manhattan Distance" $ do
        let res1 = manhattanDistance @S @Double (mkR3T (0, 0, 0)) (mkR3T (0, 0, 1))
            res2 = manhattanDistance @S @Double (mkR3T (0, 0, 0)) (mkR3T (0, -1, 1))
        res1 @?= 1
        res2 @?= 2
    , testCase "Geometric Centre" $ do
        let points =
              fmap
                (mkR3T @S @Double)
                [ (0, 0, 0)
                , (1, 0, 0)
                , (0, 1, 0)
                , (0, 0, 1)
                ]
        res <- geometricCentre points
        res @?= mkR3T (1 / 4, 1 / 4, 1 / 4)
    , testCase "Centre of Mass" $ do
        let points =
              fmap
                (\(p, w) -> (mkR3T @S @Double p, w))
                [ ((0, 0, 0), 0)
                , ((1, 0, 0), 1)
                , ((0, 1, 0), 1)
                , ((0, 0, 1), 1)
                ]
        res <- centreOfMass points
        res @?= mkR3T (1 / 3, 1 / 3, 1 / 3)
    , Spicy.Tests.Math.LinearAlgebra.R3.NeighbourList.tests
    ]
