module Spicy.Tests.Math.LinearAlgebra.R3.NeighbourList (tests) where

import Data.KdMap.Static qualified as KdMap
import Data.Massiv.Array as Massiv
import RIO.HashSet qualified as HashSet
import RIO.List.Partial ((!!))
import Spicy.FiniteCompare (RealFP (RealFP))
import Spicy.HashMap qualified as HashMap
import Spicy.Math.LinearAlgebra.R3 as R3
import Spicy.Math.LinearAlgebra.R3.NeighbourList
import Spicy.Prelude
import Test.Tasty
import Test.Tasty.HUnit

tests :: TestTree
tests =
  testGroup
    "NeighbourList"
    [ test_DistanceMatrix
    , test_CellList
    , test_KdTree
    , test_GroupNeighbours
    ]

test_DistanceMatrix :: TestTree
test_DistanceMatrix =
  testGroup
    "DistMatNeighbours"
    [ testCase "Empty" $ do
        let nl = mkNeighbourList @DistMatNeighbours euclideanDistance ([] :: [(Int, R3 S Double)]) undefined
        getDistMat nl @?= Massiv.empty
    , testCase "Euclidean Simple" $ do
        let nl = mkNeighbourList @DistMatNeighbours euclideanDistance simplePoints undefined
        testSimpleEuclidean nl

        -- Maximum distance is infinite
        maxDist nl @?= 1 / 0

        -- Test entire slice
        slc <- getAll nl 1
        slc @?= [(2, 1), (4, sqrt 2), (3, 1.1)]
    , testCase "Manhattan Simple" $ do
        let nl = mkNeighbourList @DistMatNeighbours manhattanDistance simplePoints undefined
        testSimpleManhattan nl

        -- Maximum distance is infinite
        maxDist nl @?= 1 / 0

        -- Test entire slice
        slc <- getAll nl 1
        slc @?= [(2, 1), (4, 2), (3, 1.1)]
    , testCase "Euclidean Molecular Clusters" $ do
        let nl =
              mkNeighbourList @DistMatNeighbours
                euclideanDistance
                (fmap (\(i, p, _w) -> (i, p)) molPoints)
                undefined
        testMolCluster nl
    ]

test_CellList :: TestTree
test_CellList =
  testGroup
    "CellList"
    [ testCase "Empty" $ do
        let nl = mkNeighbourList @CellList euclideanDistance ([] :: [(Int, R3 S Double)]) 1.0
        getCellList nl @?= mempty
    , testCase "Euclidean Simple" $ do
        let nl = mkNeighbourList @CellList euclideanDistance simplePoints 1.2
        testSimpleEuclidean nl

        -- Maximum distance is input distance
        maxDist nl @?= 1.2

        -- Test entire slice. One value is outside the valid distance
        slc <- getAll nl 1
        slc @?= [(2, 1), (3, 1.1)]
    , testCase "Manhattan Simple" $ do
        let nl = mkNeighbourList @CellList manhattanDistance simplePoints 2.01
        testSimpleManhattan nl

        -- Test entire slice.
        slc <- getAll nl 1
        slc @?= [(2, 1), (3, 1.1), (4, 2)]

        -- Test within
        within <- getWithin nl 1.2 1
        within @?= [(2, 1), (3, 1.1)]
    , testCase "Euclidean Molecular Clusters" $ do
        let nl =
              mkNeighbourList @CellList
                euclideanDistance
                (fmap (\(i, p, _w) -> (i, p)) molPoints)
                6
        testMolCluster nl
    ]

test_KdTree :: TestTree
test_KdTree =
  testGroup
    "KdNeighbours"
    [ testCase "Empty" $ do
        let nl = mkNeighbourList @KdNeighbours euclideanDistance ([] :: [(Int, R3 S Double)]) undefined
        assertBool "KDMap is not empty" . KdMap.null . getKDT $ nl
    , testCase "Euclidean Simple" $ do
        let nl = mkNeighbourList @KdNeighbours euclideanDistance simplePoints undefined
        -- Test nearest neighbour
        nearest <- getNearest nl 1
        nearest @?= Just (2, 1)

        -- Test within
        within <- getWithin nl 1.2 1
        within @?= [(3, 1.1), (2, 1)]

        -- Maximum distance is infinite
        maxDist nl @?= 1 / 0

        -- Test entire slice
        slc <- getAll nl 1
        slc @?= [(2, 1), (3, 1.1), (4, sqrt 2)]
    , testCase "Manhattan Simple" $ do
        let nl = mkNeighbourList @KdNeighbours manhattanDistance simplePoints undefined
        -- Test nearest neighbour
        nearest <- getNearest nl 1
        nearest @?= Just (2, 1)

        -- Test within
        within1 <- getWithin nl 1.2 1
        within1 @?= [(3, 1.1), (2, 1)]

        within3 <- getWithin nl 1.2 3
        within3 @?= [(1, 1.1)]

        -- Maximum distance is infinite
        maxDist nl @?= 1 / 0

        -- Test entire slice
        slc <- getAll nl 1
        slc @?= [(2, 1), (3, 1.1), (4, 2)]
    , testCase "Euclidean Molecular Clusters" $ do
        let nl =
              mkNeighbourList @KdNeighbours
                euclideanDistance
                (fmap (\(i, p, _w) -> (i, p)) molPoints)
                undefined
        testMolCluster nl
    ]

test_GroupNeighbours :: TestTree
test_GroupNeighbours =
  testGroup
    "GroupNeighbours"
    [ testCase "Euclidean Simple" $ do
        let nl =
              mkNeighbourList @KdNeighbours
                euclideanDistance
                (fmap (\(i, p, _w) -> (i, p)) molPoints)
                undefined
            wghtGroups =
              [ (1, fmap (\i -> (i, molPoints !! (i - 1) ^. _3)) [1 .. 11])
              , (2, fmap (\i -> (i, molPoints !! (i - 1) ^. _3)) [15 .. 17])
              , (3, fmap (\i -> (i, molPoints !! (i - 1) ^. _3)) [12 .. 14])
              , (4, fmap (\i -> (i, molPoints !! (i - 1) ^. _3)) [18 .. 20])
              ] ::
                [(Natural, [(Int, Double)])]

        -- Group-Group distance neigbourlist with different metrics
        grpNlMin <- mkSetNeighbourList @SetNeighbours (setDistMin euclideanDistance) nl 6 wghtGroups
        grpNlMax <- mkSetNeighbourList @SetNeighbours (setDistMax euclideanDistance) nl 6 wghtGroups
        grpNlAvg <- mkSetNeighbourList @SetNeighbours (setDistGeometricCentre euclideanDistance) nl 6 wghtGroups
        grpNlCom <- mkSetNeighbourList @SetNeighbours (setDistCentreOfMass euclideanDistance) nl 6 wghtGroups

        -- Values below verified with Avogadro2

        -- Minimal distance
        benzenNghbrsMin <- fmap (RealFP @3) <$> getGroupNeighbours grpNlMin 1
        benzenNghbrsMin @?= fmap RealFP (HashMap.fromList [(2, 2.8587), (3, 3.5302)])

        -- Maximum distance
        benzenNghbrsMax <- fmap (RealFP @3) <$> getGroupNeighbours grpNlMax 1
        benzenNghbrsMax @?= fmap RealFP (HashMap.fromList [(2, 5.5104)])

        -- Geometric centre distance
        benzenNghbrsAvg <- fmap (RealFP @3) <$> getGroupNeighbours grpNlAvg 1
        benzenNghbrsAvg @?= fmap RealFP (HashMap.fromList [(2, 3.2935), (3, 4.1559)])

        -- Centre of mass distance
        benzenNghbrsCom <- fmap (RealFP @3) <$> getGroupNeighbours grpNlCom 1
        benzenNghbrsCom @?= fmap RealFP (HashMap.fromList [(2, 3.0520), (3, 3.9425)])
    ]

simplePoints :: [(Int, R3 S Double)]
simplePoints =
  [ (1, mkR3T (0, 0, 0))
  , (2, mkR3T (0, 1, 0))
  , (4, mkR3T (1, 1, 0))
  , (3, mkR3T (0, 0, 1.1))
  ]

testSimpleEuclidean :: (Floating a, Eq a, Show a, Num i) => (NeighbourList l i r a) => l i r a -> Assertion
testSimpleEuclidean nl = do
  -- Test nearest neighbour
  nearest <- getNearest nl 1
  nearest @?= Just (2, 1)

  -- Test within
  within <- getWithin nl 1.2 1
  within @?= [(2, 1), (3, 1.1)]

testSimpleManhattan :: (Floating a, Eq a, Show a, Num i) => (NeighbourList l i r a) => l i r a -> Assertion
testSimpleManhattan nl = do
  -- Test nearest neighbour
  nearest <- getNearest nl 1
  nearest @?= Just (2, 1)

  -- Test within
  within <- getWithin nl 1.2 1
  within @?= [(2, 1), (3, 1.1)]

testMolCluster :: (RealFloat a, Show a, Num i) => (NeighbourList l i r a) => l i r a -> Assertion
testMolCluster nl = do
  -- Get the nearest neighbour of C1, which should be H11
  nearest <- getNearest nl 1
  fmap (second (RealFP @3)) nearest @?= Just (11, RealFP 1.087)

  -- Check that two of three water molecules are within a given distance
  -- and that the third one is not
  within <- HashSet.fromList . fmap fst <$> getWithin nl 6 1
  HashSet.fromList [15, 16, 17, 12, 13, 14] `HashSet.difference` within @?= HashSet.empty
  within `HashSet.intersection` HashSet.fromList [18, 19, 20] @?= HashSet.empty

{- | Coordinates of a simple molecular cluster:

  * Benzen in the centre
  * Each one water above and below the ring plane
  * One distant water
-}
molPoints :: [(Int, R3 S Double, Double)]
molPoints =
  [ (1, mkR3T (-4.84670, 4.32320, -0.00140), 12)
  , (2, mkR3T (-4.90030, 2.93055, -0.00170), 12)
  , (3, mkR3T (-2.48685, 2.83820, -0.00083), 12)
  , (4, mkR3T (-1.47098, 4.73673, 0.00350), 1)
  , (5, mkR3T (-3.61269, 4.97285, -0.00117), 12)
  , (6, mkR3T (-5.86273, 2.42437, -0.00104), 1)
  , (7, mkR3T (-3.71906, 2.18789, -0.00229), 12)
  , (8, mkR3T (-3.75681, 1.10133, -0.00056), 1)
  , (9, mkR3T (-2.43291, 4.23033, -0.00015), 12)
  , (10, mkR3T (-3.57237, 6.05862, 0.00222), 1)
  , (11, mkR3T (-5.76476, 4.90581, -0.00062), 1)
  , (12, mkR3T (-3.59939, 4.54678, 3.77848), 16)
  , (13, mkR3T (-4.13501, 5.45206, 3.45710), 1)
  , (14, mkR3T (-3.06377, 5.13913, 4.53468), 1)
  , (15, mkR3T (-2.84410, 4.02747, -2.85940), 16)
  , (16, mkR3T (-3.65651, 4.76869, -2.85224), 1)
  , (17, mkR3T (-2.21949, 4.72601, -3.43518), 1)
  , (18, mkR3T (-2.45044, -4.44717, -17.62348), 16)
  , (19, mkR3T (-1.77854, -4.38095, -16.95516), 1)
  , (20, mkR3T (-2.59091, -3.59396, -18.01693), 1)
  ]
