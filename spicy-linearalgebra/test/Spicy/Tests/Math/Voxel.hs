module Spicy.Tests.Math.Voxel (tests) where

import Data.Massiv.Array as Massiv
import Spicy.FiniteCompare (RealFP (RealFP))
import Spicy.Math.LinearAlgebra.R3 as R3
import Spicy.Math.Voxel
import Spicy.Prelude hiding ((:>))
import Test.Tasty
import Test.Tasty.HUnit

tests :: TestTree
tests =
  testGroup
    "Voxel"
    [ test_voxelToCoord
    , test_coordToVoxelInterp
    ]

stdCell :: Cell
stdCell = Cell (mkR3T (0, 0, 0)) (mkR3T (10, 0, 0)) (mkR3T (0, 10, 0)) (mkR3T (0, 0, 5))

stdSz :: Sz3
stdSz = Sz $ 101 :> 101 :. 51

test_voxelToCoord :: TestTree
test_voxelToCoord =
  testGroup
    "voxelToCoord"
    [ testCase "(0 :> 0 :. 0) no shift" $ do
        coord :: R3 S Double <- voxelToCoord stdCell stdSz (0 :> 0 :. 0)
        coord @?= mkR3T (0, 0, 0)
    , testCase "(0 :> 0 :. 0) shift" $ do
        let cell = stdCell{origin = mkR3T (0, 1, 0)}
        coord :: R3 S Double <- voxelToCoord cell stdSz (0 :> 0 :. 0)
        coord @?= mkR3T (0, 1, 0)
    , testCase "(n :> n :. n)" $ do
        coord :: R3 S Double <- voxelToCoord stdCell stdSz (100 :> 100 :. 50)
        coord @?= mkR3T (10, 10, 5)
    , testCase "(n :> 0.5 n :. n)" $ do
        coord :: R3 S Double <- voxelToCoord stdCell stdSz (100 :> 50 :. 50)
        coord @?= mkR3T (10, 5, 5)
    , testCase "non-rectangular" $ do
        let cell = stdCell{cellVecB = mkR3T (5, 10, 0)}
        coord :: R3 S Double <- voxelToCoord cell stdSz (100 :> 100 :. 50)
        coord @?= mkR3T (15, 10, 5)
    , testCase "outside box" $ do
        let coord :: Maybe (R3 S Double) = voxelToCoord stdCell stdSz (100 :> 100 :. 51)
        coord @?= Nothing
    ]

test_coordToVoxelInterp :: TestTree
test_coordToVoxelInterp =
  testGroup
    "coordToVoxelInterp"
    [ testCase "(0, 0, 0)" $ do
        let coord = mkR3T (0, 0, 0)
        res <- coordToVoxelInterp stdCell stdSz coord
        res @?= [(0 :> 0 :. 0, 1)]
    , testCase "(0, 0.5 s, 0)" $ do
        let coord = mkR3T (0, 0.05, 0)
            tF = fmap (\(i, c) -> (i, RealFP @8 c))
        res <- coordToVoxelInterp stdCell stdSz coord
        tF res @?= tF [(0 :> 0 :. 0, 0.5), (0 :> 1 :. 0, 0.5)]
    , testCase "(0.5 s, 0.5 s, 0.5 s)" $ do
        let coord = mkR3T (0.05, 0.05, 0.05)
            tF = fmap (\(i, c) -> (i, RealFP @8 c))
        res <- coordToVoxelInterp stdCell stdSz coord
        tF res
          @?= tF
            [ (0 :> 0 :. 0, 0.125)
            , (0 :> 0 :. 1, 0.125)
            , (0 :> 1 :. 0, 0.125)
            , (0 :> 1 :. 1, 0.125)
            , (1 :> 0 :. 0, 0.125)
            , (1 :> 0 :. 1, 0.125)
            , (1 :> 1 :. 0, 0.125)
            , (1 :> 1 :. 1, 0.125)
            ]
    , testCase "Outermost corner" $ do
        let coord = mkR3T (10, 10, 5)
        res <- coordToVoxelInterp stdCell stdSz coord
        res @?= [(100 :> 100 :. 50, 1)]
    , testCase "Edge" $ do
        let coord = mkR3T (10, 5, 5)
            tF = fmap (\(i, c) -> (i, RealFP @8 c))
        res <- coordToVoxelInterp stdCell stdSz coord
        tF res @?= tF [(100 :> 50 :. 50, 1)]
    , testCase "non-rectangular" $ do
        let cell = stdCell{cellVecB = mkR3T (5, 10, 0)}
            coord = mkR3T (15, 10, 5)
        res <- coordToVoxelInterp cell stdSz coord
        res @?= [(100 :> 100 :. 50, 1)]
    ]
