module Main where

import Spicy.Prelude
import Spicy.Tests.Math.Internal qualified
import Spicy.Tests.Math.LinearAlgebra qualified
import Spicy.Tests.Math.Voxel qualified
import Test.Tasty

main :: IO ()
main = defaultMain tests

tests :: TestTree
tests =
  testGroup
    "Spicy.Math"
    [ Spicy.Tests.Math.LinearAlgebra.tests
    , Spicy.Tests.Math.Internal.tests
    , Spicy.Tests.Math.Voxel.tests
    ]
