name:                spicy-prelude
version:             0.1.0
git:                 https://gitlab.com/theoretical-chemistry-jena/quantum-chemistry/Spicy
license:             AGPL-3.0-only
author:              "Phillip Seeber, Sebastian Seidenath"
maintainer:          "phillip.seeber@uni-jena.de, sebastian.seidenath@uni-jena.de"
copyright:           "2024 Phillip Seeber, Sebastian Seidenath"

build-type: Simple

extra-source-files:
  - LICENSE

description: A custom prelude for Spicy based on RIO + some other goodies

language: GHC2021

default-extensions:
  - NoImplicitPrelude
  - StrictData
  - OverloadedStrings
  - OverloadedLabels
  - OverloadedRecordDot
  - RecordWildCards
  - CApiFFI
  - DeriveGeneric
  - DerivingVia
  - DerivingStrategies
  - DuplicateRecordFields
  - GADTs
  - LambdaCase
  - QuantifiedConstraints
  - TypeFamilies
  - ViewPatterns
  - DataKinds

ghc-options:
  - -Wall
  - -Wcompat
  - -Widentities
  - -Wincomplete-record-updates
  - -Wincomplete-uni-patterns
  - -Wpartial-fields
  - -Wredundant-constraints

dependencies:
  - base >= 4.16 && < 4.21
  - rio >= 0.1.13.0 && < 0.2
  - text >= 1.2.3.1 && < 2.2
  - pathtype >= 0.8.1 && < 0.9
  - binary >= 0.8.8.0 && < 0.11
  - optics >= 0.4 && < 0.5
  - aeson >= 1.4.4.0 && < 2.3
  - massiv >= 1.0.1.0 && < 1.1
  - yaml >= 0.11.2.0 && < 0.12
  - split >= 0.2.3.3 && < 0.3
  - data-default >= 0.7.1.1 && < 0.8
  - tasty >= 1.2.3 && < 1.6
  - tasty-hunit >= 0.10.0.2 && < 0.11
  - tasty-hedgehog >= 1.3.1.0 && < 1.5
  - hedgehog >= 1.0.5 && < 1.6

library:
  source-dirs: src

tests:
  spicy-prelude-test:
    main: test.hs
    source-dirs:
      - test
    dependencies:
      - spicy-prelude
    ghc-options:
      - -threaded
      - -rtsopts
      - -with-rtsopts=-N
