{- |
Module      : Spicy.Metrology
Description : Physical units and their conversion
Copyright   : Phillip Seeber, Sebastian Seidenath 2024
License     : AGPL-3
Maintainer  : phillip.seeber@uni-jena.de
Stability   : experimental
Portability : POSIX, Windows

Numbers in Spicy are untyped with respect to their unit but must always be represented in terms of the default unit specified in this module.
A class 'ConvertUnit' to convert the simple base units is provided.
-}
module Spicy.Metrology (
  ConvertUnit (..),
  Lengths (..),
  Energies (..),
  Charges (..),
  Angle (..),
) where

import Data.Default
import Spicy.Prelude

class (Default dim, Fractional val) => ConvertUnit dim val where
  -- | The conversion factor to the default unit
  fact :: dim -> val

  -- | Convert from one to another unit
  convert ::
    -- | From unit
    dim ->
    -- | To unit
    dim ->
    -- | Value to convert
    val ->
    val
  convert orgn trgt val = (fact trgt / fact orgn) * val

-- | Units of lengths
data Lengths
  = -- | Bohr \(a_0\), the default unit in Spicy for lengths
    Bohr
  | -- | Ångstrom Å
    Angstrom
  | -- | Pico meter pm
    PicoMeter
  | -- | Nano meter nm
    NanoMeter
  deriving (Eq, Show)

instance Default Lengths where
  def = Bohr

instance (Fractional val) => ConvertUnit Lengths val where
  fact Bohr = 1
  fact Angstrom = 0.529177249
  fact PicoMeter = fact Angstrom * 100
  fact NanoMeter = fact Angstrom * 0.1

-- | Units of energy
data Energies
  = -- | Atomic unit of energy, the Hartree \(E_\mathrm{H}\)
    Hartree
  | -- | SI unit of energy \(\mathrm{k \, J \, mol^{\-1}}\)
    KiloJoulePerMole
  | -- | \( \mathrm{k cal \, mol^{\-1}} \)
    KiloCaloriePerMole
  | -- | Electronvolt \(\mathrm{eV}\)
    ElectronVolt

instance Default Energies where
  def = Hartree

instance (Fractional val) => ConvertUnit Energies val where
  fact Hartree = 1
  fact KiloJoulePerMole = 2625.5002
  fact KiloCaloriePerMole = 627.5096080305927
  fact ElectronVolt = 27.211399

data Charges
  = -- | The elementary charge \(\mathrm{e}\)
    ElementaryCharge

instance Default Charges where
  def = ElementaryCharge

instance (Fractional val) => ConvertUnit Charges val where
  fact ElementaryCharge = 1

-- | Units of masses
data Masses
  = -- | The atomic mass unit \(\mathrm{u}\)/Dalton \(\mathrm{Da}\)
    AtomicMassUnit
  | -- | SI Kilogram \(\mathrm{kg}\)
    KiloGram

instance Default Masses where
  def = AtomicMassUnit

instance (Fractional val) => ConvertUnit Masses val where
  fact AtomicMassUnit = 1
  fact KiloGram = 1.6605402e-27

-- | Angle units
data Angle
  = -- | Radian \(\mathrm{rad}\)
    Radian
  | -- | Degree \(\mathrm{deg}\)
    Degree

instance Default Angle where
  def = Radian

instance (Floating val) => ConvertUnit Angle val where
  fact Radian = 1
  fact Degree = pi / 180
