{-# LANGUAGE AllowAmbiguousTypes #-}
{-# LANGUAGE UndecidableInstances #-}
{-# OPTIONS_GHC -Wno-orphans #-}

{- |
Module      : Spicy.Prelude
Description : A Prelude for Spicy.
Copyright   : Phillip Seeber, Sebastian Seidenath, 2024
License     : AGPL-3
Maintainer  : sebastian.seidenath@uni-jena.de
Stability   : experimental
Portability : POSIX, Windows

Custom prelude for Spicy, shortening import lists and eliminating common import conflicts.
This should always be used instead if 'RIO'.
-}
module Spicy.Prelude (
  -- * Reexports
  module Optics,
  module Data.Binary,
  module Data.Binary.Get,
  module Data.Binary.Put,
  module Data.Kind,
  module System.Path,

  -- * Redefinitions
  -- $redefinitions
  (^),

  -- * RIO And Error Handling
  -- $rioAndErrors
  module RIO,
  envView,
  maybe2MThrow,

  -- * Utilities
  (?->),

  -- * MPI Task Control
  WorkerTask (..),

  -- * Text
  -- $textOperations
  readFileUtf8,
  writeFileUtf8,
  appendFileUtf8,
  tellN,

  -- * JSON
  spicyToEncoding,
  spicyToJSON,
  spicyParseJSON,
  InvalidField,
  IsValidField,

  -- * Demotion of Type level to Value level
  TypeOf (..),

  -- * Ternary Logic
  Ternary (..),

  -- * Type level utilities
  -- $typeLevelLFunctions
  If,
  And,
  Or,
  Any,
  All,

  -- * Orphan Instances

  -- ** JSON
  -- $jsonOrphans

  -- ** Binary
  BinaryLE (..),
)
where

import Data.Aeson
import Data.Aeson.Types (Parser)
import Data.Binary hiding (decode, encode)
import Data.Binary.Get hiding (isEmpty)
import Data.Binary.Put
import Data.Kind (Type)
import Data.Massiv.Array as Massiv hiding (isEmpty)
import Data.Text.IO as T
import Data.Yaml ()
import GHC.Generics
import Optics hiding (
  Empty,
  element,
  elements,
  view,
 )
import RIO hiding (
  FilePath,
  Lens,
  Lens',
  Vector,
  lens,
  over,
  preview,
  readFileUtf8,
  set,
  sets,
  takeWhile,
  to,
  view,
  withFile,
  writeFileUtf8,
  (%~),
  (.~),
  (^),
  (^.),
  (^..),
  (^?),
 )
import RIO qualified
import RIO.Char qualified as Char
import RIO.Text qualified as Text
import RIO.Writer
import System.IO (hSetEncoding, utf8)
import System.Path hiding (abs, maybe, (<.>), (</>))
import System.Path.IO

{- $redefinitions
Some redefinitions of RIO Functions for convenience.
-}

(^) :: (Num a) => a -> Int -> a
(^) = (RIO.^)

--------------------------------------------------------------------------------

{- $rioAndErrors
These are some simple helper functions for RIO and error handling patterns,
commonly used in Spicy.
-}

{- | A function as @view@ in RIO. It fetches the 'MonadReader' environment with
an Optics' Lens.
-}
envView :: (Is k A_Getter, MonadReader s f) => Optic' k is s b -> f b
envView lns = (^. lns) <$> ask

{- | Generalisation of a 'Maybe' value to an arbitrary monad, that is an
instance of 'MonadThrow'. An exception to throw must be provided, in case a
'Nothing' was given.
-}
maybe2MThrow :: (MonadThrow m, Exception e) => e -> Maybe a -> m a
maybe2MThrow exc Nothing = throwM exc
maybe2MThrow _ (Just a) = return a

--------------------------------------------------------------------------------

{- | An optional string for error messages and similar. If the condition is
 satisfied the string will be printed with some indentation and a line break.
 Otherwise `mempty` is obtained.
-}
(?->) :: (Monoid s, IsString s) => Bool -> s -> s
c ?-> s = if c then "  " <> s <> "\n" else mempty

--------------------------------------------------------------------------------

-- | Types encoding different worker tasks to be communicated via MPI.
data WorkerTask
  = -- | Run an MPI client for distributed block-sparse matrix multiplication
    -- via DBCSR backend.
    DbcsrClient
  | -- | Terminate the workers and prepare for shutdown
    Terminate
  deriving (Generic, Show, Eq)

instance Binary WorkerTask

--------------------------------------------------------------------------------

{- $textOperations
Operations on strict 'Text'.
-}

{- | Wrapper around RIO's writing of unicode formatted text to a file
('writeFileUtf8'), compatible with typed paths.
-}
writeFileUtf8 :: (MonadIO m) => AbsRelFile -> Text -> m ()
writeFileUtf8 path' text' = RIO.writeFileUtf8 (toString path') text'

{- | Wrapper around RIO's reading of unicode formatted text to a file
('readFileUtf8'), compatible with typed paths.
-}
readFileUtf8 :: (MonadIO m) => AbsRelFile -> m Text
readFileUtf8 path' = RIO.readFileUtf8 (toString path')

{- | Appending for UTF-8 encoded files in RIO's style of writing formatted text
to a file, compatible with typed paths.
-}
appendFileUtf8 :: (MonadIO m) => AbsRelFile -> Text -> m ()
appendFileUtf8 path' text' = liftIO . withFile path' AppendMode $ \h -> do
  hSetEncoding h utf8
  T.hPutStr h text'

-- | 'tell' with an appended line break.
tellN :: (IsString w, MonadWriter w m) => w -> m ()
tellN c = tell $ c <> "\n"

--------------------------------------------------------------------------------

-- | JSON serialisation options for pysisyphus.
spicyOptions :: Options
spicyOptions =
  defaultOptions
    { fieldLabelModifier = fmt
    , constructorTagModifier = fmt
    , allNullaryToStringTag = True
    , omitNothingFields = True
    , sumEncoding = ObjectWithSingleField
    , unwrapUnaryRecords = False
    , tagSingleConstructors = False
    , rejectUnknownFields = True
    }
 where
  fmt = fmap Char.toLower . camelTo2 '_' . filter (/= '\'')

-- | JSON encoding via generics with Spicy's serialisation options.
spicyToEncoding :: (Generic a, GToJSON' Encoding Zero (Rep a)) => a -> Encoding
spicyToEncoding = genericToEncoding spicyOptions

-- | JSON conversion via generics with Spicy's serialisation options.
spicyToJSON :: (Generic a, GToJSON' Value Zero (Rep a)) => a -> Value
spicyToJSON = genericToJSON spicyOptions

-- | JSON parsing via generics with Spicy's serialisation options.
spicyParseJSON :: (Generic a, GFromJSON Zero (Rep a)) => Value -> Parser a
spicyParseJSON = genericParseJSON spicyOptions

{- | The point of this type level stunt over just using a 'Maybe' is that we are able
to have a single super-type for something (say, Pisysiphus optimisers) but can
still selectively allow/reject specific fields dependingon type arguments. Since
'Maybe' is hardcoded into Aeson, this is the only option we could find that does
not involve massive amounts of code.
-}
type InvalidField = Maybe Void

type IsValidField :: Bool -> Type -> Type
type family IsValidField b t where
  IsValidField 'True t = t
  IsValidField 'False _ = InvalidField

--------------------------------------------------------------------------------

-- \$typeLevelFunctions
type If :: Bool -> Type -> Type -> Type
type family If c t e where
  If 'True t e = t
  If 'False t e = e

type Or :: Bool -> Bool -> Bool
type family Or a b where
  Or 'True _ = 'True
  Or _ 'True = 'True
  Or _ _ = 'False

type Any :: [Bool] -> Bool
type family Any l where
  Any '[] = 'False
  Any (b ': bs) = Or b (Any bs)

type And :: Bool -> Bool -> Bool
type family And a b where
  And 'False _ = 'False
  And _ 'False = 'False
  And _ _ = 'True

type All :: [Bool] -> Bool
type family All l where
  All '[] = 'True
  All (b ': bs) = And b (All bs)

class TypeOf k t where
  toValue :: t

instance (TypeOf a b, TypeOf as [b]) => TypeOf (a ': as) [b] where
  toValue = toValue @a : toValue @as
instance TypeOf '[] [x] where
  toValue = []

--------------------------------------------------------------------------------

{- | Basically ternary Booleans with a third option for possibly 'False' or
possibly 'True'.
-}
data Ternary
  = Yes
  | Possible
  | No
  deriving (Eq, Ord, Show, Generic)

--------------------------------------------------------------------------------

{- $jsonOrphans
These orphan instances provide JSON serialisation for common values
-}

instance ToJSON AbsRelFile where
  toJSON p = toJSON . toString . normalise $ p

instance FromJSON AbsRelFile where
  parseJSON (String a) = case parse (Text.unpack a) of
    Left e -> fail e
    Right p -> return . normalise $ p
  parseJSON o = fail $ "Can not parse " <> show o <> "as path."

instance ToJSON AbsFile where
  toJSON p = toJSON . toString . normalise $ p

instance FromJSON AbsFile where
  parseJSON (String a) = case parse (Text.unpack a) of
    Left e -> fail e
    Right p -> return . normalise $ p
  parseJSON o = fail $ "Can not parse " <> show o <> "as path."

instance ToJSON RelFile where
  toJSON p = toJSON . toString . normalise $ p

instance FromJSON RelFile where
  parseJSON (String a) = case parse (Text.unpack a) of
    Left e -> fail e
    Right p -> return . normalise $ p
  parseJSON o = fail $ "Can not parse " <> show o <> "as path."

instance ToJSON AbsRelDir where
  toJSON p = toJSON . toString . normalise $ p

instance FromJSON AbsRelDir where
  parseJSON (String a) = case parse (Text.unpack a) of
    Left e -> fail e
    Right p -> return . normalise $ p
  parseJSON o = fail $ "Can not parse " <> show o <> "as path."

instance ToJSON AbsDir where
  toJSON p = toJSON . toString . normalise $ p

instance FromJSON AbsDir where
  parseJSON (String a) = case parse (Text.unpack a) of
    Left e -> fail e
    Right p -> return . normalise $ p
  parseJSON o = fail $ "Can not parse " <> show o <> "as path."

instance ToJSON RelDir where
  toJSON p = toJSON . toString . normalise $ p

instance FromJSON RelDir where
  parseJSON (String a) = case parse (Text.unpack a) of
    Left e -> fail e
    Right p -> return . normalise $ p
  parseJSON o = fail $ "Can not parse " <> show o <> "as path."

--------------------------------------------------------------------------------

{- $binaryOrphans
These orphan instances provide 'Binary' support for some common data types.
-}

-- | Numerical data types that must be encoded in little endian.
class BinaryLE a where
  putLE :: a -> Put
  getLE :: Get a

instance BinaryLE Float where
  getLE = getFloatle
  putLE = putFloatle

instance BinaryLE Double where
  getLE = getDoublele
  putLE = putDoublele

instance BinaryLE Int where
  getLE = fromIntegral <$> getInt64le
  putLE = putInt64le . fromIntegral

instance {-# OVERLAPPABLE #-} (Binary a) => BinaryLE a where
  getLE = get
  putLE = put
