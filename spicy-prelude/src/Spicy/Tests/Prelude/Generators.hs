module Spicy.Tests.Prelude.Generators (
  genVecL,
  genVecLint,
  genMatL,
  genMatLint,
  genRelDir,
  genAbsDir,
  genRelFile,
  genAbsFile,
) where

import Data.List.Split (chunksOf)
import Hedgehog
import Hedgehog.Gen qualified as Gen
import Hedgehog.Range qualified as Range
import Spicy.Prelude
import System.Path ((</>))
import System.Path qualified as Path

-- | Generator of lists representing vectors
genVecL :: Gen [Double]
genVecL =
  Gen.list
    (Range.linear 0 2000)
    (Gen.double $ Range.exponentialFloat 1e-200 1e200)

-- | Generator of lists representing vectors
genVecLint :: Gen [Int]
genVecLint =
  Gen.list
    (Range.linear 0 2000)
    (Gen.int $ Range.linear (-1000000000) 1000000000)

-- | Generator of lists representing matrices
genMatL :: Gen [[Double]]
genMatL = do
  r <- Gen.int $ Range.linear 0 100
  c <- Gen.int $ Range.linear 0 100
  l <-
    Gen.list
      (Range.singleton $ r * c)
      (Gen.double $ Range.exponentialFloat 1e-200 1e200)
  return . chunksOf c $ l

-- | Generator of lists representing matrices
genMatLint :: Gen [[Int]]
genMatLint = do
  r <- Gen.int $ Range.linear 0 100
  c <- Gen.int $ Range.linear 0 100
  l <-
    Gen.list
      (Range.singleton $ r * c)
      (Gen.int $ Range.linear (-1000000000) 1000000000)
  return . chunksOf c $ l

-- | Generate arbitrary absolute directory paths
genAbsDir :: Gen Path.AbsDir
genAbsDir = do
  relDirs <- genRelDir
  return $ Path.absDir "/" </> relDirs

-- | Generate arbitrary absolute directory paths
genRelDir :: Gen Path.RelDir
genRelDir = do
  dirs <-
    Gen.list
      (Range.linear 0 20)
      (Gen.string (Range.linear 1 40) Gen.alphaNum)
  return $ Path.joinPath dirs

genRelFile :: Gen Path.RelFile
genRelFile = do
  fileName <- Gen.string (Range.linear 1 40) Gen.alphaNum
  relD <- genRelDir
  return $ relD </> Path.relFile fileName

genAbsFile :: Gen Path.AbsFile
genAbsFile = do
  fileName <- Gen.string (Range.linear 1 40) Gen.alphaNum
  absD <- genAbsDir
  return $ absD </> Path.relFile fileName
