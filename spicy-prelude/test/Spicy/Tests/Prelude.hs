module Spicy.Tests.Prelude (tests) where

import Data.Aeson as Aeson
import Data.Binary as Binary
import Data.Massiv.Array as Massiv
import Hedgehog
import Spicy.Prelude
import Spicy.Tests.Prelude.Generators
import Test.Tasty
import Test.Tasty.Hedgehog

tests :: TestTree
tests = testGroup "Prelude" [test_json]

test_json :: TestTree
test_json =
  testGroup
    "JSON Serialisation"
    [ testProperty "Abs Dir Path" . property $ do
        p <- forAll genAbsDir
        tripping p Aeson.encode Aeson.decode
    , testProperty "Rel Dir Path" . property $ do
        p <- forAll genRelDir
        tripping p Aeson.encode Aeson.decode
    , testProperty "Abs File Path" . property $ do
        p <- forAll genAbsFile
        tripping p Aeson.encode Aeson.decode
    , testProperty "Rel File Path" . property $ do
        p <- forAll genRelFile
        tripping p Aeson.encode Aeson.decode
    ]
