#include "libcint_helper.h"

// Free allocated members of our basis struct
void free_basis(libcint_basis *basis_struct)
{
    free(basis_struct->env);
    free(basis_struct->bas);
    free(basis_struct->atm);
}

// Print the contents of a basis struct
void print_basis(libcint_basis *basis_struct)
{
    printf("natm: %d\n", basis_struct->natm);
    printf("nbas: %d\n", basis_struct->nbas);
    printf("nenv: %d\n", basis_struct->nenv);

    printf("atm:\n");
    for (int i = 0; i < basis_struct->natm * ATM_SLOTS; i++)
        printf("%d  ", basis_struct->atm[i]);
    printf("\n");

    printf("bas:\n");
    for (int i = 0; i < basis_struct->nbas * BAS_SLOTS; i++)
        printf("%d  ", basis_struct->bas[i]);
    printf("\n");

    // Do not print the leading zeros of the env array
    printf("env:\n");
    for (int i = PTR_ENV_START; i < basis_struct->nenv; i++)
        printf("%lf  ", basis_struct->env[i]);
    printf("\n");
}

// Create a libcint basis compliant basis struct coming from a Psi4 calculation
void gen_libcint_basis(PyObject *Result, libcint_basis *basis)
{
    PyGILState_STATE state = PyGILState_Ensure();

    // The WFN object is the second element of the returned result
    if (PyTuple_Size(Result) < 2)
    {
        fprintf(stderr, "Tuple has no element 1\n");
        exit(EXIT_FAILURE);
    }
    PyObject *WFNObj = PyTuple_GetItem(Result, 1);
    sanity_check(WFNObj);

    // Get the basis set from the wfn object, and the molecule from the resulting basis set object
    PyObject *Basis = get_method(WFNObj, "basisset");
    PyObject *BasisObj = PyObject_CallNoArgs(Basis);
    PyObject *Molecule = get_method(BasisObj, "molecule");
    PyObject *MoleculeObj = PyObject_CallNoArgs(Molecule);

    // The molecule can contain a.u. or not
    // Set the appropriate conversion factor since libcint expects Bohr
    PyObject *Units = get_method(MoleculeObj, "units");
    PyObject *UnitsObj = PyObject_CallNoArgs(Units);
    char *unit = get_string(UnitsObj);
    double conversion;
    if (strcmp(unit, "Angstrom") == 0)
    {
        conversion = BOHR;
    }
    else if (strcmp(unit, "Bohr") == 0)
    {
        conversion = ANGSTROM;
    }
    else
    {
        fprintf(stderr, "Python function did not return a integer\n");
        exit(EXIT_FAILURE);
    }

    // Get the geometry information and convert them to an internal array
    PyObject *Geometry = get_method(MoleculeObj, "geometry");
    PyObject *GeometryObj = PyObject_CallNoArgs(Geometry);
    PyObject *GeomArray = get_method(GeometryObj, "to_array");
    PyArrayObject *GeomArrayObj = (PyArrayObject *)PyObject_CallNoArgs(GeomArray);
    pyarray_struct geometry;
    array2struct(GeomArrayObj, &geometry);

    // Get some basis set lengths
    PyObject *MaxNprim = get_method(BasisObj, "max_nprimitive");
    PyObject *MaxNprimObj = PyObject_CallNoArgs(MaxNprim);
    PyObject *Nshell = get_method(BasisObj, "nshell");
    PyObject *NshellObj = PyObject_CallNoArgs(Nshell);

    // The shape of the geometry array is (natom, 3)
    basis->natm = geometry.dimensions[0];
    // In Psi4 nshell is natoms * nshells(atom), so the shells are not unique
    basis->nbas = get_long(NshellObj);

    // Allocate the arrays of the libcint basis struct, see the provided example and documentation for details:
    // https://github.com/sunqm/libcint/blob/master/examples/c_call_spheric.c
    // https://raw.githubusercontent.com/sunqm/libcint/master/doc/program_ref.pdf
    // Use calloc to make sure we have zeros at all unused flags
    basis->atm = calloc(sizeof(int), basis->natm * ATM_SLOTS);
    basis->bas = calloc(sizeof(int), basis->nbas * BAS_SLOTS);
    // Magic length calculation of the env array size
    // This can easily be understood when looking at the variable "off" below
    // Since we use max_nprimitive instead of all nprimitive per shell the array can be a bit larger than needed
    basis->env = calloc(sizeof(double), PTR_ENV_START + 3 * basis->natm + 2 * get_long(MaxNprimObj) * basis->nbas);

    // Get the charge per atom
    double *charge = malloc(basis->natm * sizeof(double));
    PyObject *Charge = get_method(MoleculeObj, "charge");
    PyObject *ChargeObj;
    for (int i = 0; i < basis->natm; i++)
    {
        ChargeObj = PyObject_CallFunction(Charge, "i", i);
        charge[i] = get_double(ChargeObj);
    }

    // Initialize the atm array and save geometries in env
    int off = PTR_ENV_START;
    for (int i = 0; i < basis->natm; i++)
    {
        basis->atm[CHARGE_OF + ATM_SLOTS * i] = charge[i]; // Charge of the i-th atom
        basis->atm[PTR_COORD + ATM_SLOTS * i] = off;       // Pointer to the coordinates
        // The following flags are unused (by us)
        // basis->atm[NUC_MOD_OF + ATM_SLOTS * i] = 0;
        // basis->atm[PTR_ZETA + ATM_SLOTS * i] = 0;
        // basis->atm[PTR_FRAC_CHARGE + ATM_SLOTS * i] = 0;
        basis->env[off + 0] = geometry.data[i * 3 + 0] * conversion; // x-coordinate
        basis->env[off + 1] = geometry.data[i * 3 + 1] * conversion; // y-coordinate
        basis->env[off + 2] = geometry.data[i * 3 + 2] * conversion; // z-coordinate
        off += 3;
    }

    // In Psi4 the shells given in the Python interface are not unique
    // Since we want them to be unique we have to work for it
    // Use the idx2shell as a map, where the index in the array corresponds to the first shell of an atom
    // Since the shells are sorted by atoms we can save a length for said atom to know how many shells it has
    // Only the first atom of each label will be written to the array, the rest is set to zero
    int *idx2shell_starts = calloc(sizeof(int), basis->natm);
    int *idx2shell_lens = calloc(sizeof(int), basis->natm);

    PyObject *ShellObj, *Exp, *ExpObj, *Coeff, *CoeffObj, *NCenter, *AM, *NPrimitive, *Label, *LabelObj;
    PyObject *TmpShellObj, *TmpNCenter, *TmpLabelObj;
    int nprim, ncenter, tmp_ncenter = 0, starting_shell;
    char *atom_label, *tmp_label;
    PyObject *Shell = get_method(BasisObj, "shell");
    Label = get_method(MoleculeObj, "label");
    // Initialize the bas array and save basis set information in env
    // Since every atom has its own shell, iterate over them (ishell) and check if the shells are unique or not
    for (int ishell = 0; ishell < basis->nbas; ishell++)
    {
        ShellObj = PyObject_CallFunction(Shell, "i", ishell);
        // ncenter is in fact the index of the atom the shell belongs to
        NCenter = get_attribute(ShellObj, "ncenter");
        ncenter = (int)get_long(NCenter);
        // The label of the atom corresponds to the atom symbol
        // Since we arent interested in mixing basis set for same species we assume that all labels have the same basis
        LabelObj = PyObject_CallFunction(Label, "i", ncenter);
        atom_label = get_string(LabelObj);

        // This is where we check if the current shell is unique or has been used previously (by iterating over jshell)
        for (int jshell = 0; jshell <= ishell; jshell++)
        {
            TmpShellObj = PyObject_CallFunction(Shell, "i", jshell);
            TmpNCenter = get_attribute(TmpShellObj, "ncenter");
            TmpLabelObj = PyObject_CallFunction(Label, "i", tmp_ncenter);
            tmp_ncenter = (int)get_long(TmpNCenter);
            tmp_label = get_string(TmpLabelObj);
            // Check if the current label has been used in an previous shell
            if (strcmp(atom_label, tmp_label) == 0)
            {
                // If the shell with the same label has the same center (i.e., it belongs to the same atom),
                // we have a new unique shell to save
                if (ncenter == tmp_ncenter)
                {
                    // If the length is still zero we set our starting shell id to the current one
                    if (idx2shell_lens[ncenter] == 0)
                    {
                        idx2shell_starts[ncenter] = ishell;
                    }
                    // Increase the number of shells for the current atom by one
                    idx2shell_lens[ncenter] += 1;

                    basis->bas[ATOM_OF + BAS_SLOTS * ishell] = ncenter; // Atom the shell belongs to
                    AM = get_attribute(ShellObj, "am");
                    basis->bas[ANG_OF + BAS_SLOTS * ishell] = (int)get_long(AM); // Angular momentum of the shell
                    // Number of primitives IN ALL contractions
                    NPrimitive = get_attribute(ShellObj, "nprimitive");
                    nprim = (int)get_long(NPrimitive);
                    basis->bas[NPRIM_OF + BAS_SLOTS * ishell] = nprim; // Number of primitives
                    // It looks like Psi4 only gives uncontracted coefficient in the basis, so nctr is always 1 (?)
                    basis->bas[NCTR_OF + BAS_SLOTS * ishell] = 1;
                    // The following flag is unused (by us)
                    // basis->bas[KAPPA_OF + BAS_SLOTS * ishell] = 0;

                    basis->bas[PTR_EXP + BAS_SLOTS * ishell] = off;
                    // Save basis exponents
                    Exp = get_method(ShellObj, "exp");
                    // For an uncontracted basis nexp is equal to nprim
                    for (int nexp = 0; nexp < nprim; nexp++)
                    {
                        ExpObj = PyObject_CallFunction(Exp, "i", nexp);
                        basis->env[off + nexp] = get_double(ExpObj);
                    }
                    off += nprim;

                    basis->bas[PTR_COEFF + BAS_SLOTS * ishell] = off; // Pointer to the basis coefficients
                    // Save basis coefficients
                    // original_coef are the oroginal unnormalized coefficients
                    // Since Spicy will normalize itself it doesnt really matter what function to use
                    // Other options are coef and erd_coef
                    // When printing or writing basis information in Psi4 it looks like original_coef is always used
                    Coeff = get_method(ShellObj, "original_coef");
                    for (int ncoeff = 0; ncoeff < nprim; ncoeff++)
                    {
                        CoeffObj = PyObject_CallFunction(Coeff, "i", ncoeff);
                        basis->env[off + ncoeff] = get_double(CoeffObj);
                    }
                    off += nprim;
                }
                else
                {
                    // If we have the same label but different centers the shells are already saved in env
                    // Iterate over all the shells of the already used atom
                    // Skip the shells we iterate over in the outermost loop
                    starting_shell = idx2shell_starts[tmp_ncenter];
                    for (int i = 0; i < idx2shell_lens[tmp_ncenter]; i++)
                    {
                        basis->bas[ATOM_OF + BAS_SLOTS * ishell] = ncenter; // ncenter is the only value that differs
                        // Point to the basis information that are already saved in env by using the pointer in bas
                        basis->bas[ANG_OF + BAS_SLOTS * ishell] = basis->bas[ANG_OF + BAS_SLOTS * (starting_shell + i)];
                        basis->bas[NPRIM_OF + BAS_SLOTS * ishell] = basis->bas[NPRIM_OF + BAS_SLOTS * (starting_shell + i)];
                        basis->bas[NCTR_OF + BAS_SLOTS * ishell] = basis->bas[NCTR_OF + BAS_SLOTS * (starting_shell + i)];
                        // The following flag is unused (by us)
                        // basis->bas[KAPPA_OF + BAS_SLOTS * ishell] = basis->bas[KAPPA_OF + BAS_SLOTS * (starting_shell + i)];;
                        basis->bas[PTR_EXP + BAS_SLOTS * ishell] = basis->bas[PTR_EXP + BAS_SLOTS * (starting_shell + i)];
                        basis->bas[PTR_COEFF + BAS_SLOTS * ishell] = basis->bas[PTR_COEFF + BAS_SLOTS * (starting_shell + i)];
                        ishell++;
                    }
                }
                break;
            }
        }
    }

    // Save the actual length of env and reallocate accordingly
    basis->nenv = off;
    basis->env = realloc(basis->env, off * sizeof(double));

    // The grave freeup
    free(idx2shell_lens);
    free(idx2shell_starts);
    free(charge);
    Py_DECREF(TmpLabelObj);
    Py_DECREF(TmpNCenter);
    Py_DECREF(TmpShellObj);
    Py_DECREF(LabelObj);
    Py_DECREF(Label);
    Py_DECREF(NPrimitive);
    Py_DECREF(AM);
    Py_DECREF(NCenter);
    Py_DECREF(CoeffObj);
    Py_DECREF(Coeff);
    Py_DECREF(ExpObj);
    Py_DECREF(Exp);
    Py_DECREF(ShellObj);
    Py_DECREF(Shell);
    Py_DECREF(NshellObj);
    Py_DECREF(Nshell);
    Py_DECREF(MaxNprimObj);
    Py_DECREF(MaxNprim);
    Py_DECREF(ChargeObj);
    Py_DECREF(Charge);
    Py_DECREF(GeomArrayObj);
    Py_DECREF(GeomArray);
    Py_DECREF(GeometryObj);
    Py_DECREF(Geometry);
    Py_DECREF(Units);
    Py_DECREF(UnitsObj);
    Py_DECREF(MoleculeObj);
    Py_DECREF(Molecule);
    Py_DECREF(BasisObj);
    Py_DECREF(Basis);
    // DO NOT DECREF the items from PyTuple_GetItem since the return a borrowed reference
    // decreffing would free the object in the tuple itself (not so nice)
    // Py_DECREF(WFNObj);

    PyGILState_Release(state);
}
