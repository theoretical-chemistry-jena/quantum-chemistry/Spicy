#ifndef LIBCINT_HELPER_H_
#define LIBCINT_HELPER_H_

#include <string.h>
#include <cint.h>
#include "py_helper.h"

// Some unit definitions
#define ANGSTROM 1
#define BOHR 0.529177210903

// Struct to create input for libcint including all geometry and basis information
// Save the length of all arrays
// Note that env can be a bit longer than nenv, but env will only hold zeros after nenv
typedef struct libcint_basis
{
    int natm;
    int nbas;
    int nenv;
    int *atm;
    int *bas;
    double *env;
} libcint_basis;

void free_basis(libcint_basis *basis_struct);
void print_basis(libcint_basis *basis_struct);
void gen_libcint_basis(PyObject *Result, libcint_basis *basis);

#endif