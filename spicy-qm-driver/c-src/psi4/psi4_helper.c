#include "psi4_helper.h"

// Import the Psi4 Python module
PyObject *initialize_psi4()
{
    PyGILState_STATE state = PyGILState_Ensure();

    PyObject *Psi4 = import_module("psi4");

    PyGILState_Release(state);
    return Psi4;
}

// Build a python object from an options struct
PyObject *option2obj(PyObject *Obj, options_struct *option)
{
    PyGILState_STATE state = PyGILState_Ensure();

    if (option->value_type == "i")
    {
        Obj = Py_BuildValue("i", option->value.value_int);
    }
    else if (option->value_type == "d")
    {
        Obj = Py_BuildValue("d", option->value.value_double);
    }
    else if (option->value_type == "s")
    {
        Obj = Py_BuildValue("s", option->value.value_str);
    }

    PyGILState_Release(state);
    return Obj;
}

// Set psi4 options
// Psi4 uses dictionaries as inputs with a string key and a value of type integer, double, string, or list
// Fun fact: The lists to, e.g, define active orbitals have to be given as strings (bruh)
void set_options(PyObject *Psi4, const char *memory, const int n_options, options_struct *options)
{
    PyGILState_STATE state = PyGILState_Ensure();

    // Only set the memory if it is given
    if (memory)
    {
        PyObject *Memory = get_method(Psi4, "set_memory");
        PyObject *MemoryObj = PyObject_CallFunction(Memory, "s", memory);
        sanity_check(MemoryObj);
        Py_DECREF(MemoryObj);
        Py_DECREF(Memory);
    }

    PyObject *Options = get_method(Psi4, "set_options");
    PyObject *Dict, *Ref, *Obj, *InnerList, *ListElem;
    for (int i = 0; i < n_options; i++)
    {
        Obj = option2obj(Obj, &options[i]); // Skip lists in this function
        // Support doubly-nested lists of mixed types
        if (options[i].value_type == "l")
        {
            Obj = PyList_New(options[i].n_elements);
            for (int j = 0; j < options[i].n_elements; j++)
            {
                if (options[i].value.value_option[j].value_type == "l")
                {
                    InnerList = PyList_New(options[i].value.value_option[j].n_elements);
                    for (int k = 0; k < options[i].value.value_option[j].n_elements; k++)
                    {
                        PyList_SetItem(InnerList, k, option2obj(ListElem, &options[i].value.value_option[j].value.value_option[k]));
                    }
                    PyList_SetItem(Obj, j, option2obj(InnerList, &options[i].value.value_option[j]));
                    Py_DECREF(InnerList);
                }
                else
                {
                    PyList_SetItem(Obj, j, option2obj(ListElem, &options[i].value.value_option[j]));
                    Py_DECREF(ListElem);
                }
            }
            Py_DECREF(Obj);
        }
        Dict = Py_BuildValue("{s:O}", options[i].key, Obj);
        Ref = PyObject_CallOneArg(Options, Dict);
        sanity_check(Ref);
        Py_DECREF(Dict);
        Py_DECREF(Ref);
    }

    Py_DECREF(Options);
    PyGILState_Release(state);
}

// Create a Psi4 Molecule object from a given geometry string
// Somehow Psi4 knows that this Object has been created and does not need to be passed to the actual calculation
// The energy function will use the latest created Molecule
// decref the Molecule object at last
PyObject *create_molecule(PyObject *Psi4, const char *molecule)
{
    PyGILState_STATE state = PyGILState_Ensure();

    PyObject *Molecule = get_method(Psi4, "geometry");

    PyObject *MoleculeObj = PyObject_CallFunction(Molecule, "s", molecule);
    sanity_check(MoleculeObj);

    Py_DECREF(Molecule);
    PyGILState_Release(state);
    return MoleculeObj;
}

// Do a psi4 calculation for a given property
PyObject *calc_property(PyObject *Psi4, const char *property, const char *method, pyarray_struct *external_potential)
{
    PyGILState_STATE state = PyGILState_Ensure();

    PyObject *Method = get_method(Psi4, property);

    // Pass the method as a simple string
    PyObject *MethodArgs = Py_BuildValue("(s)", method);
    // Return the WFN object, too, to get the basis set information
    PyObject *MethodKWArgs;
    // Do not feed in the external_potential as a keyword if it is NULL
    if (!external_potential)
    {
        MethodKWArgs = Py_BuildValue("{s:O}", "return_wfn", Py_True);
    }
    else
    {
        // Somehow we need to call import_array here again, otherwise PyArray_SimpleNewFromData will segfault
        import_array();
        PyObject *ExtPot = PyArray_SimpleNewFromData(external_potential->nd, external_potential->dimensions, NPY_DOUBLE, external_potential->data);
        sanity_check(ExtPot);
        MethodKWArgs = Py_BuildValue("{s:O,s:O}", "return_wfn", Py_True, "external_potentials", ExtPot);
        Py_DECREF(ExtPot);
    }
    PyObject *Result = call_method_args_kwargs(Method, MethodArgs, MethodKWArgs);
    sanity_check(Result);
    if (!PyTuple_Check(Result))
    {
        fprintf(stderr, "Object is not a tuple\n");
        exit(EXIT_FAILURE);
    }

    Py_DECREF(MethodKWArgs);
    Py_DECREF(MethodArgs);
    Py_DECREF(Method);
    Py_DECREF(Psi4);

    PyGILState_Release(state);
    return Result;
}

// Calc energy wrapper
PyObject *calc_energy(PyObject *Psi4, const char *method, pyarray_struct *external_potential)
{
    return calc_property(Psi4, "energy", method, external_potential);
}

// Calc gradient wrapper
PyObject *calc_gradient(PyObject *Psi4, const char *method, pyarray_struct *external_potential)
{
    return calc_property(Psi4, "gradient", method, external_potential);
}

// Calc Hessian wrapper
PyObject *calc_hessian(PyObject *Psi4, const char *method, pyarray_struct *external_potential)
{
    return calc_property(Psi4, "hessian", method, external_potential);
}

// Get the total energy from a psi4 calculation object
double get_energy(PyObject *Result)
{
    PyGILState_STATE state = PyGILState_Ensure();

    // The energy is the first element of the returned result
    if (PyTuple_Size(Result) == 0)
    {
        fprintf(stderr, "Tuple has no elements\n");
        exit(EXIT_FAILURE);
    }
    PyObject *EnergyObj = PyTuple_GetItem(Result, 0);
    sanity_check(EnergyObj);

    double energy = get_double(EnergyObj);

    // DO NOT DECREF the items from PyTuple_GetItem since the return a borrowed reference
    // decreffing would free the object in the tuple itself (not so nice)
    // Py_DECREF(EnergyObj);

    PyGILState_Release(state);
    return energy;
}

// Get the gradient from a psi4 calculation object
void get_gradient(PyObject *Result, pyarray_struct *array)
{
    PyGILState_STATE state = PyGILState_Ensure();

    // The energy is the first element of the returned result
    if (PyTuple_Size(Result) == 0)
    {
        fprintf(stderr, "Tuple has no elements\n");
        exit(EXIT_FAILURE);
    }
    PyObject *ResObj = PyTuple_GetItem(Result, 0);
    sanity_check(ResObj);

    PyObject *ResArray = get_method(ResObj, "to_array");
    PyArrayObject *ResArrayObj = (PyArrayObject *)PyObject_CallNoArgs(ResArray);
    array2struct(ResArrayObj, array);

    // DO NOT DECREF the items from PyTuple_GetItem since the return a borrowed reference
    // decreffing would free the object in the tuple itself (not so nice)
    // Py_DECREF(ResObj);
    Py_DECREF(ResArrayObj);
    Py_DECREF(ResArray);

    PyGILState_Release(state);
    return;
}

// Get the Hessian from a psi4 calculation object
void get_hessian(PyObject *Result, pyarray_struct *array)
{
    // Same as the gradient getter
    get_gradient(Result, array);
    return;
}

// Get the total dipole moment
void get_dipole(PyObject *Psi4, pyarray_struct *dipole)
{
    PyGILState_STATE state = PyGILState_Ensure();

    PyObject *Core = get_attribute(Psi4, "core");
    // Use the strange variable function that reads elements from the global memory
    PyObject *Variable = get_method(Core, "variable");
    PyArrayObject *Result = (PyArrayObject *)PyObject_CallFunction(Variable, "s", "CURRENT DIPOLE");
    array2struct(Result, dipole);

    Py_DECREF(Result);
    Py_DECREF(Variable);
    Py_DECREF(Core);

    PyGILState_Release(state);
    return;
}

// Get the density matrices for the alpha and beta channel
void get_dm(PyObject *Result, pyarray_struct *dm_alpha, pyarray_struct *dm_beta)
{
    PyGILState_STATE state = PyGILState_Ensure();

    // The WFN object is the second element of the returned result
    if (PyTuple_Size(Result) < 2)
    {
        fprintf(stderr, "Tuple has no element 1\n");
        exit(EXIT_FAILURE);
    }
    PyObject *WFNObj = PyTuple_GetItem(Result, 1);
    sanity_check(WFNObj);

    // Get the density matrix for the alpha channel and convert to an internal array
    PyObject *Da = get_method(WFNObj, "Da");
    PyObject *DaObj = PyObject_CallNoArgs(Da);
    PyObject *DaArray = get_method(DaObj, "to_array");
    PyArrayObject *DaArrayObj = (PyArrayObject *)PyObject_CallNoArgs(DaArray);
    array2struct(DaArrayObj, dm_alpha);

    // Do the same for the beta channel
    PyObject *Db = get_method(WFNObj, "Db");
    PyObject *DbObj = PyObject_CallNoArgs(Db);
    PyObject *DbArray = get_method(DbObj, "to_array");
    PyArrayObject *DbArrayObj = (PyArrayObject *)PyObject_CallNoArgs(DbArray);
    array2struct(DbArrayObj, dm_beta);

    Py_DECREF(DbArrayObj);
    Py_DECREF(DbArray);
    Py_DECREF(DbObj);
    Py_DECREF(Db);
    Py_DECREF(DaArrayObj);
    Py_DECREF(DaArray);
    Py_DECREF(DaObj);
    Py_DECREF(Da);
    // DO NOT DECREF the items from PyTuple_GetItem since the return a borrowed reference
    // decreffing would free the object in the tuple itself (not so nice)
    // Py_DECREF(WFNObj);

    PyGILState_Release(state);
}