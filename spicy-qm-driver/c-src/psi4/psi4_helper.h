#ifndef PSI4_HELPER_H_
#define PSI4_HELPER_H_

#include <string.h>
#include "py_helper.h"

typedef struct options_struct
{
    int n_elements;
    char *key;
    char *value_type;
    union
    {
        int value_int;
        double value_double;
        char *value_str;
        struct options_struct *value_option;
    } value;
} options_struct;

PyObject *initialize_psi4();
PyObject *option2obj(PyObject *Obj, options_struct *option);
void set_options(PyObject *Psi4, const char *memory, const int n_options, options_struct *options);
PyObject *create_molecule(PyObject *Psi4, const char *geom);
PyObject *calc_property(PyObject *Psi4, const char *property, const char *method, pyarray_struct *external_potential);
PyObject *calc_energy(PyObject *Psi4, const char *method, pyarray_struct *external_potential);
PyObject *calc_gradient(PyObject *Psi4, const char *method, pyarray_struct *external_potential);
PyObject *calc_hessian(PyObject *Psi4, const char *method, pyarray_struct *external_potential);
double get_energy(PyObject *Result);
void get_gradient(PyObject *Result, pyarray_struct *array);
void get_hessian(PyObject *Result, pyarray_struct *array);
void get_dipole(PyObject *Psi4, pyarray_struct *dipole);
void get_dm(PyObject *Result, pyarray_struct *dm_alpha, pyarray_struct *dm_beta);

#endif