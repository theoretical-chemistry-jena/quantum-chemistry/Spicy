#include "py_helper.h"

// Initialize Python interpreter and Numpy
void *initialize()
{
    Py_Initialize();
    // Numpy has to be initialized to make some functions available
    import_array();
    return (void *)1;
}

// Close Python interpreter
void finalize()
{
    Py_Finalize();
}

// Free allocated members of our pyarray imitation
void free_pyarray(pyarray_struct *array_struct)
{
    free(array_struct->data);
    free(array_struct->dimensions);
}

// Simple Null check for PyObjects
void sanity_check(PyObject *Obj)
{
    if (!Obj)
    {
        PyErr_Print();
        fprintf(stderr, "Object is NULL\n");
        exit(EXIT_FAILURE);
    }
}

// Make sure that Numpy arrays are C contiguous, otherwise we could run into problems
void array_check(PyArrayObject *Array)
{
    sanity_check((PyObject *)Array);
    if (!PyArray_Check(Array))
    {
        fprintf(stderr, "Object is not a NumPy array\n");
        exit(EXIT_FAILURE);
    }
    if (!PyArray_IS_C_CONTIGUOUS(Array))
    {
        fprintf(stderr, "Array is not C contiguous\n");
        exit(EXIT_FAILURE);
    }
}

// Convert PyArrayObject to a pure C struct
// Copy the data so we can safely free the PyArrayObject
// This has the side effect that we manually have to free each struct data member
void array2struct(PyArrayObject *Array, pyarray_struct *array_struct)
{
    PyGILState_STATE state = PyGILState_Ensure();

    array_check(Array);

    int ndim = PyArray_NDIM(Array);
    if (ndim == 0)
    {
        fprintf(stderr, "Array is zero-dimensional\n");
        exit(EXIT_FAILURE);
    }
    array_struct->nd = ndim;

    npy_intp *shape = PyArray_DIMS(Array);
    if (!shape)
    {
        fprintf(stderr, "Object has no shape\n");
        exit(EXIT_FAILURE);
    }
    array_struct->dimensions = malloc(ndim * sizeof(npy_intp));
    memcpy(array_struct->dimensions, shape, ndim * sizeof(npy_intp));

    int size = PyArray_SIZE(Array);
    array_struct->data = malloc(size * sizeof(double));
    memcpy(array_struct->data, PyArray_DATA(Array), size * sizeof(double));

    PyGILState_Release(state);
}

// Print the representation of a python object
// Taken from https://stackoverflow.com/a/46202119
void print_object(PyObject *Obj)
{
    PyGILState_STATE state = PyGILState_Ensure();

    PyObject *Repr = PyObject_Repr(Obj);
    PyObject *Str = PyUnicode_AsEncodedString(Repr, "utf-8", "~E~");
    const char *bytes = PyBytes_AS_STRING(Str);
    printf("%s\n", bytes);
    Py_DECREF(Repr);
    Py_DECREF(Str);

    PyGILState_Release(state);
}

// Simple print function for 1d and 2d arrays
// (Higher dimensions can be printed, too, but will have weird linebreaks)
void print_array(pyarray_struct *array_struct)
{
    // Print array informations
    printf("ndim: %d\n", array_struct->nd);
    printf("shape: (");
    int size = 1;
    for (int i = 0; i < array_struct->nd; i++)
    {
        printf(" %ld ", array_struct->dimensions[i]);
        size *= array_struct->dimensions[i];
    }
    printf(")\n");

    // Print data
    for (int i = 0; i < size; i++)
    {
        if (array_struct->nd > 1 && i != 0 && i % array_struct->dimensions[1] == 0)
        {
            printf("\n");
        }
        printf("%+e  ", *(array_struct->data + i));
    }
    printf("\n");
}

// Import modules by its name
// This function will not work for local modules, unless they are added to the path manually
PyObject *import_module(const char *module)
{
    PyGILState_STATE state = PyGILState_Ensure();

    PyObject *Module = PyImport_ImportModule(module);

    sanity_check(Module);

    PyGILState_Release(state);
    return Module;
}

// Get a method by its name from an object
// PyObject_GetAttrString can be used to get methods and attributes, check that we get a callable
PyObject *get_method(PyObject *Module, const char *method)
{
    PyGILState_STATE state = PyGILState_Ensure();

    PyObject *Method = PyObject_GetAttrString(Module, method);

    sanity_check(Method);
    if (!PyCallable_Check(Method))
    {
        fprintf(stderr, "Attribut %s is not callable\n", method);
        exit(EXIT_FAILURE);
    }

    PyGILState_Release(state);
    return Method;
}

// Get a attribute by its name from an object
// Same as get_method but without the callable check
PyObject *get_attribute(PyObject *Module, const char *attribute)
{
    PyGILState_STATE state = PyGILState_Ensure();

    PyObject *Attribute = PyObject_GetAttrString(Module, attribute);

    sanity_check(Attribute);

    PyGILState_Release(state);
    return Attribute;
}

// Simply get the double representation from a PyObject
double get_double(PyObject *Number)
{
    PyGILState_STATE state = PyGILState_Ensure();

    sanity_check(Number);
    if (!PyFloat_Check(Number))
    {
        fprintf(stderr, "Python function did not return a float\n");
        exit(EXIT_FAILURE);
    }

    double res = PyFloat_AsDouble(Number);

    PyGILState_Release(state);
    return res;
}

// Simply get the long int representation from a PyObject
long get_long(PyObject *Number)
{
    PyGILState_STATE state = PyGILState_Ensure();

    sanity_check(Number);
    if (!PyLong_Check(Number))
    {
        fprintf(stderr, "Python function did not return a integer\n");
        exit(EXIT_FAILURE);
    }

    long res = PyLong_AsLong(Number);

    PyGILState_Release(state);
    return res;
}

// "Simply" get the string representation from a PyObject
char *get_string(PyObject *String)
{
    PyGILState_STATE state = PyGILState_Ensure();

    sanity_check(String);
    if (!PyUnicode_Check(String))
    {
        fprintf(stderr, "Python function did not return a integer\n");
        exit(EXIT_FAILURE);
    }

    // Make sure we have an unicode object
    PyObject *UnicodeStr = PyUnicode_FromObject(String);
    sanity_check(UnicodeStr);
    // Encode object as utf-8
    PyObject *EncodedStr = PyUnicode_AsEncodedString(UnicodeStr, "utf-8", NULL);
    sanity_check(EncodedStr);
    // Get the string representation
    char *res = PyBytes_AS_STRING(EncodedStr);

    Py_DECREF(EncodedStr);
    Py_DECREF(UnicodeStr);

    PyGILState_Release(state);
    return res;
}

// Safely call a method with arguments and keyword arguments
// Args and KWArgs have to be PyObjects, i.e, one a tuple and one a dict that have to be build, e.g., with Py_BuildValue
PyObject *call_method_args_kwargs(PyObject *Method, PyObject *Args, PyObject *KWArgs)
{
    PyGILState_STATE state = PyGILState_Ensure();

    sanity_check(Args);
    if (!PyTuple_Check(Args))
    {
        fprintf(stderr, "Argument is not a tuple\n");
        exit(EXIT_FAILURE);
    }
    sanity_check(KWArgs);
    if (!PyDict_Check(KWArgs))
    {
        fprintf(stderr, "Keyword argument is not a dictionary\n");
        exit(EXIT_FAILURE);
    }

    PyObject *Res = PyObject_Call(Method, Args, KWArgs);

    sanity_check(Res);

    PyGILState_Release(state);
    return Res;
}