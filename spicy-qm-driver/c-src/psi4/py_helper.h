#ifndef PY_HELPER_H_
#define PY_HELPER_H_

#define NPY_NO_DEPRECATED_API NPY_1_7_API_VERSION
#include <Python.h>
#include <numpy/arrayobject.h>

// Struct to imitate the PyArrayObject struct
// Member naming is identical to the PyArrayObject
// https://numpy.org/devdocs/reference/c-api/types-and-structures.html#pyarray-type-and-pyarrayobject
typedef struct pyarray_struct
{
    int nd;
    npy_intp *dimensions;
    double *data;
} pyarray_struct;

void *initialize();
void finalize();
void free_pyarray(pyarray_struct *array_struct);
void sanity_check(PyObject *Obj);
void array_check(PyArrayObject *Array);
void array2struct(PyArrayObject *Array, pyarray_struct *array_struct);
void print_object(PyObject *Obj);
void print_array(pyarray_struct *array_struct);
PyObject *import_module(const char *module);
PyObject *get_method(PyObject *Module, const char *MethodName);
PyObject *get_attribute(PyObject *Module, const char *attribute);
double get_double(PyObject *Number);
long get_long(PyObject *Number);
char *get_string(PyObject *String);
PyObject *call_method_args_kwargs(PyObject *Method, PyObject *Args, PyObject *KWArgs);

#endif