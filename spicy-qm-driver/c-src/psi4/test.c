#include <stdio.h>
#include "py_helper.h"
#include "psi4_helper.h"
#include "libcint_helper.h"

int cint1e_ovlp_sph(double *buf, int *shls, int *atm, int natm, int *bas, int nbas, double *env);

int main()
{
    initialize();

    PyObject *Psi4 = initialize_psi4();
    int n_options = 5;
    options_struct *options = malloc(n_options * sizeof(options_struct));
    options[0].n_elements = 1;
    options[0].key = "basis";
    options[0].value_type = "s";
    options[0].value.value_str = "sto-3g";
    options[1].n_elements = 1;
    options[1].key = "reference";
    options[1].value_type = "s";
    options[1].value.value_str = "rhf";
    options[2].n_elements = 1;
    options[2].key = "frozen_docc";
    options[2].value_type = "s";
    options[2].value.value_str = "[1, 0, 0, 0]";
    options[3].n_elements = 1;
    options[3].key = "active";
    options[3].value_type = "s";
    options[3].value.value_str = "[3, 0, 1, 2]";
    options[4].n_elements = 1;
    options[4].key = "print";
    options[4].value_type = "i";
    options[4].value.value_int = 0;

    set_options(Psi4, "512MiB", n_options, options);
    free(options);

    PyObject *Molecule = create_molecule(Psi4, "O 0 0 0.1173\nH 0 0.7572 -0.4692\nH 0 -0.7572 -0.4692\nSYMMETRY C1");
    pyarray_struct external_potential;
    external_potential.nd = 2;
    external_potential.dimensions = malloc(sizeof(npy_intp));
    external_potential.dimensions[0] = 1;
    external_potential.dimensions[1] = 4;
    external_potential.data = calloc(4, sizeof(double));
    external_potential.data[0] = -1;
    PyObject *Result = calc_energy(Psi4, "casscf", &external_potential);
    free_pyarray(&external_potential);

    double etot = get_energy(Result);
    printf("Total energy: %lf\n", etot);
    pyarray_struct dipole;
    get_dipole(Psi4, &dipole);
    printf("Dipole moment\n");
    print_array(&dipole);
    free_pyarray(&dipole);

    // Result = calc_gradient(Psi4, "casscf", NULL);
    // pyarray_struct gradient;
    // get_gradient(Result, &gradient);
    // printf("Gradient\n");
    // print_array(&gradient);
    // free_pyarray(&gradient);

    // Result = calc_hessian(Psi4, "casscf", NULL);
    // pyarray_struct hessian;
    // get_hessian(Result, &hessian);
    // printf("Hessian\n");
    // print_array(&hessian);
    // free_pyarray(&hessian);

    // pyarray_struct dm_alpha;
    // pyarray_struct dm_beta;
    // get_dm(Result, &dm_alpha, &dm_beta);
    // printf("Alpha density matrix:\n");
    // print_array(&dm_alpha);
    // printf("Beta density matrix:\n");
    // print_array(&dm_beta);

    // libcint_basis basis;
    // gen_libcint_basis(Result, &basis);
    // printf("Basis:\n");
    // print_basis(&basis);

    // int i, j;
    // int di, dj;
    // int shls[4];
    // double* buf;
    // i = 0; shls[0] = i; di = CINTcgto_spheric(i, basis.bas);
    // j = 2; shls[1] = j; dj = CINTcgto_spheric(j, basis.bas);
    // buf = malloc(sizeof(double) * di * dj * 3);
    // double ovlp = cint1e_ovlp_sph(buf, shls, basis.atm, basis.natm, basis.bas, basis.nbas, basis.env);
    // printf("Overlap: %lf\n", ovlp);
    // free(buf);

    // free_basis(&basis);
    // free_pyarray(&dm_beta);
    // free_pyarray(&dm_alpha);
    Py_DECREF(Result);
    Py_DECREF(Molecule);
    Py_DECREF(Psi4);
    finalize();
}