final: prev: {
  haskell = prev.haskell // {
    packageOverrides = hfinal: hprev:
      let
        pyEnv = final.qchem.python3.withPackages (p: with p; [
          numpy
          psi4
        ]);

        baseOvrrds = {
          cint = final.libcint;
          python3 = pyEnv;
        };

        turbomoleOvrrds = {
          define = final.qchem.turbomole;
          ridft = final.qchem.turbomole;
          dscf = final.qchem.turbomole;
          rigrad = final.qchem.turbomole;
          egrad = final.qchem.turbomole;
          ricc2 = final.qchem.turbomole;
        };

        orcaOvrrds = { inherit (final.qchem) orca; };

        configPkg = drv: prev.haskell.lib.overrideCabal drv (old: {
          buildTools = with final; [ autoconf automake ];
          preCompileBuildDriver = ''
            autoreconf

            cat > Setup.hs << EOF
            import Distribution.Simple
            main = defaultMainWithHooks autoconfUserHooks
            EOF
          '';
          # The configure script may look for additional libraries, that are not 
          # listed in Cabal yet. Those should be added here
          extraLibraries = (old.extraLibraries or [ ]) ++ [ final.libxcrypt ];
          passthru = {
            python = pyEnv;
          };
        });

        callPkgWithArgs = args: ovrrds: configPkg (hfinal.callCabal2nixWithOptions "spicy-qm-driver" ./. args ovrrds);

      in
      prev.haskell.packageOverrides hfinal hprev // {
        spicy-qm-driver = callPkgWithArgs "" baseOvrrds;
        spicy-qm-driver-orca = callPkgWithArgs "-forca" (baseOvrrds // orcaOvrrds);
        spicy-qm-driver-turbomole = callPkgWithArgs "-fturbomole" (baseOvrrds // turbomoleOvrrds);
        spicy-qm-driver-all = callPkgWithArgs "-forca -fturbomole" (baseOvrrds // orcaOvrrds // turbomoleOvrrds);
      };
  };
}
