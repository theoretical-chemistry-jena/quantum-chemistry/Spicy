{-# OPTIONS_GHC -Wno-unrecognised-pragmas #-}

{-# HLINT ignore "Use camelCase" #-}

{- |
Module      : Spicy.QMDriver
Description : Interfaces to quantum chemistry programmes
Copyright   : Phillip Seeber, Sebastian Seidenath, Wanja Schulze 2024
License     : AGPL-3
Maintainer  : phillip.seeber@uni-jena.de
Stability   : experimental
Portability : POSIX
-}
module Spicy.QMDriver where

import Spicy.Prelude
