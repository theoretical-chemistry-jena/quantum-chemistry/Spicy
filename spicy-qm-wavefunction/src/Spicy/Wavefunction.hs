{-# OPTIONS_GHC -Wno-unrecognised-pragmas #-}

{-# HLINT ignore "Use camelCase" #-}

{- |
Module      : Spicy.Wavefunction
Description : GTO-based wave functions and basis functions
Copyright   : Phillip Seeber, Sebastian Seidenath 2024
License     : AGPL-3
Maintainer  : phillip.seeber@uni-jena.de
Stability   : experimental
Portability : POSIX
-}
module Spicy.Wavefunction where

import Spicy.Prelude
