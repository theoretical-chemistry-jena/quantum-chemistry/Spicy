{ runCommand
, taco
, lib
}:

with lib;

let
  maxOrder = 4; # Maximum derivative order of the energy with respect to the nuclear coordinates

  fragIndices = [ "a" "b" "c" "d" ]; # Indices associated with nuclear derivatives of the fragment
  superIndices = [ "t" "u" "v" "w" ]; # Indices associated with nuclear derivatives of the supersystem

  mkTacoPrompt = n: # the property tensor order
    let # Transform all n indices
      trgtInds = lists.take n superIndices; # Indices of the supersystem P
      orgnInds = lists.take n fragIndices; # Indices of the fragment P
      jacobianPairs = lists.zipLists orgnInds trgtInds; # Pairs of Jacobian indices
      pMode = strings.concatStrings (builtins.genList (_: "d") n); # Sparsity mode of the property matrices -> P is always dense
      piMode = strings.concatStrings (builtins.genList (_: "d") (n)); # Sparsity mode of the property matrices -> P is always dense
      jMode = "ds"; # sparsity mode of the Jacobian -> J is always CSR 
      jacobianChain = strings.concatMapStringsSep " * " (jIxPair: "Ji(${jIxPair.fst},${jIxPair.snd})") jacobianPairs; # The chain of Jacobian transformations

      # Transform last n-1 indices, first ones linearised
      jacobianPairs' = lists.zipLists (lists.drop 1 orgnInds) (lists.drop 1 trgtInds);
      jacobianChain' = strings.concatMapStringsSep " * " (jIxPair: "Ji(${jIxPair.fst},${jIxPair.snd})") jacobianPairs'; # The chain of Jacobian transformations

      # Avoid name clashes
      pref = "${builtins.toString n}_${builtins.toString n}_";
      pref' = "${builtins.toString n}_${builtins.toString (n - 1)}_";
      doubledFunctions = [
        "omp_get_num_threads"
        "omp_get_thread_num"
        "omp_get_max_threads"
        "cmp"
        "taco_gallop"
        "taco_binarySearchAfter"
        "taco_binarySearchBefore"
        "init_taco_tensor_t"
        "deinit_taco_tensor_t"
      ];
      cppLines = s: strings.concatMapStringsSep "\n" (f: "# define ${f} ${s}${pref}${f}") doubledFunctions;
      cppLines' = s: strings.concatMapStringsSep "\n" (f: "# define ${f} ${s}${pref'}${f}") doubledFunctions;
    in

    ''
      # Transform all n indices
      ${taco}/bin/taco \
        "P(${strings.concatStringsSep "," trgtInds}) = Pi(${strings.concatStringsSep "," orgnInds}) * ${jacobianChain}" \
        -f=P:${pMode} -f=Pi:${piMode} -f=Ji:${jMode} \
        -t=P:double -t=Pi:double -t=Ji:double \
        -write-compute=t_${pref}.c \
        -write-source=t_${pref}.c \
        -prefix=t_"${pref}"
    ''
    + strings.optionalString (n == 1) ''
      cp t_${pref}.c $out/.
    ''
    + strings.optionalString (n != 1)
      ''
        cat > header.h << EOF
        ${cppLines "t_"}
        EOF
        cat header.h t_${pref}.c > $out/t_${pref}.c 
    
        # Transform last n-1 indices
        ${taco}/bin/taco \
          "P(${strings.concatStringsSep "," ([(lists.head orgnInds)] ++ lists.drop 1 trgtInds)}) = Pi(${strings.concatStringsSep "," orgnInds}) * ${jacobianChain'}" \
          -f=P:${pMode} -f=Pi:${piMode} -f=Ji:${jMode} \
          -t=P:double -t=Pi:double -t=Ji:double \
          -write-compute=t_${pref'}_.c \
          -write-source=t_${pref'}.c \
          -prefix=t_${pref'}
      
        cat > header.h << EOF
        ${cppLines' "t_"}
        EOF
        cat header.h t_${pref'}.c > $out/t_${pref'}.c 
      '';


in
runCommand "makeSpicyTacos" { } ''
  mkdir $out
  ${strings.concatStrings (builtins.genList (n: mkTacoPrompt (n + 1)) maxOrder)}
''
