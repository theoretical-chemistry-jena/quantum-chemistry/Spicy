{-# OPTIONS_GHC -Wno-incomplete-patterns #-}
{-# OPTIONS_GHC -Wno-unused-matches #-}
{-# OPTIONS_GHC -fno-cse #-}
{-# OPTIONS_GHC -fno-full-laziness #-}

{- |
Module      : Spicy.Math.Internal.JacobianTransformation
Description : Interface to Generated Taco Code
Copyright   : Phillip Seeber, Sebastian Seidenath 2024
License     : AGPL-3
Maintainer  : phillip.seeber@uni-jena.de
Stability   : experimental
Portability : POSIX, Windows

These are the Haskell bindings to the Taco code in @src\/C\/JacobianTransformations@.
The implementation in this module is very memory-unsafe black C magic with lots of pointers and unsafe casts.
However, the exposed Haskell API is quite heavily abstracted and should be safe (provided the underlying implementations have no stupid mistakes).
-}
module Spicy.Math.Internal.JacobianTransformation (
  -- * Exceptions
  TacoException (..),

  -- * Tensor Types
  TacoTensor,
  ModeTypes (..),
  TacoState (..),
  -- COO-Sparse Massiv

  -- * Array Conversion

  -- ** Massiv -> Taco
  mkCsrMat,
  mkDenseArr1,
  mkDenseArr2,
  mkDenseArr3,
  mkDenseArr4,

  -- ** Taco -> Massiv

  -- getCooRes11,
  getDenseRes1,
  getDenseRes22,
  getDenseRes21,
  getDenseRes33,
  getDenseRes32,
  getDenseRes44,
  getDenseRes43,

  -- * Transformations
  -- $transformations
  computeP1T1,
  computeP2T2,
  computeP2T1,
  computeP3T3,
  computeP3T2,
  computeP4T4,
  computeP4T3,
) where

import Data.Coerce (coerce)
import Data.Massiv.Array as Massiv hiding (dimensions)
import Data.Massiv.Array.Unsafe as Massiv
import Foreign
import Foreign.C
import GHC.IO (unsafePerformIO)
import GHC.Stack (callStack, prettyCallStack)
import RIO.Partial (toEnum)
import RIO.Vector.Storable.Unsafe qualified as VS
import Spicy.Prelude hiding (Index, (:>))

-- | Any exception the Taco code could raise
data TacoException
  = UnknownTacoException String
  | TacoIndexException String

instance Show TacoException where
  show (UnknownTacoException msg) =
    "UnknownTacoException: "
      <> msg
      <> "\n"
      <> prettyCallStack callStack
  show (TacoIndexException msg) =
    "TacoIndexException:"
      <> msg
      <> "\n"
      <> prettyCallStack callStack

instance Exception TacoException

data TacoTensorStruct

{- | The taco tensor is an opaque C struct, that we need to pass around, but
 do not handle explicitly.
-}
data TacoTensor (state :: TacoState) (mode :: [ModeTypes]) = TacoTensor
  { tensorFPtr :: ForeignPtr TacoTensorStruct
  , tensorSz :: [Natural]
  }
  deriving (Eq, Show)

{- | Force deallaction of a 'TacoTensor'. Any operation on the tensor will
afterwards be undefined behaviour and result in a crash!
-}
deallocTacoTensor :: (MonadIO m) => TacoTensor state mode -> m ()
deallocTacoTensor TacoTensor{..} = liftIO $ finalizeForeignPtr tensorFPtr

-- | The sparsity of a tensor per dimension
data ModeTypes
  = Dense
  | Sparse
  deriving (Eq, Show, Enum)

instance TypeOf 'Dense ModeTypes where
  toValue = Dense

instance TypeOf 'Sparse ModeTypes where
  toValue = Sparse

instance Storable ModeTypes where
  sizeOf _ = sizeOf (undefined :: CInt)
  alignment _ = alignment (undefined :: CInt)
  peek ptr = do
    mt :: CInt <- peek . castPtr $ ptr
    return . toEnum . fromIntegral $ mt
  poke ptr mt =
    poke (castPtr ptr :: Ptr CInt)
      . (fromIntegral :: Int -> CInt)
      . fromEnum
      $ mt

{- | A 'TacoTensor' might be in a state of actual computed values (similar to #
 'S' 'Array') or delayed (a 'D' 'Array').
-}
data TacoState
  = Delayed
  | Computed

--------------------------------------------------------------------------------

-- | Initialise an opaque C struct holding a taco tensor
foreign import ccall "init_taco_tensor_t"
  init_taco_tensor_t ::
    -- | Rank of the tensor
    Int32 ->
    -- | Size of the tensor elements in bytes
    Int32 ->
    -- | Vector of the size along each dimension
    Ptr Int32 ->
    -- | Mode ordering, should always be [0 .. rank - 1]
    Ptr Int32 ->
    -- | Vector of sparsity modes along each dimension
    Ptr ModeTypes ->
    IO (Ptr TacoTensorStruct)

-- | Deallocate the taco tensor C struct and free its memory
foreign import ccall "&deinit_taco_tensor_t"
  ptr_deinit_taco_tensor_t ::
    FunPtr (Ptr TacoTensorStruct -> IO ())

{- | Obtain an empty taco tensor. The elements used later __must__ be 'Double's
but we can not enforce this here. If you put other elements in, be prepared
for segfaults and nonsense!
-}
initTacoTensor :: (MonadIO m, MonadThrow m) => [(Natural, ModeTypes)] -> m (TacoTensor 'Delayed mode)
initTacoTensor dimSpec
  | null dimSpec = throwM . TacoIndexException $ "Can not construct scalar (0D) tensor"
  | length dimSpec > 4 = throwM . TacoIndexException $ "Can not construct >4D tensor"
  | otherwise = liftIO . unsafeWithPtr dimensions_v $ \dimensions ->
      unsafeWithPtr mode_ordering_v $ \mode_ordering -> unsafeWithPtr mode_types_v $ \mode_types -> do
        tensorPtr <- init_taco_tensor_t order csize dimensions mode_ordering mode_types
        tensorFPtr <- newForeignPtr ptr_deinit_taco_tensor_t tensorPtr
        return $ TacoTensor{..}
 where
  order = fromIntegral . length $ dimSpec
  csize = fromIntegral . sizeOf $ (undefined :: Double)
  dimensions_v = Massiv.fromList Par . fmap (fromIntegral . fst) $ dimSpec
  mode_ordering_v = Massiv.fromList Par [0 .. order - 1]
  mode_types_v = Massiv.fromList Par . fmap snd $ dimSpec
  tensorSz = fmap fst dimSpec

-- | Initialise a CSR-sparse Jacobian from a COO-like representation
foreign import ccall "t_1_1_pack_Ji"
  packCsr ::
    -- | Buffer tensor to fill
    Ptr TacoTensorStruct ->
    -- | A vector of exactly two elements, [0, n], where n is the number of non-zero elements
    Ptr CInt ->
    -- | Row indices of non-zero elements
    Ptr CInt ->
    -- | Column indices of non-zero elements
    Ptr CInt ->
    -- | Vector of non-zero elements
    Ptr Double ->
    -- | Return code
    IO CInt

mkCsrMat ::
  (MonadThrow m) =>
  -- | Size of the target matrix
  Sz2 ->
  -- | Non-zero elements in COO layout
  Vector U (Ix2, Double) ->
  m (TacoTensor 'Delayed ['Dense, 'Sparse])
{-# NOINLINE mkCsrMat #-}
mkCsrMat sz@(Sz (r :. c)) cooDat
  | not . Massiv.all (isSafeIndex sz) . Massiv.map fst $ cooSort =
      throwM
        . TacoIndexException
        $ "At least one COO index is not within the valid range of the Jacobian to be created"
  | otherwise = return . unsafePerformIO $ do
      tensor@TacoTensor{..} <-
        initTacoTensor
          [(fromIntegral r, Dense), (fromIntegral c, Sparse)]

      ec <- withForeignPtr tensorFPtr $ \tensorPtr ->
        unsafeWithPtr coo1_pos_v $ \coo1_pos ->
          unsafeWithPtr coo1_crd_v $ \coo1_crd ->
            unsafeWithPtr coo2_crd_v $ \coo2_crd ->
              unsafeWithPtr coo_vals_v $ \coo_vals ->
                packCsr tensorPtr coo1_pos coo1_crd coo2_crd coo_vals

      ec ~~> tensor
 where
  Sz cooSz = Massiv.size cooDat
  cooSort = quicksort cooDat
  coo1_pos_v = Massiv.fromList Par [0, fromIntegral cooSz]
  coo1_crd_v = compute @S . Massiv.map (\(i1 :. _, _) -> fromIntegral i1) $ cooSort
  coo2_crd_v = compute @S . Massiv.map (\(_ :. i2, _) -> fromIntegral i2) $ cooSort
  coo_vals_v = compute @S . Massiv.map snd $ cooSort

foreign import ccall "t_1_1_pack_Pi"
  packArr1 ::
    -- | Buffer tensor to fill
    Ptr TacoTensorStruct ->
    -- | A vector of exactly two elements, [0, n], where n is the number of non-zero elements
    Ptr CInt ->
    -- | Indices of non-zero elements
    Ptr CInt ->
    -- | Values
    Ptr Double ->
    IO CInt

mkDenseArr1 :: Vector S Double -> TacoTensor 'Delayed '[ 'Dense]
{-# NOINLINE mkDenseArr1 #-}
mkDenseArr1 vec = unsafePerformIO $ do
  tensor@TacoTensor{..} <- initTacoTensor [(fromIntegral n, Dense)]
  ec <- withForeignPtr tensorFPtr $ \tensorPtr ->
    unsafeWithPtr coo1_pos_v $ \coo_pos1 ->
      unsafeWithPtr coo1_crd_v $ \coo1_crd ->
        unsafeWithPtr coo_vals_v $ \coo_vals ->
          packArr1 tensorPtr coo_pos1 coo1_crd coo_vals
  ec ~~> tensor
 where
  Sz n = Massiv.size vec
  coo1_pos_v = Massiv.fromList Par [0, fromIntegral n]
  coo1_crd_v = Massiv.fromList Par [0 .. fromIntegral n - 1]
  coo_vals_v = vec

foreign import ccall "t_2_2_pack_Pi"
  packArr2 ::
    -- | A buffer tensor to fill
    Ptr TacoTensorStruct ->
    -- | A vector of exactly two elements, [0, n], where n is the number of non-zero elements
    Ptr CInt ->
    -- | Dim1 coordinates
    Ptr CInt ->
    -- | Dim2 coordinates
    Ptr CInt ->
    -- | Values
    Ptr Double ->
    IO CInt

mkDenseArr2 :: Matrix S Double -> TacoTensor 'Delayed '[ 'Dense, 'Dense]
{-# NOINLINE mkDenseArr2 #-}
mkDenseArr2 mat = unsafePerformIO $ do
  tensor@TacoTensor{..} <- initTacoTensor [(fromIntegral r, Dense), (fromIntegral c, Dense)]
  ec <- withForeignPtr tensorFPtr $ \tensorPtr ->
    unsafeWithPtr coo1_pos_v $ \coo1_pos ->
      unsafeWithPtr coo1_crd_v $ \coo1_crd ->
        unsafeWithPtr coo2_crd_v $ \coo2_crd ->
          unsafeWithPtr coo_vals_v $ \coo_vals ->
            packArr2 tensorPtr coo1_pos coo1_crd coo2_crd coo_vals
  ec ~~> tensor
 where
  Sz (r :. c) = Massiv.size mat
  coo1_pos_v = Massiv.fromList Par [0, fromIntegral $ r * c]
  ixMat = flatten . Massiv.imap (\(ri :. ci) _ -> (ri, ci)) $ mat
  coo1_crd_v = compute . Massiv.map (fromIntegral . fst) $ ixMat
  coo2_crd_v = compute . Massiv.map (fromIntegral . snd) $ ixMat
  coo_vals_v = flatten mat

foreign import ccall "t_3_3_pack_Pi"
  packArr3 ::
    -- | A buffer tensor to fill
    Ptr TacoTensorStruct ->
    -- | A vector of exactly two elements, [0, n], where n is the number of non-zero elements
    Ptr CInt ->
    -- | Dim1 coordinates
    Ptr CInt ->
    -- | Dim2 coordinates
    Ptr CInt ->
    -- | Dim3 coordinates
    Ptr CInt ->
    -- | Values
    Ptr Double ->
    IO CInt

mkDenseArr3 :: Array S Ix3 Double -> TacoTensor 'Delayed '[ 'Dense, 'Dense, 'Dense]
{-# NOINLINE mkDenseArr3 #-}
mkDenseArr3 arr = unsafePerformIO $ do
  tensor@TacoTensor{..} <- initTacoTensor [(fromIntegral i1, Dense), (fromIntegral i2, Dense), (fromIntegral i3, Dense)]
  ec <- withForeignPtr tensorFPtr $ \tensorPtr ->
    unsafeWithPtr coo1_pos_v $ \coo1_pos ->
      unsafeWithPtr coo1_crd_v $ \coo1_crd ->
        unsafeWithPtr coo2_crd_v $ \coo2_crd ->
          unsafeWithPtr coo3_crd_v $ \coo3_crd ->
            unsafeWithPtr coo_vals_v $ \coo_vals ->
              packArr3 tensorPtr coo1_pos coo1_crd coo2_crd coo3_crd coo_vals
  ec ~~> tensor
 where
  Sz (i1 :> i2 :. i3) = Massiv.size arr
  coo1_pos_v = Massiv.fromList Par [0, fromIntegral $ i1 * i2 * i3]
  ixArr = flatten . Massiv.imap (\(a :> b :. c) _ -> (a, b, c)) $ arr
  coo1_crd_v = compute . Massiv.map (fromIntegral . (^. _1)) $ ixArr
  coo2_crd_v = compute . Massiv.map (fromIntegral . (^. _2)) $ ixArr
  coo3_crd_v = compute . Massiv.map (fromIntegral . (^. _3)) $ ixArr
  coo_vals_v = flatten arr

foreign import ccall "t_4_4_pack_Pi"
  packArr4 ::
    -- | A buffer tensor to fill
    Ptr TacoTensorStruct ->
    -- | A vector of exactly two elements, [0, n], where n is the number of non-zero elements
    Ptr CInt ->
    -- | Dim1 coordinates
    Ptr CInt ->
    -- | Dim2 coordinates
    Ptr CInt ->
    -- | Dim3 coordinates
    Ptr CInt ->
    -- | Dim4 coordinates
    Ptr CInt ->
    -- | Values
    Ptr Double ->
    IO CInt

mkDenseArr4 :: Array S Ix4 Double -> TacoTensor 'Delayed '[ 'Dense, 'Dense, 'Dense, 'Dense]
{-# NOINLINE mkDenseArr4 #-}
mkDenseArr4 arr = unsafePerformIO $ do
  tensor@TacoTensor{..} <- initTacoTensor [(fromIntegral i1, Dense), (fromIntegral i2, Dense), (fromIntegral i3, Dense), (fromIntegral i4, Dense)]
  ec <- withForeignPtr tensorFPtr $ \tensorPtr ->
    unsafeWithPtr coo1_pos_v $ \coo1_pos ->
      unsafeWithPtr coo1_crd_v $ \coo1_crd ->
        unsafeWithPtr coo2_crd_v $ \coo2_crd ->
          unsafeWithPtr coo3_crd_v $ \coo3_crd ->
            unsafeWithPtr coo4_crd_v $ \coo4_crd ->
              unsafeWithPtr coo_vals_v $ \coo_vals ->
                packArr4 tensorPtr coo1_pos coo1_crd coo2_crd coo3_crd coo4_crd coo_vals
  ec ~~> tensor
 where
  Sz (i1 :> i2 :> i3 :. i4) = Massiv.size arr
  coo1_pos_v = Massiv.fromList Par [0, fromIntegral $ i1 * i2 * i3 * i4]
  ixArr = flatten . Massiv.imap (\(a :> b :> c :. d) _ -> (a, b, c, d)) $ arr
  coo1_crd_v = compute . Massiv.map (fromIntegral . (^. _1)) $ ixArr
  coo2_crd_v = compute . Massiv.map (fromIntegral . (^. _2)) $ ixArr
  coo3_crd_v = compute . Massiv.map (fromIntegral . (^. _3)) $ ixArr
  coo4_crd_v = compute . Massiv.map (fromIntegral . (^. _4)) $ ixArr
  coo_vals_v = flatten arr

foreign import ccall "t_1_1_unpack"
  unpackDense1 ::
    -- | Pointer to buffer vector of exactly two elements, [0, n]
    Ptr (Ptr CInt) ->
    -- | Pointer to buffer vector of the Dim1 indices of the elements
    Ptr (Ptr CInt) ->
    -- | Pointer to buffer vector of the values
    Ptr (Ptr Double) ->
    -- | Pointer to the calculated TacoTensor to unwrap
    Ptr TacoTensorStruct ->
    IO CInt

getDenseRes1 :: TacoTensor 'Computed '[ 'Dense] -> Vector S Double
{-# NOINLINE getDenseRes1 #-}
getDenseRes1 TacoTensor{tensorFPtr, tensorSz = [n]} = unsafePerformIO $ do
  (ec, res) <- withForeignPtr tensorFPtr $ \tensorPtr ->
    alloca $ \coo1_pos_ptr ->
      alloca $ \coo1_crd_ptr ->
        alloca $ \coo_vals_ptr -> do
          ec <- unpackDense1 coo1_pos_ptr coo1_crd_ptr coo_vals_ptr tensorPtr
          peek coo1_pos_ptr >>= free
          peek coo1_crd_ptr >>= free
          coo_vals_v <- peek coo_vals_ptr >>= newForeignPtr finalizerFree
          let dat = Massiv.fromStorableVector Par . VS.unsafeFromForeignPtr0 coo_vals_v . fromIntegral $ n
              res = compute dat
          return (ec, res)
  ec ~~> res

-- | Function signature for unpack functions of 2D arrays
type Unpack2F =
  -- | Pointer to buffer vector of exactly two elements, [0, n]
  Ptr (Ptr CInt) ->
  -- | Pointer to buffer vector of the Dim1 indices of the elements
  Ptr (Ptr CInt) ->
  -- | Pointer to buffer vector of the Dim2 indices of the elements
  Ptr (Ptr CInt) ->
  -- | Pointer to buffer vector of the values
  Ptr (Ptr Double) ->
  -- | Pointer to the calculated TacoTensor to unwrap
  Ptr TacoTensorStruct ->
  IO CInt

getDenseRes2Common :: Unpack2F -> TacoTensor 'Computed '[ 'Dense, 'Dense] -> Matrix S Double
{-# NOINLINE getDenseRes2Common #-}
getDenseRes2Common unpckF TacoTensor{tensorFPtr, tensorSz = [r, c]} = unsafePerformIO $ do
  (ec, res) <- withForeignPtr tensorFPtr $ \tensorPtr ->
    alloca $ \coo1_pos_ptr ->
      alloca $ \coo1_crd_ptr ->
        alloca $ \coo2_crd_ptr ->
          alloca $ \coo_vals_ptr -> do
            ec <- unpckF coo1_pos_ptr coo1_crd_ptr coo2_crd_ptr coo_vals_ptr tensorPtr
            coo_pos_v <- peek coo1_pos_ptr >>= newForeignPtr finalizerFree
            coo1_crd_v <- peek coo1_crd_ptr >>= newForeignPtr finalizerFree
            coo2_crd_v <- peek coo2_crd_ptr >>= newForeignPtr finalizerFree
            coo_vals_v <- peek coo_vals_ptr >>= newForeignPtr finalizerFree
            let nCooV = unsafeArrayFromForeignPtr0 Par coo_pos_v $ 2
                nCoo = fromIntegral $ nCooV Massiv.! 1
                cooVals = Massiv.unsafeArrayFromForeignPtr0 Par coo_vals_v $ nCoo
                rIx = Massiv.unsafeArrayFromForeignPtr0 Par coo1_crd_v $ nCoo
                cIx = Massiv.unsafeArrayFromForeignPtr0 Par coo2_crd_v $ nCoo
                cooDat = Massiv.zipWith3 (\iR iC e -> (fromIntegral iR :. fromIntegral iC, e)) rIx cIx cooVals
            res <- coo2Dense (Sz $ fromIntegral r :. fromIntegral c) cooDat
            return (ec, res)
  ec ~~> res

foreign import ccall "t_2_2_unpack" t_2_2_unpack :: Unpack2F

getDenseRes22 :: TacoTensor 'Computed '[ 'Dense, 'Dense] -> Matrix S Double
getDenseRes22 = getDenseRes2Common t_2_2_unpack

foreign import ccall "t_2_1_unpack" t_2_1_unpack :: Unpack2F

getDenseRes21 :: TacoTensor 'Computed '[ 'Dense, 'Dense] -> Matrix S Double
getDenseRes21 = getDenseRes2Common t_2_1_unpack

type Unpack3F =
  -- | Pointer to buffer vector of exactly two elements, [0, n]
  Ptr (Ptr CInt) ->
  -- | Pointer to buffer vector of the Dim1 indices of the elements
  Ptr (Ptr CInt) ->
  -- | Pointer to buffer vector of the Dim2 indices of the elements
  Ptr (Ptr CInt) ->
  -- | Pointer to buffer vector of the Dim3 indices of the elements
  Ptr (Ptr CInt) ->
  -- | Pointer to buffer vector of the values
  Ptr (Ptr Double) ->
  -- | Pointer to the calculated TacoTensor to unwrap
  Ptr TacoTensorStruct ->
  IO CInt

getDenseRes3Common :: Unpack3F -> TacoTensor 'Computed '[ 'Dense, 'Dense, 'Dense] -> Array S Ix3 Double
{-# NOINLINE getDenseRes3Common #-}
getDenseRes3Common unpckF TacoTensor{tensorFPtr, tensorSz = [s1, s2, s3]} = unsafePerformIO $ do
  (ec, res) <- withForeignPtr tensorFPtr $ \tensorPtr ->
    alloca $ \coo1_pos_ptr ->
      alloca $ \coo1_crd_ptr ->
        alloca $ \coo2_crd_ptr ->
          alloca $ \coo3_crd_ptr ->
            alloca $ \coo_vals_ptr -> do
              ec <- unpckF coo1_pos_ptr coo1_crd_ptr coo2_crd_ptr coo3_crd_ptr coo_vals_ptr tensorPtr
              coo1_pos_v <- peek coo1_pos_ptr >>= newForeignPtr finalizerFree
              coo1_crd_v <- peek coo1_crd_ptr >>= newForeignPtr finalizerFree
              coo2_crd_v <- peek coo2_crd_ptr >>= newForeignPtr finalizerFree
              coo3_crd_v <- peek coo3_crd_ptr >>= newForeignPtr finalizerFree
              coo_vals_v <- peek coo_vals_ptr >>= newForeignPtr finalizerFree
              let nCooV = Massiv.unsafeArrayFromForeignPtr0 Par coo1_pos_v $ 2
                  nCoo = fromIntegral $ nCooV Massiv.! 1
                  cooVals = Massiv.unsafeArrayFromForeignPtr0 Par coo_vals_v $ nCoo
                  ix1 = Massiv.unsafeArrayFromForeignPtr0 Par coo1_crd_v $ nCoo
                  ix2 = Massiv.unsafeArrayFromForeignPtr0 Par coo2_crd_v $ nCoo
                  ix3 = Massiv.unsafeArrayFromForeignPtr0 Par coo3_crd_v $ nCoo
                  cooDat = Massiv.zipWith4 (\a b c e -> (fromIntegral a :> fromIntegral b :. fromIntegral c, e)) ix1 ix2 ix3 cooVals
              res <- coo2Dense (Sz $ fromIntegral s1 :> fromIntegral s2 :. fromIntegral s3) cooDat
              return (ec, res)
  ec ~~> res

foreign import ccall "t_3_3_unpack" t_3_3_unpack :: Unpack3F

getDenseRes33 :: TacoTensor 'Computed '[ 'Dense, 'Dense, 'Dense] -> Array S Ix3 Double
getDenseRes33 = getDenseRes3Common t_3_3_unpack

foreign import ccall "t_3_2_unpack" t_3_2_unpack :: Unpack3F

getDenseRes32 :: TacoTensor 'Computed '[ 'Dense, 'Dense, 'Dense] -> Array S Ix3 Double
{-# NOINLINE getDenseRes32 #-}
getDenseRes32 = getDenseRes3Common t_3_2_unpack

type Unpack4F =
  -- | Pointer to buffer vector of exactly two elements, [0, n]
  Ptr (Ptr CInt) ->
  -- | Pointer to buffer vector of the Dim1 indices of the elements
  Ptr (Ptr CInt) ->
  -- | Pointer to buffer vector of the Dim2 indices of the elements
  Ptr (Ptr CInt) ->
  -- | Pointer to buffer vector of the Dim3 indices of the elements
  Ptr (Ptr CInt) ->
  -- | Pointer to buffer vector of the Dim4 indices of the elements
  Ptr (Ptr CInt) ->
  -- | Pointer to buffer vector of the values
  Ptr (Ptr Double) ->
  -- | Pointer to the calculated TacoTensor to unwrap
  Ptr TacoTensorStruct ->
  IO CInt

getDenseRes4Common :: Unpack4F -> TacoTensor 'Computed '[ 'Dense, 'Dense, 'Dense, 'Dense] -> Array S Ix4 Double
{-# NOINLINE getDenseRes4Common #-}
getDenseRes4Common unpckF TacoTensor{tensorFPtr, tensorSz = [s1, s2, s3, s4]} = unsafePerformIO $ do
  (ec, res) <- withForeignPtr tensorFPtr $ \tensorPtr ->
    alloca $ \coo1_pos_ptr ->
      alloca $ \coo1_crd_ptr ->
        alloca $ \coo2_crd_ptr ->
          alloca $ \coo3_crd_ptr ->
            alloca $ \coo4_crd_ptr ->
              alloca $ \coo_vals_ptr -> do
                ec <- unpckF coo1_pos_ptr coo1_crd_ptr coo2_crd_ptr coo3_crd_ptr coo4_crd_ptr coo_vals_ptr tensorPtr
                coo1_pos_v <- peek coo1_pos_ptr >>= newForeignPtr finalizerFree
                coo1_crd_v <- peek coo1_crd_ptr >>= newForeignPtr finalizerFree
                coo2_crd_v <- peek coo2_crd_ptr >>= newForeignPtr finalizerFree
                coo3_crd_v <- peek coo3_crd_ptr >>= newForeignPtr finalizerFree
                coo4_crd_v <- peek coo4_crd_ptr >>= newForeignPtr finalizerFree
                coo_vals_v <- peek coo_vals_ptr >>= newForeignPtr finalizerFree
                let nCooV = unsafeArrayFromForeignPtr0 Par coo1_pos_v $ 2
                    nCoo = fromIntegral $ nCooV Massiv.! 1
                    cooVals = unsafeArrayFromForeignPtr0 Par coo_vals_v $ nCoo
                    ix1 = unsafeArrayFromForeignPtr0 Par coo1_crd_v $ nCoo
                    ix2 = unsafeArrayFromForeignPtr0 Par coo2_crd_v $ nCoo
                    ix3 = unsafeArrayFromForeignPtr0 Par coo3_crd_v $ nCoo
                    ix4 = unsafeArrayFromForeignPtr0 Par coo4_crd_v $ nCoo
                    ixes = Massiv.zipWith4 (\a b c d -> fromIntegral a :> fromIntegral b :> fromIntegral c :. fromIntegral d) ix1 ix2 ix3 ix4
                    cooDat = Massiv.zip ixes cooVals
                res <- coo2Dense (Sz $ fromIntegral s1 :> fromIntegral s2 :> fromIntegral s3 :. fromIntegral s4) cooDat
                return (ec, res)
  ec ~~> res

foreign import ccall "t_4_4_unpack" t_4_4_unpack :: Unpack4F

getDenseRes44 :: TacoTensor 'Computed '[ 'Dense, 'Dense, 'Dense, 'Dense] -> Array S Ix4 Double
{-# NOINLINE getDenseRes44 #-}
getDenseRes44 = getDenseRes4Common t_4_4_unpack

foreign import ccall "t_4_3_unpack" t_4_3_unpack :: Unpack4F

getDenseRes43 :: TacoTensor 'Computed '[ 'Dense, 'Dense, 'Dense, 'Dense] -> Array S Ix4 Double
{-# NOINLINE getDenseRes43 #-}
getDenseRes43 = getDenseRes4Common t_4_3_unpack

--------------------------------------------------------------------------------

{- $transformations

Jacobian transformations to change the basis in equations like

\[
    P_{\dots u} = \sum\limits_r J_{i, r u} P_{l_i, \dots r}
\]

\[
    P_{\dots u v} = \sum\limits_{r, s} J_{i, r u} J_{i, s v} P_{i, \dots r s}
\]

\[
P_{\dots u v w} = \sum\limits_{r,s, t} J_{i, r u} J_{i, s v} J_{i, t w} P_{i, \dots r s t}
\]

The @PxTy@ nomenclature describes a transformation of the last @y@ indices in a tensor of rank @x@.
Transformations where @y < x - 1@ can be achieved by linearising the first indices before the computation.
-}

type EvaluateF =
  -- | P buffer
  Ptr TacoTensorStruct ->
  -- | Pi
  Ptr TacoTensorStruct ->
  -- | Ji
  Ptr TacoTensorStruct ->
  IO CInt

foreign import ccall "t_1_1_evaluate" t11Compute :: EvaluateF

computeP1T1 :: (MonadThrow m) => TacoTensor s1 '[ 'Dense, 'Sparse] -> TacoTensor s2 '[ 'Dense] -> m (TacoTensor 'Computed '[ 'Dense])
{-# NOINLINE computeP1T1 #-}
computeP1T1
  TacoTensor{tensorSz = [jiR, jiC], tensorFPtr = jiFPtr}
  TacoTensor{tensorSz = [piR], tensorFPtr = piFPtr}
    | jiR /= piR = throwM $ SizeElementsMismatchException jiSz piSz
    | otherwise = return . unsafePerformIO $ do
        p@TacoTensor{tensorFPtr = pFPtr} <- initTacoTensor [(jiC, Dense)]
        ec <- withForeignPtr pFPtr $ \pPtr ->
          withForeignPtr piFPtr $ \piPtr ->
            withForeignPtr jiFPtr $ \jiPtr ->
              t11Compute pPtr piPtr jiPtr
        ec ~~> coerce p
   where
    jiSz = Sz $ fromIntegral jiR :. fromIntegral jiC
    piSz = Sz1 $ fromIntegral piR

foreign import ccall "t_2_2_evaluate" t_2_2_evaluate :: EvaluateF

computeP2T2 :: (MonadThrow m) => TacoTensor s1 '[ 'Dense, 'Sparse] -> TacoTensor s2 '[ 'Dense, 'Dense] -> m (TacoTensor 'Computed '[ 'Dense, 'Dense])
{-# NOINLINE computeP2T2 #-}
computeP2T2
  TacoTensor{tensorSz = [jiR, jiC], tensorFPtr = jiFPtr}
  TacoTensor{tensorSz = [piR, piC], tensorFPtr = piFPtr}
    | jiR /= piR = throwM $ SizeElementsMismatchException jiSz piSz
    | jiR /= piC = throwM $ SizeElementsMismatchException jiSz piSz
    | otherwise = return . unsafePerformIO $ do
        p@TacoTensor{tensorFPtr = pFPtr} <- initTacoTensor [(jiC, Dense), (jiC, Dense)]
        ec <- withForeignPtr pFPtr $ \pPtr ->
          withForeignPtr piFPtr $ \piPtr ->
            withForeignPtr jiFPtr $ \jiPtr ->
              t_2_2_evaluate pPtr piPtr jiPtr
        ec ~~> coerce p
   where
    jiSz = Sz $ fromIntegral jiR :. fromIntegral jiC
    piSz = Sz $ fromIntegral piR :. fromIntegral piC

foreign import ccall "t_2_1_evaluate" t_2_1_evaluate :: EvaluateF

computeP2T1 :: (MonadThrow m) => TacoTensor s1 '[ 'Dense, 'Sparse] -> TacoTensor s2 '[ 'Dense, 'Dense] -> m (TacoTensor 'Computed '[ 'Dense, 'Dense])
{-# NOINLINE computeP2T1 #-}
computeP2T1
  TacoTensor{tensorSz = [jiR, jiC], tensorFPtr = jiFPtr}
  TacoTensor{tensorSz = [piR, piC], tensorFPtr = piFPtr}
    | jiR /= piC = throwM $ SizeElementsMismatchException jiSz piSz
    | otherwise = return . unsafePerformIO $ do
        p@TacoTensor{tensorFPtr = pFPtr} <- initTacoTensor [(piR, Dense), (jiC, Dense)]
        ec <- withForeignPtr pFPtr $ \pPtr ->
          withForeignPtr piFPtr $ \piPtr ->
            withForeignPtr jiFPtr $ \jiPtr ->
              t_2_1_evaluate pPtr piPtr jiPtr
        ec ~~> coerce p
   where
    jiSz = Sz $ fromIntegral jiR :. fromIntegral jiC
    piSz = Sz $ fromIntegral piR :. fromIntegral piC

foreign import ccall "t_3_3_evaluate" t_3_3_evaluate :: EvaluateF

computeP3T3 :: (MonadThrow m) => TacoTensor s1 '[ 'Dense, 'Sparse] -> TacoTensor s2 '[ 'Dense, 'Dense, 'Dense] -> m (TacoTensor 'Computed '[ 'Dense, 'Dense, 'Dense])
{-# NOINLINE computeP3T3 #-}
computeP3T3
  TacoTensor{tensorSz = [jiR, jiC], tensorFPtr = jiFPtr}
  TacoTensor{tensorSz = [pi1, pi2, pi3], tensorFPtr = piFPtr}
    | jiR /= pi1 = throwM $ SizeElementsMismatchException jiSz piSz
    | otherwise = return . unsafePerformIO $ do
        p@TacoTensor{tensorFPtr = pFPtr} <- initTacoTensor [(jiC, Dense), (jiC, Dense), (jiC, Dense)]
        ec <- withForeignPtr pFPtr $ \pPtr ->
          withForeignPtr jiFPtr $ \jiPtr ->
            withForeignPtr piFPtr $ \piPtr ->
              t_3_3_evaluate pPtr piPtr jiPtr
        ec ~~> coerce p
   where
    jiSz = Sz $ fromIntegral jiR :. fromIntegral jiC
    piSz = Sz $ fromIntegral pi1 :> fromIntegral pi2 :. fromIntegral pi3

foreign import ccall "t_3_2_evaluate" t_3_2_evaluate :: EvaluateF

computeP3T2 :: (MonadThrow m) => TacoTensor s1 '[ 'Dense, 'Sparse] -> TacoTensor s2 '[ 'Dense, 'Dense, 'Dense] -> m (TacoTensor 'Computed '[ 'Dense, 'Dense, 'Dense])
{-# NOINLINE computeP3T2 #-}
computeP3T2
  TacoTensor{tensorSz = [jiR, jiC], tensorFPtr = jiFPtr}
  TacoTensor{tensorSz = [pi1, pi2, pi3], tensorFPtr = piFPtr}
    | jiR /= pi2 = throwM $ SizeElementsMismatchException jiSz piSz
    | jiR /= pi3 = throwM $ SizeElementsMismatchException jiSz piSz
    | otherwise = return . unsafePerformIO $ do
        p@TacoTensor{tensorFPtr = pFPtr} <- initTacoTensor [(pi1, Dense), (jiC, Dense), (jiC, Dense)]
        ec <- withForeignPtr pFPtr $ \pPtr ->
          withForeignPtr jiFPtr $ \jiPtr ->
            withForeignPtr piFPtr $ \piPtr ->
              t_3_2_evaluate pPtr piPtr jiPtr
        ec ~~> coerce p
   where
    jiSz = Sz $ fromIntegral jiR :. fromIntegral jiC
    piSz = Sz $ fromIntegral pi1 :> fromIntegral pi2 :. fromIntegral pi3

foreign import ccall "t_4_4_evaluate" t_4_4_evaluate :: EvaluateF

computeP4T4 :: (MonadThrow m) => TacoTensor s1 '[ 'Dense, 'Sparse] -> TacoTensor s2 '[ 'Dense, 'Dense, 'Dense, 'Dense] -> m (TacoTensor 'Computed '[ 'Dense, 'Dense, 'Dense, 'Dense])
{-# NOINLINE computeP4T4 #-}
computeP4T4
  TacoTensor{tensorSz = [jiR, jiC], tensorFPtr = jiFPtr}
  TacoTensor{tensorSz = [pi1, pi2, pi3, pi4], tensorFPtr = piFPtr}
    | jiR /= pi1 = throwM $ SizeElementsMismatchException jiSz piSz
    | jiR /= pi2 = throwM $ SizeElementsMismatchException jiSz piSz
    | jiR /= pi3 = throwM $ SizeElementsMismatchException jiSz piSz
    | jiR /= pi4 = throwM $ SizeElementsMismatchException jiSz piSz
    | otherwise = return . unsafePerformIO $ do
        p@TacoTensor{tensorFPtr = pFPtr} <- initTacoTensor [(jiC, Dense), (jiC, Dense), (jiC, Dense), (jiC, Dense)]
        ec <- withForeignPtr pFPtr $ \pPtr ->
          withForeignPtr piFPtr $ \piPtr ->
            withForeignPtr jiFPtr $ \jiPtr ->
              t_4_4_evaluate pPtr piPtr jiPtr
        ec ~~> coerce p
   where
    jiSz = Sz $ fromIntegral jiR :. fromIntegral jiC
    piSz = Sz $ fromIntegral pi1 :> fromIntegral pi2 :> fromIntegral pi3 :. fromIntegral pi4

foreign import ccall "t_4_3_evaluate" t43Compute :: EvaluateF

computeP4T3 :: (MonadThrow m) => TacoTensor s1 '[ 'Dense, 'Sparse] -> TacoTensor s2 '[ 'Dense, 'Dense, 'Dense, 'Dense] -> m (TacoTensor 'Computed '[ 'Dense, 'Dense, 'Dense, 'Dense])
{-# NOINLINE computeP4T3 #-}
computeP4T3
  TacoTensor{tensorSz = [jiR, jiC], tensorFPtr = jiFPtr}
  TacoTensor{tensorSz = [pi1, pi2, pi3, pi4], tensorFPtr = piFPtr}
    | jiR /= pi2 = throwM $ SizeElementsMismatchException jiSz piSz
    | jiR /= pi3 = throwM $ SizeElementsMismatchException jiSz piSz
    | jiR /= pi4 = throwM $ SizeElementsMismatchException jiSz piSz
    | otherwise = return . unsafePerformIO $ do
        p@TacoTensor{tensorFPtr = pFPtr} <- initTacoTensor [(pi1, Dense), (jiC, Dense), (jiC, Dense), (jiC, Dense)]
        ec <- withForeignPtr pFPtr $ \pPtr ->
          withForeignPtr piFPtr $ \piPtr ->
            withForeignPtr jiFPtr $ \jiPtr ->
              t43Compute pPtr piPtr jiPtr
        ec ~~> coerce p
   where
    jiSz = Sz $ fromIntegral jiR :. fromIntegral jiC
    piSz = Sz $ fromIntegral pi1 :> fromIntegral pi2 :> fromIntegral pi3 :. fromIntegral pi4

--------------------------------------------------------------------------------

-- | Helper function to check C return codes
(~~>) :: (MonadThrow m) => CInt -> a -> m a
exitCode ~~> a = case exitCode of
  0 -> return a
  _ -> throwM . UnknownTacoException $ "Exit code was: " <> show exitCode

-- | Convert COO data to a dense array
coo2Dense ::
  forall ix e r.
  ( Storable e
  , Load S ix e
  , Num e
  , Load r Ix1 (ix, e)
  ) =>
  Sz ix ->
  Vector r (ix, e) ->
  IO (Array S ix e)
coo2Dense sz cooDat = do
  zeroArr <- unsafeThaw . Massiv.replicate @S Par sz $ 0
  Massiv.forIO_ cooDat $ \(i, e) -> write_ zeroArr i e
  unsafeFreeze Par zeroArr
