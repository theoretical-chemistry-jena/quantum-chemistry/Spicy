{- |
Module      : Spicy.Math.Math.Taco
Description : Jacobian Transformation of Property Tensors
Copyright   : Phillip Seeber, Sebastian Seidenath 2024
License     : AGPL-3
Maintainer  : phillip.seeber@uni-jena.de
Stability   : experimental
Portability : POSIX, Windows

This module provides methods to transform property tensors from a subsystem to the supersystem by means of a 'Jacobian'.
The 'Jacobian' itself is an opaque representation of a CSR matrix, constructed from non-zero values in COO-format.
The 'JacobianTransformation' class implements transformations of type

\[
P_{\dots u} = \sum\limits_r J_{i, r u} P_{l_i, \dots r}
\]

\[
P_{\dots u v} = \sum\limits_{r, s} J_{i, r u} J_{i, s v} P_{i, \dots r s}
\]

\[
P_{\dots u v w} = \sum\limits_{r, s, t} J_{i, r u} J_{i, s v} J_{i, t w} P_{i, \dots r s t}
\]

etc, with the sparse Jacobian \(\boldsymbol{J}\) and the dense property tensors \(\boldsymbol{P}\).
The transformation applies always to the last \(n\) indices of the property tensor, which must consequently be constructed to be suitable for this transformation.
-}
module Spicy.Math.JacobianTransformation (
  -- * Exceptions
  JacobianTransformationException (..),

  -- * (Sparse) Jacobian Matrix
  Jacobian,
  mkJacobian,

  -- * Jacobian Transformations
  JacobianTransformation (..),
) where

import Data.Massiv.Array as Massiv
import GHC.Stack
import Spicy.Math.Internal.JacobianTransformation
import Spicy.Prelude hiding (Index, (:>))

-- | Exceptions specific to Jacobian transformations
newtype JacobianTransformationException
  = -- | Transformation of a given number of indices is not possible
    JacobianTransformationDimensionException String

instance Show JacobianTransformationException where
  show (JacobianTransformationDimensionException msg) =
    "JacobianTransformationDimensionException: "
      <> msg
      <> "\n"
      <> prettyCallStack callStack

instance Exception JacobianTransformationException

-- | An opaque representation of a sparse Jacobian
data Jacobian = Jacobian
  { jacobianMat :: TacoTensor 'Delayed '[ 'Dense, 'Sparse]
  , jacobianSize :: Sz2
  }

-- | Safe constructor for a sparse 'Jacobian' from COO-values
mkJacobian ::
  (MonadThrow m) =>
  -- | Final size of the 'Jacobian'
  Sz2 ->
  -- | Non-zero values of the 'Jacobian' in COO format
  Vector U (Ix2, Double) ->
  m Jacobian
mkJacobian sz cooData = Jacobian <$> mkCsrMat sz cooData <*> pure sz

class JacobianTransformation prop where
  jacobianTrans ::
    (MonadThrow m) =>
    Jacobian ->
    -- | Last \(n\) indices to transform with the given Jacobian
    Natural ->
    prop ->
    m prop

instance JacobianTransformation (Vector S Double) where
  jacobianTrans (Jacobian{jacobianMat = jI}) nInds prop = case nInds of
    0 -> return prop
    1 -> fmap getDenseRes1 . computeP1T1 jI . mkDenseArr1 $ prop
    _ -> throwM . JacobianTransformationDimensionException $ "can not transform " <> show nInds <> " dimensions in a rank 1 tensor"

instance JacobianTransformation (Matrix S Double) where
  jacobianTrans (Jacobian{jacobianMat = jI}) nInds prop = case nInds of
    0 -> return prop
    1 -> fmap getDenseRes21 . computeP2T1 jI . mkDenseArr2 $ prop
    2 -> fmap getDenseRes22 . computeP2T2 jI . mkDenseArr2 $ prop
    _ -> throwM . JacobianTransformationDimensionException $ "can not transform " <> show nInds <> " dimensions in a rank 2 tensor"

instance JacobianTransformation (Array S Ix3 Double) where
  jacobianTrans (Jacobian{jacobianMat = jI, jacobianSize = Sz (_rJ :. cJ)}) nInds prop = case nInds of
    0 -> return prop
    1 -> fmap (resize' (Sz $ pA :> pB :. cJ) . getDenseRes21) . computeP2T1 jI . mkDenseArr2 . resize' (Sz $ pA * pB :. pC) $ prop
    2 -> fmap getDenseRes32 . computeP3T2 jI . mkDenseArr3 $ prop
    3 -> fmap getDenseRes33 . computeP3T3 jI . mkDenseArr3 $ prop
    _ -> throwM . JacobianTransformationDimensionException $ "can not transform " <> show nInds <> " dimensions in a rank 3 tensor"
   where
    Sz (pA :> pB :. pC) = size prop

instance JacobianTransformation (Array S Ix4 Double) where
  jacobianTrans (Jacobian{jacobianMat = jI, jacobianSize = Sz (_rJ :. cJ)}) nD prop = case nD of
    0 -> return prop
    1 -> fmap (resize' (Sz $ pA :> pB :> pC :. cJ) . getDenseRes21) . computeP2T1 jI . mkDenseArr2 . resize' (Sz $ pA * pB * pC :. pD) $ prop
    2 -> fmap (resize' (Sz $ pA :> pB :> cJ :. cJ) . getDenseRes32) . computeP3T2 jI . mkDenseArr3 . resize' (Sz $ pA * pB :> pC :. pD) $ prop
    3 -> fmap getDenseRes43 . computeP4T3 jI . mkDenseArr4 $ prop
    4 -> fmap getDenseRes44 . computeP4T4 jI . mkDenseArr4 $ prop
    _ -> throwM . JacobianTransformationDimensionException $ "can not transform " <> show nD <> " dimensions in a rank 4 tensor"
   where
    Sz (pA :> pB :> pC :. pD) = size prop
