{- |
Module      : Spicy.ResponseProperties
Description : Extensive Response Properties and their Transformation
Copyright   : Phillip Seeber, Sebastian Seidenath 2024
License     : AGPL-3
Maintainer  : phillip.seeber@uni-jena.de
Stability   : experimental
Portability : POSIX, Windows

A variety of molecular properties \(P\) can be obtained from response theorey, i.e. partial derivatives of the energy

\[
P(A'_i, M_j) \propto \frac{\partial^{n_f + n_b +  n_i + n_r}E (A'_i, M_j)}{\partial \boldsymbol{f}^{n_f} \partial \boldsymbol{b}^{n_b} \partial \boldsymbol{i}^{n_i} \partial \boldsymbol{r}^{n_r}} \, .
\]

Here, we use \(A'_i\) to refer to a link-atom augmented set of atoms in some fragment $f_i$ (see also 'Spicy.Chemistry.Molecule' and 'Spicy.FragmentMethods.Fragment').
In case a derivative with respect to nuclear coordinates is performed, the size of the result tensor depends on the number of atoms, which is different from that in the supersystem.
This change of basis from atoms of the fragment to atoms of the supersystem is encoded via a Jacobi transformation, see 'Spicy.ResponseProperties.Common'.
-}
module Spicy.ResponseProperties (
  module Spicy.ResponseProperties.Common,
  module Spicy.ResponseProperties.CircularDichroism,
  module Spicy.ResponseProperties.Dipole,
  module Spicy.ResponseProperties.Gradient,
  module Spicy.ResponseProperties.Hessian,
  module Spicy.ResponseProperties.Infrared,
  module Spicy.ResponseProperties.MagneticDipole,
  module Spicy.ResponseProperties.Magnetisability,
  module Spicy.ResponseProperties.Polarisability,
  module Spicy.ResponseProperties.Raman,
) where

import Spicy.ResponseProperties.CircularDichroism
import Spicy.ResponseProperties.Common
import Spicy.ResponseProperties.Dipole
import Spicy.ResponseProperties.Gradient
import Spicy.ResponseProperties.Hessian
import Spicy.ResponseProperties.Infrared
import Spicy.ResponseProperties.MagneticDipole
import Spicy.ResponseProperties.Magnetisability
import Spicy.ResponseProperties.Polarisability
import Spicy.ResponseProperties.Raman
