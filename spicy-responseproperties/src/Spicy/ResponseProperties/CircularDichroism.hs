{-# OPTIONS_GHC -Wno-orphans #-}

{- |
Module      : Spicy.ResponseProperties.CircularDichroism
Copyright   : Phillip Seeber, Sebastian Seidenath 2024
License     : AGPL-3
Maintainer  : phillip.seeber@uni-jena.de
Stability   : experimental
Portability : POSIX, Windows
-}
module Spicy.ResponseProperties.CircularDichroism where

import Data.Aeson
import Data.Massiv.Array as Massiv hiding (B)
import Spicy.Prelude
import Spicy.ResponseProperties.Common

{- | Circular Dichroism/Optical Rotation tensor

\[
    \frac{\partial^2 E}{\partial \boldsymbol{f}^1 \boldsymbol{b}^1}
\]
-}
newtype instance ResponseTensor '[ 'F, 'B] = CircularDichroismRT (Matrix S Double)
  deriving (Show, Generic)

type CircularDichroism = ResponseTensor '[ 'F, 'B]

instance AccumulateResponseTensor CircularDichroism where
  accRespTensorF (CircularDichroismRT acc) (CircularDichroismRT nextVal) =
    CircularDichroismRT <$> acc .+. nextVal

instance Binary CircularDichroism
instance ToJSON CircularDichroism
instance FromJSON CircularDichroism
