{- |
Module      : Spicy.ResponseProperties.Common
Description : Common Types and Classes for Response Properties
Copyright   : Phillip Seeber, Sebastian Seidenath 2024
License     : AGPL-3
Maintainer  : phillip.seeber@uni-jena.de
Stability   : experimental
Portability : POSIX, Windows
-}
module Spicy.ResponseProperties.Common (
  -- * Exceptions
  ResponseTensorException (..),

  -- * Jacobian
  -- $jacobian
  getJacobian,
  getJacobian',

  -- * General Response Tensors
  Derivative (..),
  ResponseTensor,
  NucRefTransformation (..),
  AccumulateResponseTensor (..),
  accRespTensor,
) where

import Data.Massiv.Array as Massiv hiding (B)
import GHC.Stack
import RIO.List.Partial qualified as List
import RIO.Partial (fromJust)
import Spicy.Chemistry.Molecule
import Spicy.FragmentMethods.Fragment
import Spicy.Graph
import Spicy.IntMap qualified as IntMap
import Spicy.Massiv
import Spicy.Math.JacobianTransformation
import Spicy.Math.LinearAlgebra.R3 as R3
import Spicy.Prelude hiding (Index)

data ResponseTensorException where
  -- | Response tensor has the wrong size to represent the property
  RespTensorSizeException :: (Index ix) => (Sz ix) -> String -> ResponseTensorException
  -- | Two instances of the same property with respect to an origin do not
  -- have the same origin, and thus can not be combined
  RespTensorReferenceException :: (Show a) => a -> a -> ResponseTensorException

instance Show ResponseTensorException where
  show (RespTensorSizeException sz msg) =
    "RespTensorSizeException: data has wrong size. Got "
      <> show sz
      <> "\n"
      <> msg
      <> "\n"
      <> prettyCallStack callStack
  show (RespTensorReferenceException ref val) =
    "RespTensorReferenceException: can not combine response tensors of different origin.\
    \ Reference origin was: "
      <> show ref
      <> " and actual values is: "
      <> show val
      <> "\n"
      <> prettyCallStack callStack

instance Exception ResponseTensorException

--------------------------------------------------------------------------------

{- $jacobian
A Jacobian matrix is used to transform properties back from a 'Fragment' into the associated supersystem.
The elements of the Jacobian are given as:

\[
\boldsymbol{J}_i = \begin{pmatrix}
  \boldsymbol{J}_{i, 11} & \dots  & \boldsymbol{J}_{i, 1 b} \\
  \vdots              & \ddots & \vdots               \\
  \boldsymbol{J}_{i, a1} & \dots  & \boldsymbol{J}_{i, ab}
\end{pmatrix}
\]

with

\[
\boldsymbol{J}_{i, o p} = \boldsymbol{I}_3 \cdot \begin{cases}
  g (m_o, n_p)     & m_o \in A_i^\mathrm{l} \land n_p \text{ is the real host partner of } m_o \\
  1 - g (m_o, n_p) & m_o \in A_i^\mathrm{l} \land n_p \text{ is the model partner of } m_o     \\
  1                & m_o = n_p                                                                 \\
  0                & \text{otherwise}
\end{cases}
\]

where \(m_o \in (A_i' = \{m_1, \dots, m_a\}) \) is any 'Atom' in the 'Fragment' and \(n_p \in (A = \{ n_1, \dots, n_b\} ) \) is any atom in the original supersystem.
-}

{- | Obtain the supersystem-subsystem 'Jacobian' from a 'Fragment'. The
function assumes, that the link atoms have been positioned along the
original bond vector.
-}
getJacobian ::
  forall m mol atom bond frag.
  (Molecule mol atom bond, MolecularFragment frag atom bond, MonadThrow m) =>
  mol atom bond ->
  frag atom bond ->
  m Jacobian
getJacobian m f = do
  (sz, vals) <- getJacobian' m f
  mkJacobian sz vals

-- | Make a Jacobian in COO format.
getJacobian' ::
  forall m mol atom bond frag.
  (Molecule mol atom bond, MolecularFragment frag atom bond, MonadThrow m) =>
  mol atom bond ->
  frag atom bond ->
  m (Sz2, Vector U (Ix2, Double))
getJacobian' supMol frag
  | not $ IntMap.null frgDiff = throwM $ MoleculeException "The supplied supersystem is not a superset of the fragment's atoms"
  | otherwise = do
      ones <- fmap sconcat <$> traverse laF . IntMap.toList $ fragSupAtms
      gVals <- fmap sconcat <$> traverse laRpF . IntMap.toList $ fragLinkAtms
      gInvVals <- fmap sconcat <$> traverse laMpF . IntMap.toList $ fragLinkAtms
      return
        ( Sz $ (IntMap.size fragAtms * 3) :. (IntMap.size supAtms * 3)
        , compute . sconcat $ [ones, gVals, gInvVals]
        )
 where
  frgGr = getMolGraph frag
  fragAtms = IntMap.fromList . getAtoms $ frag
  frgMp = s2dMapping . IntMap.keys $ fragAtms
  supAtms = IntMap.fromList . getAtoms $ supMol
  supMp = s2dMapping . IntMap.keys $ supAtms
  frgDiff = IntMap.difference fragAtms supAtms
  fragLinkAtms = IntMap.fromList . getLinkInfo $ frag
  fragSupAtms = IntMap.difference fragAtms fragLinkAtms
  ix3trans :: Maybe Double -> AtomId -> AtomId -> m (Vector DS (Ix2, Double))
  ix3trans val sub sup = do
    rIx <- s2d frgMp sub
    cIx <- s2d supMp sup
    return
      . sfromList
      $ [ (rIx * 3 + 0 :. cIx * 3 + 0, fromMaybe 1 val)
        , (rIx * 3 + 1 :. cIx * 3 + 1, fromMaybe 1 val)
        , (rIx * 3 + 2 :. cIx * 3 + 2, fromMaybe 1 val)
        ]
  laF (laId, _) = ix3trans Nothing laId laId
  laRpF (laId, (_, LinkInfo{gVal})) = ix3trans (Just gVal) laId laId
  laMpF (laId, (_, LinkInfo{gVal})) =
    ix3trans
      (Just $ 1 - gVal)
      laId
      (List.head $ neighbors frgGr laId)

--------------------------------------------------------------------------------

{- | A queue of values may contain a value or assure no more values will follow
and signal possible shutdown of the queue.
-}
data QueueVal a
  = Value a
  | NoMoreValues
  deriving (Eq, Show)

--------------------------------------------------------------------------------

data Derivative
  = -- | Electric field
    F
  | -- | Magnetic field
    B
  | -- | Nuclear spin
    I
  | -- | Nuclear coordinates
    R

instance TypeOf 'F Derivative where
  toValue = F

instance TypeOf 'B Derivative where
  toValue = B

instance TypeOf 'I Derivative where
  toValue = I

instance TypeOf 'R Derivative where
  toValue = R

-- | General response tensors.
data family ResponseTensor (derivs :: [Derivative])

{- | 'ResponseTensor's that can and need to be transformed with respect to
nuclear coordinate derivatives.
-}
class NucRefTransformation prop where
  nucRefTransform ::
    (MonadThrow m) =>
    Jacobian ->
    prop ->
    m prop

-- | 'ResponseTensor's that can be accumulated to obtain a supersystem value
class AccumulateResponseTensor prop where
  accRespTensorF ::
    (MonadThrow m) =>
    -- | Previous value
    prop ->
    -- | Next value
    prop ->
    m prop

instance (Index ix, Numeric r e) => AccumulateResponseTensor (Array r ix e) where
  {-# NOINLINE accRespTensorF #-}
  accRespTensorF accVal nextVal = accVal .+. nextVal

instance (Numeric r e) => AccumulateResponseTensor (R3 r e) where
  accRespTensorF accVal nextVal = fromJust . mkR3 <$> r3 accVal .+. r3 nextVal

{- | Accumulate values of a response tensor that come in via a 'TQueue'.
The function may throw an Exception in 'STM', which will propagate to
'IO' when called. However, the error is anyway unrecoverable.
-}
accRespTensor ::
  ( AccumulateResponseTensor prop
  ) =>
  TQueue (QueueVal prop) ->
  prop ->
  STM prop
accRespTensor queue acc =
  readTQueue queue >>= \case
    Value a -> do
      newAcc <- accRespTensorF acc a
      accRespTensor queue newAcc
    NoMoreValues -> return acc
