{-# OPTIONS_GHC -Wno-orphans #-}

{- |
Module      : Spicy.ResponseProperties.Dipole
Copyright   : Phillip Seeber, Sebastian Seidenath 2024
License     : AGPL-3
Maintainer  : phillip.seeber@uni-jena.de
Stability   : experimental
Portability : POSIX, Windows
-}
module Spicy.ResponseProperties.Dipole where

import Data.Aeson
import Data.Massiv.Array as Massiv
import Spicy.Math.LinearAlgebra.R3 as R3
import Spicy.Prelude
import Spicy.ResponseProperties.Common

{- | The dipole tensor

\[
    \frac{\partial^1 E}{\partial \boldsymbol{f}^1}
\]
-}
newtype instance ResponseTensor '[ 'F] = DipoleRT (R3 S Double)
  deriving (Show, Generic)

type Dipole = ResponseTensor '[ 'F]

-- | Safe constructor for 'Dipole'
mkDipole :: R3 S Double -> Dipole
mkDipole vec = DipoleRT vec

instance AccumulateResponseTensor Dipole where
  accRespTensorF (DipoleRT a) (DipoleRT b) =
    return . DipoleRT $ a R3.!+! b

instance Binary Dipole
instance ToJSON Dipole
instance FromJSON Dipole
