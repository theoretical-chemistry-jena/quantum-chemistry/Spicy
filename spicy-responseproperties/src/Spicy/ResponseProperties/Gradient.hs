{-# OPTIONS_GHC -Wno-orphans #-}

{- |
Module      : Spicy.ResponseProperties.Gradient
Copyright   : Phillip Seeber, Sebastian Seidenath 2024
License     : AGPL-3
Maintainer  : phillip.seeber@uni-jena.de
Stability   : experimental
Portability : POSIX, Windows
-}
module Spicy.ResponseProperties.Gradient where

import Data.Aeson
import Data.Massiv.Array as Massiv
import Spicy.Math.JacobianTransformation
import Spicy.Prelude
import Spicy.ResponseProperties.Common

{- | The gradient vector

\[
    \frac{\partial^1 E}{\partial \boldsymbol{r}^1}
\]
-}
newtype instance ResponseTensor '[ 'R] = GradientRT (Vector S Double)
  deriving (Show, Generic)

type Gradient = ResponseTensor '[ 'R]

-- | Safe constructor for 'Gradient'
mkGradient :: (MonadThrow m, Load r Ix1 Double, Size r) => Vector r Double -> m Gradient
mkGradient vec
  | n `mod` 3 /= 0 = throwM $ RespTensorSizeException (Sz n) "Vector length is not multiples of 3"
  | otherwise = return . GradientRT . compute $ vec
 where
  Sz n = size vec

instance NucRefTransformation Gradient where
  {-# NOINLINE nucRefTransform #-}
  nucRefTransform jac (GradientRT grad) = GradientRT <$> jacobianTrans jac 1 grad

instance AccumulateResponseTensor Gradient where
  accRespTensorF (GradientRT acc) (GradientRT nextVal) = GradientRT <$> acc .+. nextVal

instance Binary Gradient
instance ToJSON Gradient
instance FromJSON Gradient
