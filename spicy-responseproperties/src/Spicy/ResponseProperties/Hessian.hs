{-# OPTIONS_GHC -Wno-orphans #-}

{- |
Module      : Spicy.ResponseProperties.Hessian
Copyright   : Phillip Seeber, Sebastian Seidenath 2024
License     : AGPL-3
Maintainer  : phillip.seeber@uni-jena.de
Stability   : experimental
Portability : POSIX, Windows
-}
module Spicy.ResponseProperties.Hessian
where

import Data.Aeson
import Data.Massiv.Array as Massiv
import Spicy.Math.JacobianTransformation
import Spicy.Prelude
import Spicy.ResponseProperties.Common

{- | The non-projected Hessian matrix

\[
    \frac{\partial^2 E}{\partial \boldsymbol{r}^2}
\]
-}
newtype instance ResponseTensor '[ 'R, 'R] = HessianRT (Matrix S Double)
  deriving (Show, Generic)

type Hessian = ResponseTensor '[ 'R, 'R]

-- | Safe constructor for Hessians
mkHessian :: (Size r, Load r Ix2 Double, MonadThrow m) => Matrix r Double -> m Hessian
mkHessian mat
  | r /= c = throwM $ RespTensorSizeException szMat "Matrix is not square"
  | r `mod` 3 /= 0 = throwM $ RespTensorSizeException szMat "Matrix dimension is not multiples of 3"
  | otherwise = return . HessianRT . compute $ mat
 where
  szMat@(Sz (r :. c)) = Massiv.size mat

instance NucRefTransformation Hessian where
  {-# NOINLINE nucRefTransform #-}
  nucRefTransform jac (HessianRT hess) = HessianRT <$> jacobianTrans jac 2 hess

instance AccumulateResponseTensor Hessian where
  accRespTensorF (HessianRT acc) (HessianRT nextVal) = HessianRT <$> acc .+. nextVal

instance Binary Hessian
instance ToJSON Hessian
instance FromJSON Hessian
