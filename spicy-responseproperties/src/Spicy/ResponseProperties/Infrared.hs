{-# OPTIONS_GHC -Wno-orphans #-}

{- |
Module      : Spicy.ResponseProperties.Infrared
Copyright   : Phillip Seeber, Sebastian Seidenath 2024
License     : AGPL-3
Maintainer  : phillip.seeber@uni-jena.de
Stability   : experimental
Portability : POSIX, Windows
-}
module Spicy.ResponseProperties.Infrared where

import Data.Aeson
import Data.Massiv.Array as Massiv
import Spicy.Math.JacobianTransformation
import Spicy.Prelude
import Spicy.ResponseProperties.Common

{- | The infrared absorption tensor

\[
    \frac{\partial^2 E}{\partial \boldsymbol{f}^1 \boldsymbol{r}^1}
\]
-}
newtype instance ResponseTensor '[ 'F, 'R] = InfraredRT (Matrix S Double)
  deriving (Show, Generic)

type Infrared = ResponseTensor '[ 'F, 'R]

-- | Safe constructor for 'Infrared'
mkInfrared :: (MonadThrow m, Load r Ix2 Double, Size r) => Matrix r Double -> m Infrared
mkInfrared mat
  | r /= 3 = throwM $ RespTensorSizeException irSz "Expected exactly 3 rows"
  | c `mod` 3 /= 0 = throwM $ RespTensorSizeException irSz "Number of columns needs to be multiple of 3"
  | otherwise = return . InfraredRT . compute $ mat
 where
  irSz@(Sz (r :. c)) = size mat

instance NucRefTransformation Infrared where
  {-# NOINLINE nucRefTransform #-}
  nucRefTransform jac (InfraredRT ir) = InfraredRT <$> jacobianTrans jac 1 ir

instance AccumulateResponseTensor Infrared where
  accRespTensorF (InfraredRT acc) (InfraredRT nextVal) = InfraredRT <$> acc .+. nextVal

instance Binary Infrared
instance ToJSON Infrared
instance FromJSON Infrared
