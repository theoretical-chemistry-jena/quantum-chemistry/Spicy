{-# OPTIONS_GHC -Wno-orphans #-}

{- |
Module      : Spicy.ResponseProperties.MagneticDipole
Copyright   : Phillip Seeber, Sebastian Seidenath 2024
License     : AGPL-3
Maintainer  : phillip.seeber@uni-jena.de
Stability   : experimental
Portability : POSIX, Windows
-}
module Spicy.ResponseProperties.MagneticDipole where

import Data.Aeson
import Data.Massiv.Array as Massiv hiding (B)
import Spicy.Math.LinearAlgebra.R3 as R3
import Spicy.Prelude
import Spicy.ResponseProperties.Common

{- | The magnetic dipole tensor

\[
    \frac{\partial^1 E}{\partial \boldsymbol{b}^1}
\]
-}
newtype instance ResponseTensor '[ 'B] = MagneticDipoleRT (R3 S Double)
  deriving (Show, Generic)

type MagneticDipole = ResponseTensor '[ 'B]

-- | Safe constructor for 'MagneticDipole'
mkMagneticDipole :: R3 S Double -> MagneticDipole
mkMagneticDipole vec = MagneticDipoleRT vec

instance AccumulateResponseTensor MagneticDipole where
  accRespTensorF (MagneticDipoleRT a) (MagneticDipoleRT b) =
    return . MagneticDipoleRT $ a R3.!+! b

instance Binary MagneticDipole
instance ToJSON MagneticDipole
instance FromJSON MagneticDipole
