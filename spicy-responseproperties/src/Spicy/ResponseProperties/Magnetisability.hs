{-# OPTIONS_GHC -Wno-orphans #-}

{- |
Module      : Spicy.ResponseProperties.Magnetisability
Copyright   : Phillip Seeber, Sebastian Seidenath 2024
License     : AGPL-3
Maintainer  : phillip.seeber@uni-jena.de
Stability   : experimental
Portability : POSIX, Windows
-}
module Spicy.ResponseProperties.Magnetisability where

import Data.Aeson
import Data.Massiv.Array as Massiv hiding (B)
import Spicy.Prelude
import Spicy.ResponseProperties.Common

{- | The magnetisability

\[
    \frac{\partial^2 E}{\partial \boldsymbol{b}^2}
\]
-}
newtype instance ResponseTensor '[ 'B, 'B] = MagnetisabilityRT (Matrix S Double)
  deriving (Show, Generic)

type Magnetisability = ResponseTensor '[ 'B, 'B]

-- | Safe constructor for 'Magnetisability'
mkMagnetisability :: (MonadThrow m, Size r, Load r Ix2 Double) => Matrix r Double -> m Magnetisability
mkMagnetisability mat
  | r /= c = throwM $ RespTensorSizeException mSz "Magnetisabiliy tensor must be square"
  | r /= 3 = throwM $ RespTensorSizeException mSz "Magnetisabiliy tensor must be 3x3"
  | otherwise = return . MagnetisabilityRT . compute $ mat
 where
  mSz@(Sz (r :. c)) = size mat

instance AccumulateResponseTensor Magnetisability where
  accRespTensorF (MagnetisabilityRT a) (MagnetisabilityRT b) =
    MagnetisabilityRT
      <$> a
      .+. b

instance Binary Magnetisability
instance ToJSON Magnetisability
instance FromJSON Magnetisability
