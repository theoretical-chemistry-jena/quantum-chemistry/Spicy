{-# OPTIONS_GHC -Wno-orphans #-}

{- |
Module      : Spicy.ResponseProperties.Polarisability
Copyright   : Phillip Seeber, Sebastian Seidenath 2024
License     : AGPL-3
Maintainer  : phillip.seeber@uni-jena.de
Stability   : experimental
Portability : POSIX, Windows
-}
module Spicy.ResponseProperties.Polarisability where

import Data.Aeson
import Data.Massiv.Array as Massiv hiding (B)
import Spicy.Prelude
import Spicy.ResponseProperties.Common

{- | The electric polarisability

\[
    \frac{\partial^2 E}{\partial \boldsymbol{f}^2}
\]
-}
newtype instance ResponseTensor '[ 'F, 'F] = PolarisabilityRT (Matrix S Double)
  deriving (Show, Generic)

type Polarisability = ResponseTensor '[ 'F, 'F]

-- | Safe constructor for 'Polarisability'
mkPolarisability :: (MonadThrow m, Size r, Load r Ix2 Double) => Matrix r Double -> m Polarisability
mkPolarisability mat
  | r /= c = throwM $ RespTensorSizeException mSz "Polarisability tensor must be square"
  | r /= 3 = throwM $ RespTensorSizeException mSz "Polarisability tensor must be 3x3"
  | otherwise = return . PolarisabilityRT . compute $ mat
 where
  mSz@(Sz (r :. c)) = size mat

instance AccumulateResponseTensor Polarisability where
  accRespTensorF (PolarisabilityRT a) (PolarisabilityRT b) =
    PolarisabilityRT
      <$> a
      .+. b

instance Binary Polarisability
instance ToJSON Polarisability
instance FromJSON Polarisability
