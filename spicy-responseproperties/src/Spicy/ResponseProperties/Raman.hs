{-# OPTIONS_GHC -Wno-orphans #-}

{- |
Module      : Spicy.ResponseProperties.Raman
Copyright   : Phillip Seeber, Sebastian Seidenath 2024
License     : AGPL-3
Maintainer  : phillip.seeber@uni-jena.de
Stability   : experimental
Portability : POSIX, Windows
-}
module Spicy.ResponseProperties.Raman where

import Data.Massiv.Array as Massiv
import Spicy.Math.JacobianTransformation
import Spicy.Prelude hiding ((:>))
import Spicy.ResponseProperties.Common

{- | The Raman tensor

\[
    \frac{\partial^3 E}{\partial \boldsymbol{f}^2 \boldsymbol{r}^1}
\]
-}
newtype instance ResponseTensor '[ 'F, 'F, 'R] = RamanRT (Array S Ix3 Double)

type Raman = ResponseTensor '[ 'F, 'F, 'R]

-- | Safe constructor for 'Raman'
mkRaman :: (MonadThrow m, Size r, Load r Ix3 Double) => Array r Ix3 Double -> m Raman
mkRaman arr
  | i1 /= i2 = throwM $ RespTensorSizeException rSz "Index 1 and 2 of the Raman tensor must be identical"
  | i1 /= 3 = throwM $ RespTensorSizeException rSz "Raman tensor must have 3 elements in first two indices"
  | i3 `mod` 3 /= 0 = throwM $ RespTensorSizeException rSz "Index 3 of Raman tensor must have a multiple of 3 elements"
  | otherwise = return . RamanRT . compute $ arr
 where
  rSz@(Sz (i1 :> i2 :. i3)) = size arr

instance NucRefTransformation Raman where
  {-# NOINLINE nucRefTransform #-}
  nucRefTransform jac (RamanRT raman) = RamanRT <$> jacobianTrans jac 1 raman

instance AccumulateResponseTensor Raman where
  accRespTensorF (RamanRT acc) (RamanRT nextVal) = RamanRT <$> acc .+. nextVal
