module Spicy.Tests.Math.JacobianTransformation (tests) where

import Data.Massiv.Array as Massiv
import RIO.Partial (fromJust)
import Spicy.Math.JacobianTransformation
import Spicy.Prelude hiding ((:>))
import Test.Tasty
import Test.Tasty.HUnit

tests :: TestTree
tests =
  testGroup
    "JacobianTrans"
    [ testCase "transform R1 T1" $ do
        let expc = Massiv.fromList @S @Double Seq [1, 4, 12, 0, 6]
        res <- jacobianTrans jacobian1 1 prop1
        res @?= expc
    , testCase "transform R2 T2" $ do
        let expc =
              Massiv.fromLists'
                Seq
                [ [1, 4, 12, 0, 6]
                , [8, 20, 48, 0, 30]
                , [28, 64, 144, 0, 96]
                , [0, 0, 0, 0, 0]
                , [12, 30, 72, 0, 45]
                ]
        res <- jacobianTrans jacobian1 2 prop2
        res @?= expc
    , testCase "transform R2 T1" $ do
        let expc =
              Massiv.fromLists'
                Seq
                [ [1, 4, 12, 0, 6]
                , [4, 10, 24, 0, 15]
                , [7, 16, 36, 0, 24]
                ]
        res <- jacobianTrans jacobian1 1 prop2
        res @?= expc
    , testCase "transform R3 T3" $ do
        let expc =
              Massiv.fromLists'
                Seq
                [
                  [ [1, 4, 6]
                  , [6, 16, 24]
                  , [9, 24, 36]
                  ]
                ,
                  [ [10, 24, 36]
                  , [28, 64, 96]
                  , [42, 96, 144]
                  ]
                ,
                  [ [15, 36, 54]
                  , [42, 96, 144]
                  , [63, 144, 216]
                  ]
                ]
        res <- jacobianTrans jacobian2 3 prop3
        res @?= expc
    , testCase "transform R3 T2" $ do
        let expc =
              Massiv.fromLists'
                Seq
                [
                  [ [1, 4, 6]
                  , [6, 16, 24]
                  , [9, 24, 36]
                  ]
                ,
                  [ [5, 12, 18]
                  , [14, 32, 48]
                  , [21, 48, 72]
                  ]
                ]
        res <- jacobianTrans jacobian2 2 prop3
        res @?= expc
    , testCase "transform R3 T1" $ do
        let expc =
              Massiv.fromLists'
                Seq
                [
                  [ [1, 4, 6]
                  , [3, 8, 12]
                  ]
                ,
                  [ [5, 12, 18]
                  , [7, 16, 24]
                  ]
                ]
        res <- jacobianTrans jacobian2 1 prop3
        res @?= expc
    , testCase "transform R4 T4" $ do
        let expc =
              Massiv.fromLists'
                Seq
                [
                  [
                    [ [1, 4, 6]
                    , [6, 16, 24]
                    , [9, 24, 36]
                    ]
                  ,
                    [ [10, 24, 36]
                    , [28, 64, 96]
                    , [42, 96, 144]
                    ]
                  ,
                    [ [15, 36, 54]
                    , [42, 96, 144]
                    , [63, 144, 216]
                    ]
                  ]
                ,
                  [
                    [ [18, 40, 60]
                    , [44, 96, 144]
                    , [66, 144, 216]
                    ]
                  ,
                    [ [52, 112, 168]
                    , [120, 256, 384]
                    , [180, 384, 576]
                    ]
                  ,
                    [ [78, 168, 252]
                    , [180, 384, 576]
                    , [270, 576, 864]
                    ]
                  ]
                ,
                  [
                    [ [27, 60, 90]
                    , [66, 144, 216]
                    , [99, 216, 324]
                    ]
                  ,
                    [ [78, 168, 252]
                    , [180, 384, 576]
                    , [270, 576, 864]
                    ]
                  ,
                    [ [117, 252, 378]
                    , [270, 576, 864]
                    , [405, 864, 1296]
                    ]
                  ]
                ]
        res <- jacobianTrans jacobian2 4 prop4
        res @?= expc
    , testCase "transform R4 T3" $ do
        let expc =
              Massiv.fromLists'
                Seq
                [
                  [
                    [ [1, 4, 6]
                    , [6, 16, 24]
                    , [9, 24, 36]
                    ]
                  ,
                    [ [10, 24, 36]
                    , [28, 64, 96]
                    , [42, 96, 144]
                    ]
                  ,
                    [ [15, 36, 54]
                    , [42, 96, 144]
                    , [63, 144, 216]
                    ]
                  ]
                ,
                  [
                    [ [9, 20, 30]
                    , [22, 48, 72]
                    , [33, 72, 108]
                    ]
                  ,
                    [ [26, 56, 84]
                    , [60, 128, 192]
                    , [90, 192, 288]
                    ]
                  ,
                    [ [39, 84, 126]
                    , [90, 192, 288]
                    , [135, 288, 432]
                    ]
                  ]
                ]
        res <- jacobianTrans jacobian2 3 prop4
        res @?= expc
    , testCase "transform R4 T2" $ do
        let expc =
              Massiv.fromLists'
                Seq
                [
                  [
                    [ [1, 4, 6]
                    , [6, 16, 24]
                    , [9, 24, 36]
                    ]
                  ,
                    [ [5, 12, 18]
                    , [14, 32, 48]
                    , [21, 48, 72]
                    ]
                  ]
                ,
                  [
                    [ [9, 20, 30]
                    , [22, 48, 72]
                    , [33, 72, 108]
                    ]
                  ,
                    [ [13, 28, 42]
                    , [30, 64, 96]
                    , [45, 96, 144]
                    ]
                  ]
                ]
        res <- jacobianTrans jacobian2 2 prop4
        res @?= expc
    , testCase "transform R4 T1" $ do
        let expc =
              Massiv.fromLists'
                Seq
                [
                  [
                    [ [1, 4, 6]
                    , [3, 8, 12]
                    ]
                  ,
                    [ [5, 12, 18]
                    , [7, 16, 24]
                    ]
                  ]
                ,
                  [
                    [ [9, 20, 30]
                    , [11, 24, 36]
                    ]
                  ,
                    [ [13, 28, 42]
                    , [15, 32, 48]
                    ]
                  ]
                ]
        res <- jacobianTrans jacobian2 1 prop4
        res @?= expc
    ]

jacobian1 :: Jacobian
jacobian1 =
  fromJust
    . mkJacobian (Sz $ 3 :. 5)
    . Massiv.fromList Par
    $ [ (0 :. 0, 1)
      , (1 :. 1, 2)
      , (1 :. 4, 3)
      , (2 :. 2, 4)
      ]

jacobian2 :: Jacobian
jacobian2 =
  fromJust
    . mkJacobian (Sz $ 2 :. 3)
    . Massiv.fromList Par
    $ [ (0 :. 0, 1)
      , (1 :. 1, 2)
      , (1 :. 2, 3)
      ]

prop1 :: Vector S Double
prop1 = Massiv.fromList @S @Double Seq [1, 2, 3]

prop2 :: Matrix S Double
prop2 =
  Massiv.fromLists'
    Seq
    [ [1, 2, 3]
    , [4, 5, 6]
    , [7, 8, 9]
    ]

prop3 :: Array S Ix3 Double
prop3 =
  Massiv.fromLists'
    Seq
    [[[1, 2], [3, 4]], [[5, 6], [7, 8]]]

prop4 :: Array S Ix4 Double
prop4 =
  Massiv.fromLists'
    Seq
    [[[[1, 2], [3, 4]], [[5, 6], [7, 8]]], [[[9, 10], [11, 12]], [[13, 14], [15, 16]]]]
