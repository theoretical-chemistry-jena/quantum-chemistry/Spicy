module Spicy.Tests.ResponseProperties where

import Spicy.Tests.ResponseProperties.Common qualified
import Test.Tasty

tests :: TestTree
tests =
  testGroup
    "ResponseProperties"
    [Spicy.Tests.ResponseProperties.Common.tests]
