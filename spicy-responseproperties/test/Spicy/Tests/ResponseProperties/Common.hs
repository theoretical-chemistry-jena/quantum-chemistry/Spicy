module Spicy.Tests.ResponseProperties.Common (tests) where

import Data.Massiv.Array as Massiv
import Spicy.Prelude
import Spicy.ResponseProperties.Common
import Spicy.Tests.FragmentMethods.Fragment.Generators (refFrag)
import Spicy.Tests.FragmentMethods.Group.Generators (refGroupGr)
import Test.Tasty
import Test.Tasty.HUnit

tests :: TestTree
tests =
  testGroup
    "Common"
    [ testCase "Ethanal Jacobian" $ do
        res <- getJacobian' refGroupGr refFrag
        let expc =
              ( Sz $ 12 :. 21
              , Massiv.fromList @U @(Ix2, Double)
                  Seq
                  [ (0 :. 0, 1.0)
                  , (1 :. 1, 1.0)
                  , (2 :. 2, 1.0)
                  , (3 :. 3, 1.0)
                  , (4 :. 4, 1.0)
                  , (5 :. 5, 1.0)
                  , (6 :. 6, 1.0)
                  , (7 :. 7, 1.0)
                  , (8 :. 8, 1.0)
                  , (9 :. 9, 0.7039473684210527)
                  , (10 :. 10, 0.7039473684210527)
                  , (11 :. 11, 0.7039473684210527)
                  , (9 :. 0, 0.29605263157894735)
                  , (10 :. 1, 0.29605263157894735)
                  , (11 :. 2, 0.29605263157894735)
                  ]
              )
        res @?= expc
    ]
