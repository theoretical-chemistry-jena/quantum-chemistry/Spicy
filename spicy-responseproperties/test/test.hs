module Main where

import Spicy.Prelude
import Spicy.Tests.Math.JacobianTransformation qualified
import Spicy.Tests.ResponseProperties qualified
import Test.Tasty

main :: IO ()
main = defaultMain tests

tests :: TestTree
tests =
  testGroup
    "Spicy"
    [ Spicy.Tests.Math.JacobianTransformation.tests
    , Spicy.Tests.ResponseProperties.tests
    ]
