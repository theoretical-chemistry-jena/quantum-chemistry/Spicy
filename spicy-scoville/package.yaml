name:                spicy-scoville
version:             0.1.0
git:                 https://gitlab.com/theoretical-chemistry-jena/quantum-chemistry/Spicy
license:             AGPL-3.0-only
author:              "Phillip Seeber, Sebastian Seidenath"
maintainer:          "phillip.seeber@uni-jena.de, sebastian.seidenath@uni-jena.de"
copyright:           "2024 Phillip Seeber, Sebastian Seidenath"

build-type: Simple

extra-source-files:
  - LICENSE

description: Simple scripting language for Spicy

language: GHC2021

default-extensions:
  - NoImplicitPrelude
  - StrictData
  - OverloadedStrings
  - OverloadedLabels
  - OverloadedRecordDot
  - RecordWildCards
  - CApiFFI
  - DeriveGeneric
  - DerivingVia
  - DerivingStrategies
  - DuplicateRecordFields
  - GADTs
  - LambdaCase
  - QuantifiedConstraints
  - TypeFamilies
  - ViewPatterns
  - DataKinds

ghc-options:
  - -Wall
  - -Wcompat
  - -Widentities
  - -Wincomplete-record-updates
  - -Wincomplete-uni-patterns
  - -Wpartial-fields
  - -Wredundant-constraints

dependencies:
  - base >= 4.16 && < 4.21
  - rio >= 0.1.13.0 && < 0.2
  - spicy-prelude >= 0.1.0 && < 0.2
  - attoparsec >= 0.13.1.0 && < 0.15
  - spicy-textual >= 0.1.0 && < 0.2
  - spicy-containers >= 0.1.0 && < 0.2
  - containers >= 0.6.0.1 && < 0.8
  - monad-validate >= 1.2 && < 1.4
  - pathtype >= 0.8.1 && < 0.9
  - text >= 1.2.3.1 && < 2.2

library:
  source-dirs: src

tests:
  spicy-scoville-test:
    main: test.hs
    source-dirs:
      - test
    dependencies:
      - spicy-scoville
      - tasty >= 1.2.3 && < 1.6
      - tasty-hunit >= 0.10.0.2 && < 0.11
      - tasty-hedgehog >= 1.3.1.0 && < 1.5
    ghc-options:
      - -threaded
      - -rtsopts
      - -with-rtsopts=-N
