{- |
Module      : Spicy.Scoville
Copyright   : Phillip Seeber, Sebastian Seidenath 2024
License     : AGPL-3
Maintainer  : sebastian.seidenath@uni-jena.de
Stability   : experimental
Portability : POSIX, Windows

This module reexports the most important functionalities around Scoville as well as providing functions to actually read Scoville from a file and run it.
-}
module Spicy.Scoville (
  module Spicy.Scoville.Frontend,
  module Spicy.Scoville.Core.Value,
  module Spicy.Scoville.Common,
  module Spicy.Scoville.Pretty,
  scovilleFile,
  eval,
  evalFile,
  run,
  runFile,
  prettyFrontend,
  prettyCore,
) where

import Spicy.Prelude
import Spicy.Scoville.Common
import Spicy.Scoville.Core
import Spicy.Scoville.Core.Value
import Spicy.Scoville.Frontend
import Spicy.Scoville.Pretty
import Spicy.Textual

-- | Evaluate a parsed program. Will not execute side effects.
eval :: (MonadThrow m) => [Statement] -> m Value
eval source = do
  prog <- compileProgram source
  return $ runReader (evalM prog) fullEnv

-- | Helper for defining functions that read a Scoville program from a file.
scovilleFile ::
  (MonadThrow m, MonadIO m) =>
  ([Statement] -> m a) ->
  AbsRelFile ->
  m a
scovilleFile f pth = do
  inp <- readFileUtf8 pth
  ex <- parse' pProgram inp
  f ex

-- | Evaluate a file.
evalFile :: AbsRelFile -> IO Value
evalFile = scovilleFile eval

-- | Run a Scoville program. Unlike `eval`, this will run side effects.
run :: [Statement] -> IO ()
run ex = do
  eval ex >>= \case
    Effect e -> void e
    _ -> error "Not an IO action!"

-- | Run a file. Unlike `evalFile`, this will execute side effects.
runFile :: AbsRelFile -> IO ()
runFile = scovilleFile run

-- | Pretty print a parsed program.
prettyFrontend :: AbsRelFile -> IO Text
prettyFrontend = scovilleFile (pure . format . ppLined . fmap toPP)

-- | Pretty print the core representation of a parsed program.
prettyCore :: AbsRelFile -> IO Text
prettyCore = scovilleFile (pure . format . toPP <=< compileProgram)
