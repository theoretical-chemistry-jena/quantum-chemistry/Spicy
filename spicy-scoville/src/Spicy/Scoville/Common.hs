{- |
Module      : Spicy.Scoville.Common
Copyright   : Phillip Seeber, Sebastian Seidenath 2024
License     : AGPL-3
Maintainer  : sebastian.seidenath@uni-jena.de
Stability   : experimental
Portability : POSIX, Windows

This module provides types and utilities that appear across many Scoville modules.
-}
module Spicy.Scoville.Common (
  -- * Types for expressions
  -- $texpr
  Name (..),
  TName (..),
  Alt,
  Dec (..),
  Literal (..),
  IsSet (..),

  -- * Types for types
  -- $ttype
  TypeEnv,
  ScovilleType (..),
  TypeConstant (..),
  Scheme (..),
  Predicate (..),
  typeOfLit,
  tUnit,
  tBool,
  tInt,
  tDouble,
  tString,
  tFragment,
  tLayer,
  tInput,
  tIO,

  -- * Types for exceptions
  -- $texcp
  err,
  ScovilleException (..),
  CompilationException (..),
  TypecheckException (..),
  ConversionException (..),
)
where

import Data.Map.Lazy qualified as Map
import RIO.Set qualified as Set
import Spicy.IntSet qualified as IntSet
import Spicy.Prelude
import Spicy.Textual (ParseException)

{- | A program identifier, i.e. the name of a variable. Note the convenient
'IsString' instance.
-}
newtype Name = Name {getName :: Text}
  deriving stock (Show)
  deriving newtype (Semigroup, Monoid, Eq, Ord, IsString)

{- | A type identifier, i.e. the name of a type variable. Note the convenient
'IsString' instance.
-}
newtype TName = TName {getTName :: Text}
  deriving stock (Show)
  deriving newtype (Semigroup, Monoid, Eq, Ord, IsString)

-- | An alternative in a case statement.
type Alt a = (Either Name Literal, a)

{- | The declaration of a value or function (same thing, really), usually in a
let statement. This is parametrized over @b@, the type of the bound variables,
and @a@, the type of the declared expression.
-}
data Dec b a = Dec
  { binder :: Name
  -- ^ Name of the declared entity, i.e. the @f@ in @f x = y@
  , bound :: b
  -- ^ Bound variables, i.e. the @x@ in @f x = y@
  , expression :: a
  -- ^ Body of the declaration, i.e the @y@ in @f x = y@
  }
  deriving (Show, Eq)

{- | The types of literal that may appear in a Scoville expression. Note that
layers and fragments are rather hard-coded, as Scoville does not contain
higher-kinded types such as arbitrary Sets.
-}
data Literal
  = LBool Bool
  | LInt Int
  | LDouble Double
  | LString Text
  | LFragment IntSet
  | LLayer (Set IntSet)
  deriving (Show, Eq)

-- | Auxilliary class to make some operations on these types polymorphic.
class IsSet s where
  gunion :: s -> s -> s
  gintersection :: s -> s -> s
  gdifference :: s -> s -> s

instance IsSet IntSet where
  gunion = IntSet.union
  gintersection = IntSet.intersection
  gdifference = IntSet.difference

instance (Ord x) => IsSet (Set x) where
  gunion = Set.union
  gintersection = Set.intersection
  gdifference = Set.difference

instance (Ord k) => IsSet (Map k v) where
  gunion = Map.union
  gintersection = Map.intersection
  gdifference = Map.difference

--------------------------------------------------------------------------------

{- $ttype
Types about types.
These are not much used as work on the typechecker is incomplete.
-}

-- | An environment for type schemes, linking bound variables to their types.
type TypeEnv = Map Name Scheme

{- | A type, which is either:

  * a known 'TypeConstant' such as @Int@, @Bool@, etc.
  * a function between two types
  * a type variable.

Note that Scoville does not support custom data constructors, nor any
higher-kinded types (besides the function type).
-}
data ScovilleType
  = TCon TypeConstant
  | TVar TName
  | ScovilleType :-> ScovilleType
  deriving (Show, Eq, Ord)

-- Right associativity so that a :-> b :-> c means what you think it should.
infixr 5 :->

-- | The arity zero types known to Scoville.
data TypeConstant
  = TUnit
  | TBool
  | TInt
  | TDouble
  | TString
  | TFragment
  | TLayer
  | TInput
  | -- Needs to be elaborated on, if typechecking is ever properly implemented.
    TIO
  deriving (Show, Eq, Ord)

-- | Handy synonyms for common types.
tUnit, tBool, tInt, tDouble, tString, tFragment, tLayer, tInput, tIO :: ScovilleType
tUnit = TCon TUnit
tBool = TCon TBool
tInt = TCon TInt
tDouble = TCon TDouble
tString = TCon TString
tFragment = TCon TFragment
tLayer = TCon TLayer
tInput = TCon TInput
tIO = TCon TIO

{- | A type scheme, i.e. a type where some of the type variables have been
 quantified over. Possibly includes some predicates on the bound types.
 In Haskell notation, this is a type signature of the form
 @forall a_1 ... a_n. (C_1 a_i ... C_n a_j) => t@.
-}
data Scheme = Forall
  { schemeBound :: [TName]
  -- ^ The types that are being quantified over.
  , schemePredicates :: [Predicate]
  -- ^ Predicates on these types.
  , schemeType :: ScovilleType
  -- ^ Signature of the type.
  }
  deriving (Show)

{- | Predicate on a type. Scoville only supports simple constraints where a type
 variable must equate to one of a set of primitive type constructors.
-}
data Predicate = OneOf
  { predHead :: [TypeConstant]
  , predBody :: ScovilleType
  }
  deriving (Show)

-- | Utility to get the type of a 'Literal'.
typeOfLit :: Literal -> ScovilleType
typeOfLit = \case
  LInt _ -> tInt
  LBool _ -> tBool
  LDouble _ -> tDouble
  LString _ -> tString
  LFragment _ -> tFragment
  LLayer _ -> tLayer

--------------------------------------------------------------------------------

{- $texcp
Here, types for any exceptions that may arise during parsing, translating,
checking, executing, and possibly converting a Scoville programme are defined.
-}

-- | Generic error message for errors that /should/ be impossible.
err :: a
err =
  error
    "Evaluation error - Typechecking should have prevented this.\
    \Please report this as a bug."

{- | The exceptions that can arise during the stages of processing Scoville from
source to result.
-}
data ScovilleException
  = ParseFailed ParseException
  | CompilationFailed CompilationException
  | TypecheckFailed TypecheckException
  | ConversionFailed (Set ConversionException)
  deriving (Show)

instance Exception ScovilleException

-- | Exceptions that could arise while translating a program to core Scoville.
data CompilationException
  = EmptyProgram
  | EndOnDefinition
  deriving (Show)

instance Exception CompilationException

{- | An exception during typechecking. Not much used since work on the
typechecker is incomplete.
-}
data TypecheckException
  = UndefinedVariable Name
  | UnificationFailure ScovilleType ScovilleType
  | InfiniteType TName ScovilleType
  | EmptyCase
  | UnsatisfiableConstraint
  deriving (Show)

instance Exception TypecheckException

{- | An exception during conversion of a Scoville 'VInput' value to a Spicy
type.
-}
data ConversionException
  = UnexpectedType ScovilleType ScovilleType
  | MissingField Name
  deriving (Show, Eq, Ord)

instance Exception ConversionException
