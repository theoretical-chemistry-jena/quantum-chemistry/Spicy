{- |
Module      : Spicy.Scoville.Core
Copyright   : Phillip Seeber, Sebastian Seidenath 2024
License     : AGPL-3
Maintainer  : sebastian.seidenath@uni-jena.de
Stability   : experimental
Portability : POSIX, Windows

This module reexports the constructs from the Scoville core language.

The core language, inspired by GHC's core, is a simple functional language based on the (untyped, as of now) lambda calculus.
By first translating to this intermediate language, excecution becomes much simpler, less error-prone and easier to debug.

It would also be handy for implementing optimizations, although that does not seem necessary at the moment.
-}
module Spicy.Scoville.Core (
  module Spicy.Scoville.Core.Builtins,
  module Spicy.Scoville.Core.Evaluator,
  module Spicy.Scoville.Core.Language,
  module Spicy.Scoville.Core.Value,
)
where

import Spicy.Scoville.Core.Builtins
import Spicy.Scoville.Core.Evaluator
import Spicy.Scoville.Core.Language
import Spicy.Scoville.Core.Value
