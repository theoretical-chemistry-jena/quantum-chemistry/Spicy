{-# LANGUAGE ApplicativeDo #-}

{- |
Module      : Spicy.Scoville.Core.Builtins
Copyright   : Phillip Seeber, Sebastian Seidenath 2024
License     : AGPL-3
Maintainer  : sebastian.seidenath@uni-jena.de
Stability   : experimental
Portability : POSIX, Windows

This module defines the builtin primitives for Scoville.
-}
module Spicy.Scoville.Core.Builtins (fullEnv) where

import Data.Map.Lazy qualified as Map
import Data.Text.IO (putStrLn)
import Spicy.Prelude hiding (and, or)
import Spicy.Scoville.Common
import Spicy.Scoville.Core.Evaluator
import Spicy.Scoville.Core.Value
import Spicy.Scoville.Pretty

{- | The environment that Scoville programs run in. It provides all the
necessary primitives.
-}
fullEnv :: Map Name Value
fullEnv =
  Map.fromList
    [ -- Basic arithmetic
      ("Negate", neg)
    , ("Add", add)
    , ("Sub", sub)
    , ("Mul", mul)
    , ("Div", dvd)
    , ("Eql", eql)
    , ("Neq", neq)
    , ("Grt", grt)
    , ("Lst", lst)
    , ("Geq", geq)
    , ("Leq", leq)
    , -- Set Operations
      ("And", and)
    , ("Or", or)
    , ("Xor", xor)
    , ("Uni", uni)
    , ("Isc", isc)
    , ("Dif", dif)
    , -- Input / Map Operations
      ("Insert", inserter)
    , -- I/O Operations
      ("Print", printS)
    , -- , ("ReadMolecule", undefined)
      ("RunSpicy", runSpicy)
    , ("Bind", bindS)
    , ("Random", randomNumber)
    ]

add, sub, mul, dvd :: Value
add = numBinOp (+)
sub = numBinOp (-)
mul = numBinOp (*)

eql, neq, grt, lst, geq, leq :: Value
eql = compOp (==)
neq = compOp (/=)
grt = compOp (>)
lst = compOp (<)
geq = compOp (>=)
leq = compOp (<=)

and, or, xor :: Value
and = boolOp (&&)
or = boolOp (||)
xor = boolOp (\a b -> (a || b) && not (a && b))

uni, isc, dif :: Value
uni = setOp gunion
isc = setOp gintersection
dif = setOp gdifference

numBinOp :: (forall n. (Num n) => n -> n -> n) -> Value
numBinOp op = Builtin $ \case
  VInt i -> Builtin $ VInt . (i `op`) . fromValue'
  VDouble d -> Builtin $ VDouble . (d `op`) . fromValue'
  _ -> err

dvd = Builtin $ \case
  VInt i -> Builtin $ VInt . (i `div`) . fromValue'
  VDouble d -> Builtin $ VDouble . (d /) . fromValue'
  _ -> err

neg :: Value
neg = Builtin $ \case
  VInt i -> VInt (-i)
  VDouble d -> VDouble (-d)
  _ -> err

compOp :: (forall n. (Ord n) => n -> n -> Bool) -> Value
compOp op = Builtin $ \case
  VInt i ->
    Builtin $ VBool . (i `op`) . fromValue'
  VDouble d ->
    Builtin $ VBool . (d `op`) . fromValue'
  VBool b ->
    Builtin $ VBool . (b `op`) . fromValue'
  VString s ->
    Builtin $ VBool . (s `op`) . fromValue'
  VFragment f ->
    Builtin $ VBool . (f `op`) . fromValue'
  VLayer l ->
    Builtin $ VBool . (l `op`) . fromValue'
  _ -> err

boolOp :: (Bool -> Bool -> Bool) -> Value
boolOp op = Builtin $ \case
  VBool b -> Builtin $ VBool . (b `op`) . fromValue'
  _ -> err

setOp :: (forall s. (IsSet s) => s -> s -> s) -> Value
setOp op = Builtin $ \case
  VFragment f -> Builtin $ VFragment . (f `op`) . fromValue'
  VLayer l -> Builtin $ VLayer . (l `op`) . fromValue'
  VInput d -> Builtin $ VInput . (d `op`) . fromValue'
  _ -> err

inserter :: Value
inserter = Builtin $ \case
  VString s -> Builtin $ \v ->
    Builtin $ VInput . Map.insert (Name s) v . fromValue'
  _ -> err

merger :: Value
merger = Builtin $ \case
  VInput i -> Builtin $ VInput . (`mergef` i) . fromValue'
  _ -> err

mergef :: Map Name Value -> Map Name Value -> Map Name Value
mergef = Map.unionWith f
 where
  f v1 v2 = case (v1, v2) of
    (VInput i, VInput j) -> VInput $ mergef i j
    (_, _) -> v2

bindS :: Value
bindS = Builtin $ \case
  Effect me -> Builtin $ \case
    VClosure n r env -> Effect $ do
      e <- me
      case runReader (evalM r) (Map.insert n e env) of
        Effect e2 -> e2
        _ -> err
    _ -> err
  _ -> err

printS :: Value
printS = Builtin $ \v ->
  Effect $ putStrLn (format . toPP $ v) >> return VUnit

{- | Simulates random number generation, but it's really just a simple mock I/O
operation for testing.
-}
randomNumber :: Value
randomNumber = Effect . return $ VInt 4 -- Chosen by fair dice roll.

{- | A placeholder – Eventually, a builtin like this will run the calculation.
For now, this is just for testing.
-}
runSpicy :: Value
runSpicy = Builtin $ \case
  VInput m -> Effect $ do
    putStrLn $ "Running Calculation with input " <> (format . toPP . VInput) m
    return VUnit
  _ -> err
