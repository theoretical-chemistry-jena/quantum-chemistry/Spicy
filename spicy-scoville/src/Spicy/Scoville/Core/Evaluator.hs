{- |
Module      : Spicy.Scoville.Core.Evaluator
Copyright   : Phillip Seeber, Sebastian Seidenath 2024
License     : AGPL-3
Maintainer  : sebastian.seidenath@uni-jena.de
Stability   : experimental
Portability : POSIX, Windows

This module defines the evaluator that actually runs a Scoville program.
-}
module Spicy.Scoville.Core.Evaluator (
  evalM,
)
where

import Data.Map.Lazy qualified as Map
import Spicy.Prelude
import Spicy.Scoville.Common
import Spicy.Scoville.Core.Language
import Spicy.Scoville.Core.Value

evalM :: (MonadReader Env m) => ExprC -> m Value
evalM = \case
  -- Variable: look it up in the environment.
  Var n -> asks (fromMaybe err . (Map.!? n))
  -- Literal: trivial conversion to a value.
  Lit p -> return $ litToVal p
  -- Lambda: Get the current environment and use it to create a closure.
  Lam n e -> VClosure n e <$> ask
  -- Case: Scrutinize the expression & matches the alternatives.
  Case l es -> do
    v <- evalM l
    case es of
      -- A variable pattern matches anything.
      -- Bind the value to the variable and evaluate RHS.
      ((Left t, e) : _) -> local (Map.insert t v) $ evalM e
      -- Matched a literal; evaluate RHS
      ((Right lit, e) : _) | valEq (litToVal lit) v -> evalM e
      -- If there's no match, go to the next alternative.
      (_ : xs) -> evalM $ Case l xs
      -- Empty case alternative or no match, should have been caught beforehand.
      [] -> err
  Ap x' y' -> do
    y <- evalM y'
    x <- evalM x'
    case x of
      -- First argument is a function
      (VClosure n e env) -> local (Map.insert n y env `Map.union`) $ evalM e
      -- First argument is a builtin
      (Builtin f) -> return $ f y
      -- First argument is something else
      _ -> err
  Let bindings e -> do
    let evalRec' bs = do
          -- Pretend we already (lazily) know the result
          env <- evalRec' bs
          -- Evaluate the bindings in the env of the already–known bindings
          local (env `Map.union`) (Map.fromList <$> traverse evalDecCM bs)
    bindMap <- evalRec' bindings
    local (bindMap `Map.union`) $ evalM e
  -- Evaluate each field individually
  Input m -> VInput <$> traverse evalM m

evalDecCM :: (MonadReader Env m) => DecC -> m (Name, Value)
evalDecCM Dec{..} = (binder,) <$> evalM expression
