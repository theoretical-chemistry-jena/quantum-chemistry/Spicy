{- |
Module      : Spicy.Scoville.Core.Language
Copyright   : Phillip Seeber, Sebastian Seidenath 2024
License     : AGPL-3
Maintainer  : sebastian.seidenath@uni-jena.de
Stability   : experimental
Portability : POSIX, Windows

This module defines the core Scoville language, which is a simplified form designed to be easy to manipulate and execute.
-}
module Spicy.Scoville.Core.Language (
  ExprC (..),
  DecC,
  AltC,
  freeVars,
) where

import Data.Map.Lazy qualified as Map
import RIO.Set qualified as Set
import Spicy.Prelude
import Spicy.Scoville.Common
import Spicy.Scoville.Pretty

{- | This defines the core language of Scoville, which is the part that is
actually executed. It is essentially an augmented version of the untyped
lambda calculus including @let@, @case@ and some primitives.
-}
data ExprC
  = -- | Literal occurences of values, such as @True@ or @1@.
    Lit Literal
  | -- | Variables such as @x@.
    Var Name
  | -- | Function application.
    Ap ExprC ExprC
  | -- | λ, i.e. a function.
    Lam Name ExprC
  | -- | A let declaration. Supports multiple, potentially mutally recursive
    -- definitions.
    Let [DecC] ExprC
  | -- | Case statement, the easiest way to allow branching.
    Case ExprC [AltC]
  | -- | A primitive for Input blocks.
    Input (Map Name ExprC)
  deriving (Show)

-- | Specialization of 'Dec' to the form actually used in 'ExprC'.
type DecC = Dec () ExprC

-- | Specialization of 'Alt to the form actually used in 'ExprC'.
type AltC = Alt ExprC

instance Pretty ExprC where
  toPP = \case
    Lit l -> toPP l
    Var n -> toPP n
    Ap x y@(Ap _ _) -> ppConcat [toPP x, " (", toPP y, ")"]
    Ap x y -> ppConcat [toPP x, " ", toPP y]
    Lam n e -> ppConcat ["(λ", toPP n, ". ", toPP e, ")"]
    Let bs e ->
      ppConcat
        ["let", PNewline, "  ", ppEntries (ppDecC <$> bs), "in ", toPP e]
    Case e les ->
      ppConcat
        ["case ", toPP e, ";", PNewline, "  ", ppEntries (ppAlt <$> les)]
    Input m -> toPP m

{- | Calculate the set of free variables in an 'ExprC'. A free variable is one
that is not bound anywhere.
-}
freeVars :: ExprC -> Set Name
freeVars = \case
  Lit _ -> Set.empty
  Var n -> Set.singleton n
  Ap x y -> freeVars x `Set.union` freeVars y
  Lam n e -> Set.delete n $ freeVars e
  Let bs e ->
    Set.union (Set.unions $ freeVars . expression <$> bs) (freeVars e)
      `Set.difference` (Set.fromList . fmap binder $ bs)
  Case e alts -> freeVars e `Set.union` Set.unions (freeAlt <$> alts)
  Input m ->
    Map.foldl' Set.union Set.empty (freeVars <$> m)
      `Set.difference` Map.keysSet m
 where
  freeAlt (Right _, e) = freeVars e
  freeAlt (Left v, e) = Set.delete v $ freeVars e
