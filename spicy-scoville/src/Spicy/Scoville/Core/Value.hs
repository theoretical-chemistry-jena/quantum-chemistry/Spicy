{- |
Module      : Spicy.Scoville.Core.Value
Copyright   : Phillip Seeber, Sebastian Seidenath 2024
License     : AGPL-3
Maintainer  : sebastian.seidenath@uni-jena.de
Stability   : experimental
Portability : POSIX, Windows

This module defines the core Scoville language, which is a simplified form designed to be easy to manipulate and execute.
-}
module Spicy.Scoville.Core.Value (
  -- * Scoville Values
  -- $svalues
  Env,
  Value (..),
  litToVal,
  valEq, -- This is partial, making it a slightly annoying export.
  -- But it's needed during evaluation.

  -- * Converting to Haskell Types
  -- $sconversion
  FromValue (..),
  (?),
) where

import Control.Monad.Validate
import Data.Functor.Classes
import Data.Map.Lazy qualified as Map
import RIO.Set qualified as Set
import Spicy.Prelude
import Spicy.Scoville.Common
import Spicy.Scoville.Core.Language
import Spicy.Scoville.Pretty

--------------------------------------------------------------------------------

{- $svalues
This module defines the values that can occur during and as a result of Scoville evaluation.
'Value' is the main data type that captures these.
-}

{- | A runtime environment that carries the 'Value's bound by the programme
identifiers.
-}
type Env = Map Name Value

-- | The sort of values that Scoville can evaluate to.
data Value
  = VUnit
  | VBool Bool
  | VInt Int
  | VDouble Double
  | VString Text
  | VFragment IntSet
  | VLayer (Set IntSet)
  | VInput (Map Name Value)
  | -- | Closures are essentially the value–level representation of functions,
    -- carrying around the environment (hopefully) containing any free variables.
    VClosure Name ExprC Env
  | Builtin (Value -> Value)
  | Effect (IO Value)

{- | Get the type of a 'Value'. Some current wrinkles: Builtins and closures do
not know their types and throw a runtime exception. This could probably be
fixed with some more type magic.
-}
typeOfValue :: Value -> ScovilleType
typeOfValue = \case
  VUnit -> tUnit
  VBool _ -> tBool
  VInt _ -> tInt
  VDouble _ -> tDouble
  VString _ -> tString
  VFragment _ -> tFragment
  VLayer _ -> tLayer
  VInput _ -> tInput
  VClosure{} -> error "Cannot give type for a closure."
  Builtin _ -> error "Cannot give type for a builtin."
  Effect _ -> tIO

instance Pretty Value where
  toPP = \case
    VUnit -> "()"
    VBool b -> toPP b
    VInt i -> toPP i
    VDouble d -> toPP d
    VString s -> PText s
    VFragment f -> toPP f
    VLayer l -> toPP l
    VInput m -> toPP m
    VClosure{} -> "<Closure>"
    Builtin _ -> "<Builtin Function>"
    Effect _ -> "<I/O Effect>"

{- | Equality among 'Value's. Fails with a runtime error if two values of
different types or values for which equality is undefined are encountered.
There *should* be a typechecker to prevent this from happening. The alternative
is just returning False, but that does not quite sit right with me.
-}
valEq :: Value -> Value -> Bool
valEq a b = case (a, b) of
  (VBool x, VBool y) -> x == y
  (VInt x, VInt y) -> x == y
  (VString x, VString y) -> x == y
  (VDouble x, VDouble y) -> x == y
  (VFragment x, VFragment y) -> x == y
  (VLayer x, VLayer y) -> x == y
  (VInput x, VInput y) -> liftEq2 (==) valEq x y
  _ -> error "Bad value type. Please consult your typechecker immediately."

-- | Convert a 'Literal' to a 'Value'.
litToVal :: Literal -> Value
litToVal = \case
  LBool b -> VBool b
  LInt i -> VInt i
  LDouble d -> VDouble d
  LString s -> VString s
  LFragment f -> VFragment f
  LLayer l -> VLayer l

--------------------------------------------------------------------------------

{- $sconversion
The 'FromValue' class allows Scoville's 'Value's to be converted into Haskell types.
Any type that can be constructed from a 'Value' should be an instance of the 'FromValue' class.
The 'fromValue' function returns a result in the 'MonadValidate' class, which gives detailed information in case the conversion has failed.
-}

-- | Helper writing 'FromValue' instances.
packConversionException :: ScovilleType -> ScovilleType -> Set ConversionException
packConversionException t u = Set.singleton $ UnexpectedType t u

{- | Helper for constructing records form 'Value's. Will automatically report
'MissingField' and 'UnexpectedType' errors in the 'MonadValidate'.
-}
(?) :: (MonadValidate (Set ConversionException) m, FromValue a) => Map Name Value -> Name -> m a
m ? k = case m Map.!? k of
  Nothing -> refute . Set.singleton . MissingField $ k
  Just x -> fromValue x

class FromValue a where
  fromValue :: (MonadValidate (Set ConversionException) m) => Value -> m a
  fromValue' :: Value -> a
  fromValue' = fVAbstract' "fromValue'"

-- | Helper for defining 'fromValue''.
fVAbstract' :: (FromValue a) => String -> Value -> a
fVAbstract' s = fromRight (error s) . runValidate . fromValue

instance FromValue Bool where
  fromValue = \case
    VBool b -> return b
    x -> refute . packConversionException tBool $ typeOfValue x
  fromValue' = fVAbstract' "fromValue' Bool"

instance FromValue Int where
  fromValue = \case
    VInt i -> return i
    x -> refute . packConversionException tInt $ typeOfValue x
  fromValue' = fVAbstract' "fromValue' Int"

instance FromValue Double where
  fromValue = \case
    VDouble d -> return d
    x -> refute . packConversionException tDouble $ typeOfValue x
  fromValue' = fVAbstract' "fromValue' Double"

instance FromValue Text where
  fromValue = \case
    VString s -> return s
    x -> refute . packConversionException tString $ typeOfValue x
  fromValue' = fVAbstract' "fromValue' Text"

instance FromValue IntSet where
  fromValue = \case
    VFragment f -> return f
    x -> refute . packConversionException tFragment $ typeOfValue x
  fromValue' = fVAbstract' "fromValue' IntSet"

instance FromValue (Set IntSet) where
  fromValue = \case
    VLayer l -> return l
    x -> refute . packConversionException tLayer $ typeOfValue x
  fromValue' = fVAbstract' "fromValue' (Set IntSet)"

instance FromValue (Map Name Value) where
  fromValue = \case
    VInput i -> return i
    x -> refute . packConversionException tInput $ typeOfValue x
  fromValue' = fVAbstract' "fromValue' (Map Name Value)"
