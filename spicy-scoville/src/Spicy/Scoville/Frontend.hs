{- |
Module      : Spicy.Scoville
Copyright   : Phillip Seeber, Sebastian Seidenath 2024
License     : AGPL-3
Maintainer  : sebastian.seidenath@uni-jena.de
Stability   : experimental
Portability : POSIX, Windows

This module reexports the most important functionalities around Scovilles frontend language, which is the language that users can actually write code in.
-}
module Spicy.Scoville.Frontend (
  module Spicy.Scoville.Frontend.Language,
  module Spicy.Scoville.Frontend.Compiler,
  module Spicy.Scoville.Frontend.Parser,
)
where

import Spicy.Scoville.Frontend.Compiler
import Spicy.Scoville.Frontend.Language
import Spicy.Scoville.Frontend.Parser
