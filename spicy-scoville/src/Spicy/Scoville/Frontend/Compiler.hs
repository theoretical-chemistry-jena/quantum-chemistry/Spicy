{-# OPTIONS_GHC -Wno-incomplete-uni-patterns #-}

{- |
Module      : Spicy.Scoville.Frontend.Compiler
Copyright   : Phillip Seeber, Sebastian Seidenath 2024
License     : AGPL-3
Maintainer  : sebastian.seidenath@uni-jena.de
Stability   : experimental
Portability : POSIX, Windows

This module defines the functionality needed to compile the expressive but complex frontend language into the simpler core language.
-}
module Spicy.Scoville.Frontend.Compiler (
  compileProgram,
  compileExprF,
) where

import Control.Monad
import RIO.Set qualified as Set
import RIO.Text qualified as T
import Spicy.Prelude
import Spicy.Scoville.Common
import Spicy.Scoville.Core.Language
import Spicy.Scoville.Frontend.Language qualified as F

--------------------------------------------------------------------------------
-- Compiler
--------------------------------------------------------------------------------

{- | Wrapper around 'compileExprF' that takes care of linking different
statements.
-}
compileProgram :: (MonadThrow m) => [F.Statement] -> m ExprC
compileProgram [] = throwM EmptyProgram
compileProgram [x] = case x of
  F.Action a -> pure $ compileExprF a
  F.Bind _ a -> pure $ compileExprF a
  F.Definition _ -> throwM EndOnDefinition
compileProgram (x : xs) = case x of
  F.Action a -> do
    rest <- compileProgram xs
    -- This incomplete pattern is safe since vars is guaranteed to be infinite.
    let (v : _) = filter (not . (`Set.member` freeVars rest)) vars
    return $ Var "Bind" `Ap` compileExprF a `Ap` Lam v rest
  F.Bind n a -> do
    rest <- compileProgram xs
    return $ Var "Bind" `Ap` compileExprF a `Ap` Lam n rest
  F.Definition a -> do
    rest <- compileProgram xs
    return $ Let (lambdafy <$> a) rest
 where
  vars = Name . T.pack <$> ([1 ..] >>= flip replicateM ['a' .. 'z'])

{- | Main compilation function. Notable simplifications are the inlining of
operator primitives, desugaring of if statements and expansion of setters
into functions. Declarations are also simplified by turning them into lambdas.
-}
compileExprF :: F.ExprF -> ExprC
compileExprF = \case
  F.Lit l -> Lit l
  F.Var n -> Var n
  F.Input m -> Input $ compileExprF <$> m
  F.Setter ad ->
    -- This must be tested rigourously…
    let f (x :| []) v =
          (Var "Insert" `Ap` (Lit . LString . getName $ x) `Ap` Var v `Ap` Input mempty)
        f (x :| (y : ys)) v =
          Var "Insert" `Ap` (Lit . LString . getName $ x) `Ap` f (y :| ys) v `Ap` Input mempty
     in Lam "p" . Lam "q" $ (Var "Merge" `Ap` Var "q" `Ap` f ad "p")
  F.Ap x y -> Ap (compileExprF x) (compileExprF y)
  F.Let decls expr -> Let (lambdafy <$> decls) (compileExprF expr)
  F.Case e alts -> Case (compileExprF e) (second compileExprF <$> alts)
  F.Op x op y ->
    -- Binary infix operators are turned into prefix functions
    Ap (Ap (Var $ F.opName op) (compileExprF x)) (compileExprF y)
  F.If x t f ->
    -- Ifs are turned into case statements on True and False
    Case
      (compileExprF x)
      [ (Right . LBool $ True, compileExprF t)
      , (Right . LBool $ False, compileExprF f)
      ]

{- | Convert a declaration in the frontend language into one in the core language
by introducing lambdas, meaning that this takes a declaration of the form
@ f x y = e @ to the form @ f = λx. λy. e @
-}
lambdafy :: F.DecF -> DecC
lambdafy Dec{..} =
  Dec
    { binder = binder
    , bound = ()
    , expression = go bound expression
    }
 where
  go [] e = compileExprF e
  go (n : xs) e = Lam n (go xs e)
