{- |
Module      : Spicy.Scoville.Frontend.Language
Copyright   : Phillip Seeber, Sebastian Seidenath 2024
License     : AGPL-3
Maintainer  : sebastian.seidenath@uni-jena.de
Stability   : experimental
Portability : POSIX, Windows

This module defines the Language used in the frontend of Scoville, i.e. the
language that comes out of the parse.
-}
module Spicy.Scoville.Frontend.Language (
  -- * The Scoville Language
  -- $slang
  Statement (..),
  ExprF (..),
  AltF,
  DecF,
  lBool,
  lInt,
  lDouble,
  lString,
  lFragment,
  lLayer,

  -- * Binary operators
  -- $sbinary
  BinOp (..),
  opRepr,
  opName,
  opPred,
  opType,
) where

import Spicy.Prelude
import Spicy.Scoville.Common
import Spicy.Scoville.Pretty

--------------------------------------------------------------------------------

{- $slang
Scoville source code is parsed into a list of 'Statement's, which each contain
expressions ('ExprF') and/or declarations ('DecF'). Later, these are converted
into a simpler language (see 'Spicy.Scoville.Core') and run. Perhaps in the
future this language will have source annotations and type inference to give
the user more helpful error messages.
-}

{- | A program is essentially a list of statements. The constructors vaguely
correspond to different expressions in do notation.
-}
data Statement
  = -- | Plain action. Only meaningful if the expression does I/O.
    Action ExprF
  | -- | Essentially a monadic bind.
    Bind Name ExprF
  | -- | A declaration for the rest of the program.
    Definition [DecF]
  deriving (Show, Eq)

instance Pretty Statement where
  toPP = \case
    Action a -> ppConcat [toPP a, ";"]
    Bind n a -> ppConcat [toPP n, " <- ", toPP a, ";"]
    Definition ds -> ppConcat ["def ", ppEntries (ppDecF <$> ds)]

instance Pretty [Statement] where
  toPP l = ppLined $ fmap toPP l

-- | This is the main language for the Scoville frontend.
data ExprF
  = -- | Literal, such as @1@ or @True@.
    Lit Literal
  | -- | Variable, standing for some other expression.
    Var Name
  | -- | Application of a function.
    Ap ExprF ExprF
  | -- | Let … in … declaration. The defined functions are generalized.
    Let [DecF] ExprF
  | -- | Case expression. Bound variables are /not/ generalized.
    Case ExprF [AltF]
  | -- | Application of a binary operator such as +, -, <, ==, etc.
    Op ExprF BinOp ExprF
  | -- | If expression.
    If ExprF ExprF ExprF
  | -- | Declaration of an input object. These are generally write-only and exist
    -- only to be fed to Spicy, which will interpret them into calculations.
    Input (Map Name ExprF)
  | -- | Setter for an Input.
    Setter (NonEmpty Name)
  deriving (Show, Eq)

instance Pretty ExprF where
  toPP = go 0
   where
    go p = \case
      Lit l -> toPP l
      Var n -> toPP n
      Ap x y -> ppConcat [toPP x, " ", go 9 y]
      Let decls e ->
        ppConcat ["let", PNewline, "  ", ppEntries (ppDecF <$> decls), "in ", toPP e]
      Case e alts ->
        ppConcat ["case ", toPP e, ";", PNewline, "  ", ppEntries (ppAlt <$> alts)]
      Op x op y ->
        let pred = opPred op
         in if p <= pred
              then ppConcat [go pred x, toPP op, go pred y]
              else ppConcat ["(", go pred x, toPP op, go pred y, ")"]
      If x t f ->
        ppEntries
          [ PAppend "if " $ PIndent (toPP x)
          , PAppend "  then " $ PIndent (toPP t)
          , PAppend "  else " $ PIndent (toPP f)
          ]
      Input m -> toPP m
      Setter ad -> ppConcat ["!", ppIntersperse "." (toPP <$> toList ad)]

type AltF = Alt ExprF
type DecF = Dec [Name] ExprF

lBool :: Bool -> ExprF
lBool = Lit . LBool

lInt :: Int -> ExprF
lInt = Lit . LInt

lDouble :: Double -> ExprF
lDouble = Lit . LDouble

lString :: Text -> ExprF
lString = Lit . LString

lFragment :: IntSet -> ExprF
lFragment = Lit . LFragment

lLayer :: Set IntSet -> ExprF
lLayer = Lit . LLayer

--------------------------------------------------------------------------------

{- $sbinary
Binary operators get special treatment, since they have additional rules that regular prefix function application does not suffer from, such as precedence and associativity.
We also want to be able to print operators as a human would write them.
-}

{- | Binary operations such as addition, comparison etc. All of these are parsed as left-associative.
This representation is only to make the frontend language more in line to how a human would write and read.
These get transformed into simple prefix functions in core.

Implementation note: the ordering of these constructors is significant, since the parser will try them in that order.
In particular, it will happily try parsing "<" and then fail on the "=" of "<=" if these are in the wrong order.
-}
data BinOp where
  Eql :: BinOp
  Neq :: BinOp
  And :: BinOp
  Or :: BinOp
  Xor :: BinOp
  Geq :: BinOp
  Leq :: BinOp
  Grt :: BinOp
  Lst :: BinOp
  Add :: BinOp
  Sub :: BinOp
  Mul :: BinOp
  Div :: BinOp
  Uni :: BinOp
  Isc :: BinOp
  Dif :: BinOp
  deriving (Show, Eq, Enum, Bounded)

instance Pretty BinOp where
  toPP op = PText $ " " <> opRepr op <> " "

-- | The textual representation of an operator.
opRepr :: BinOp -> Text
opRepr = \case
  Eql -> "=="
  Neq -> "/="
  And -> "&&"
  Or -> "||"
  Xor -> "><"
  Geq -> ">="
  Leq -> "<="
  Grt -> ">"
  Lst -> "<"
  Add -> "+"
  Sub -> "-"
  Mul -> "*"
  Div -> "/"
  Uni -> "∪"
  Isc -> "∩"
  Dif -> "\\\\"

{- | Simple helper. The name produced by this function and the name of the
corresponding primitive in the runtime environment must be the same.
-}
opName :: BinOp -> Name
opName = Name . tshow

{- | Get the precedence of an operator, using the precedences as implemented in
Haskell itself where applicable.
-}
opPred :: BinOp -> Int
opPred = \case
  Add -> 6
  Sub -> 6
  Mul -> 7
  Div -> 7
  Eql -> 4
  Neq -> 4
  Grt -> 4
  Lst -> 4
  Geq -> 4
  Leq -> 4
  And -> 3
  Or -> 2
  Xor -> 2
  Uni -> 5
  Isc -> 5
  Dif -> 5

-- | Get the type scheme for an operator.
opType :: BinOp -> Scheme
opType = \case
  Add -> nums
  Sub -> nums
  Mul -> nums
  Div -> nums
  Eql -> eqs
  Neq -> eqs
  Grt -> ords
  Lst -> ords
  Geq -> ords
  Leq -> ords
  And -> logics
  Or -> logics
  Xor -> logics
  Uni -> setlike
  Isc -> setlike
  Dif -> setlike
 where
  nums =
    Forall
      [TName "a"]
      [OneOf [TInt, TDouble] (TVar $ TName "a")]
      (TVar "a" :-> TVar "a" :-> TVar "a")
  eqs =
    Forall
      [TName "a"]
      [OneOf [TBool, TInt, TDouble, TString, TFragment, TLayer, TInput] (TVar $ TName "a")]
      (TVar "a" :-> TVar "a" :-> tBool)
  ords =
    Forall
      [TName "a"]
      [OneOf [TInt, TDouble, TString] (TVar $ TName "a")]
      (TVar "a" :-> TVar "a" :-> tBool)
  setlike =
    Forall
      [TName "a"]
      [OneOf [TFragment, TLayer, TInput] (TVar $ TName "a")]
      (TVar "a" :-> TVar "a" :-> TVar "a")
  logics = Forall [] [] (tBool :-> tBool :-> tBool)
