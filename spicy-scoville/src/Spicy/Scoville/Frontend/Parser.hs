{- |
Module      : Spicy.Scoville.Frontend.Parser
Copyright   : Phillip Seeber, Sebastian Seidenath 2024
License     : AGPL-3
Maintainer  : sebastian.seidenath@uni-jena.de
Stability   : experimental
Portability : POSIX, Windows

This module contains the parser for Scoville, which takes the source code read from a file into an abstract syntax tree.
-}
module Spicy.Scoville.Frontend.Parser (
  pProgram,
  pExpr,
) where

import Control.Applicative
import Data.Map qualified as Map
import RIO.Char
import RIO.Set qualified as Set
import Spicy.IntSet qualified as IntSet
import Spicy.Prelude
import Spicy.Scoville.Common
import Spicy.Scoville.Frontend.Language
import Spicy.Textual hiding (lex, (**>), (<**))

{- | This overrides the definition of 'Spicy.Textual.lex', with the difference
being that this 'lex' also skips over line breaks (which Scoville does not
care about).
-}
lex :: Parser a -> Parser a
lex p = skipSpace *> p

-- | Modified version of 'Spicy.Textual.(<**)'. See 'lex' for more information.
(<**) :: Parser a -> Parser b -> Parser a
x <** y = x <* lex y

-- | Modified version of 'Spicy.Textual.(**>)'. See 'lex' for more information.
(**>) :: Parser a -> Parser b -> Parser b
x **> y = x *> lex y

{- | Parses a program identifier, i.e. a variable. This only accepts identifies
consisting purely of letters, meaning no numbers or special characters.
-}
pName :: Parser Name
pName = Name <$> takeWhile1 isAlpha

{- | Combines a parser for tokens and a parser for operators into a parser that
will parse expressions in a left-associative fashion. Must parse at least one
token to succeed.

This function is more or less taken from the parsers package.
-}
chainl1 :: (Alternative p) => p a -> p (a -> a -> a) -> p a
chainl1 p op = p <**> rst
 where
  rst = (\f y g x -> g (f x y)) <$> op <*> p <*> rst <|> pure id

--------------------------------------------------------------------------------

-- | Top level parser for a programme.
pProgram :: Parser [Statement]
pProgram = sepBy1' pStatement (lex ";" **> skipSpace)

pStatement :: Parser Statement
pStatement = choice [pDefinition, pBind, pAction]

pBind :: Parser Statement
pBind = do
  n <- pName <** "<-"
  e <- lex pExpr
  return $ Bind n e

pAction :: Parser Statement
pAction = Action <$> pExpr

pDefinition :: Parser Statement
pDefinition = do
  void "def "
  Definition <$> sepBy1' pDecl (lex ";" **> skipSpace)

--------------------------------------------------------------------------------

{- $pexpressions
Parsers for individual expressions.
-}

-- | Parse an expression.
pExpr :: Parser ExprF
pExpr = pPred 2

{- | Parses an expression made of subexpressions connected by operators of
precedence @i@ or higher.
-}
pPred :: Int -> Parser ExprF
pPred i
  | i >= 7 = chainl1 pPredInf $ pOpWithPred 7
  | i < 2 = pPred 2
  | otherwise = chainl1 (pPred $ i + 1) $ pOpWithPred i

{- | Parses a token, i.e. an expression that does not contain binary 'Op'erators
at the top level (though it may contain them in subexpressions).
-}
pToken :: Parser ExprF
pToken = lex $ choice [pParens, pSetter, pLet, pCase, pIf, pInput, pLit, pVar]

-- | Parses an expression surrounded by parenthesis.
pParens :: Parser ExprF
pParens = "(" **> pExpr <** ")"

--------------------------------------------------------------------------------

{- $pconstructors
Parsers that correspond directly to the constructors of the 'ExprF' type.
-}

-- Parse a 'Lit'eral.
pLit :: Parser ExprF
pLit = Lit <$> pLit'

-- | Auxilliary parser that returns an unwrapped 'Literal'.
pLit' :: Parser Literal
pLit' = choice [pDouble, pInt, pString, pBool, pFragment, pLayer]
 where
  pInt = LInt <$> signed decimal
  pDouble = LDouble <$> double
  pString = LString <$> ("\"" *> takeTill (== '\"') <* "\"")
  pFragment = LFragment <$> pFragment'
  pFragment' = IntSet.fromList <$> "{" **> many' (lex $ signed decimal) <** "}"
  pLayer = LLayer . Set.fromList <$> "{" **> many' (lex pFragment') <** "}"
  pBool = pTrue <|> pFalse
  pTrue = do
    fortran <- optional "."
    void $ asciiCI "true"
    when (isJust fortran) (void ".")
    return $ LBool True
  pFalse = do
    fortran <- optional "."
    void $ asciiCI "false"
    when (isJust fortran) (void ".")
    return $ LBool False

-- | Parse a 'Var'iable ocurring in an expression.
pVar :: Parser ExprF
pVar = Var <$> pName

-- | Parse a 'Let' expression.
pLet :: Parser ExprF
pLet = do
  _ <- "let"
  ds <- lex $ many' (lex pDecl <** ";")
  _ <- lex "in"
  e <- lex pExpr
  return $ Let ds e

-- | Parse a single declaration inside a let.
pDecl :: Parser DecF
pDecl = do
  binder <- pName
  bound <- many' (lex pName)
  _ <- lex "="
  expression <- lex pExpr
  return $ Dec{..}

-- | Parse a 'Case' expression.
pCase :: Parser ExprF
pCase = do
  e <- "case" **> pExpr <** ";"
  alts <- lex $ many' (lex pAlt <** ";")
  return $ Case e alts

-- | Parse a single alternative inside a case expression.
pAlt :: Parser AltF
pAlt = do
  x <- pCaseLit <|> pCaseVar
  e <- lex "->" **> pExpr
  return (x, e)
 where
  pCaseVar = Left <$> pName
  pCaseLit = Right <$> pLit'

-- | Parse an 'If' statement.
pIf :: Parser ExprF
pIf = do
  x <- "if" **> pExpr <** ";"
  t <- lex "then" **> pExpr <** ";"
  f <- lex "else" **> pExpr <** ";"
  return $ If x t f

-- | Parse an 'Input' block.
pInput :: Parser ExprF
pInput = Input . Map.fromList <$> "[" **> many' (lex pEntry) <** "]"
 where
  pEntry = do
    k <- pName <** "="
    v <- lex pExpr <** ";"
    return (k, v)

-- | Parse a 'Setter'.
pSetter :: Parser ExprF
pSetter = do
  x : xs <- "!" *> sepBy1' pName (char '.')
  return . Setter $ x :| xs

-- | Parses all 'BinOp's with a given precedence.
pOpWithPred :: Int -> Parser (ExprF -> ExprF -> ExprF)
pOpWithPred i = choice $ lex . pOp <$> filter ((i ==) . opPred) ops
 where
  ops = [minBound .. maxBound]

{- | Parses application between tokens, which is considered a binary operation
with infinite precedence.
-}
pPredInf :: Parser ExprF
pPredInf = chainl1 pToken pApp

-- | Basic parser for application.
pApp :: Parser (ExprF -> ExprF -> ExprF)
pApp = many1' " " $> Ap

-- | Parses a binary operator.
pOp :: BinOp -> Parser (ExprF -> ExprF -> ExprF)
pOp op = string (opRepr op) $> flip Op op
