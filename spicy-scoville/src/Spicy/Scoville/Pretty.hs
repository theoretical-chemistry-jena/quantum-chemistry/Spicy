{- |
Module      : Spicy.Scoville.Pretty
Copyright   : Phillip Seeber, Sebastian Seidenath 2024
License     : AGPL-3
Maintainer  : sebastian.seidenath@uni-jena.de
Stability   : experimental
Portability : POSIX, Windows

This module implements a simple prettyprinter based on the [Wadler/Leijen algorithm](https://homepages.inf.ed.ac.uk/wadler/papers/prettier/prettier.pdf).
In order to pretty print a type, only an instence of the `Pretty` class is needed.
Here, some basic instances are defined, and several utilities are provided to make the definition of new instances a cinch.

This module completely ignores the existence of carriage return in newlines on Windows.
-}
module Spicy.Scoville.Pretty (
  -- * Pretty Printing Infrastructure
  -- $ppinfra
  PP (..),
  Pretty (..),
  format,

  -- * Pretty Printing Utilities
  -- $pputil
  ppConcat,
  ppIntersperse,
  ppLined,
  ppSpaced,
  ppEntries,
  ppThen,
  ppAlt,
  ppDec,
  ppDecC,
  ppDecF,
) where

import Data.Foldable
import Data.Map.Lazy qualified as Map
import Data.Text qualified as T
import RIO.Set qualified as Set
import Spicy.IntSet qualified as IntSet
import Spicy.Prelude
import Spicy.Scoville.Common

--------------------------------------------------------------------------------

{- $ppinfra
The pretty printing algorithm (inspired by [Simon Peyton Jones](https://www.microsoft.com/en-us/research/publication/implementing-functional-languages-a-tutorial/), based on an [algorithm by Wadler] (https://homepages.inf.ed.ac.uk/wadler/papers/prettier/prettier.pdf)) works by first translating a type to an intermediate value of type `PP`, which is then "typeset" by the `format` function.
Any type that is to be pretty printed should be made an instance of the 'Pretty' class.
When defining instances of that class, take note of the utilities defined here, in particular 'ppConcat'.
-}

{- | The intermediate representation of a structure to be pretty-printed. Note
that this is an instance of `IsString`.
-}
data PP
  = -- | Simply a text, which will be output verbatim. Note an
    -- important invariant: this should not contain newlines. The 'IsString'
    -- instance will handle this correctly, but be careful when directly using
    -- this constructor.
    PText Text
  | -- | Concatenation of two 'PP' values.
    PAppend PP PP
  | -- | A newline.
    PNewline
  | -- | The enclosed 'PP' will be indented at the current depth.
    PIndent PP

instance IsString PP where
  fromString = toPP . T.pack

-- | A simple class to provide convenient overloading for 'toPP'.
class Pretty a where
  toPP :: a -> PP

instance Pretty Bool where
  toPP = PText . tshow

instance Pretty Int where
  toPP = PText . tshow

instance Pretty Double where
  toPP = PText . tshow

instance Pretty IntSet where
  toPP f = ppConcat ["{ ", ppSpaced $ toPP <$> IntSet.toList f, " }"]

instance (Pretty a) => Pretty (Set a) where
  toPP s = ppConcat ["{ ", ppSpaced $ toPP <$> Set.toList s, " }"]

instance (Pretty k, Pretty v) => Pretty (Map k v) where
  toPP m = ppConcat ["[", PNewline, "  ", ppEntries (f <$> Map.toList m), "]"]
   where
    f (n, a) = ppConcat [toPP n, " = ", toPP a]

instance Pretty Text where
  toPP t = case T.lines t of
    [] -> PText ""
    xs -> foldr1 PAppend (PText <$> xs)

instance Pretty Name where
  toPP = toPP . getName

instance Pretty TName where
  toPP = toPP . getTName

instance Pretty Literal where
  toPP = \case
    LBool b -> PText . tshow $ b
    LInt i -> PText . tshow $ i
    LDouble d -> PText . tshow $ d
    LString s -> PText $ "\"" <> s <> "\""
    LFragment f -> toPP f
    LLayer l -> toPP l

instance Pretty ScovilleType where
  toPP = \case
    TCon t -> toPP t
    TVar v -> toPP v
    a :-> b -> ppConcat [toPP a, " -> ", toPP b]

instance Pretty TypeConstant where
  toPP = \case
    TUnit -> "()"
    TBool -> "Bool"
    TInt -> "Int"
    TDouble -> "Double"
    TString -> "String"
    TFragment -> "Fragment"
    TLayer -> "Layer"
    TInput -> "Input"
    TIO -> "I/O Action"

-- | The core function for converting a 'PP' value into a text to be printed.
format :: PP -> Text
format = flatten 0 . (: []) . (0 :: Int,)
 where
  spaces n = T.replicate n " "
  flatten ::
    Int -> -- The current column.
    [(Int, PP)] -> -- The 'PP' to be processed, bundled with their indentation.
    Text
  flatten _ [] = ""
  flatten c ((_, PText t) : rest) = t <> flatten (c + T.length t) rest
  flatten c ((i, PAppend x y) : rest) = flatten c ((i, x) : (i, y) : rest)
  flatten c ((_, PIndent x) : rest) = flatten c ((c, x) : rest)
  flatten _ ((i, PNewline) : rest) = "\n" <> spaces i <> flatten i rest

--------------------------------------------------------------------------------

{- $pputils
Here we provide several simple helper function for defining instances of 'Pretty'.
In particular, 'ppConcat' and 'ppIntersperse' are to be preferred over manual recursion.
-}

{- | Helper function for the common case of wanting to concatenate multiple
'PP' values together.
-}
ppConcat :: [PP] -> PP
ppConcat = \case
  [] -> PText ""
  [x] -> x -- This alternative is just here to prevent (PAppend x (PText "")).
  (x : xs) -> PAppend x (ppConcat xs)

{- | Helper function for the common case of wanting to insert a `PP` in between
 each element of a list of `PP`s.
-}
ppIntersperse :: PP -> [PP] -> PP
ppIntersperse i = \case
  [] -> PText ""
  [x] -> x -- Don't insert the separator at the end!
  (x : xs) -> ppConcat [x, i] `PAppend` ppIntersperse i xs

{- | Specialization of 'ppIntersperse' to newlines: this function inserts a
'PNewline' between each element of the input list.
-}
ppLined :: [PP] -> PP
ppLined = ppIntersperse PNewline

{- | Specialization of 'ppIntersperse' to spaces: this function inserts a
single space between each element of the input list.
-}
ppSpaced :: [PP] -> PP
ppSpaced = ppIntersperse " "

{- | Helper function for printing the entries of a case expression, let
 statement, etc. Inserts semicolons and newlines after each entry, including
 the final one.
-}
ppEntries :: [PP] -> PP
ppEntries xs0 =
  let xs = fmap (ppThen ";") xs0
   in ppConcat [PIndent (ppLined xs), PNewline]

-- | Minor sugar for appending text.
ppThen :: PP -> PP -> PP
ppThen = flip PAppend

-- | Pretty print an alternative in a case expression.
ppAlt :: (Pretty a) => Alt a -> PP
ppAlt (Left n, e) = ppConcat [toPP n, " -> ", toPP e]
ppAlt (Right l, e) = ppConcat [toPP l, " -> ", toPP e]

-- | Pretty print a `Dec`laration.
ppDec :: (Pretty a) => (b -> PP) -> Dec b a -> PP
ppDec f Dec{..} = ppConcat [toPP binder, f bound, " = ", toPP expression]

-- | `ppDec` specialized to the declarations used in core Scoville.
ppDecC :: (Pretty a) => Dec () a -> PP
ppDecC = ppDec $ const ""

-- | `ppDec` specialized to the declarations used in frontend Scoville.
ppDecF :: (Pretty a) => Dec [Name] a -> PP
ppDecF = ppDec $ ppConcat . fmap (toPP . (" " <>))
