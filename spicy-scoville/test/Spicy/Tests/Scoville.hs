module Spicy.Tests.Scoville (tests) where

import Spicy.Tests.Scoville.Core.Evaluator qualified
import Spicy.Tests.Scoville.Core.Value qualified
import Spicy.Tests.Scoville.Frontend.Parser qualified
import Spicy.Tests.Scoville.Frontend.Printing qualified
import Test.Tasty

tests :: TestTree
tests =
  testGroup
    "Scoville"
    [ Spicy.Tests.Scoville.Frontend.Parser.tests
    , Spicy.Tests.Scoville.Frontend.Printing.tests
    , Spicy.Tests.Scoville.Core.Evaluator.tests
    , Spicy.Tests.Scoville.Core.Value.tests
    ]
