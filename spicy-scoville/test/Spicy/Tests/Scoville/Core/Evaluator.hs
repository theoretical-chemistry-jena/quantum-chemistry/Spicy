module Spicy.Tests.Scoville.Core.Evaluator (tests) where

import RIO.Text qualified as T
import Spicy.Prelude hiding (assert)
import Spicy.Scoville
import Spicy.Tests.Scoville.Programs
import Spicy.Textual
import Test.Tasty
import Test.Tasty.HUnit

tests :: TestTree
tests =
  testGroup
    "Evaluation"
    [ trivialEvaluationTests
    , operatorEvaluationTests
    , precedenceEvaluationTests
    , syntaxEvaluationTests
    ]

toEvaluationTest :: (HasResult a ~ Value) => TestProgram a -> TestTree
toEvaluationTest TestProgram{..} = testCase (T.unpack name) $ do
  p <- parse' pProgram source
  r <- eval p
  assertBool "" $ r `valEq` expectedResult

trivialEvaluationTests :: TestTree
trivialEvaluationTests = testGroup "Trivial" $ toEvaluationTest <$> trivialPrograms

operatorEvaluationTests :: TestTree
operatorEvaluationTests = testGroup "Operators" $ toEvaluationTest <$> simpleOperatorPrograms

precedenceEvaluationTests :: TestTree
precedenceEvaluationTests = testGroup "Precedence" $ toEvaluationTest <$> precedencePrograms

syntaxEvaluationTests :: TestTree
syntaxEvaluationTests = testGroup "Syntax" $ toEvaluationTest <$> simpleSyntaxPrograms
