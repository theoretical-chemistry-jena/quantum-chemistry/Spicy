module Spicy.Tests.Scoville.Core.Value (tests) where

import Control.Monad.Validate
import RIO.Map qualified as Map
import Spicy.Prelude hiding (assert)
import Spicy.Scoville
import Test.Tasty
import Test.Tasty.HUnit

tests :: TestTree
tests =
  testGroup
    "Value Conversion"
    [t1, t2, t3, t4, t5, t6]

convert :: forall a. (FromValue a) => Value -> Either (Set ConversionException) a
convert v = validateToError $ fromValue @a v

data Subtarget = Subtarget
  { c :: Int
  , d :: Double
  }
  deriving (Show)

instance FromValue Subtarget where
  fromValue v = do
    m <- fromValue v
    c <- m ? "c"
    d <- m ? "d"
    pure $ Subtarget{..}

data Target = Target
  { a :: Text
  , b :: Subtarget
  }
  deriving (Show)

instance FromValue Target where
  fromValue v = do
    m <- fromValue v
    a <- m ? "a"
    b <- m ? "b"
    pure $ Target{..}

t1 :: TestTree
t1 = testCase "Record conversion" $ do
  let c = VInt 200
      d = VDouble 1e-5
      v = VInput $ Map.fromList [("c", c), ("d", d)]
  case convert @Subtarget v of
    Right _ -> pure ()
    Left _ -> assertFailure "Could not convert!"

t2 :: TestTree
t2 = testCase "Record conversion (wrong field type)" $ do
  let c = VBool True
      d = VDouble 1e-5
      v = VInput $ Map.fromList [("c", c), ("d", d)]
  case convert @Subtarget v of
    Right _ -> assertFailure "Converted despite wrong field type!"
    Left _ -> pure ()

t3 :: TestTree
t3 = testCase "Record conversion (missing field)" $ do
  let c = VInt 200
      v = VInput $ Map.fromList [("c", c)]
  case convert @Subtarget v of
    Right _ -> assertFailure "Converted despite missing field!"
    Left _ -> pure ()

t4 :: TestTree
t4 = testCase "Record conversion (irrelevant field)" $ do
  let c = VInt 200
      d = VDouble 1e-5
      e = VBool False
      v = VInput $ Map.fromList [("c", c), ("d", d), ("e", e)]
  case convert @Subtarget v of
    Right _ -> pure ()
    Left _ -> assertFailure "Could not convert!"

t5 :: TestTree
t5 = testCase "Record conversion (wrong type altogether)" $ do
  let v = VBool False
  case convert @Subtarget v of
    Right _ -> assertFailure "Converted despite wrong type!"
    Left _ -> pure ()

t6 :: TestTree
t6 = testCase "Nested Records" $ do
  let c = VInt 200
      d = VDouble 1e-5
      v = VInput $ Map.fromList [("a", a), ("b", w)]
      w = VInput $ Map.fromList [("c", c), ("d", d)]
      a = VString "Hello"
  case convert @Target v of
    Right _ -> pure ()
    Left _ -> assertFailure "Could not convert!"
