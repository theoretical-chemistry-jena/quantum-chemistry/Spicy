module Spicy.Tests.Scoville.Frontend.Parser (tests) where

import RIO.Text qualified as T
import Spicy.Prelude
import Spicy.Scoville
import Spicy.Tests.Scoville.Programs
import Spicy.Textual
import Test.Tasty
import Test.Tasty.HUnit

tests :: TestTree
tests =
  testGroup
    "Parsing"
    [ trivialParsingTests
    , operatorParsingTests
    , precedenceParsingTests
    , syntaxParsingTests
    ]

toParsingTest :: (HasFrontend a ~ [Statement]) => TestProgram a -> TestTree
toParsingTest TestProgram{..} = testCase (T.unpack name) $ do
  p <- parse' pProgram source
  p @?= expectedFrontend

trivialParsingTests :: TestTree
trivialParsingTests = testGroup "Trivial" $ toParsingTest <$> trivialPrograms

operatorParsingTests :: TestTree
operatorParsingTests = testGroup "Operators" $ toParsingTest <$> simpleOperatorPrograms

precedenceParsingTests :: TestTree
precedenceParsingTests = testGroup "Precedence" $ toParsingTest <$> precedencePrograms

syntaxParsingTests :: TestTree
syntaxParsingTests =
  testGroup "Syntax"
    $ (toParsingTest <$> simpleSyntaxPrograms)
    <> (toParsingTest <$> functionSyntaxPrograms)
