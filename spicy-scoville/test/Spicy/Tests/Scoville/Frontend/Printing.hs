module Spicy.Tests.Scoville.Frontend.Printing (tests) where

import RIO.Text qualified as T
import Spicy.Prelude
import Spicy.Scoville
import Spicy.Tests.Scoville.Programs
import Spicy.Textual
import Test.Tasty
import Test.Tasty.HUnit

tests :: TestTree
tests =
  testGroup
    "Printing"
    [ trivialPrintingTests
    , operatorPrintingTests
    , precedencePrintingTests
    , syntaxPrintingTests
    ]

toPrintingTest :: (HasFrontend a ~ [Statement]) => TestProgram a -> TestTree
toPrintingTest TestProgram{..} = testCase (T.unpack name) $ do
  let printedSource = format . toPP $ expectedFrontend
  parsedSource <- parse' pProgram printedSource
  parsedSource @?= expectedFrontend

trivialPrintingTests :: TestTree
trivialPrintingTests = testGroup "Trivial" $ toPrintingTest <$> trivialPrograms

operatorPrintingTests :: TestTree
operatorPrintingTests = testGroup "Operators" $ toPrintingTest <$> simpleOperatorPrograms

precedencePrintingTests :: TestTree
precedencePrintingTests = testGroup "Precedence" $ toPrintingTest <$> precedencePrograms

syntaxPrintingTests :: TestTree
syntaxPrintingTests =
  testGroup "Syntax"
    $ (toPrintingTest <$> simpleSyntaxPrograms)
    <> (toPrintingTest <$> functionSyntaxPrograms)
