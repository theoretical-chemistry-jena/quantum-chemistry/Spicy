module Spicy.Tests.Scoville.Programs (
  module Spicy.Tests.Scoville.Programs.Common,
  module Spicy.Tests.Scoville.Programs.Operators,
  module Spicy.Tests.Scoville.Programs.Precedence,
  module Spicy.Tests.Scoville.Programs.Syntax,
  module Spicy.Tests.Scoville.Programs.Trivial,
) where

import Spicy.Tests.Scoville.Programs.Common
import Spicy.Tests.Scoville.Programs.Operators
import Spicy.Tests.Scoville.Programs.Precedence
import Spicy.Tests.Scoville.Programs.Syntax
import Spicy.Tests.Scoville.Programs.Trivial
