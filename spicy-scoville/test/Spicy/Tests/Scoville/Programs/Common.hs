module Spicy.Tests.Scoville.Programs.Common (
  TestProgram (..),
  ProgramType (..),
  HasFrontend,
  HasResult,
  HasType,
  singleInstruction,
) where

import Spicy.Prelude
import Spicy.Scoville

data ProgramType = Regular | Simple | Function | InvalidSyntax | InvalidType

type family HasFrontend (pt :: ProgramType) where
  HasFrontend 'Regular = ()
  HasFrontend 'Simple = [Statement]
  HasFrontend 'Function = [Statement]
  HasFrontend 'InvalidSyntax = ()
  HasFrontend 'InvalidType = [Statement]

type family HasResult (pt :: ProgramType) where
  HasResult 'Regular = Value
  HasResult 'Simple = Value
  HasResult 'Function = ()
  HasResult 'InvalidSyntax = ()
  HasResult 'InvalidType = ()

type family HasType (pt :: ProgramType) where
  HasType 'Regular = ScovilleType
  HasType 'Simple = ScovilleType
  HasType 'Function = ScovilleType
  HasType 'InvalidSyntax = ()
  HasType 'InvalidType = ()

data TestProgram (pt :: ProgramType) = TestProgram
  { name :: Text
  , source :: Text
  , expectedFrontend :: HasFrontend pt
  , expectedResult :: HasResult pt
  , expectedType :: HasType pt
  }

singleInstruction :: ExprF -> [Statement]
singleInstruction = (: []) . Action
