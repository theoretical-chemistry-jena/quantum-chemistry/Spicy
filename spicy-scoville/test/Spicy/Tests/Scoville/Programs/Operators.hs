module Spicy.Tests.Scoville.Programs.Operators where

import Data.IntSet qualified as IntSet
import Spicy.Prelude
import Spicy.Scoville
import Spicy.Tests.Scoville.Programs.Common

simpleOperatorPrograms :: [TestProgram 'Simple]
simpleOperatorPrograms =
  [ simpleNegation
  , simpleConjunction
  , simpleDisjunction
  , simpleExclusiveDisjunction
  , simpleAddition
  , simpleSubtraction
  , simpleMultiplication
  , simpleIntegerDivision
  , simpleIntegerDivisionWithRemainder
  , simpleEquivalence
  , simpleNonequivalence
  , simpleGreaterThan
  , simpleLessThan
  , simpleGreaterThanOrEqualTo
  , simpleLessThanOrEqualTo
  , simpleUnion
  , simpleIntersection
  , simpleDifference
  ]

simpleNegation :: TestProgram 'Simple
simpleNegation =
  TestProgram
    { name = "Simple logical negation (NOT)"
    , source = "Negate 1"
    , expectedFrontend = singleInstruction $ Ap (Var "Negate") (lInt 1)
    , expectedResult = VInt (-1)
    , expectedType = tInt
    }
simpleConjunction :: TestProgram 'Simple
simpleConjunction =
  TestProgram
    { name = "Simple conjunction (&&)"
    , source = "True && True"
    , expectedFrontend = [Action $ Op (lBool True) And (lBool True)]
    , expectedResult = VBool True
    , expectedType = tBool
    }
simpleDisjunction :: TestProgram 'Simple
simpleDisjunction =
  TestProgram
    { name = "Simple disjunction (||)"
    , source = "True || False"
    , expectedFrontend = [Action $ Op (lBool True) Or (lBool False)]
    , expectedResult = VBool True
    , expectedType = tBool
    }
simpleExclusiveDisjunction :: TestProgram 'Simple
simpleExclusiveDisjunction =
  TestProgram
    { name = "Simple exclusive disjunction (><)"
    , source = "True >< False"
    , expectedFrontend = [Action $ Op (lBool True) Xor (lBool False)]
    , expectedResult = VBool True
    , expectedType = tBool
    }
simpleAddition :: TestProgram 'Simple
simpleAddition =
  TestProgram
    { name = "Simple addition (+)"
    , source = "1 + 1"
    , expectedFrontend = [Action $ Op (lInt 1) Add (lInt 1)]
    , expectedResult = VInt 2
    , expectedType = tInt
    }
simpleSubtraction :: TestProgram 'Simple
simpleSubtraction =
  TestProgram
    { name = "Simple subtraction (-)"
    , source = "1 - 1"
    , expectedFrontend = [Action $ Op (lInt 1) Sub (lInt 1)]
    , expectedResult = VInt 0
    , expectedType = tInt
    }
simpleMultiplication :: TestProgram 'Simple
simpleMultiplication =
  TestProgram
    { name = "Simple multiplication (*)"
    , source = "1 * 1"
    , expectedFrontend = [Action $ Op (lInt 1) Mul (lInt 1)]
    , expectedResult = VInt 1
    , expectedType = tInt
    }
simpleIntegerDivision :: TestProgram 'Simple
simpleIntegerDivision =
  TestProgram
    { name = "Simple integer division (/)"
    , source = "1 / 1"
    , expectedFrontend = [Action $ Op (lInt 1) Div (lInt 1)]
    , expectedResult = VInt 1
    , expectedType = tInt
    }
simpleIntegerDivisionWithRemainder :: TestProgram 'Simple
simpleIntegerDivisionWithRemainder =
  TestProgram
    { name = "Simple integer division (/) with remainder"
    , source = "3 / 2"
    , expectedFrontend = [Action $ Op (lInt 3) Div (lInt 2)]
    , expectedResult = VInt 1
    , expectedType = tInt
    }
simpleEquivalence :: TestProgram 'Simple -- TODO: have one for each type
simpleEquivalence =
  TestProgram
    { name = "Simple equivalence (==)"
    , source = "True == True"
    , expectedFrontend = [Action $ Op (lBool True) Eql (lBool True)]
    , expectedResult = VBool True
    , expectedType = tBool
    }
simpleNonequivalence :: TestProgram 'Simple
simpleNonequivalence =
  TestProgram
    { name = "Simple nonequivalence (/=)"
    , source = "True /= False"
    , expectedFrontend = [Action $ Op (lBool True) Neq (lBool False)]
    , expectedResult = VBool True
    , expectedType = tBool
    }
simpleGreaterThan :: TestProgram 'Simple
simpleGreaterThan =
  TestProgram
    { name = "Simple greater than relation (>)"
    , source = "1 > 0"
    , expectedFrontend = [Action $ Op (lInt 1) Grt (lInt 0)]
    , expectedResult = VBool True
    , expectedType = tBool
    }
simpleLessThan :: TestProgram 'Simple
simpleLessThan =
  TestProgram
    { name = "Simple less than relation (<)"
    , source = "0 < 1"
    , expectedFrontend = [Action $ Op (lInt 0) Lst (lInt 1)]
    , expectedResult = VBool True
    , expectedType = tBool
    }
simpleGreaterThanOrEqualTo :: TestProgram 'Simple -- TODO: Test case for equality
simpleGreaterThanOrEqualTo =
  TestProgram
    { name = "Simple greater than or equal to relation (>=)"
    , source = "1 >= 0"
    , expectedFrontend = [Action $ Op (lInt 1) Geq (lInt 0)]
    , expectedResult = VBool True
    , expectedType = tBool
    }
simpleLessThanOrEqualTo :: TestProgram 'Simple
simpleLessThanOrEqualTo =
  TestProgram
    { name = "Simple less than or equal to relation (<=)"
    , source = "0 <= 1"
    , expectedFrontend = [Action $ Op (lInt 0) Leq (lInt 1)]
    , expectedResult = VBool True
    , expectedType = tBool
    }
simpleUnion :: TestProgram 'Simple -- TODO: Have one for each type
simpleUnion =
  TestProgram
    { name = "Simple set union (∪)"
    , source = "{0} ∪ {1}"
    , expectedFrontend =
        [ Action
            $ Op
              (lFragment $ IntSet.fromList [0])
              Uni
              (lFragment $ IntSet.fromList [1])
        ]
    , expectedResult = VFragment $ IntSet.fromList [0, 1]
    , expectedType = tFragment
    }
simpleIntersection :: TestProgram 'Simple
simpleIntersection =
  TestProgram
    { name = "Simple set intersection (∩)"
    , source = "{0} ∩ {0}"
    , expectedFrontend =
        [ Action
            $ Op
              (lFragment $ IntSet.fromList [0])
              Isc
              (lFragment $ IntSet.fromList [0])
        ]
    , expectedResult = VFragment $ IntSet.fromList [0]
    , expectedType = tFragment
    }
simpleDifference :: TestProgram 'Simple
simpleDifference =
  TestProgram
    { name = "Simple set difference (\\\\)"
    , source = "{0} \\\\ {0}"
    , expectedFrontend =
        [ Action
            $ Op
              (lFragment $ IntSet.fromList [0])
              Dif
              (lFragment $ IntSet.fromList [0])
        ]
    , expectedResult = VFragment IntSet.empty
    , expectedType = tFragment
    }
