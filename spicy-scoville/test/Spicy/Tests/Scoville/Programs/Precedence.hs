module Spicy.Tests.Scoville.Programs.Precedence (precedencePrograms) where

import Data.IntSet qualified as IntSet
import Spicy.Prelude
import Spicy.Scoville
import Spicy.Tests.Scoville.Programs.Common

precedencePrograms :: [TestProgram 'Simple]
precedencePrograms =
  [ leftAssociativitySubtraction
  , leftAssociativityDivision
  , leftAssociativityDifference
  , parenthesisOverPrecedence
  , conjunctionBeforeDisjunction
  , conjunctionBeforeExclusiveDisjunction
  , multiplicationBeforeAddition
  , multiplicationBeforeSubtraction
  , divisionBeforeAddition
  , divisionBeforeSubtraction
  ]

leftAssociativitySubtraction :: TestProgram 'Simple
leftAssociativitySubtraction =
  TestProgram
    { name = "Left-associativity of addition / subtraction"
    , source = "3 - 2 + 1"
    , expectedFrontend = singleInstruction $ Op (Op (lInt 3) Sub (lInt 2)) Add (lInt 1)
    , expectedResult = VInt 2
    , expectedType = tInt
    }

leftAssociativityDivision :: TestProgram 'Simple
leftAssociativityDivision =
  TestProgram
    { name = "Left-associativity of multiplication / division"
    , source = "2 / 1 * 2"
    , expectedFrontend = singleInstruction $ Op (Op (lInt 2) Div (lInt 1)) Mul (lInt 2)
    , expectedResult = VInt 4
    , expectedType = tInt
    }

leftAssociativityDifference :: TestProgram 'Simple
leftAssociativityDifference =
  let n = lFragment $ IntSet.fromList [0]
   in TestProgram
        { name = "Left-associativity of set difference"
        , source = "{0} \\\\ {0} \\\\ {0}"
        , expectedFrontend = singleInstruction $ Op (Op n Dif n) Dif n
        , expectedResult = VFragment IntSet.empty
        , expectedType = tFragment
        }

parenthesisOverPrecedence :: TestProgram 'Simple
parenthesisOverPrecedence =
  TestProgram
    { name = "Parenthesis override operator precedence"
    , source = "(1 + 2) * 3"
    , expectedFrontend = singleInstruction $ Op (Op (lInt 1) Add (lInt 2)) Mul (lInt 3)
    , expectedResult = VInt 9
    , expectedType = tInt
    }

conjunctionBeforeDisjunction :: TestProgram 'Simple
conjunctionBeforeDisjunction =
  TestProgram
    { name = "&& has higher precedence than ||"
    , source = "True || True && False"
    , expectedFrontend = singleInstruction $ Op (lBool True) Or (Op (lBool True) And (lBool False))
    , expectedResult = VBool True
    , expectedType = tBool
    }

conjunctionBeforeExclusiveDisjunction :: TestProgram 'Simple
conjunctionBeforeExclusiveDisjunction =
  TestProgram
    { name = "&& has higher precedence than ><"
    , source = "True >< True && False"
    , expectedFrontend = singleInstruction $ Op (lBool True) Xor (Op (lBool True) And (lBool False))
    , expectedResult = VBool True
    , expectedType = tBool
    }

multiplicationBeforeAddition :: TestProgram 'Simple
multiplicationBeforeAddition =
  TestProgram
    { name = "* has higher precedence than +"
    , source = "1 + 2 * 3"
    , expectedFrontend = singleInstruction $ Op (lInt 1) Add (Op (lInt 2) Mul (lInt 3))
    , expectedResult = VInt 7
    , expectedType = tInt
    }

multiplicationBeforeSubtraction :: TestProgram 'Simple
multiplicationBeforeSubtraction =
  TestProgram
    { name = "* has higher precedence than -"
    , source = "1 - 2 * 3"
    , expectedFrontend = singleInstruction $ Op (lInt 1) Sub (Op (lInt 2) Mul (lInt 3))
    , expectedResult = VInt (-5)
    , expectedType = tInt
    }

divisionBeforeAddition :: TestProgram 'Simple
divisionBeforeAddition =
  TestProgram
    { name = "/ has higher precedence than +"
    , source = "2 + 2 / 3"
    , expectedFrontend = singleInstruction $ Op (lInt 2) Add (Op (lInt 2) Div (lInt 3))
    , expectedResult = VInt 2
    , expectedType = tInt
    }

divisionBeforeSubtraction :: TestProgram 'Simple
divisionBeforeSubtraction =
  TestProgram
    { name = "/ has higher precedence than -"
    , source = "1 - 2 / 3"
    , expectedFrontend = singleInstruction $ Op (lInt 1) Sub (Op (lInt 2) Div (lInt 3))
    , expectedResult = VInt 1
    , expectedType = tInt
    }
