module Spicy.Tests.Scoville.Programs.Syntax (
  simpleSyntaxPrograms,
  functionSyntaxPrograms,
) where

import Data.Map.Lazy qualified as Map
import Spicy.Prelude
import Spicy.Scoville
import Spicy.Tests.Scoville.Programs.Common

simpleSyntaxPrograms :: [TestProgram 'Simple]
simpleSyntaxPrograms =
  [ simpleIf
  , simpleLet
  , simpleRecursiveLet
  , simpleLetWithArgument
  , simpleCase
  , simpleInput
  , simpleNestedInput
  --  , simpleActions
  --  , simpleBind
  --  , regularBind
  --  , simpleDefinition
  ]

functionSyntaxPrograms :: [TestProgram 'Function]
functionSyntaxPrograms =
  [ simpleSetter
  , simpleNestedSetter
  ]

simpleIf :: TestProgram 'Simple
simpleIf =
  TestProgram
    { name = "If clause"
    , source =
        "if True;\
        \  then False;\
        \  else True;"
    , expectedFrontend = singleInstruction $ If (lBool True) (lBool False) (lBool True)
    , expectedResult = VBool False
    , expectedType = tBool
    }

simpleLet :: TestProgram 'Simple
simpleLet =
  TestProgram
    { name = "Let binding"
    , source =
        "let a = True;\
        \ in a;"
    , expectedFrontend = singleInstruction $ Let [Dec "a" [] (lBool True)] (Var "a")
    , expectedResult = VBool True
    , expectedType = tBool
    }

simpleRecursiveLet :: TestProgram 'Simple
simpleRecursiveLet =
  TestProgram
    { name = "Let binding (recursive)"
    , source =
        "let a = True;\
        \    b = a;\
        \ in b;"
    , expectedFrontend = singleInstruction $ Let [Dec "a" [] (lBool True), Dec "b" [] (Var "a")] (Var "b")
    , expectedResult = VBool True
    , expectedType = tBool
    }

simpleLetWithArgument :: TestProgram 'Simple
simpleLetWithArgument =
  TestProgram
    { name = "Let binding with argument"
    , source =
        "let a x = x;\
        \ in a True;"
    , expectedFrontend = singleInstruction $ Let [Dec "a" ["x"] (Var "x")] (Ap (Var "a") (lBool True))
    , expectedResult = VBool True
    , expectedType = tBool
    }

simpleCase :: TestProgram 'Simple
simpleCase =
  TestProgram
    { name = "Case statement"
    , source =
        "case True;\
        \  False -> False;\
        \  x -> x;"
    , expectedFrontend =
        singleInstruction
          $ Case (lBool True) [(Right (LBool False), lBool False), (Left "x", Var "x")]
    , expectedResult = VBool True
    , expectedType = tBool
    }

simpleSetter :: TestProgram 'Function
simpleSetter =
  TestProgram
    { name = "Setter"
    , source = "!x"
    , expectedFrontend = singleInstruction $ Setter ("x" :| [])
    , expectedResult = ()
    , expectedType = TVar "a" :-> (tInput :-> tInput) -- This isn't a well defined function, is it?
    }

simpleNestedSetter :: TestProgram 'Function
simpleNestedSetter =
  TestProgram
    { name = "Setter (nested)"
    , source = "!x.y"
    , expectedFrontend = singleInstruction $ Setter ("x" :| ["y"])
    , expectedResult = ()
    , expectedType = TVar "a" :-> (tInput :-> tInput) -- This isn't a well defined function, is it?
    }

simpleInput :: TestProgram 'Simple
simpleInput =
  TestProgram
    { name = "Input"
    , source =
        "[\
        \  a = True;\
        \]"
    , expectedFrontend = singleInstruction $ Input (Map.fromList [("a", lBool True)])
    , expectedResult = VInput $ Map.fromList [("a", VBool True)]
    , expectedType = tInput
    }

simpleNestedInput :: TestProgram 'Simple
simpleNestedInput =
  TestProgram
    { name = "Input (nested)"
    , source =
        "[\
        \  a = [\
        \         b = True;\
        \      ];\
        \];"
    , expectedFrontend =
        singleInstruction
          . Input
          $ Map.fromList [("a", Input $ Map.fromList [("b", lBool True)])]
    , expectedResult = VInput $ Map.fromList [("a", VInput $ Map.fromList [("b", VBool True)])]
    , expectedType = tInput
    }

-- simpleActions :: TestProgram 'Simple
-- simpleActions =
--  TestProgram
--    { name = "Actions"
--    , source = "True; False;"
--    , expectedFrontend = [Action (lBool True), Action (lBool False)]
--    , expectedResult = undefined
--    , expectedType = tIO -- Maybe?
--    }
--
-- simpleBind :: TestProgram 'Simple
-- simpleBind =
--  TestProgram
--    { name = "Bind"
--    , source = "a <- True;"
--    , expectedFrontend = [Bind "a" (lBool True)]
--    , expectedResult = undefined
--    , expectedType = undefined
--    }
--
-- regularBind :: TestProgram 'Simple
-- regularBind =
--  TestProgram
--    { name = ""
--    , source = ""
--    , expectedFrontend = undefined
--    , expectedResult = undefined
--    , expectedType = undefined
--    }
--
-- simpleDefinition :: TestProgram 'Simple
-- simpleDefinition =
--  TestProgram
--    { name = ""
--    , source = ""
--    , expectedFrontend = undefined
--    , expectedResult = undefined
--    , expectedType = undefined
--    }
