module Spicy.Tests.Scoville.Programs.Trivial where

import Data.IntSet qualified as IntSet
import RIO.Set qualified as Set
import Spicy.Prelude
import Spicy.Scoville
import Spicy.Tests.Scoville.Programs.Common

trivialPrograms :: [TestProgram 'Simple]
trivialPrograms =
  [ trivialTrue
  , trivialFalse
  , trivialFTrue
  , trivialFFalse
  , trivialZeroInt
  , trivialPositiveInt
  , trivialNegativeInt
  , trivialZeroDouble
  , trivialPositiveDouble
  , trivialNegativeDouble
  , trivialExpDouble
  , trivialString
  , trivialFragment
  , trivialLayer
  ]

trivialTrue :: TestProgram 'Simple
trivialTrue =
  TestProgram
    { name = "Trivial true"
    , source = "True"
    , expectedFrontend = singleInstruction $ lBool True
    , expectedResult = VBool True
    , expectedType = tBool
    }

trivialFalse :: TestProgram 'Simple
trivialFalse =
  TestProgram
    { name = "Trivial false"
    , source = "False"
    , expectedFrontend = singleInstruction $ lBool False
    , expectedResult = VBool False
    , expectedType = tBool
    }

trivialFTrue :: TestProgram 'Simple
trivialFTrue =
  TestProgram
    { name = "Trivial Fortran true"
    , source = ".TRUE."
    , expectedFrontend = singleInstruction $ lBool True
    , expectedResult = VBool True
    , expectedType = tBool
    }

trivialFFalse :: TestProgram 'Simple
trivialFFalse =
  TestProgram
    { name = "Trivial Fortran false"
    , source = ".FALSE."
    , expectedFrontend = singleInstruction $ lBool False
    , expectedResult = VBool False
    , expectedType = tBool
    }

trivialZeroInt :: TestProgram 'Simple
trivialZeroInt =
  TestProgram
    { name = "Trivial zero"
    , source = "0"
    , expectedFrontend = singleInstruction $ lInt 0
    , expectedResult = VInt 0
    , expectedType = tInt
    }

trivialPositiveInt :: TestProgram 'Simple
trivialPositiveInt =
  TestProgram
    { name = "Trivial positive 1"
    , source = "1"
    , expectedFrontend = singleInstruction $ lInt 1
    , expectedResult = VInt 1
    , expectedType = tInt
    }

trivialNegativeInt :: TestProgram 'Simple
trivialNegativeInt =
  TestProgram
    { name = "Trivial negative 1"
    , source = "-1"
    , expectedFrontend = singleInstruction $ lInt (-1)
    , expectedResult = VInt (-1)
    , expectedType = tInt
    }

trivialZeroDouble :: TestProgram 'Simple
trivialZeroDouble =
  TestProgram
    { name = "Trivial 0.0"
    , source = "0.0"
    , expectedFrontend = singleInstruction $ lDouble 0
    , expectedResult = VDouble 0
    , expectedType = tDouble
    }

trivialPositiveDouble :: TestProgram 'Simple
trivialPositiveDouble =
  TestProgram
    { name = "Trivial positive 1.0"
    , source = "1.0"
    , expectedFrontend = singleInstruction $ lDouble 1
    , expectedResult = VDouble 1
    , expectedType = tDouble
    }

trivialNegativeDouble :: TestProgram 'Simple
trivialNegativeDouble =
  TestProgram
    { name = "Trivial negative 1.0"
    , source = "-1.0"
    , expectedFrontend = singleInstruction $ lDouble (-1)
    , expectedResult = VDouble (-1)
    , expectedType = tDouble
    }

trivialExpDouble :: TestProgram 'Simple
trivialExpDouble =
  TestProgram
    { name = "Trivial exponential notation"
    , source = "1e3"
    , expectedFrontend = singleInstruction $ lDouble 1e3
    , expectedResult = VDouble 1e3
    , expectedType = tDouble
    }

trivialString :: TestProgram 'Simple
trivialString =
  TestProgram
    { name = "Trivial \"Hello World\" string"
    , source = "\"Hello World\""
    , expectedFrontend = singleInstruction $ lString "Hello World"
    , expectedResult = VString "Hello World"
    , expectedType = tString
    }

trivialFragment :: TestProgram 'Simple
trivialFragment =
  let fragment = IntSet.fromList [1, 2, 3]
   in TestProgram
        { name = "Trivial fragment literal"
        , source = "{ 1 2 3 }"
        , expectedFrontend = singleInstruction $ lFragment fragment
        , expectedResult = VFragment fragment
        , expectedType = tFragment
        }

trivialLayer :: TestProgram 'Simple
trivialLayer =
  let layer = Set.fromList [IntSet.fromList [1, 2, 3], IntSet.fromList [2, 3, 4]]
   in TestProgram
        { name = "Trivial layer literal"
        , source = "{ { 1 2 3 } { 2 3 4 } }"
        , expectedFrontend = singleInstruction $ lLayer layer
        , expectedResult = VLayer layer
        , expectedType = tLayer
        }
