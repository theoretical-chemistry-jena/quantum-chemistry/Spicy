{-# LANGUAGE UndecidableInstances #-}
{-# OPTIONS_GHC -Wno-orphans #-}

{- |
Module      : Spicy.Textual
Description : Parsing and writing textual data formats
Copyright   : Phillip Seeber, Sebastian Seidenath 2024
License     : AGPL-3
Maintainer  : phillip.seeber@uni-jena.de
Stability   : experimental
Portability : POSIX, Windows

This module provides to abstractions for working with textual data formats.
In analogy to Aeson's 'ToJSON' and 'FromJSON' classes, the 'Render' class provides a method to produce a textual representation of a data type (e.g. a MOL2 file) and the 'Parse' class provides a method to obtain the data from the textual representation.
The property @pure . id == parse . render@ must hold.
With the exception of @double@ also 'Data.Attoparsec.Text' is reexported here, but `double` is an alternative function, that can also handle Fortran's double format with a @D@ exponent.
-}
module Spicy.Textual (
  -- * Textual Data
  ParseException (..),
  module Data.TTC,
  ParseAp (..),
  parseApM,
  Parse (..),
  parseM,
  parse',
  StateParseAp (..),
  parseIntoS,

  -- * Parser Utilities
  module Data.Attoparsec.Text,
  (~<<),
  (>>~),
  (**>),
  (<**),
  lex,
  endOfLine,
  getLine,
  skipLine,
  skipHorizontalSpace,
  double,

  -- * Formatting Utilities
  (<~<),
  stripped,
  onelined,
  fA,
  fI,
  fL,
  fE,
  fF,
  rbuilder,
) where

import Control.Category ((<<<))
import Data.Attoparsec.Text hiding (double, endOfLine, parse)
import Data.Attoparsec.Text qualified as APT
import Data.Scientific
import Data.TTC (Parse (..), Render (..), Textual (..), parseWithRead, renderWithShow)
import Data.Text.Lazy.Builder qualified as TB
import Formatting hiding ((%))
import GHC.Exception (prettyCallStack)
import GHC.Stack (callStack)
import RIO.State
import RIO.Text qualified as Text
import RIO.Text.Lazy (strip)
import Spicy.Prelude hiding (Done, Fail, Partial, parse)

-- | Exceptions raised when parsing textual data
data ParseException where
  ParseException :: (HasCallStack) => String -> ParseException

instance Exception ParseException

instance Show ParseException where
  show (ParseException msg) =
    "ParseException: "
      <> msg
      <> "\n"
      <> prettyCallStack callStack

-- | A class for parsing textual data in Attoparsec's 'Parser' monad.
class ParseAp a where
  parser :: Parser a

{- | Stuff that can be be parser via 'ParseAp' is also 'Parse'able. However,
a concrete instance of 'Parse' will always be preffered over the 'ParseAp'
(this instance has an @OVERLAPPABLE@ pragma).
-}
instance {-# OVERLAPPABLE #-} (ParseAp a) => Parse a where
  parse t = case parseOnly parser . convert $ t of
    Left err -> Left $ convert err
    Right r -> return r

-- | Run a 'parser' on a 'Text' value in 'MonadThrow'.
parseApM :: (MonadThrow m, ParseAp a) => Text -> m a
parseApM t = case APT.parse parser t of
  Done _ r -> return r
  Fail _ _ e -> throwM $ ParseException e
  Partial f -> case f "" of
    Done _ r -> return r
    Fail _ _ e -> throwM $ ParseException e
    Partial _ -> throwM $ ParseException "Obtained a Partial result twice. Can not continue."

-- | Run a 'Parse' on a 'Textual' value in 'MonadThrow'.
parseM :: (MonadThrow m, Textual t, Parse a) => t -> m a
parseM t = case parse t of
  Left msg -> throwM $ ParseException msg
  Right r -> return r

--------------------------------------------------------------------------------

-- | Parsing into a stateful environment.
class (ParseAp a, Monoid a) => StateParseAp s a where
  -- | A 'A_Lens'' into the state that defines where in the state to put the
  -- result of the parser. Effectively a 'Writer' monad parser but you can
  -- decide where in the 'Writer' it should be written.
  envL :: Lens' s a

-- | Apply a parser and write the result into the state.
parseIntoS :: (StateParseAp s a) => StateT s Parser a
parseIntoS = do
  res <- lift parser
  state $ \s -> (res, s & envL %~ (<> res))

--------------------------------------------------------------------------------

{- | Wrapper around Attoparsec's 'parse' function. Fails with a composable error
 type in 'MonadThrow'.
-}
parse' :: (MonadThrow m) => Parser a -> Text -> m a
parse' p t = case APT.parse p t of
  Done _ r -> return r
  Fail _ _ e -> throwM $ ParseException e
  Partial f -> case f "" of
    Done _ r -> return r
    Fail _ _ e -> throwM $ ParseException e
    Partial _ -> throwM $ ParseException "Obtained a Partial result twice. Can not continue."

-- | Feed the result of one 'Parser' into the next.
(~<<) :: Parser a -> Text -> Parser a
(~<<) nextParser t = case parseOnly nextParser t of
  Left err -> fail err
  Right res -> return res

(>>~) :: Text -> Parser a -> Parser a
(>>~) = flip (~<<)

-- | @x **> y = x *> lex y@
(**>) :: Parser a -> Parser b -> Parser b
x **> y = x *> lex y

-- | @x <** y = x <* lex y@
(<**) :: Parser a -> Parser b -> Parser a
x <** y = x <* lex y

-- | "Lexeme": Skip all horizontal space before a parser.
lex :: Parser a -> Parser a
lex p = skipHorizontalSpace *> p

{- | Re-definition which accepts three styles of line endings, namely

  * @\n@ Unix line endings
  * @\r@ old-style Mac line endings
  * @\r\n@ Windows line endings
-}
endOfLine :: Parser ()
endOfLine = void (APT.string "\r\n") <|> void (APT.string "\r") <|> void (APT.char '\n')

-- | Consume the rest of the current line.
getLine :: Parser Text
getLine = takeTill isEndOfLine <* endOfLine

-- | Skip the rest of the line.
skipLine :: Parser ()
skipLine = void getLine

-- | As Attoparsec's 'skipSpace', but skips horizontal space only.
skipHorizontalSpace :: Parser ()
skipHorizontalSpace = void $ takeWhile isHSpace
 where
  isHSpace c = c == ' ' || c == '\t' || c == '\f' || c == '\v'

{- | Haskell's 'read' and attoparsec's 'double' don't recognise the format for
Fortrans's double precision numbers, i.e. @1.00D-03@. This parser accepts all
types of Fortran floats.
-}
double :: Parser Double
double = withDot <|> withoutDot
 where
  expoChar = APT.char 'd' <|> APT.char 'D' <|> APT.char 'e' <|> APT.char 'E'
  -- Formats like 1e3, 2e-2, etc.
  withoutDot = do
    prefix <- fromInteger <$> decimal
    expo <- expoChar *> signed (decimal @Int)
    return $ prefix * 10 ^^ expo
  -- Formats like 1.3e-2, .4e18, 100.0 etc.
  withDot = do
    -- Potentially signed. But maybe also not ...
    sign <- option '+' $ APT.char '-' <|> APT.char '+'

    -- Potentially there is a number before the decimal point but you never know
    prefix <- do
      preDot <- option "0" $ many1 digit
      dot <- APT.char '.'
      postDot <- many1 digit
      let base' = Text.pack $ (sign : preDot) <> (dot : postDot)
      base' >>~ APT.double

    -- Looking for an exponent. Might be there or not ...
    optional expoChar >>= \case
      Nothing -> return prefix
      Just _ -> do
        expo <- signed (decimal @Int)
        return $ prefix * 10 ^^ expo

--------------------------------------------------------------------------------

{- | This combines two formatters and inserts a space between them, a
very common idiom.
-}
(<~<) :: Format b c -> Format a b -> Format a c
a <~< b = a <<< " " <<< b

-- | Strips whitespace from the beginning and end of a formatted string.
stripped :: Format r a -> Format r a
stripped = alteredWith strip

-- | Removes all newlines in the formatted string.
onelined :: Format r a -> Format r a
onelined = charsRemovedIf (== '\n')

-- | Fortran's @A@ format.
fA :: Int -> Format r (Text -> r)
fA width = left width ' ' %. fitLeft width %. stext

-- | Fortran's @I@ format.
fI :: (Integral a) => Int -> Format r (a -> r)
fI width = left width ' ' %. fitLeft width %. int

-- | Fortran L formatting.
fL :: Int -> Format r (Bool -> r)
fL width = later $ \b -> bformat (left width ' ' %. builder) $ if b then "T" else "F"

-- | Fortran's @E@ format.
fE ::
  forall a r.
  (RealFloat a) =>
  -- | Width of the number in characters
  Int ->
  -- | Number of digits after the decimal point
  Int ->
  Format r (a -> r)
fE width prec = later fmt
 where
  fmt :: a -> TB.Builder
  fmt n = padString
   where
    sn = fromFloatDigits n
    (coeffDigits, expo) = toDecimalDigits sn

    coeffSign = if sn < 0 then "-" else ""
    preComma = bformat (concatenated int) . fmap abs . Spicy.Prelude.take 1 $ coeffDigits
    postComma = bformat (right prec '0' %. concatenated int) . fmap abs . Spicy.Prelude.take prec . Spicy.Prelude.drop 1 $ coeffDigits
    expoSign = if expo <= 0 then "-" else "+"
    corrExpo = expo - 1

    numString =
      coeffSign
        <> preComma
        <> "."
        <> postComma
        <> "E"
        <> expoSign
        <> bformat
          (fitLeft 2 %. left 2 '0' %. int)
          (abs corrExpo)

    padString = bformat (fitLeft width %. left width ' ') numString

-- | Fortran's @F@ format.
fF :: forall a r. (RealFloat a) => Int -> Int -> Format r (a -> r)
fF width prec = left width ' ' %. fitLeft width %. fixed prec

-- | Format a 'Render'able as a 'Builder'
rbuilder :: (Render a) => Format t (a -> t)
rbuilder = mapf render builder
