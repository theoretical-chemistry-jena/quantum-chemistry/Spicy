module Spicy.Textual.XML where

import GHC.Stack
import RIO.Map qualified as Map
import RIO.Text qualified as T
import Spicy.Prelude
import Spicy.Textual as TTC
import Text.XML

-- | Utility class for converting to XML elements.
class ToElement a where
  toElement :: a -> Element

-- | Types that can be converted from an XML element
class FromElement a where
  fromElement :: (MonadThrow m, HasCallStack) => Element -> m a

-- | Types that can be converted to XML attributes.
class ToAttributes a where
  toAttributes :: a -> Map Name Text

-- | Types that can be converted from XML attributes.
class FromAttributes a where
  fromAttributes :: (MonadThrow m, HasCallStack) => Map Name Text -> m a

-- | Query 'NodeElement's of an 'Element' that have a given 'Name'.
lookupElementElement ::
  (MonadThrow m) =>
  -- | The 'Element' whose 'elementNodes' to query
  Element ->
  -- | Query 'NodeElement's with this provided 'Name'. Nodes with constructors
  -- besides 'NodeElement' and 'NodeElement's with a different 'Name' will be
  -- discarded.
  Name ->
  -- | Accesor function to obtain the value of interest from the 'NodeElement's
  -- of interest.
  (Element -> m a) ->
  m [a]
lookupElementElement Element{elementNodes} name accF = traverse accF elems
 where
  elems = flip mapMaybe elementNodes $ \case
    NodeElement el@Element{elementName}
      | elementName == name -> Just el
      | otherwise -> Nothing
    _ -> Nothing

-- | Lookup the 'NodeContent's of an 'Element' with a given 'Name'.
lookupElementContent :: (MonadThrow m) => Element -> Name -> m [[Text]]
lookupElementContent el name = lookupElementElement el name $ \Element{elementNodes} -> do
  return . flip mapMaybe elementNodes $ \case
    NodeContent t -> Just t
    _ -> Nothing

-- | Lookup a 'Read'able attribute from an 'Elements attributes with a given 'Name'.
lookupReadAttr :: (MonadThrow m, Read a) => Element -> Name -> m a
lookupReadAttr Element{elementAttributes} name = case (readMaybe . T.unpack) =<< Map.lookup name elementAttributes of
  Nothing -> throwM $ StringException ("Expected \"" <> show name <> "\" attribute") callStack
  Just d -> return d

-- | Lookup a 'Text' from an 'Elements attributes with a given 'Name'.
lookupTextAttr :: (MonadThrow m) => Element -> Name -> m Text
lookupTextAttr Element{elementAttributes} name = case Map.lookup name elementAttributes of
  Nothing -> throwM $ StringException ("Expected \"" <> show name <> "\" attribute") callStack
  Just d -> return d

-- | Lookup a 'Parse'able value from an 'Elements attributes with a given 'Name'.
lookupParseAttr :: (MonadThrow m, Parse a) => Element -> Name -> m a
lookupParseAttr Element{elementAttributes} name = case parseM =<< Map.lookup name elementAttributes of
  Nothing -> throwM $ StringException ("Expected \"" <> show name <> "\" attribute") callStack
  Just d -> return d

-- | Check if the correct 'Element' is being processed.
checkElementName :: (MonadThrow m) => Element -> Name -> m ()
checkElementName Element{elementName} name =
  unless (elementName == name)
    . throwM
    $ StringException
      ("Expected \"" <> show name <> "\" element but got \"" <> show elementName <> "\"")
      callStack
