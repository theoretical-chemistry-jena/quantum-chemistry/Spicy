{- |
Module      : Spicy.Worker
Description : MPI Remote Clients for Spicy
Copyright   : Phillip Seeber, Sebastian Seidenath 2024
License     : AGPL-3
Maintainer  : phillip.seeber@uni-jena.de
Stability   : experimental
Portability : POSIX, Windows

The Spicy main rank (MPI rank 0) communicates with workers to perform a variety of tasks, such as starting computational chemistry calculations or performing distributed matrix multiplications.
This is the main module for workers, which listens to request from the rook rank and launches MPI-enabled routines as requested.
Logging of this module is local to each node unless MPI messages are sent explicitly.
-}
module Spicy.Worker (workerMain) where

import Control.Distributed.MPI.Binary qualified as MPI
import GHC.Stack
import Spicy.Math.BlockSparse (matMulBsWorker)
import Spicy.Prelude

-- | Exceptions of the MPI workers
newtype WorkerException
  = WorkerMPIException String

instance Show WorkerException where
  show (WorkerMPIException msg) =
    "WorkerMPIException: "
      <> msg
      <> "\n"
      <> prettyCallStack callStack

instance Exception WorkerException

{- | Main programme of workers. Controlled via MPI messages from the main
programme (MPI rank 0), multiple tasks can be performed.

MPI must have been initialised by the caller already.
-}
workerMain :: (HasLogFunc env) => RIO env ()
workerMain = do
  wRank <- liftIO $ MPI.commRank MPI.commWorld
  let logSrc = "worker " <> tshow wRank
  unless (wRank /= MPI.rootRank)
    . throwM
    . WorkerMPIException
    $ "Workers must have an MPI rank /= 0, but this instance has rank "
    <> show wRank

  workerTask <- liftIO $ MPI.bcast Nothing MPI.rootRank MPI.commWorld
  case workerTask of
    DbcsrClient -> do
      logDebugS logSrc "Starting DBCSR client"
      matMulBsWorker -- Run DBCSR client code
      logDebugS logSrc "Terminating DBCSR client"
      workerMain
    Terminate -> do
      logDebugS logSrc "Shutting down worker"
